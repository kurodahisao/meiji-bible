;;; -*- mode:emacs-lisp; coding: utf-8 -*-
;;; 西方の人

(defun kana-to-utf (args)
  ;; \kana記述を消す
  (interactive "P")
  (loop initially (beginning-of-buffer)
        for pos1 = (goto-kana-kaishi args) ; CID開始
        while pos1 do
        (kill-sexp 1)
        (forward-sexp 1)
        (kill-sexp 1)))

(defun goto-kana-kaishi (args)
  (interactive "P")
  (and (re-search-forward "\\\\[Kk]ana" nil t nil)
       (progn (backward-char 5) (point))))

(defun cid-to-utf (args)
  ;; CID記述をUTFキャラに戻す
  (interactive "P")
  (loop initially (beginning-of-buffer)
        for pos1 = (goto-cid-kaishi args) ; CID開始
        while pos1 do
        (kill-sexp 2)
        (let ((cid (first kill-ring)))
          (insert (car (rassoc cid jitai-convert-table))))))

(defun goto-cid-kaishi (args)
  (interactive "P")
  (and (re-search-forward "\\\\CID" nil t nil)
       (search-forward "{" nil t nil)
       (progn (backward-char 5) (point))))

(defun utf-to-utf (args)
  ;; UTF記述をUTFキャラに戻す
  (interactive "P")
  (loop initially (beginning-of-buffer)
        for pos1 = (goto-utf-kaishi args) ; CID開始
        while pos1 do
        (kill-sexp 2)
        (let ((cid (first kill-ring)))
          (insert (car (rassoc cid jitai-convert-table))))))

(defun goto-utf-kaishi (args)
  (interactive "P")
  (and (re-search-forward "\\\\UTF" nil t nil)
       (search-forward "{" nil t nil)
       (progn (backward-char 5) (point))))

(defun mjmzm-to-utf (args)
  ;; MJMZM記述をUTFキャラに戻す
  (interactive "P")
  (loop initially (beginning-of-buffer)
        for pos1 = (goto-mjmzm-kaishi args) ; CID開始
        while pos1 do
        (kill-sexp 2)
        (let ((cid (first kill-ring)))
          (insert (car (rassoc cid jitai-convert-table))))))

(defun goto-mjmzm-kaishi (args)
  (interactive "P")
  (and (re-search-forward "\\\\MJMZM" nil t nil)
       (search-forward "{" nil t nil)
       (progn (backward-char 7) (point))))

(defun kanji-nukidasi (args)
  ;; {漢字}だけ拔き出す
  (interactive "P")
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (loop initially (progn (switch-to-buffer log)
                           (erase-buffer)
                           (switch-to-buffer current)
                           (beginning-of-buffer)
                           (search-forward "わたしは" nil t nil))
          for pos1 = (goto-kakko-kaishi args) ; {}開始
          while pos1 do
          (kill-sexp 1)
          (let ((kanji (first kill-ring)))
            (switch-to-buffer "*yomi-log*")
            (insert kanji)
            (insert "\n")
            (switch-to-buffer current))
          '(undo)
          '(forward-sexp))))

(defun goto-kakko-kaishi (args)
  (interactive "P")
  (and (search-forward "{" nil t nil)
       (progn (backward-char 1) (point))))

(setq saihau-kanji-list
      '(("木下杢太郎" . "きの,した,もく,た,らう")
        ("北原白秋" . "きた,はら,はく,しう")
        ("殉教者" . "じゆんけうしや")
        ("狂信者" . "きやうしんしや")
        ("紅毛人" . "かうまうじん")
        ("眞面目" . "ま,じ,め")
        ("贔屓目" . {ひい,き,め})
        ("辯證法" . {べんしやうはふ})
        ("燕子花" . {かきつばた})
        ("無花果" . {いちじゆく})
        ("老成人" . {らうせいじん})
        ("兌銀者" . {りやうがへするもの})
        ("無何有" . {む,かい,う})
        ("彼是" . {かれこれ})
        ("病的" . {びやうてき})
        ("興味" . {きやうみ})
        ("行路" . {かう,ろ})
        ("忠實" . {ちうじつ})
        ("大目" . {おほ,め})
        ("巖疊" . {がんでふ})
        ("女性" . {ぢよせい})
        ("彼岸" . {ひ,がん})
        ("腦髓" . {なうずゐ})
        ("餘計" . {よ,けい})
        ("芥子" . {け,し})
        ("醜聞" . {しうぶん})
        ("黄金" . {わうごん})
        ("乳香" . {にうかう})
        ("沒藥" . {もつやく})
        ("反叛" . {はんぱん})
        ("野蜜" . {の,みつ})
        ("荒野" . {あれ,の})
        ("荒金" . {あらがね})
        ("慟哭" . {どうこく})
        ("暫" . {しばら})
        ("就中" . {なかんづく})
        ("男子" . {なん,し})
        ("長病" . {ながやまひ})
        ("大膽" . {だいたん})
        ("流轉" . {るてん})
        ("喇叭" . {らつ,ぱ})
        ("放蕩" . {ほうたう})
        ("西方" . {さいはう})
        ("所謂" . {いはゆる})
        ("爐邊" . {ろ,へん})
        ("饗宴" . {きやうえん})
        ("柘榴" . {ざく,ろ})
        ("人氣" . {ひと,け})
        ("飜弄" . {ほんろう})
        ("蝋燭" . {らふそく})
        ("苛立" . {いら,だ})
        ("賣買" . {うりかひ})
        ("逐出" . {おひ,だ})
        ("賣者" . {うるもの})
        ("椅子" . {こしかけ})
        ("女人" . {によにん})
        ("橄欖" . {かんらん})
        ("今日" . {こんにち})
        ("圓光" . {ゑんくわう})
        ("庭鳥" . {にはとり})
        ("白楊" . {はくやう})
        ("縊死" . {い,し})
        ("祭司" . {さい,し})
        ("憐憫" . {れんびん})
        ("不幸" . {ふ,かう})
        ("方伯" . {つかさ})
        ("一言" . {ひとこと})
        ("訊問" . {じんもん})
        ("叛逆" . {はんぎやく})
        ("幸福" . {かうふく})
        ("畢竟" . {ひつきやう})
        ("象嵌" . {ざうがん})
        ("彼女" . {かのぢよ})
        ("蓮華" . {れん,げ})
        ("手易" . {た,やす})
        ("茫々" . {ばうばう})
        ("教徒" . {けう,と})
        ("人生" . {じんせい})
        ("梯子" . {はし,ご})
        ("涅槃" . {ね,はん})
        ("佛陀" . {ぶつ,だ})
        ("山阿" . {さん,あ})
        ("多少" . {た,せう})
        ("艸木" . {さうもく})
        ("播" . {ま})
        ("鴉" . {からす})
        ("末" . {すゑ})
        ("寧" . {むし})
        ("嚴" . {いかめ})
        ("徒" . {と})
        ("忽" . {たちま})
        ("殊" . "こと")
        ("\\CID{8471}" . "けう")
        "瓶"
        "通"
        "幸"
        "尊"
        "夫"
        "寶"
        "盒"
        "僅"
        "憐"
        "邑"
        "蝗"
        "必"
        "檚"
        "漲"
        "目"
        "斥"
        "恃"
        "退"
        "吼"
        "露"
        "瘉"
        "煩"
        "怳"
        "措"
        "虐"
        "薪"
        "截"
        "兎"
        "角"
        "源"
        "劍"
        "墮"
        "憚"
        "凡"
        "汚"
        "入"
        "厠"
        "遺"
        "食"
        "潔"
        "顧"
        "譃"
        "贖"
        "其"
        "衣"
        "乎"
        "嘗"
        "愈"
        "聳"
        "甦"
        "彼"
        "堪"
        "唯"
        "雷"
        "驅"
        "詰"
        "慈"
        "揮"
        "案"
        "鴿"
        "倒"
        "殿"
        "咎"
        "注"
        "憂"
        "度"
        "筈"
        "等"
        "長"
        "憤"
        "善"
        "溢"
        "欺"
        "企"
        "比"
        "若"
        "逞"
        "荊"
        "冠"
        "袍"
        "見"
        "嘲"
        "奇"
        "畢"
        "圖"
        "來"
        "屍"
        "乞"
        "異"
        "杯"
        "閲"
        "途"
        "棘"
        "鞭"
        "蹴"
        "佇"
        "訣"
        "超"
        "徐"
        "吊"
        "合"
        "上"
        "況"
        "圓"
        "髯"
        "叩"
        "中"
        "分"
        "生"
        "悉"
        "孕"
        "塒"))
