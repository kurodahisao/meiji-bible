;;; -*- mode:common-lisp -*-

(eval-when (:compile-toplevel)
  (error "Do not compile this File."))

#+sbcl
(sb-unicode::get-user-locale)

(if (probe-file "~/quicklisp/setup.lisp")
    (load "~/quicklisp/setup.lisp")
  (load "quicklisp.lisp"))

#+allegro
(setq *locale* (find-locale "ja.JP.UTF8"))

#+allegro
(setf (sys:getenv "PATH")
  (format nil "~A;~A" (probe-file "sys:") (sys:getenv "PATH")))

#-allegro (quicklisp-client:quickload :babel)
#-allegro (quicklisp-client:quickload :asdf-encodings)
#-allegro (quicklisp-client:quickload :cl-pdf)
#-allegro (quicklisp-client:quickload :cl-ppcre)
#-allegro (quicklisp-client:quickload :flexi-streams)
#-allegro (quicklisp-client:quickload :deflate)
#-allegro (quicklisp-client:quickload :salza2)
#-allegro (quicklisp-client:quickload :drakma)
#-allegro (quicklisp-client:quickload :cl-html-parse)
;; #-allegro (quicklisp-client:quickload :zpb-ttf)

#-allegro
(load "./cl-pdf/cl-pdf.asd")
#-allegro
(load "./cl-pdf/cl-pdf-parser.asd")
#-allegro
(load "./cl-pdf/ipa-font/ipa-font.asd")
#-allegro
(load "./zpb-ttf2/zpb-ttf2.asd")
#-allegro
(load "bible-meiji.asd")

#+allegro
(defsystem :meiji-bible (:default-pathname #.*default-pathname-defaults*)
  (:serial "bible-meiji.lisp" "bible-king.lisp"))

#+allegro
(eval-when (:load-toplevel :compile-toplevel :execute)
  (let ((*locale* (find-locale "ja.JP.UTF8")))
    (load-system :meiji-bible :compile t)))

#-allegro
(defun action-accept (c)
  (let ((restart (find-restart 'asdf/action:accept)))
    (when restart
      (invoke-restart restart))))

#-allegro
(handler-bind ((error #'action-accept))
  (asdf/operate:load-system :bible-meiji))

