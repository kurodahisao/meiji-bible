;;; emacs-lisp; lexical-binding:t; coding:utf-8 -*-

(require 'cl)

;;; 手順:
;;; Open deuteronomy.wiki
;;; M-x deuteronomy-wiki-to-furikana
;;; Save as deuteronomy.furikana
;;; M-x deuteronomy-furikana-to-latex    ; 正字體變換 (引數 C-u をつけるとIPAFont利用)
;;; Save as deuteronomy.tex
;;; Compile LaTex
;;; $ dvipdfmx -f kozukapr6n.map _TZ_3170-penguin.dvi 
;;; CL-USER> (pdf:pdf-adjust-oyamoji "~/projects/meiji-bible/_TZ_3170-penguin.pdf" "deuteronomy.pdf")

(require :utf-to-jis)
(require :shingen)
(require :exodus)
(require :genesis)
(require :leviticus)
(require :numbers)

(defun deuteronomy-wiki-to-latex (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (deuteronomy-wiki-to-furikana start end)
  (deuteronomy-furikana-to-latex nil))

(defun deuteronomy-furikana-to-latex (ipa)
  (interactive "P")
  (load-file "deuteronomy.katakana")
  (convert-shingen-subsection)
  (kanakt-shingen-jukugo)
  (latex-shingen-section)
  (latex-shingen-subsection)
  (latex-shingen-white-space)
  (convert-deuteronomy-title)
  (convert-deuteronomy-kanakt)
  (utf-to-jis-2 ipa))

(defun deuteronomy-jukugo ()
  (load-file "shingen.jukugo")
  (load-file "exodus.jukugo")
  (load-file "genesis.jukugo")
  (load-file "leviticus.jukugo")
  (load-file "numbers.jukugo")
  (load-file "deuteronomy.jukugo")
  (setq *current-jukugo*
     (append *deuteronomy-jukugo* *numbers-jukugo* *leviticus-jukugo* *genesis-jukugo* *exodus-jukugo* *shingen-jukugo*)))

(defun deuteronomy-wiki-to-furikana (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (convert-deuteronomy-section)
  (hex-to-char)
  (set-buffer-file-coding-system 'utf-8)
  (furikana-shingen-jukugo 'deuteronomy (deuteronomy-jukugo) start end)
  (goto-char (point-min))
  (kill-line 1)
  (insert "-*- coding:utf-8 -*-\n"))

(defun convert-deuteronomy-section ()
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "== \\(第[0-9]+章\\) ==" nil t)
    (replace-match "==\\1==")))

(defun convert-deuteronomy-title ()
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "箴言" nil t)
    (replace-match "申命記" nil nil))
  (goto-char (point-min))
  (while (re-search-forward "言箴" nil t)
    (replace-match "記命申" nil nil)))

(defun convert-deuteronomy-kanakt ()
  ;; 辭書他で對應できない例外處理をここに書く
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "、" nil t)
    (replace-match " " nil nil))
  (goto-char (point-min))
  (while (re-search-forward "\\\\,\\\\oline" nil t)
    (backward-sexp)
    (forward-char 2)
    (insert " ")))

(provide :deuteronomy)
