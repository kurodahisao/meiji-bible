;;; emacs-lisp; lexical-binding:t; coding:utf-8 -*-

(require 'cl)

;;; 手順:
;;; Open genesis.wiki
;;; M-x genesis-wiki-to-furikana
;;; Save as genesis.furikana
;;; M-x genesis-furikana-to-latex    ; 正字體變換 (引數 C-u をつけるとIPAFont利用)
;;; Save as genesis.tex
;;; Compile LaTex
;;; $ dvipdfmx -f kozukapr6n.map _TZ_3170-penguin.dvi 
;;; CL-USER> (pdf:pdf-adjust-oyamoji "~/projects/meiji-bible/_TZ_3170-penguin.pdf" "genesis.pdf")

(require :utf-to-jis)
(require :shingen)
(require :exodus)

(defun genesis-wiki-to-latex (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (genesis-wiki-to-furikana start end)
  (genesis-furikana-to-latex nil))

(defun genesis-furikana-to-latex (ipa)
  (interactive "P")
  (load-file "genesis.katakana")
  (convert-exodus-subsection)
  (kanakt-shingen-jukugo)
  (latex-shingen-section)
  (latex-shingen-subsection)
  (latex-shingen-white-space)
  (convert-genesis-title)
  (convert-genesis-kanakt)
  (utf-to-jis-2 ipa))

(defun genesis-wiki-to-furikana (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (load-file "shingen.jukugo")
  (load-file "exodus.jukugo")
  (load-file "genesis.jukugo")
  (hex-to-char)
  (set-buffer-file-coding-system 'utf-8)
  (furikana-shingen-jukugo 'genesis (append *genesis-jukugo* *exodus-jukugo* *shingen-jukugo*) start end)
  (goto-char (point-min))
  (kill-line 1)
  (insert "-*- coding:utf-8 -*-\n"))

(defun convert-genesis-title ()
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "箴言" nil t)
    (replace-match "創世記" nil nil))
  (goto-char (point-min))
  (while (re-search-forward "言箴" nil t)
    (replace-match "記世創" nil nil)))

(defun convert-genesis-kanakt ()
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "、" nil t)
    (replace-match " " nil nil))
  (goto-char (point-min))
  (while (re-search-forward "\\\\,\\\\oline" nil t)
    (backward-sexp)
    (forward-char 2)
    (insert " ")))

;;; Utils

(defun jc (arg)
  ;; furikana buffer で手入力した熟語を jukugo.n へ反映させる
  (interactive "P")
  (kill-ring-save (point) (mark))
  (other-window 1)
  (end-of-line) (insert 10)    ; return
  (yank)
  (jc1 arg)
  (other-window 1))

(defun jc1 (arg)
  ;; {眷念}{おも}ひ → ("眷念ひ" . "おも")
  (interactive "P")
  (move-beginning-of-line 1)
  (let ((start (point)))
    (cl-loop initially (goto-char start)
          for point = (point)
          for char = (char-after point)
          while (not (= char 10)) do
          (cond ((= char 123)           ; {
                 (delete-char 1)
                 (insert 34))           ; "
                ((= char 125)           ; }
                 (delete-char 1)
                 (insert 34))           ; "
                (t (forward-char))))
    (cl-loop initially (goto-char start)
          for point = (point)
          for char = (char-after point)
          while (not (= char 10)) do
          (cond ((= char 34)            ; "
                 (insert 40)            ; (
                 (forward-sexp)
                 (insert 32) (insert 46) (insert 32) ; space . space
                 (forward-sexp)
                 (let ((beg (point))
                       (end (progn (end-of-line) (point))))
                    (kill-region beg end))
                 (insert 41)            ; )
                 (backward-char)
                 (backward-sexp)
                 (backward-char 4)
                 (yank-okurigana arg)
                 (end-of-line))
                (t (forward-char))))
    (indent-for-tab-command)
    (save-buffer)))

(defvar *okuri-kana-table*
  '(("か" "き" "く" "け")
    ("が" "ぎ" "ぐ" "げ")
    ("さ" "し" "す" "せ")
    ("ざ" "じ" "ず" "ぜ")
    ("た" "ち" "つ" "て")
    ("な" "に" "ぬ" "ね")
    ("は" "ひ" "ふ" "へ")
    ("ば" "び" "ぶ" "べ")
    ("ま" "み" "む" "め")
    ("ら" "り" "る" "れ")
    ("ぢ" "づ")))

(defun yank-okurigana (arg)
  ;; {眷念}{おも}ひ → ("眷念[はひふへ]" . "おも")
  (if (null arg)
      (yank)
    (let* ((kill (current-kill 0))
           (head (subseq kill 0 -1))
           (tail (subseq kill -1)))
      (cl-loop for kana-list in *okuri-kana-table*
            if (cl-member tail kana-list :test #'equal) do
              (insert head)
              (insert "[")
              (dolist (kana kana-list) (insert kana))
              (insert "]")))))

(defun jc2 (arg)
  ;; 章毎の熟語 (jukugo.n) を → 全体の熟語へ移す (jukugo)
  (interactive "P")
  (let* ((current (current-buffer))
         (name (buffer-name current))
         (pos (position ?. name :from-end t))
         (nameto (subseq name 0 pos))
         (jukugo (find-file nameto)))
    (switch-to-buffer current)
    (beginning-of-line)
    (kill-line)
    (switch-to-buffer jukugo)
    (beginning-buffer)
    (forward-list)
    (beginning-of-line)
    (open-line 1)
    (yank) (indent-for-tab-command)
    (save-buffer)
    (switch-to-buffer current)))

;;;;

(defun redo-jukugo ()
  ;; 登録された熟語を章の殘りへ適用して二度手間を省く
  (interactive)
  (let* ((back (point))
         (subsection (current-subsection))
         (furikana (buffer-name (current-buffer)))
         (name (subseq furikana 0 (position ?\. furikana)))
         (wiki (format "%s.wiki" name)))
    (switch-to-buffer wiki)
    (setq kill-ring nil)
    (furikana-shingen-jukugo-1 name subsection)
    (switch-to-buffer furikana)
    (re-search-backward subsection) (end-of-line)
    (yank)
    (let ((start (point))
          (end (next-section)))
      (kill-region start end)
      (goto-char back))))

(defun next-section ()
  (or (re-search-forward "第[0-9]+章" nil t) (goto-char (point-max)))
  (progn (beginning-of-line) (previous-line) (point)))

(defun current-subsection ()
  ;; 今どの章節にゐるかを見つけて返る
  (let ((back (point)))
    (re-search-backward "[0-9]+:[0-9]+" nil t)
    (let ((beg (progn (beginning-of-line) (point)))
          (end (progn (end-of-line) (point))))
      (prog1
          (buffer-substring beg end)
        (goto-char back)))))

(defun section-of (subsection)
  ;; 章節の章の部分を取り出す
  (let ((start (position 32 subsection))
        (end (position ?\: subsection)))
  (subseq subsection (1+ start) end)))

(defun read-from-buffer (buffname)
  ;; bufferの中身をread
  (let ((current (current-buffer)))
    (switch-to-buffer buffname)
    (let* ((contents (buffer-substring (point-min) (point-max)))
           (value (ignore-errors (read-from-string contents))))
      (switch-to-buffer current)
      (car value))))

(defun furikana-shingen-jukugo-1 (name subsection)
  ;; 今ゐる章の殘り部分に登録された熟語を適用する
  (let* ((section (section-of subsection))
         (xjisho (read-from-buffer (format "%s.jukugo.%s" name section)))
         (sxjisho (sort-jisho (append xjisho *jukugo*) t)))
    (beginning-of-buffer)
    (re-search-forward subsection)
    (let ((start (point))
          (end (next-section)))
      (hex-to-char start end)
      (setq end (furikana-shingen-jukugo-aux sxjisho start end t))
      (setq end (furikana-shingen-jukugo-aux sxjisho start end t))
      (goto-char start)
      (setq end (next-section))
      (kill-region start end)
      (revert-buffer t t)
      (goto-char start))))

(defvar *current-jukugo*)

(defun find-jukugo ()
  ;; region に合致する熟語を辞書 *current-jukugo* から見付けて *jukugo* に登録する
  (interactive)
  (let ((string (buffer-substring (region-beginning) (region-end))))
    (loop for (kanji . yomi) in *current-jukugo*
       if (and (string-match kanji string)
               (y-or-n-p (format "%s → %s?" kanji yomi)))
       return (pushnew (cons kanji yomi) *jukugo*))))

(provide :genesis)
