;;; -*- mode:common-lisp; coding:utf-8 -*-

#+allegro
(eval-when (:load-toplevel :compile-toplevel :execute)
  #+allegro (require :aserve) #+sbcl (ql:quickload "drakma")
  #+allegro (require :phtml) #+sbcl (ql:quickload "cl-html-parse"))

(defpackage "BIBLE"
  (:use "COMMON-LISP" #+allegro "NET.ASERVE" #-allegro "DRAKMA")
  #+allegro
  (:import-from "EXCL" "MATCH-REGEXP")
  #+allegro
  (:import-from "EXCL" "RATIOP")
  (:export "CONVERT-MATTHEW-TO-LATEX" "CONVERT-MARK-TO-LATEX" "CONVERT-LUKE-TO-LATEX" "CONVERT-JOHN-TO-LATEX"
           "CONVERT-ACTS-TO-LATEX" "CONVERT-ROMANS-TO-LATEX" "CONVERT-I-CORINTHIANS-TO-LATEX"
           "CONVERT-II-CORINTHIANS-TO-LATEX" "CONVERT-GALATIANS-TO-LATEX" "CONVERT-EPHESIANS-TO-LATEX"
           "CONVERT-PHILIPPIANS-TO-LATEX" "CONVERT-COLOSSIANS-TO-LATEX" "CONVERT-I-THESSALONIANS-TO-LATEX"
           "CONVERT-II-THESSALONIANS-TO-LATEX" "CONVERT-I-TIMOTHY-TO-LATEX" "CONVERT-II-TIMOTHY-TO-LATEX"
           "CONVERT-TITUS-TO-LATEX" "CONVERT-PHILEMON-TO-LATEX" "CONVERT-PHILEMON-TO-LATEX"
           "CONVERT-HEBREWS-TO-LATEX" "CONVERT-JAMES-TO-LATEX" "CONVERT-I-PETER-TO-LATEX"
           "CONVERT-II-PETER-TO-LATEX" "CONVERT-I-JOHN-TO-LATEX" "CONVERT-II-JOHN-TO-LATEX"
           "CONVERT-III-JOHN-TO-LATEX" "CONVERT-JUDE-TO-LATEX" "CONVERT-REVELATION-TO-LATEX"
           "CONVERT-WIKISOURCE-TO-LATEX" "CONVERT-NEW-TESTAMENT-TO-LATEX"
           "CONVERT-MATTHEW-TO-MULTICOL-LATEX" "CONVERT-MARK-TO-MULTICOL-LATEX"
           "CONVERT-LUKE-TO-MULTICOL-LATEX" "CONVERT-JOHN-TO-MULTICOL-LATEX"
           "CONVERT-ACTS-TO-MULTICOL-LATEX" "CONVERT-ROMANS-TO-MULTICOL-LATEX"
           "CONVERT-I-CORINTHIANS-TO-MULTICOL-LATEX" "CONVERT-II-CORINTHIANS-TO-MULTICOL-LATEX"
           "CONVERT-GALATIANS-TO-MULTICOL-LATEX" "CONVERT-EPHESIANS-TO-MULTICOL-LATEX"
           "CONVERT-PHILIPPIANS-TO-MULTICOL-LATEX" "CONVERT-COLOSSIANS-TO-MULTICOL-LATEX"
           "CONVERT-I-THESSALONIANS-TO-MULTICOL-LATEX" "CONVERT-II-THESSALONIANS-TO-MULTICOL-LATEX"
           "CONVERT-I-TIMOTHY-TO-MULTICOL-LATEX" "CONVERT-II-TIMOTHY-TO-MULTICOL-LATEX"
           "CONVERT-TITUS-TO-MULTICOL-LATEX" "CONVERT-PHILEMON-TO-MULTICOL-LATEX"
           "CONVERT-HEBREWS-TO-MULTICOL-LATEX" "CONVERT-JAMES-TO-MULTICOL-LATEX"
           "CONVERT-I-PETER-TO-MULTICOL-LATEX" "CONVERT-II-PETER-TO-MULTICOL-LATEX"
           "CONVERT-I-JOHN-TO-MULTICOL-LATEX" "CONVERT-II-JOHN-TO-MULTICOL-LATEX" "CONVERT-III-JOHN-TO-MULTICOL-LATEX"
           "CONVERT-JUDE-TO-MULTICOL-LATEX" "CONVERT-REVELATION-TO-MULTICOL-LATEX"
           "CONVERT-NEW-TESTAMENT-TO-MULTICOL-LATEX"))

(in-package "BIBLE")

#-allegro
(defun match-regexp (regexp string)
  (cl-ppcre:scan regexp string))

(defvar *chapter-title*)                ; 目次 (第一章)
(defvar *chapter-string*)               ; 目次 (アブラハムの裔なるダビデの裔イエス・キリストの系圖)

(defparameter +crlf-base-utf8+ #+allegro (excl:crlf-base-ef :utf8) #+sbcl :utf8)

(defparameter +bible-meiji-header+
    "%%% -*- mode: LaTeX; coding: utf-8 -*-
%%% -*- mode: LaTeX; coding: iso-2022-jp-2 -*-
\\documentclass[a5j,10pt,openany,twocolumn]{tbook}% mentuke tombo myjsbook ,oneside,tbook,treport
\\usepackage[deluxe,expert,multi]{otf}
%% \\usepackage{atbegshi}
%% \\AtBeginShipoutFirst{\\special{pdf:tounicode 90ms-RKSJ-UCS2}}
%% \\usepackage{makeidx}
%% \\usepackage[dvipdfmx,bookmarks=true,bookmarksnumbered=true]{hyperref}
\\usepackage{kochu}
\\usepackage{furikana}
\\usepackage{furiknkt}
\\usepackage{jdkintou}
\\usepackage{graphicx}
\\usepackage{wrapfig}
\\usepackage{ipamjm}
\\usepackage{pxjaschart}
\\usepackage{uline--}
\\pagestyle{bothstyle}
\\setlength\\intextsep{0.8zw}
\\setlength\\textfloatsep{0.8zw}
\\kanjiskip0.2pt plus 0.4pt minus 0.4pt
\\xkanjiskip\\kanjiskip
\\AtBeginDvi{\\special{papersize=\\the\\paperwidth,\\the\\paperheight}}
%%% \\setlength{\\evensidemargin}{\\oddsidemargin}
%% \\newcommand{\\pseudochapter}[1]{\\chapter*{#1}}
%% \\newcommand{\\pseudosection}[1]{\\section*{#1}}
%% \\makeatletter
%% \\def\\section{\\@startsection {section}{1}{\\z@}{-0ex plus -0ex minus -.0ex}{0 ex plus .0ex}{\\normalsize}}
%% \\makeatother 
%% \\makeindex
%% \\title{耶蘇降生千八百九十六年 \\\\ {\\Huge 新約全書} \\\\ 明治二十九年}
%% \\author{大日本聖書舘 \\\\ 横濱二十六番}
%% \\date{令和元年皐月 \\\\ \\href{https://bitbucket.org/kurodahisao/meiji-bible}{\\LaTeX 組版生成}}
%% \\setcounter{tocdepth}{1}
%% \\renewcommand{\\contentsname}{新約全書目録}
%% \\makeatletter
%% \\renewcommand{\\tableofcontents}{% TOCはいつでもTwoColumn
%%   \\if@twocolumn\\@restonecolfalse
%%   \\else\\@restonecoltrue\\twocolumn\\fi
%%   \\chapter*{\\contentsname
%%     \\@mkboth{\\contentsname}{\\contentsname}%
%%   }\\@starttoc{toc}%
%%   \\if@restonecol\\onecolumn\\fi
%% }
%% \\renewenvironment{theindex}
%%   {\\if@twocolumn\\@restonecolfalse\\else\\@restonecoltrue\\fi
%%    \\columnseprule\\z@ \\columnsep 35\\p@
%%    \\twocolumn%
%%    \\@mkboth{\\indexname}{\\indexname}%
%%    \\thispagestyle{jpl@in}\\parindent\\z@
%%    \\parskip\\z@ \\@plus .3\\p@\\relax
%%    \\let\\item\\@idxitem}
%%   {\\if@restonecol\\onecolumn\\else\\clearpage\\fi}
%% \\makeatother
\\begin{document}
\\topmargin-0.6in
\\newcommand{\\LRkanJI}[2]{%
  \\tbaselineshift=0.3zw  % 0pt
        \\parbox<y>{1zw}{%
            \\scalebox{0.5}[1]{%
                \\makebox[2zw]{#1\\hspace{-0.1zw}#2}}}}
\\def\\katano#1{\\linebreak[3]\\kern0.5zw\\raisebox{0.5zw}[0pt][0pt]{\\tiny#1}\\kern0.1zw}
\\makeatletter
\\def\\kochuuname{}
\\def\\arabicchuucite#1{\\rensuji{\\chuucite{#1}}}
\\def\\@kochuulabel#1{\\rensuji{#1}}
\\makeatother
%% \\maketitle
%% \\tableofcontents")

(defparameter +bible-meiji-chapter-header+
    "\\goodbreak
\\noindent
\\begin{wrapfigure}[2]{l}{0pt}
  \\vbox to 2zw {\\hbox to 3zw {\\large \\kintou{4zw}{\\bf ~A}}}
\\end{wrapfigure}
\\noindent")

(defun meiji-title (meiji)
  (third (second (second meiji))))

(defun meiji-mokuji (meiji)
  #+obsolete (fourth (fifth (sixth (fourth (third (second meiji))))))
  (fourth (second (chapter-context-text meiji))))

(defun chapter-url (chapter &key (method "https") (host "ja.wikisource.org"))
  (format nil "~A://~A~A" method host (third (first (second chapter)))))

(defun chapter-title (chapter)
  (second (second chapter)))

(defun chapter-title-number (number)
  (let ((ascii (format nil "~D" number)))
    (loop for digit across ascii
        collect (digit-char-to-kanji digit) into kanji
        finally (return (format nil "第~{~A~}章" kanji)))))

(defun chapter-context-text (chapter)
  #+obsolete (fifth (sixth (fourth (third (second chapter)))))
  (seventh (sixth (fourth (third (second chapter))))))

(defun chapter-honbun (chapter)
  #+obsolete (fourth (fifth (sixth (fourth (third (second chapter))))))
  (remove-irrelevants (fourth (second (chapter-context-text chapter)))))

(defun remove-irrelevants (honbun)
  "雜音除去"
  (flet ((link-p (elm)
           (and (listp elm) (listp (first elm)) (eql :link (first (first elm)))))
         (style-p (elm)
           (and (listp elm) (listp (first elm)) (eql :style (first (first elm))))))
    (loop for elm in honbun
        unless (or (link-p elm) (style-p elm)) collect elm)))

(defun chapter-kouchuu (chapter)
  (sixth (chapter-context-text chapter)))

(defun ruby-kanji (ruby)
  (second (assoc :rb (rest ruby))))

(defun ruby-kana (ruby)
  #+obsolete (second (assoc :rt (rest ruby)))
  (second (fourth ruby)))

(defun digit-char-to-kanji (char)
  (ecase char
    (#\0 #\〇) (#\1 #\一) (#\2 #\二) (#\3 #\三) (#\4 #\四)
    (#\5 #\五) (#\6 #\六) (#\7 #\七) (#\8 #\八) (#\9 #\九)
    (#\- #\、)))

(defun ruby-p (elm)
  (and (listp elm) (eql :ruby (first elm))))

(defvar *very-first-p*)
(defvar *after-newline-p*)

(defun parse-chapter-honbun (honbun &optional stream)
  (let ((*very-first-p* t)
        (*after-newline-p* nil))
    (catch 'quit
      (loop for onelm on honbun
         for elm = (first onelm)
         if (stringp elm) do
           (parse-honbun-string elm stream)
         else if (ruby-p elm) do
           (parse-honbun-ruby elm (get-next-elm onelm) stream)
         else if (eql elm :br) do
           (format stream "~%")))))

(defun get-next-elm (onelm)
  (let ((second (second onelm)))
    (multiple-value-bind (pos1 pos2)
        (comment-string-p second)
      (if (and pos1 pos2)               ; 例: "テヲピロ(※1)よ"
          (let ((string1 (subseq second 0 pos1))
                (string2 (subseq second (1+ pos2))))
            (let ((string (concatenate 'string string1 string2)))
              (if (> (length string) 0)
                  string
                (third onelm))))
        second))))

(defun comment-string-p (string)
  (when (stringp string)
    (let ((pos1 (position #\( string))
          (pos2 (position #\) string)))
      (and pos1 pos2 (values pos1 pos2)))))

(defun parse-honbun-string (string &optional stream)
  (setq string (string-trim '(#\return #\linefeed) string))
  (flet ((parse-honbun-string-aux (string)
           (cond ((= 0 (length string)))
                 ((digit-char-p (char string 0))
                  (loop for char across string
                      for i from 0
                      while (or (digit-char-p char) (char= #\- char)) collect
                        (digit-char-to-kanji char) into kanjinum
                      finally (cond (*very-first-p*
                                     (setq *very-first-p* nil)
                                     (write-string "\\noindent" stream))
                                    (*after-newline-p*
                                     (setq *after-newline-p* nil)
                                     (write-string "\\indent" stream)
                                     (terpri stream)))
                              ;; 目次を「第一章 アブラハムの裔なるダビデの裔イエス・キリストの系圖」のやうにする
                              (when (equal kanjinum '(#\一))
                                (setq *chapter-string* (subseq string i)))
                              (when (equal kanjinum '(#\二))
                                (let* ((end (min (length *chapter-string*) 23))
                                       (string (subseq *chapter-string* 0 end)))
                                  (format stream "%% \\refstepcounter{section}\\addcontentsline{toc}{section}{~A {\\tiny ~A}}~%" *chapter-title* string)))
                              ;; \nobreakは效果無く、\nolinebreakがある程度有效?
                              (format stream "\\katano{~{~A~}} \\protect\\nolinebreak ~A" kanjinum (subseq string i))))
                 (t (setq *chapter-string*
                      (format nil "~A~A" *chapter-string* string))
                    (format stream "~A" string)))))
    (if (= 0 (length string))
        (progn
          (format stream "~%")
          (setq *after-newline-p* t))     ; (format stream "\\newline")
      (if (eql (position #\※ string) 0) ; 例: "※1 明治14年版では…"
          (throw 'quit nil)
        (multiple-value-bind (pos1 pos2)
            (comment-string-p string)
          (if (and pos1 pos2)           ; 例: "テヲピロ(※1)よ"
              (let ((string1 (subseq string 0 pos1))
                    (string2 (subseq string (1+ pos2))))
                (parse-honbun-string-aux string1)
                (parse-honbun-string-aux string2))
            (parse-honbun-string-aux string)))))))

(defun katakanap (char)
  (<= (char-code #\ア) (char-code char) (char-code #\ン)))

(defun parse-honbun-ruby (ruby next &optional stream)
  (let ((kanji (ruby-kanji ruby))
        (kana (ruby-kana ruby)))
    (setq *chapter-string*              ; 目次文字列
      (format nil "~A~A" *chapter-string* kanji))
    ;; ルビ振り
    (make-kanji-kana-form kanji kana next stream)))

(defun make-kanji-kana-form (kanji kana next &optional stream)
  (if (and (> (length kana) (* 2 (length kanji)))
           (or (ruby-p next)
               (and (stringp next) (katakanap (char next 0)))))
      ;; 次の文字にかからないやうに
      (format stream "\\kanakt[3]{~A}{~A}" kanji kana)
      (if (= (length kana) (1+ (length kanji)))
          ;; 隣接するルビの調整
          (format stream "\\kana{~A}{~A}" kanji kana)
          ;; 肩つきルビ
          (format stream "\\kanakt{~A}{~A}" kanji kana))))

(defvar *book*)

(defun book-of (string)
  (setq *book*
    (let ((position (search "(明治元訳)" string)))
      (if position
          (subseq string 0 position)
          string))))

(defparameter +new-testament+ "\\Kanakt{新,約,全,書}{しん,やく,ぜん,しよ}")
(defparameter +matthew+ "\\Kanakt{馬,太,傳,福,音,書}{ま,たい,でん,ふく,いん,しよ}")
(defparameter +mark+ "\\Kanakt{馬,可,傳,福,音,書}{ま,こ,でん,ふく,いん,しよ}")
(defparameter +luke+ "\\Kanakt{路,加,傳,福,音,書}{る,か,でん,ふく,いん,しよ}")
(defparameter +john+ "\\Kanakt{約,翰,傳,福,音,書}{よ,はね,でん,ふく,いん,しよ}")
(defparameter +acts+ "\\Kanakt{使,徒}{し,と}\\kana[3]{行}{ぎやう}\\kana{傳}{でん}")
(defparameter +romans+ "\\Kanakt{羅,馬}{ろ,ま}\\kana[3]{書}{のふみ}")
(defparameter +long-romans+ "\\Kanakt{使,徒}{し,と}パウロ、ロマ\\kana{人}{びと}に\\kana{贈}{おく}れる\\kana{書}{ふみ}")
(defparameter +i-corinthians+ "\\Kanakt{哥,林,多}{こ,りん,と}\\kana[3]{前}{さきの}\\kana{書}{ふみ}")
(defparameter +ii-corinthians+ "\\Kanakt{哥,林,多}{こ,りん,と}\\kana[3]{後}{のちの}\\kana{書}{ふみ}")
(defparameter +long-i-corinthians+ "\\Kanakt{使,徒}{し,と}パウロ、コリント\\kana{人}{びと}に\\kana{贈}{おく}れる\\kana{前書}{さきのふみ}")
(defparameter +long-ii-corinthians+ "\\Kanakt{使,徒}{し,と}パウロ、コリント\\kana{人}{びと}に\\kana{贈}{おく}れる\\kana{後書}{のちのふみ}")
(defparameter +galatians+ "\\Kanakt{加,拉,太}{が,ら,てや}\\kana[3]{書}{のふみ}")
(defparameter +long-galatians+ "\\Kanakt{使,徒}{し,と}パウロ、ガラテヤ\\kana{人}{びと}に\\kana{贈}{おく}れる\\kana{書}{ふみ}")
(defparameter +ephesians+ "\\Kanakt{以,弗,所}{え,ぺ,そ}\\kana[3]{書}{のふみ}")
(defparameter +long-ephesians+ "\\Kanakt{使,徒}{し,と}パウロ、エペソ\\kana{人}{びと}に\\kana{贈}{おく}れる\\kana{書}{ふみ}")
(defparameter +philippians+ "\\Kanakt{腓,立,比}{ぴ,り,ぴ}\\kana[3]{書}{のふみ}")
(defparameter +long-philippians+ "\\Kanakt{使,徒}{し,と}パウロ、ピリピ\\kana{人}{びと}に\\kana{贈}{おく}れる\\kana{書}{ふみ}")
(defparameter +colossians+ "\\Kanakt{哥,羅,西}{こ,ろ,さい}\\kana[3]{書}{のふみ}")
(defparameter +long-colossians+ "\\Kanakt{使,徒}{し,と}パウロ、コロサイ\\kana{人}{びと}に\\kana{贈}{おく}れる\\kana{書}{ふみ}")
(defparameter +i-thessalonians+ "\\Kanakt{帖,撒,羅,尼,迦}{て,さ,ろ,に,け}\\kana[3]{前}{さきの}\\kana{書}{ふみ}")
(defparameter +ii-thessalonians+ "\\Kanakt{帖,撒,羅,尼,迦}{て,さ,ろ,に,け}\\kana[3]{後}{のちの}\\kana{書}{ふみ}")
(defparameter +long-i-thessalonians+ "\\Kanakt{使,徒}{し,と}パウロ、テサロニケ\\kana{人}{びと}に\\kana{贈}{おく}れる\\kana{前書}{さきのふみ}")
(defparameter +long-ii-thessalonians+ "\\Kanakt{使,徒}{し,と}パウロ、テサロニケ\\kana{人}{びと}に\\kana{贈}{おく}れる\\kana{後書}{のちのふみ}")
(defparameter +i-timothy+ "\\Kanakt{提,摩,太}{て,も,て}\\kana[3]{前}{さきの}\\kana{書}{ふみ}")
(defparameter +ii-timothy+ "\\Kanakt{提,摩,太}{て,も,て}\\kana[3]{後}{のちの}\\kana{書}{ふみ}")
(defparameter +long-i-timothy+ "\\Kanakt{使,徒}{し,と}パウロ、テモテに\\kana{贈}{おく}れる\\kana{前書}{さきのふみ}")
(defparameter +long-ii-timothy+ "\\Kanakt{使,徒}{し,と}パウロ、テモテに\\kana{贈}{おく}れる\\kana{後書}{のちのふみ}")
(defparameter +titus+ "\\Kanakt{提,多}{て,とす}\\kana[3]{書}{のふみ}")
(defparameter +long-titus+ "\\Kanakt{使,徒}{し,と}パウロ、テトスに\\kana{贈}{おく}れる\\kana{書}{ふみ}")
(defparameter +philemon+ "\\Kanakt{腓,利,門}{ぴ,れ,もん}\\kana[3]{書}{のふみ}")
(defparameter +long-philemon+ "\\Kanakt{使,徒}{し,と}パウロ、ピレモンに\\kana{贈}{おく}れる\\kana{書}{ふみ}")
(defparameter +hebrews+ "\\Kanakt{希,伯,來}{へ,ぶ,る}\\kana[3]{書}{のふみ}")
(defparameter +long-hebrews+ "\\Kanakt{使,徒}{し,と}パウロ、ヘブル\\kana{人}{びと}に\\kana{贈}{おく}れる\\kana{書}{ふみ}")
(defparameter +james+ "\\Kanakt{雅,各}{や,こぶ}\\kana[3]{書}{のふみ}")
(defparameter +long-james+ "\\Kanakt{使,徒}{し,と}ヤコブの\\kana{書}{ふみ}")
(defparameter +i-peter+ "\\Kanakt{使,徒,彼,得}{し,と,ぺ,てろ}\\kana[3]{前}{さきの}\\kana{書}{ふみ}")
(defparameter +ii-peter+ "\\Kanakt{使,徒,彼,得}{し,と,ぺ,てろ}\\kana[3]{後}{のちの}\\kana{書}{ふみ}")
(defparameter +long-i-peter+ "\\Kanakt{使,徒}{し,と}ペテロ\\kana{前書}{さきのふみ}")
(defparameter +long-ii-peter+ "\\Kanakt{使,徒}{し,と}ペテロ\\kana{後書}{のちのふみ}")
(defparameter +i-john+ "\\Kanakt{使,徒,約,翰}{し,と,よ,はね}\\kana{第}{だい}\\kana{一}{いつ}\\kana{書}{しよ}")
(defparameter +ii-john+ "\\Kanakt{使,徒,約,翰}{し,と,よ,はね}\\kana{第}{だい}\\Kanakt{二}{に}\\kana{書}{しよ}")
(defparameter +iii-john+ "\\Kanakt{使,徒,約,翰}{し,と,よ,はね}\\kana{第}{だい}\\kana{三}{さん}\\kana{書}{しよ}")
(defparameter +long-i-john+ "\\Kanakt{使,徒}{し,と}ヨハネ\\Kanakt{第,一,書}{だい,いつ,しよ}")
(defparameter +long-ii-john+ "\\Kanakt{使,徒}{し,と}ヨハネ\\Kanakt{第,二,書}{だい,に,しよ}")
(defparameter +long-iii-john+ "\\Kanakt{使,徒}{し,と}ヨハネ\\Kanakt{第,三,書}{だい,さん,しよ}")
(defparameter +jude+ "\\Kanakt{猶,太}{ゆ,だ}\\kana[3]{書}{のふみ}")
(defparameter +long-jude+ "\\Kanakt{使,徒}{し,と}ユダの\\kana{書}{ふみ}")
(defparameter +revelation+ "\\Kanakt{約,翰}{よ,はね}\\Kanakt{默,示,録}{もく,し,ろく}")
(defparameter +the-end+ "\\kana[3]{終}{をはり}")

(defun long-title (book &optional stream)
  (cond ((string= book "馬太傳福音書")
         (format stream "\\Large ~A~A" +new-testament+ +matthew+))
        ((string= book "馬可傳福音書")
         (format stream "\\Large ~A~A" +new-testament+ +mark+))
        ((string= book "路加傳福音書")
         (format stream "\\Large ~A~A" +new-testament+ +luke+))
        ((string= book "約翰傳福音書")
         (format stream "\\Large ~A~A" +new-testament+ +john+))
        ((string= book "使徒行傳")
         (format stream "\\Large ~A~A" +new-testament+ +acts+))
        ((string= book "羅馬書")
         (format stream "\\Large ~A~A" +new-testament+ +long-romans+))
        ((string= book "哥林多前書")
         (format stream "\\Large ~A~A" +new-testament+ +long-i-corinthians+))
        ((string= book "哥林多後書")
         (format stream "\\Large ~A~A" +new-testament+ +long-ii-corinthians+))
        ((string= book "加拉太書")
         (format stream "\\Large ~A~A" +new-testament+ +long-galatians+))
        ((string= book "以弗所書")
         (format stream "\\Large ~A~A" +new-testament+ +long-ephesians+))
        ((string= book "腓立比書")
         (format stream "\\Large ~A~A" +new-testament+ +long-philippians+))
        ((string= book "哥羅西書")
         (format stream "\\Large ~A~A" +new-testament+ +long-colossians+))
        ((string= book "帖撒羅尼迦前書")
         (format stream "\\Large ~A~A" +new-testament+ +long-i-thessalonians+))
        ((string= book "帖撒羅尼迦後書")
         (format stream "\\Large ~A~A" +new-testament+ +long-ii-thessalonians+))
        ((string= book "提摩太前書")
         (format stream "\\Large ~A~A" +new-testament+ +long-i-timothy+))
        ((string= book "提摩太後書")
         (format stream "\\Large ~A~A" +new-testament+ +long-ii-timothy+))
        ((string= book "提多書")
         (format stream "\\Large ~A~A" +new-testament+ +long-titus+))
        ((string= book "腓利門書")
         (format stream "\\Large ~A~A" +new-testament+ +long-philemon+))
        ((string= book "希伯來書")
         (format stream "\\Large ~A~A" +new-testament+ +long-hebrews+))
        ((string= book "雅各書")
         (format stream "\\Large ~A~A" +new-testament+ +long-james+))
        ((string= book "彼得前書")
         (format stream "\\Large ~A~A" +new-testament+ +long-i-peter+))
        ((string= book "彼得後書")
         (format stream "\\Large ~A~A" +new-testament+ +long-ii-peter+))
        ((string= book "約翰第一書")
         (format stream "\\Large ~A~A" +new-testament+ +long-i-john+))
        ((string= book "約翰第二書")
         (format stream "\\Large ~A~A" +new-testament+ +long-ii-john+))
        ((string= book "約翰第三書")
         (format stream "\\Large ~A~A" +new-testament+ +long-iii-john+))
        ((string= book "猶太書")
         (format stream "\\Large ~A~A" +new-testament+ +long-jude+))
        ((string= book "約翰默示録")
         (format stream "\\Large ~A~A" +new-testament+ +revelation+))))

(defun end-title (book &optional stream)
  (cond ((string= book "馬太傳福音書")
         (format stream "~A~A~A" +new-testament+ +matthew+ +the-end+))
        ((string= book "馬可傳福音書")
         (format stream "~A~A~A" +new-testament+ +mark+ +the-end+))
        ((string= book "路加傳福音書")
         (format stream "~A~A~A" +new-testament+ +luke+ +the-end+))
        ((string= book "約翰傳福音書")
         (format stream "~A~A~A" +new-testament+ +john+ +the-end+))
        ((string= book "使徒行傳")
         (format stream "~A~A~A" +new-testament+ +acts+ +the-end+))
        ((string= book "羅馬書")
         (format stream "~A~A~A" +new-testament+ +romans+ +the-end+))
        ((string= book "哥林多前書")
         (format stream "~A~A~A" +new-testament+ +i-corinthians+ +the-end+))
        ((string= book "哥林多後書")
         (format stream "~A~A~A" +new-testament+ +ii-corinthians+ +the-end+))
        ((string= book "加拉太書")
         (format stream "~A~A~A" +new-testament+ +galatians+ +the-end+))
        ((string= book "以弗所書")
         (format stream "~A~A~A" +new-testament+ +ephesians+ +the-end+))
        ((string= book "腓立比書")
         (format stream "~A~A~A" +new-testament+ +philippians+ +the-end+))
        ((string= book "哥羅西書")
         (format stream "~A~A~A" +new-testament+ +colossians+ +the-end+))
        ((string= book "帖撒羅尼迦前書")
         (format stream "~A~A~A" +new-testament+ +i-thessalonians+ +the-end+))
        ((string= book "帖撒羅尼迦後書")
         (format stream "~A~A~A" +new-testament+ +ii-thessalonians+ +the-end+))
        ((string= book "提摩太前書")
         (format stream "~A~A~A" +new-testament+ +i-timothy+ +the-end+))
        ((string= book "提摩太後書")
         (format stream "~A~A~A" +new-testament+ +ii-timothy+ +the-end+))
        ((string= book "提多書")
         (format stream "~A~A~A" +new-testament+ +titus+ +the-end+))
        ((string= book "腓利門書")
         (format stream "~A~A~A" +new-testament+ +philemon+ +the-end+))
        ((string= book "希伯來書")
         (format stream "~A~A~A" +new-testament+ +hebrews+ +the-end+))
        ((string= book "雅各書")
         (format stream "~A~A~A" +new-testament+ +james+ +the-end+))
        ((string= book "彼得前書")
         (format stream "~A~A~A" +new-testament+ +i-peter+ +the-end+))
        ((string= book "彼得後書")
         (format stream "~A~A~A" +new-testament+ +ii-peter+ +the-end+))
        ((string= book "約翰第一書")
         (format stream "~A~A~A" +new-testament+ +i-john+ +the-end+))
        ((string= book "約翰第二書")
         (format stream "~A~A~A" +new-testament+ +ii-john+ +the-end+))
        ((string= book "約翰第三書")
         (format stream "~A~A~A" +new-testament+ +iii-john+ +the-end+))
        ((string= book "猶太書")
         (format stream "~A~A~A" +new-testament+ +jude+ +the-end+))
        ((string= book "約翰默示録")
         (format stream "~A~A~A" +new-testament+ +revelation+ +the-end+))))

(defun convert-wikisource-to-latex (url external-format &optional stream)
  "タイトルを打ったあと、一般に複數の章建てと本文とがある"
  (let ((lhtml (write-title-and-get-lhtml url external-format stream)))
    (loop with *chapter-string* = ""
        for chapter in (rest (meiji-mokuji lhtml))
        for i from 1
        for chapter-url = (chapter-url chapter)
        for *chapter-title* = (chapter-title-number i)
        for chapter-html = #+allegro (net.aserve.client:do-http-request chapter-url :external-format external-format)
                           #+sbcl (drakma:http-request chapter-url)
        for chapter-lhtml = (net.html.parser:parse-html chapter-html)
        do (generate-chapter-honbun-latex *chapter-title* chapter-lhtml stream)
        finally (format stream "\\vspace*{\\fill}~%\\noindent~%"))))

(defun write-title-and-get-lhtml (url external-format &optional stream)
  #+sbcl (declare (ignore external-format))
  (let* ((html #+allegro (net.aserve.client:do-http-request url :external-format external-format)
               #+sbcl (drakma:http-request url))
         (lhtml (net.html.parser:parse-html html)))
    (let* ((title (meiji-title lhtml))
           (book (book-of (second title))))
      ;; (write-string +bible-meiji-header+ stream)
      (format stream "~%\\chapter*{~A}~%" (long-title book))
      (format stream "%% \\addcontentsline{toc}{chapter}{~A}~%" book)
      lhtml)))

(defun generate-chapter-honbun-latex (chapter-title chapter-lhtml &optional stream)
  (generate-markboth chapter-title stream)
  (format stream +bible-meiji-chapter-header+ chapter-title)
  (parse-chapter-honbun (chapter-honbun chapter-lhtml) stream))

(defun generate-markboth (chapter-title &optional stream)
  (let ((mark (format nil "~A~A"  *book* chapter-title)))
    (format t "~A~%" mark)
    (format stream "~%\\markboth{~A}{~A}~%" (reverse mark) mark)))

(defun convert-wikisource-no-chapter-to-latex (url external-format &optional stream)
  "腓利門書は章建てがなく一つの本文だけからなる"
  (let ((lhtml (write-title-and-get-lhtml url external-format stream)))
    (let ((*chapter-string* "")
          (*chapter-title* "")
          (chapter-lhtml lhtml))
      (generate-markboth *chapter-title* stream)
      (parse-chapter-honbun (chapter-honbun chapter-lhtml) stream)
      (format stream "\\vspace*{\\fill}~%\\noindent~%"))))


;;;;

(defun convert-matthew-to-latex (&key (url "https://ja.wikisource.org/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8%28%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3%29")
                                      (external-format +crlf-base-utf8+)
                                      (stream t)
                                      (header +bible-meiji-header+))
  "馬太福音書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-mark-to-latex (&key (url "https://ja.wikisource.org/wiki/%E9%A6%AC%E5%8F%AF%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8%28%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3%29")
                                   (external-format +crlf-base-utf8+)
                                   (stream t)
                                   (header +bible-meiji-header+))
  "馬可福音書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-luke-to-latex (&key (url "https://ja.wikisource.org/wiki/%E8%B7%AF%E5%8A%A0%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8%28%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3%29")
                                   (external-format +crlf-base-utf8+)
                                   (stream t)
                                   (header +bible-meiji-header+))
  "路加福音書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-john-to-latex (&key (url "https://ja.wikisource.org/wiki/%E7%B4%84%E7%BF%B0%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8%28%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3%29")
                                   (external-format +crlf-base-utf8+)
                                   (stream t)
                                   (header +bible-meiji-header+))
  "約翰福音書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-acts-to-latex (&key (url "https://ja.wikisource.org/wiki/%E4%BD%BF%E5%BE%92%E8%A1%8C%E5%82%B3(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                   (external-format +crlf-base-utf8+)
                                   (stream t)
                                   (header +bible-meiji-header+))
  "使徒行傳"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)
    #+ignore
    (format stream "
  \\onecolumn
  \\newpage
  \\begin{figure}[p]
    \\vskip 11cm
    \\includegraphics[scale=.13]{IMG_20180705_162221.jpg}
  \\end{figure}

  \\newpage
  \\begin{figure}[p]
    \\vskip 11cm
    \\includegraphics[scale=.13]{IMG_20180705_162515.jpg}
  \\end{figure}
  \\newpage
  \\begin{figure}[p]
    \\vskip 11cm
    \\includegraphics[scale=.13]{IMG_20180705_162600.jpg}
  \\end{figure}~%")))

(defun convert-romans-to-latex (&key (url "https://ja.wikisource.org/wiki/%E7%BE%85%E9%A6%AC%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                     (external-format +crlf-base-utf8+)
                                     (stream t)
                                     (header +bible-meiji-header+))
  "羅馬書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-i-corinthians-to-latex (&key (url "https://ja.wikisource.org/wiki/%E5%93%A5%E6%9E%97%E5%A4%9A%E5%89%8D%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                            (external-format +crlf-base-utf8+)
                                            (stream t)
                                            (header +bible-meiji-header+))
  "哥林多前書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ii-corinthians-to-latex (&key (url "https://ja.wikisource.org/wiki/%E5%93%A5%E6%9E%97%E5%A4%9A%E5%BE%8C%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                             (external-format +crlf-base-utf8+)
                                             (stream t)
                                             (header +bible-meiji-header+))
  "哥林多後書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-galatians-to-latex (&key (url "https://ja.wikisource.org/wiki/%E5%8A%A0%E6%8B%89%E5%A4%AA%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                        (external-format +crlf-base-utf8+)
                                        (stream t)
                                        (header +bible-meiji-header+))
  "加拉太書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ephesians-to-latex (&key (url "https://ja.wikisource.org/wiki/%E4%BB%A5%E5%BC%97%E6%89%80%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                        (external-format +crlf-base-utf8+)
                                        (stream t)
                                        (header +bible-meiji-header+))
  "以弗所書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-philippians-to-latex (&key (url "https://ja.wikisource.org/wiki/%E8%85%93%E7%AB%8B%E6%AF%94%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                          (external-format +crlf-base-utf8+)
                                          (stream t)
                                          (header +bible-meiji-header+))
  "腓立比書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-colossians-to-latex (&key (url "https://ja.wikisource.org/wiki/%E5%93%A5%E7%BE%85%E8%A5%BF%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                         (external-format +crlf-base-utf8+)
                                         (stream t)
                                         (header +bible-meiji-header+))
  "哥羅西書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-i-thessalonians-to-latex (&key (url "https://ja.wikisource.org/wiki/%E5%B8%96%E6%92%92%E7%BE%85%E5%B0%BC%E8%BF%A6%E5%89%8D%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                              (external-format +crlf-base-utf8+)
                                              (stream t)
                                              (header +bible-meiji-header+))
  "帖撒羅尼迦前書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ii-thessalonians-to-latex (&key (url "https://ja.wikisource.org/wiki/%E5%B8%96%E6%92%92%E7%BE%85%E5%B0%BC%E8%BF%A6%E5%BE%8C%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                               (external-format +crlf-base-utf8+)
                                               (stream t)
                                               (header +bible-meiji-header+))
  "帖撒羅尼迦後書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-i-timothy-to-latex (&key (url "https://ja.wikisource.org/wiki/%E6%8F%90%E6%91%A9%E5%A4%AA%E5%89%8D%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                        (external-format +crlf-base-utf8+)
                                        (stream t)
                                        (header +bible-meiji-header+))
  "提摩太前書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ii-timothy-to-latex (&key (url "https://ja.wikisource.org/wiki/%E6%8F%90%E6%91%A9%E5%A4%AA%E5%BE%8C%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                         (external-format +crlf-base-utf8+)
                                         (stream t)
                                         (header +bible-meiji-header+))
  "提摩太後書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-titus-to-latex (&key (url "https://ja.wikisource.org/wiki/%E6%8F%90%E5%A4%9A%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                    (external-format +crlf-base-utf8+)
                                    (stream t)
                                    (header +bible-meiji-header+))
  "提多書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-philemon-to-latex (&key (url "https://ja.wikisource.org/wiki/%E8%85%93%E5%88%A9%E9%96%80%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                       (external-format +crlf-base-utf8+)
                                       (stream t)
                                       (header +bible-meiji-header+))
  "腓利門書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-no-chapter-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-hebrews-to-latex (&key (url "https://ja.wikisource.org/wiki/%E5%B8%8C%E4%BC%AF%E4%BE%86%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                      (external-format +crlf-base-utf8+)
                                      (stream t)
                                      (header +bible-meiji-header+))
  "希伯來書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-james-to-latex (&key (url "https://ja.wikisource.org/wiki/%E9%9B%85%E5%90%84%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                    (external-format +crlf-base-utf8+)
                                    (stream t)
                                    (header +bible-meiji-header+))
  "雅各書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-i-peter-to-latex (&key (url "https://ja.wikisource.org/wiki/%E5%BD%BC%E5%BE%97%E5%89%8D%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                      (external-format +crlf-base-utf8+)
                                      (stream t)
                                      (header +bible-meiji-header+))
  "彼得前書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ii-peter-to-latex (&key (url "https://ja.wikisource.org/wiki/%E5%BD%BC%E5%BE%97%E5%BE%8C%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                       (external-format +crlf-base-utf8+)
                                       (stream t)
                                       (header +bible-meiji-header+))
  "彼得後書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-i-john-to-latex (&key (url "https://ja.wikisource.org/wiki/%E7%B4%84%E7%BF%B0%E7%AC%AC%E4%B8%80%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                     (external-format +crlf-base-utf8+)
                                     (stream t)
                                     (header +bible-meiji-header+))
  "約翰第一書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ii-john-to-latex (&key (url "https://ja.wikisource.org/wiki/%E7%B4%84%E7%BF%B0%E7%AC%AC%E4%BA%8C%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                      (external-format +crlf-base-utf8+)
                                      (stream t)
                                      (header +bible-meiji-header+))
  "約翰第二書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-no-chapter-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-iii-john-to-latex (&key (url "https://ja.wikisource.org/wiki/%E7%B4%84%E7%BF%B0%E7%AC%AC%E4%B8%89%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                       (external-format +crlf-base-utf8+)
                                       (stream t)
                                       (header +bible-meiji-header+))
  "約翰第三書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-no-chapter-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-jude-to-latex (&key (url "https://ja.wikisource.org/wiki/%E7%8C%B6%E5%A4%AA%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                   (external-format +crlf-base-utf8+)
                                   (stream t)
                                   (header +bible-meiji-header+))
  "猶太書"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-no-chapter-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-revelation-to-latex (&key (url "https://ja.wikisource.org/wiki/%E7%B4%84%E7%BF%B0%E9%BB%98%E7%A4%BA%E9%8C%B2(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                         (external-format +crlf-base-utf8+)
                                         (stream t)
                                         (header +bible-meiji-header+))
  "約翰默示録"
  (let (*book*)
    (and header (write-string header stream))
    (convert-wikisource-to-latex url external-format stream)
    (end-title *book* stream)
    (terpri stream)))


(defun convert-new-testament-to-latex (&key (stream t))
  "新約全書"
  (write-string +bible-meiji-header+ stream)
  (convert-matthew-to-latex :stream stream :header nil)
  (convert-mark-to-latex :stream stream :header nil)
  (convert-luke-to-latex :stream stream :header nil)
  (convert-john-to-latex :stream stream :header nil)
  (convert-acts-to-latex :stream stream :header nil)
  (convert-romans-to-latex :stream stream :header nil)
  (convert-i-corinthians-to-latex :stream stream :header nil)
  (convert-ii-corinthians-to-latex :stream stream :header nil)
  (convert-galatians-to-latex :stream stream :header nil)
  (convert-ephesians-to-latex :stream stream :header nil)
  (convert-philippians-to-latex :stream stream :header nil)
  (convert-colossians-to-latex :stream stream :header nil)
  (convert-i-thessalonians-to-latex :stream stream :header nil)
  (convert-ii-thessalonians-to-latex :stream stream :header nil)
  (convert-i-timothy-to-latex :stream stream :header nil)
  (convert-ii-timothy-to-latex :stream stream :header nil)
  (convert-titus-to-latex :stream stream :header nil)
  (convert-philemon-to-latex :stream stream :header nil)
  (convert-hebrews-to-latex :stream stream :header nil)
  (convert-james-to-latex :stream stream :header nil)
  (convert-i-peter-to-latex :stream stream :header nil)
  (convert-ii-peter-to-latex :stream stream :header nil)
  (convert-i-john-to-latex :stream stream :header nil)
  (convert-ii-john-to-latex :stream stream :header nil)
  (convert-iii-john-to-latex :stream stream :header nil)
  (convert-jude-to-latex :stream stream :header nil)
  (convert-revelation-to-latex :stream stream :header nil))

;;;; Utilities

(defun find-tex-zap-file-name (&optional (type "tex"))
  (let* ((zap-path (format nil "_TZ_*.~A" type))
         (zap-files (directory zap-path)))
    (if (> (length zap-files) 1)
        (error "Multiple Zap Files ~A" zap-files)
        (first zap-files))))

(defun pdf-adjust-oyamoji (&key (zap (find-tex-zap-file-name "pdf")) (out "foo.pdf"))
  (pdf:pdf-adjust-oyamoji zap out))

#||

NET.ASERVE(57): (setq *locale* (find-locale "ja_JP.JIS"))
NET.ASERVE(58): (require :aserve)
NET.ASERVE(58): (require :phtml)
NET.ASERVE(58): (net.aserve.client:do-http-request "https://ja.wikisource.org/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8%28%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3%29" :external-format +crlf-base-utf8+)
NET.ASERVE(58): (net.html.parser:parse-html *)
NET.ASERVE(58): (setq mathew *)
NET.ASERVE(58): (third (second (second mathew)))
(:TITLE "馬太傳福音書(明治元訳) - Wikisource")
NET.ASERVE(58): (chapter-honbun mathew) ; meiji-mokuji

NET.ASERVE(58): (loop for li in (rest *) do (print li))
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%B8%80%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第一章")
  "第一章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%8C%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第二章")
  "第二章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%B8%89%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第三章")
  "第三章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%9B%9B%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第四章")
  "第四章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%94%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第五章")
  "第五章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%85%AD%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第六章")
  "第六章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%B8%83%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第七章")
  "第七章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%85%AB%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第八章")
  "第八章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%B9%9D%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第九章")
  "第九章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%8D%81%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第十章")
  "第十章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%8D%81%E4%B8%80%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第十一章")
  "第十一章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%8D%81%E4%BA%8C%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第十二章")
  "第十二章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%8D%81%E4%B8%89%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第十三章")
  "第十三章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%8D%81%E5%9B%9B%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第十四章")
  "第十四章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%8D%81%E4%BA%94%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第十五章")
  "第十五章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%8D%81%E5%85%AD%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第十六章")
  "第十六章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%8D%81%E4%B8%83%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第十七章")
  "第十七章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%8D%81%E5%85%AB%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第十八章")
  "第十八章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E5%8D%81%E4%B9%9D%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第十九章")
  "第十九章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%8C%E5%8D%81%E7%AB%A0" :TITLE
   "馬太傳福音書(明治元訳) 第二十章")
  "第二十章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%80%E7%AB%A0"
   :TITLE "馬太傳福音書(明治元訳) 第二十一章")
  "第二十一章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%BA%8C%E7%AB%A0"
   :TITLE "馬太傳福音書(明治元訳) 第二十二章")
  "第二十二章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E7%AB%A0"
   :TITLE "馬太傳福音書(明治元訳) 第二十三章")
  "第二十三章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%8C%E5%8D%81%E5%9B%9B%E7%AB%A0"
   :TITLE "馬太傳福音書(明治元訳) 第二十四章")
  "第二十四章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%BA%94%E7%AB%A0"
   :TITLE "馬太傳福音書(明治元訳) 第二十五章")
  "第二十五章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%8C%E5%8D%81%E5%85%AD%E7%AB%A0"
   :TITLE "馬太傳福音書(明治元訳) 第二十六章")
  "第二十六章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%83%E7%AB%A0"
   :TITLE "馬太傳福音書(明治元訳) 第二十七章")
  "第二十七章")) 
(:LI
 ((:A :HREF "/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%BA%8C%E5%8D%81%E5%85%AB%E7%AB%A0"
   :TITLE "馬太傳福音書(明治元訳) 第二十八章")
  "第二十八章")) 

NET.ASERVE(71): (parse-uri "https://ja.wikisource.org/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8%28%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3%29")
#<URI https://ja.wikisource.org/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)>
NET.ASERVE(72): (uri-host *)
"ja.wikisource.org"
NET.ASERVE(73): (uri-scheme **)
:HTTPS
NET.ASERVE(64): (third (first (second (first (rest (chapter-honbun mathew))))))
"/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)_%E7%AC%AC%E4%B8%80%E7%AB%A0"

NET.ASERVE(105): (with-open-file (stream "meiji-mathew.tex" :direction :output :if-exists :supersede :external-format :utf-8)
                  (convert-matthew-to-latex :stream stream))

||#
