;;; -*- mode:common-lisp; coding:utf-8 -*-

#+allegro
(eval-when (:load-toplevel :compile-toplevel :execute)
  #+allegro (require :aserve) #+sbcl (ql:quickload "drakma")
  #+allegro (require :phtml) #+sbcl (ql:quickload "cl-html-parse"))

(in-package "BIBLE")

#-allegro
(defun ratiop (n)
  (eql 'ratio (type-of n)))

(defun king-james-title (sexp)
  ;; (second (second (second sexp)))
  (string-trim '(#\space #\tab #\linefeed #\return)
               (second (second (second (second sexp))))))

(defun king-james-body (sexp)
  (third (second sexp)))

(defun king-james-contents (sexp)
  (second (king-james-body sexp)))

(defun king-james-toc (sexp)
  (fourth (king-james-contents sexp)))

(defun king-james-section (sexp)
  ;; (sixth (king-james-contents sexp))
  ;; (tenth (king-james-contents sexp))
  (ninth (king-james-contents sexp)))

(defun king-james-chapter (sexp)
  ;; (third (second (second (king-james-section sexp))))
  (third (second (second (king-james-section sexp)))))

(defun king-james-chapter-contents (sexp)
  ;; (third (second (third (second (king-james-chapter sexp)))))
  ;; (third (second (fourth (second (king-james-chapter sexp)))))
  (third (second (third (second (king-james-chapter sexp))))))

(defun king-james-chapter-verses (sexp)
  ;; (rest (fourth (second (second (king-james-chapter-contents sexp)))))
  ;; (rest (rest (fourth (second (second (king-james-chapter-contents sexp))))))
  (remove-if-not #'listp
                 (rest (fourth (second (second (king-james-chapter-contents sexp)))))))

(defun king-james-comments (sexp)
  (seventh (king-james-chapter sexp)))

(defun king-james-paragraph (sexp)
  (second sexp))

(defun king-james-verse (sexp)
  ;; (third sexp)
  (format nil "~{~A~}"
          (loop for exp in sexp
              if (stringp exp) collect exp into strings
              else if (listp exp) collect
                (let ((first (first exp)))
                  (cond ((eql :em first)
                         (second exp))
                        ((equal '(:SPAN :CLASS "jesus") first)
                         (king-james-verse (rest exp)))
                        (t "")))
              into strings
              finally (return strings))))

(defun king-james-verse-with-tag (sexp)
  (format nil "~{~A~}"
          (loop for exp in sexp
              if (stringp exp) collect exp into strings
              else if (listp exp) collect
                (let ((first (first exp)))
                  (cond ((eql :em first)
                         (format nil "\\textit{~A}" (second exp)))
                        ((equal '(:SPAN :CLASS "jesus") first)
                         (let ((string (king-james-verse-with-tag (rest exp))))
                           (format nil "\\textbf{~A}" string)))
                        (t "")))
              into strings
              finally (return strings))))

(defun chapter-king-url (first-url i)
  (let* ((key "Chapter-")
         (position (+ (length key) (search key first-url))))
    (format nil "~A~A/" (subseq first-url 0 position) i)))

(defparameter +bible-multicol-header+
    "%%% -*- mode: LaTeX; coding: utf-8 -*-
%%% -*- mode: LaTeX; coding: iso-2022-jp-2 -*-
\\documentclass[a5j,10pt,notitlepage,openany]{jsbook}
\\usepackage[deluxe,expert,multi]{otf}
%% \\usepackage{makeidx}
%% \\usepackage[dvipdfmx,bookmarks=true,bookmarksnumbered=true]{hyperref}
\\usepackage{kochu}
\\usepackage{furikana}
\\usepackage{furiknkt}
\\usepackage{jdkintou}
\\usepackage{graphicx}
\\usepackage{wrapfig}
\\usepackage{ipamjm}
\\usepackage{pxjaschary}
\\usepackage{uline--}
\\usepackage{multicolpar}
\\usepackage{dropping}
\\kanjiskip0.4pt plus 0.4pt minus 0.4pt
\\xkanjiskip\\kanjiskip
%%% \\setlength{\\oddsidemargin}{0in}
%%% \\setlength{\\evensidemargin}{0in}
\\setlength{\\hoffset}{0in}
%%% \\setlength{\\paperwidth}{597pt}
\\setlength{\\textwidth}{\\fullwidth}
\\setlength{\\evensidemargin}{\\oddsidemargin}
\\renewcommand{\\rmdefault}{lmr}
%% \\newcommand{\\pseudochapter}[1]{\\chapter*{#1}}
%% \\newcommand{\\pseudosection}[1]{\\section*{#1} \\vskip -6.0zw}
%%% \\makeatletter
%%% \\def\\section{\\@startsection {section}{1}{\\z@}{-0ex plus -0ex minus -.0ex}{0 ex plus .0ex}{\\normalsize}}
%%% \\makeatother 
%% \\makeindex
%% \\renewcommand{\\indexname}{Index}
%% \\title{英和對照 \\\\ {\\Huge 新約全書} ~@[\\\\ ~A~] \\\\ {\\scriptsize The} \\\\ {\\Huge NEW TESTAMENT} ~@[\\\\ ~A~] \\\\ {\\scriptsize ENGLISH AND JAPANESE}}
%% \\author{大日本聖書舘 横濱二十六番  \\\\ 明治二十九年 \\\\ {\\scriptsize AND} \\\\ \\href{https://www.kingjamesbibleonline.org/}{KING JAMES BIBLE ONLINE}}
%% \\date{令和元年皐月 \\\\ \\href{https://bitbucket.org/kurodahisao/meiji-bible}{\\LaTeX 組版生成}}
%% \\setcounter{tocdepth}{1}
%% \\renewcommand{\\contentsname}{新約全書~@[~A~]目録}
%%% \\makeatletter
%%% \\renewcommand{\\tableofcontents}{% TOCをTwoColumn
%%%   \\settowidth\\jsc@tocl@width{\\headfont\\prechaptername\\postchaptername}%
%%%   \\settowidth\\@tempdima{\\headfont\\appendixname}%
%%%   \\ifdim\\jsc@tocl@width<\\@tempdima \\setlength\\jsc@tocl@width{\\@tempdima}\\fi
%%%   \\ifdim\\jsc@tocl@width<2zw \\divide\\jsc@tocl@width by 2 \\advance\\jsc@tocl@width 1zw\\fi
%%%   \\if@twocolumn
%%%     \\@restonecolfalse\\twocolumn
%%%   \\else
%%%     \\@restonecoltrue\\twocolumn
%%%   \\fi
%%%   \\chapter*{\\contentsname}%
%%%   \\@mkboth{\\contentsname}{}%
%%%   \\@starttoc{toc}%
%%%   \\if@restonecol\\onecolumn\\fi
%%% }
%%% \\makeatother
%% \\makeatletter
%% \\renewenvironment{theindex}
%%   {\\if@twocolumn\\@restonecolfalse\\else\\@restonecoltrue\\fi
%%    \\columnseprule\\z@ \\columnsep 35\\p@
%%    \\twocolumn%
%%    \\@mkboth{\\indexname}{\\indexname}%
%%    \\thispagestyle{jpl@in}\\parindent\\z@
%%    \\parskip\\z@ \\@plus .3\\p@\\relax
%%    \\let\\item\\@idxitem}
%%   {\\if@restonecol\\onecolumn\\else\\clearpage\\fi}
%% \\makeatother
\\renewcommand{\\headfont}{\\mcfamily\\rmfamily} 
\\begin{document}
%% \\maketitle
%% {\\makeatletter
%%  \\let\\ps@jpl@in\\ps@empty
%%  \\makeatother
%%  \\pagestyle{empty}
%%  \\tableofcontents
%%  \\clearpage}
")

(defvar *king-book*)

(defun king-book-of (string)
  (prog1
      (let ((position (search "CHAPTER" string)))
        (if position
            (subseq string 0 (1- position))
          string))
    (setq *king-book* string)))

(defvar *chapter-first-1-p*)
(defvar *chapter-first-2-p*)

(defvar *chapter-king-title*)

(defun convert-wikisource-to-multicol-latex (king-url url external-format &optional stream (titlep t))
  #+sbcl (declare (ignore external-format))
  (let* ((html #+allegro (net.aserve.client:do-http-request url :external-format external-format)
               #+sbcl (drakma:http-request url))
         (lhtml (net.html.parser:parse-html html))
         (king-html #+allegro (net.aserve.client:do-http-request king-url)
               #+sbcl (drakma:http-request king-url))
         (king-lhtml (net.html.parser:parse-html king-html)))
    (let* ((title (meiji-title lhtml))
           (king-title (king-james-title king-lhtml))
           (book (book-of (second title)))
           (king-book (king-book-of king-title)))
      (when titlep
        (format stream +bible-multicol-header+ book king-book book))
      (format stream "\\chapter*{}~%\\thispagestyle{empty}~%\\vskip -14.0zw~%")
      (format stream "%% \\addcontentsline{toc}{chapter}{~A}~%" king-book)
      (loop for chapter in (rest (meiji-mokuji lhtml))
          for i from 1
          for chapter-url = (chapter-url chapter)
          for chapter-king-url = (chapter-king-url king-url i)
          for chapter-title = (chapter-title-number i)
          for chapter-html = #+allegro (net.aserve.client:do-http-request chapter-url :external-format external-format)
           #+sbcl (drakma:http-request chapter-url)
          for chapter-king-html = #+allegro (net.aserve.client:do-http-request chapter-king-url)
           #+sbcl (drakma:http-request chapter-king-url)
          for chapter-lhtml = (net.html.parser:parse-html chapter-html)
          for chapter-king-lhtml = (net.html.parser:parse-html chapter-king-html)
          for chapter-honbun = (chapter-honbun chapter-lhtml)
          for chapter-king-honbun = (king-james-chapter-verses chapter-king-lhtml)
          for *chapter-string* = ""
          for *chapter-king-title* = (king-james-title chapter-king-lhtml)
          for chapter-king-book = (king-book-of *chapter-king-title*)
          for *chapter-first-1-p* = t
          for *chapter-first-2-p* = t 
          do (format stream "\\vskip 1.0zw~%\\section*{\\mc ~A ~A~A}"
                     chapter-king-book *book* chapter-title)
             (let ((mark (format nil "~A ~A~A"
                                 chapter-king-book *book* chapter-title)))
               (format t "{~A}~%" mark)
               (format stream "~%\\markboth{~A}{~A}~%" (reverse mark) mark))
             (parse-multicol-chapter-honbun chapter-king-honbun chapter-honbun stream)
          finally (format stream "\\vspace*{\\fill}~%\\noindent~%")))))

(defun convert-wikisource-no-chapter-to-multicol-latex (king-url url external-format &optional stream (titlep t))
  #+sbcl (declare (ignore external-format))
  (let* ((html #+allegro (net.aserve.client:do-http-request url :external-format external-format)
               #+sbcl (drakma:http-request url))
         (lhtml (net.html.parser:parse-html html))
         (king-html #+allegro (net.aserve.client:do-http-request king-url)
               #+sbcl (drakma:http-request king-url))
         (king-lhtml (net.html.parser:parse-html king-html)))
    (let* ((title (meiji-title lhtml))
           (king-title (king-james-title king-lhtml))
           (book (book-of (second title)))
           (king-book (king-book-of king-title)))
      (when titlep
        (format stream +bible-multicol-header+ book king-book book))
      (format stream "\\chapter*{}~%\\thispagestyle{empty}~%\\vskip -14.0zw~%")
      (format stream "%% \\addcontentsline{toc}{chapter}{~A}~%" king-book)
      (let* ((*chapter-string* "")
             (chapter-title nil)
             (chapter-honbun (chapter-honbun lhtml))
             (chapter-king-url (chapter-king-url king-url 1))
             (chapter-king-html #+allegro (net.aserve.client:do-http-request chapter-king-url)
                                #+sbcl (drakma:http-request chapter-king-url))
             (chapter-king-lhtml (net.html.parser:parse-html chapter-king-html))
             (chapter-king-honbun (king-james-chapter-verses chapter-king-lhtml))
             (*chapter-king-title* (king-james-title chapter-king-lhtml))
             (chapter-king-book (king-book-of *chapter-king-title*))
             (*chapter-first-1-p* t)
             (*chapter-first-2-p* t))
        (format stream "\\section*{\\mc ~A ~A~@[~A~]}"
                chapter-king-book *book* chapter-title)
        (let ((mark (format nil "~A ~A~@[~A~]"
                            chapter-king-book *book* chapter-title)))
          (format t "{~A}~%" mark)
          (format stream "~%\\markboth{~A}{~A}~%" (reverse mark) mark))
        (parse-multicol-chapter-honbun chapter-king-honbun chapter-honbun stream nil)
        (format stream "\\vspace*{\\fill}~%\\noindent~%")))))

(defparameter +dropping-rmfamily-command+
    "\\dropping{2}{\\rmfamily{} ~A}~A")

(defparameter +dropping-bfseries-command+
    "\\dropping{2}{\\bfseries{} ~A}~A")

(defun dropping-verse (verse)
  "Add dropping"
  (flet ((upcase-first (string)         ; 最初の單語を大文字にする
           (loop for i from 0 below (length string)
               for char = (char string i)
               until (char= #\space char) do
                 (setf (char string i) (char-upcase char))
               finally (return string))))
    (let ((pos1 (search "\\text" verse :end2 5)))
      (if pos1                          ; \text{....} or \textit{...}...
          (let* ((pos2 (position #\{ verse :start pos1))
                 (pos3 (position #\} verse :start pos2)))
            (if (and pos2 pos3 (>= (- pos3 pos2) 2))
                (format nil +dropping-bfseries-command+
                        (subseq verse (+ 1 pos2) (+ 2 pos2)) ; The First Char
                        (format nil "~A{~A}~A" ; "\\textbf{...}"
                                (subseq verse 0 pos2)
                                (upcase-first (subseq verse
                                                      (+ 2 pos2) pos3))
                                (subseq verse (1+ pos3))))
              (error "Something Wrong with ~S" verse)))
        (format nil +dropping-rmfamily-command+
                (subseq verse 0 1) (upcase-first (subseq verse 1)))))))

(defun parse-multicol-chapter-honbun (king-honbun honbun &optional stream (sectionp t))
  (loop with rest-honbun = honbun
      with again = nil
      for nth from 1
      for king-verse-sexp in king-honbun
      while (listp king-verse-sexp) do
        (let ((king-verse (king-james-verse-with-tag (king-james-paragraph king-verse-sexp))))
          (if *chapter-first-1-p*
              (progn
                (assert (= 1 nth))
                (setq *chapter-string* king-verse)
                (format stream "\\begin{multicolpar}{2}~%~A" (dropping-verse king-verse))
                (setq *chapter-first-1-p* nil))
            (progn
              (assert (not (= 1 nth)))
              (when (eql nth 2)
                (let* ((end2 (position #\space *chapter-king-title* :from-end t))
                       (end1 (position #\space *chapter-king-title* :from-end t :end end2))
                       (title (if sectionp
                                  (format nil "CH.~A" (subseq *chapter-king-title* end1 end2))
                                ""))
                       (string-max (min (length *chapter-string*) 74))
                       (string-end-1 (position #\. *chapter-string* :end string-max :from-end t))
                       (string-end-2 (or string-end-1 
                                         (position #\, *chapter-string* :end string-max :from-end t)
                                         0))
                       (string-end-3 (or string-end-1
                                         (position #\space *chapter-string* :end string-max :from-end t)))
                       (string (subseq *chapter-string* 0 (max string-end-2 string-end-3)))
                       (count (- (count #\{ string) (count #\} string))) ; { と } を合はせる
                       (string1 (format nil "~A~[~;}~:;}}~]" string count)))
                  (format stream "%% \\addcontentsline{toc}{section}{~A {\\tiny ~A}}~%" title string1)))
              (if (ratiop again)   ; "43-44"
                  (format stream " \\break ~%~A. ~A" nth king-verse)
                (format stream "\\begin{multicolpar}{2}~%~A. ~A" nth king-verse))))
          (setq rest-honbun
            (loop initially (setq again nil)
                for onelm on rest-honbun
                for elm = (first onelm)
                if (stringp elm) do
                  (setq again (parse-honbun-multicol-string elm nth stream))
                  (unless (null again)  ; do it again
                    (return rest-honbun))
                else if (ruby-p elm) do
                  (parse-honbun-multicol-ruby elm (get-next-elm onelm) stream)
                else if (eql elm :br) return
                  (loop for onnext on (rest onelm)
                      for next = (first onnext)
                      for string = (and (stringp next)
                                        (string-trim '(#\return #\linefeed) next))
                      if (eql next :br) do
                        ;; :BRが二つあり、章の半ばであれば「。」を打つ
                        (unless (null (nth (1+ nth) king-honbun))
                          (format stream "。"))
                        (continue)
                      else if (= 0 (length string)) do
                        (continue)
                      else do (loop-finish)
                      finally (return onnext))))
          (unless (ratiop again)   ; "43-44"
            (format stream "~%\\end{multicolpar}~%~%")))))

(defparameter dropping-huge-command
    "~%~%\\dropping{2}{\\rmfamily{} \\HUGE ~A}~A")

(defmacro execute-exception (title index sentence)
  `(when (string= ,title *king-book*)
     (cond ((= nth ,index)
            (format stream ,sentence)
            (return-from parse-honbun-multicol-string :exception))
           ((> nth ,index)
            (setq nth (1- nth))))))

(defun parse-honbun-multicol-string (string nth &optional stream)
  (execute-exception "LUKE CHAPTER 17 KJV" 36 "~%~%\\noindent なし {\\footnotesize （二人の男畑に居んに一人は執れ一人は遺さるべし）}")
                                        ; 路加傳17章KJV36は明治譯37にあたる
                                        ; 大正譯で36は「無し」なので此れに倣ふ
  (execute-exception "ACTS CHAPTER 15 KJV" 34 "~%~%\\noindent なし {\\footnotesize （シラスはそこに留るをよしとせり）}")
                                        ; 使徒行傳15章KJV35は明治譯34にあたる
                                        ; 大正譯で34は「無し」なので此れに倣ふ
  (execute-exception "ACTS CHAPTER 20 KJV" 37 "~%~%\\noindent 　")
                                        ; 使徒行傳20章KJV37,38は明治譯37にあたる
                                        ; 37を「空」として對應する
  (setq string (string-trim '(#\return #\linefeed #\space) string))
  (labels ((dropping-string (string)
             (when (> (length string) 0)
               (prog1
                   (format stream dropping-huge-command (subseq string 0 1) (subseq string 1))
                 (setq *chapter-first-2-p* nil))))
           (print-honbun-subseq (string i)
             #+ignore
             (setq *chapter-string*
                (format nil "~A~A" *chapter-string* (subseq string (1+ i))))
             (if *chapter-first-2-p*
                 (dropping-string (subseq string (1+ i)))
               (format stream "~%~%\\noindent ~A" (subseq string (1+ i)))))
           (print-num-honbun (string i nth num)
             (let ((meiji-num (format nil "~{~A~}" num))
                   (kjv-num (format nil "~A" nth)))
               (if (string= meiji-num kjv-num) ; "11" vs 11
                   (print-honbun-subseq string i)
                 (let* ((pos1 (position #\- meiji-num))
                        (pos2 (and pos1 (position #\- meiji-num :start (1+ pos1)))))
                   (cond ((and pos1 pos2) ; "21-22-23"
                          (cond ((string= meiji-num kjv-num :end1 pos1)
                                 ;; (format stream "~A" meiji-num)
                                 (format t "~A~%" meiji-num)
                                 '1/3)  ; "21-22-23" vs 21 のときは何も出力せずに一旦返し、
                                ((string= meiji-num kjv-num :start1 (1+ pos1) :end1 pos2)
                                 ;; (format stream "~A" meiji-num)
                                 (format t "~A~%" meiji-num)
                                 '2/3)  ; "21-22-23" vs 22 のときは何も出力せずに一旦返し、
                                ((string= meiji-num kjv-num :start1 (1+ pos2))
                                 (print-honbun-subseq string i)) ; "21-22-23" vs 23 のときに對譯を出力する
                                (t (error "Wrong Verse num. (~A and ~A)" meiji-num kjv-num))))
                         (pos1          ; "43-44"
                          (if (string= meiji-num kjv-num :end1 pos1)
                              (progn
                                ;; (format stream "~A" meiji-num)
                                (format t "~A~%" meiji-num)
                                '1/2)   ; "43-44" vs 43 のときは何も出力せずに一旦返し、
                            (if (string= meiji-num kjv-num :start1 (1+ pos1))
                                (print-honbun-subseq string i) ; 次の "43-44" vs 44 のときに對譯を出力する
                              (error "Wrong Verse num. (~A and ~A)" meiji-num kjv-num))))
                         (t (error "Contradicted Verse num. (~A and ~A)." meiji-num kjv-num)))))))
           (parse-honbun-string-aux (string)
             (cond ((= 0 (length string)) nil)
                   ((digit-char-p (char string 0))
                    (loop for char across string
                        for i from 0
                        while (or (digit-char-p char) (char= #\- char)) collect
                          char into num
                        finally (return (print-num-honbun string i nth num))))
                   (t #+ignore 
                      (setq *chapter-string*
                        (format nil "~A~A" *chapter-string* string))
                      (if *chapter-first-2-p*
                          (dropping-string string)
                        (format stream "~A" string))))))
    (if (= 0 (length string))
        nil
      (if (eql (position #\※ string) 0) ; 例: "※1 明治14年版では…"
          (error "Should Not Happen.")
        (multiple-value-bind (pos1 pos2)
            (comment-string-p string)
          (if (and pos1 pos2)           ; 例: "テヲピロ(※1)よ"
              (let ((string1 (subseq string 0 pos1))
                    (string2 (subseq string (1+ pos2))))
                (let ((ret1 (parse-honbun-string-aux string1))
                      (ret2 (parse-honbun-string-aux string2)))
                  ;; string1=="25-26"のやうな場合nilが返ると拙い
                  (or ret1 ret2)))
            (parse-honbun-string-aux string)))))))

(defun parse-honbun-multicol-ruby (ruby next &optional stream)
  (let ((kanji (ruby-kanji ruby))
        (kana (ruby-kana ruby)))
    #+ignore
    (setq *chapter-string*
        (format nil "~A~A" *chapter-string* kanji))
    (if *chapter-first-2-p*
        (progn                          ; 先頭文字強調
          (if (> (length kanji) 1)
              (let ((first (subseq kanji 0 1))
                    (rest (subseq kanji 1)))
                (format stream dropping-huge-command
                        (format nil "\\kanakt{~A}{~A}" first kana) rest))
            (format stream dropping-huge-command
                    (format nil "\\kanakt{~A}{~A}" kanji kana) ""))
          (setq *chapter-first-2-p* nil))
      ;; ルビ振り
      (make-kanji-kana-form kanji kana next stream))))


(defun convert-matthew-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Matthew-Chapter-1/")
                                               (url-meiji "https://ja.wikisource.org/wiki/%E9%A6%AC%E5%A4%AA%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8%28%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3%29")
                                               (external-format +crlf-base-utf8+)
                                               (stream t)
                                               (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-mark-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Mark-Chapter-1/")
                                            (url-meiji "https://ja.wikisource.org/wiki/%E9%A6%AC%E5%8F%AF%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8%28%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3%29")
                                            (external-format +crlf-base-utf8+)
                                            (stream t)
                                            (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-luke-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Luke-Chapter-1/")
                                            (url-meiji "https://ja.wikisource.org/wiki/%E8%B7%AF%E5%8A%A0%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8%28%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3%29")
                                            (external-format +crlf-base-utf8+)
                                            (stream t)
                                            (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-john-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/John-Chapter-1/")
                                            (url-meiji "https://ja.wikisource.org/wiki/%E7%B4%84%E7%BF%B0%E5%82%B3%E7%A6%8F%E9%9F%B3%E6%9B%B8%28%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3%29")
                                            (external-format +crlf-base-utf8+)
                                            (stream t)
                                            (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-acts-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Acts-Chapter-1/")
                                            (url-meiji "https://ja.wikisource.org/wiki/%E4%BD%BF%E5%BE%92%E8%A1%8C%E5%82%B3(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                            (external-format +crlf-base-utf8+)
                                            (stream t)
                                            (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-romans-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Romans-Chapter-1/")
                                              (url-meiji "https://ja.wikisource.org/wiki/%E7%BE%85%E9%A6%AC%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                              (external-format +crlf-base-utf8+)
                                              (stream t)
                                              (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-i-corinthians-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/1-Corinthians-Chapter-1/")
                                                     (url-meiji "https://ja.wikisource.org/wiki/%E5%93%A5%E6%9E%97%E5%A4%9A%E5%89%8D%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                     (external-format +crlf-base-utf8+)
                                                     (stream t)
                                                     (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ii-corinthians-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/2-Corinthians-Chapter-1/")
                                                      (url-meiji "https://ja.wikisource.org/wiki/%E5%93%A5%E6%9E%97%E5%A4%9A%E5%BE%8C%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                      (external-format +crlf-base-utf8+)
                                                      (stream t)
                                                      (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-galatians-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Galatians-Chapter-1/")
                                                 (url-meiji "https://ja.wikisource.org/wiki/%E5%8A%A0%E6%8B%89%E5%A4%AA%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                 (external-format +crlf-base-utf8+)
                                                 (stream t)
                                                 (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ephesians-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Ephesians-Chapter-1/")
                                                 (url-meiji "https://ja.wikisource.org/wiki/%E4%BB%A5%E5%BC%97%E6%89%80%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                 (external-format +crlf-base-utf8+)
                                                 (stream t)
                                                 (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-philippians-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Philippians-Chapter-1/")
                                                   (url-meiji "https://ja.wikisource.org/wiki/%E8%85%93%E7%AB%8B%E6%AF%94%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                   (external-format +crlf-base-utf8+)
                                                   (stream t)
                                                   (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-colossians-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Colossians-Chapter-1/")
                                                  (url-meiji "https://ja.wikisource.org/wiki/%E5%93%A5%E7%BE%85%E8%A5%BF%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                  (external-format +crlf-base-utf8+)
                                                  (stream t)
                                                  (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-i-thessalonians-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/1-Thessalonians-Chapter-1/")
                                                       (url-meiji "https://ja.wikisource.org/wiki/%E5%B8%96%E6%92%92%E7%BE%85%E5%B0%BC%E8%BF%A6%E5%89%8D%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                       (external-format +crlf-base-utf8+)
                                                       (stream t)
                                                       (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ii-thessalonians-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/2-Thessalonians-Chapter-1/")
                                                        (url-meiji "https://ja.wikisource.org/wiki/%E5%B8%96%E6%92%92%E7%BE%85%E5%B0%BC%E8%BF%A6%E5%BE%8C%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                        (external-format +crlf-base-utf8+)
                                                        (stream t)
                                                        (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-i-timothy-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/1-Timothy-Chapter-1/")
                                                 (url-meiji "https://ja.wikisource.org/wiki/%E6%8F%90%E6%91%A9%E5%A4%AA%E5%89%8D%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                 (external-format +crlf-base-utf8+)
                                                 (stream t)
                                                 (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ii-timothy-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/2-Timothy-Chapter-1/")
                                                  (url-meiji "https://ja.wikisource.org/wiki/%E6%8F%90%E6%91%A9%E5%A4%AA%E5%BE%8C%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                  (external-format +crlf-base-utf8+)
                                                  (stream t)
                                                  (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-titus-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Titus-Chapter-1/")
                                             (url-meiji "https://ja.wikisource.org/wiki/%E6%8F%90%E5%A4%9A%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                             (external-format +crlf-base-utf8+)
                                             (stream t)
                                             (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-philemon-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Philemon-Chapter-1/")
                                                (url-meiji "https://ja.wikisource.org/wiki/%E8%85%93%E5%88%A9%E9%96%80%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                (external-format +crlf-base-utf8+)
                                                (stream t)
                                                (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-no-chapter-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-hebrews-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Hebrews-Chapter-1/")
                                               (url-meiji "https://ja.wikisource.org/wiki/%E5%B8%8C%E4%BC%AF%E4%BE%86%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                               (external-format +crlf-base-utf8+)
                                               (stream t)
                                               (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-james-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/James-Chapter-1/")
                                             (url-meiji "https://ja.wikisource.org/wiki/%E9%9B%85%E5%90%84%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                             (external-format +crlf-base-utf8+)
                                             (stream t)
                                             (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-i-peter-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/1-Peter-Chapter-1/")
                                               (url-meiji "https://ja.wikisource.org/wiki/%E5%BD%BC%E5%BE%97%E5%89%8D%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                               (external-format +crlf-base-utf8+)
                                               (stream t)
                                               (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ii-peter-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/2-Peter-Chapter-1/")
                                                (url-meiji "https://ja.wikisource.org/wiki/%E5%BD%BC%E5%BE%97%E5%BE%8C%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                (external-format +crlf-base-utf8+)
                                                (stream t)
                                                (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-i-john-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/1-John-Chapter-1/")
                                              (url-meiji "https://ja.wikisource.org/wiki/%E7%B4%84%E7%BF%B0%E7%AC%AC%E4%B8%80%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                              (external-format +crlf-base-utf8+)
                                              (stream t)
                                              (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-ii-john-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/2-John-Chapter-1/")
                                               (url-meiji "https://ja.wikisource.org/wiki/%E7%B4%84%E7%BF%B0%E7%AC%AC%E4%BA%8C%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                               (external-format +crlf-base-utf8+)
                                               (stream t)
                                               (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-no-chapter-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-iii-john-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/3-John-Chapter-1/")
                                                (url-meiji "https://ja.wikisource.org/wiki/%E7%B4%84%E7%BF%B0%E7%AC%AC%E4%B8%89%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                (external-format +crlf-base-utf8+)
                                                (stream t)
                                                (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-no-chapter-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-jude-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Jude-Chapter-1/")
                                            (url-meiji "https://ja.wikisource.org/wiki/%E7%8C%B6%E5%A4%AA%E6%9B%B8(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                            (external-format +crlf-base-utf8+)
                                            (stream t)
                                            (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-no-chapter-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-revelation-to-multicol-latex (&key (url-king "https://www.kingjamesbibleonline.org/Revelation-Chapter-1/")
                                                  (url-meiji "https://ja.wikisource.org/wiki/%E7%B4%84%E7%BF%B0%E9%BB%98%E7%A4%BA%E9%8C%B2(%E6%98%8E%E6%B2%BB%E5%85%83%E8%A8%B3)")
                                                  (external-format +crlf-base-utf8+)
                                                  (stream t)
                                                  (titlep t))
  (let (*book* *king-book*)
    (convert-wikisource-to-multicol-latex url-king url-meiji external-format stream titlep)
    (end-title *book* stream)
    (terpri stream)))

(defun convert-new-testament-to-multicol-latex (&key (stream t))
  "新約全書"
  (format stream +bible-multicol-header+ nil nil nil)
  (convert-matthew-to-multicol-latex :stream stream :titlep nil)
  (convert-mark-to-multicol-latex :stream stream :titlep nil)
  (convert-luke-to-multicol-latex :stream stream :titlep nil)
  (convert-john-to-multicol-latex :stream stream :titlep nil)
  (convert-acts-to-multicol-latex :stream stream :titlep nil)
  (convert-romans-to-multicol-latex :stream stream :titlep nil)
  (convert-i-corinthians-to-multicol-latex :stream stream :titlep nil)
  (convert-ii-corinthians-to-multicol-latex :stream stream :titlep nil)
  (convert-galatians-to-multicol-latex :stream stream :titlep nil)
  (convert-ephesians-to-multicol-latex :stream stream :titlep nil)
  (convert-philippians-to-multicol-latex :stream stream :titlep nil)
  (convert-colossians-to-multicol-latex :stream stream :titlep nil)
  (convert-i-thessalonians-to-multicol-latex :stream stream :titlep nil)
  (convert-ii-thessalonians-to-multicol-latex :stream stream :titlep nil)
  (convert-i-timothy-to-multicol-latex :stream stream :titlep nil)
  (convert-ii-timothy-to-multicol-latex :stream stream :titlep nil)
  (convert-titus-to-multicol-latex :stream stream :titlep nil)
  (convert-philemon-to-multicol-latex :stream stream :titlep nil)
  (convert-hebrews-to-multicol-latex :stream stream :titlep nil)
  (convert-james-to-multicol-latex :stream stream :titlep nil)
  (convert-i-peter-to-multicol-latex :stream stream :titlep nil)
  (convert-ii-peter-to-multicol-latex :stream stream :titlep nil)
  (convert-i-john-to-multicol-latex :stream stream :titlep nil)
  (convert-ii-john-to-multicol-latex :stream stream :titlep nil)
  (convert-iii-john-to-multicol-latex :stream stream :titlep nil)
  (convert-jude-to-multicol-latex :stream stream :titlep nil)
  (convert-revelation-to-multicol-latex :stream stream :titlep nil))

#||

CL-USER(164): (drakma:http-request "https://www.kingjamesbibleonline.org/Matthew-Chapter-1/")
CL-USER(165): (net.html.parser:parse-html *)
CL-USER(165): (loop for i from 1
                  for verse in (bible::king-james-chapter-verses *)
                  do (print (list i (bible::king-james-verse (bible::king-james-paragraph verse)))))
(1
 "The book of the generation of Jesus Christ, the son of David, the son of Abraham.") 
(2
 "Abraham begat Isaac; and Isaac begat Jacob; and Jacob begat Judas and his brethren;") 
(3
 "And Judas begat Phares and Zara of Thamar; and Phares begat Esrom; and Esrom begat Aram;") 
(4
 "And Aram begat Aminadab; and Aminadab begat Naasson; and Naasson begat Salmon;") 
(5
 "And Salmon begat Booz of Rachab; and Booz begat Obed of Ruth; and Obed begat Jesse;") 
(6
 "And Jesse begat David the king; and David the king begat Solomon of her ") 
(7
 "And Solomon begat Roboam; and Roboam begat Abia; and Abia begat Asa;") 
(8
 "And Asa begat Josaphat; and Josaphat begat Joram; and Joram begat Ozias;") 
(9
 "And Ozias begat Joatham; and Joatham begat Achaz; and Achaz begat Ezekias;") 
(10
 "And Ezekias begat Manasses; and Manasses begat Amon; and Amon begat Josias;") 
(11
 "And Josias begat Jechonias and his brethren, about the time they were carried away to Babylon:") 
(12
 "And after they were brought to Babylon, Jechonias begat Salathiel; and Salathiel begat Zorobabel;") 
(13
 "And Zorobabel begat Abiud; and Abiud begat Eliakim; and Eliakim begat Azor;") 
(14
 "And Azor begat Sadoc; and Sadoc begat Achim; and Achim begat Eliud;") 
(15
 "And Eliud begat Eleazar; and Eleazar begat Matthan; and Matthan begat Jacob;") 
(16
 "And Jacob begat Joseph the husband of Mary, of whom was born Jesus, who is called Christ.") 
(17 "So all the generations from Abraham to David ") 
(18
 "Now the birth of Jesus Christ was on this wise: When as his mother Mary was espoused to Joseph, before they came together, she was found with child of the Holy Ghost.") 
(19 "Then Joseph her husband, being a just ") 
(20
 "But while he thought on these things, behold, the angel of the Lord appeared unto him in a dream, saying, Joseph, thou son of David, fear not to take unto thee Mary thy wife: for that which is conceived in her is of the Holy Ghost.") 
(21
 "And she shall bring forth a son, and thou shalt call his name JESUS: for he shall save his people from their sins.") 
(22
 "Now all this was done, that it might be fulfilled which was spoken of the Lord by the prophet, saying,") 
(23
 "Behold, a virgin shall be with child, and shall bring forth a son, and they shall call his name Emmanuel, which being interpreted is, God with us.") 
(24
 "Then Joseph being raised from sleep did as the angel of the Lord had bidden him, and took unto him his wife:") 
(25
 "And knew her not till she had brought forth her firstborn son: and he called his name JESUS.") 
NIL
||#
