;;; -*- mode:common-lisp; coding:utf-8 -*-

(in-package "BIBLE")

;;;;
;;;; 箴言
;;;;

(defvar *shingen-jukugo*)

(eval-when (:load-toplevel :compile-toplevel :execute)
  (load "shingen.jukugo"))

(defun collect-jukugo (input)
  "熟語を集める"
  (loop for line = (read-line input nil nil nil)
      while line append
        (cond ((match-regexp "第[0-9].*章" line)
               '())
              ((match-regexp "[0-9].:[0-9]." line)
               '())
              (t (collect-line-jukugo line)))))

(defun collect-line-jukugo (line)
  (loop with charbag = (make-array 0 :element-type 'character :adjustable t :fill-pointer t)
      for char across line
      if (or (<= (char-code #\あ) (char-code char) (char-code #\ん))
             (<= (char-code #\ア) (char-code char) (char-code #\ン))
             (<= 0 (char-code char) 255))
      append
        (if (> (fill-pointer charbag) 0)
            (prog1
                (list charbag)
              (setq charbag
                (make-array 0 :element-type 'character :adjustable t :fill-pointer t)))
          nil)
      else do
           (vector-push-extend char charbag)))

;;; (with-open-file (input "shingen.wiki" :external-format :iso-2022-jp)
;;;   (with-open-file (output "shingen.utf8" :direction :output :if-exists :supersede :external-format :utf-8)
;;;     (bible::hex-to-char input output)))
(defun hex-to-char (input output)
  "汝らのほろび&#x98ba;風の如くきたり → UTF文字變換"
  (loop for char = (read-char input nil nil nil)
      while char
      if (char= char #\&) do
        (let ((code (read input)))
          (write-char (code-char code) output))
      else if (char= char #\;) do
        'nil                            ; skip
      else do
           (write-char char output)))

;;;
(defun jukugo-length (jukugo)
  "交は[らりるれ]→3"
  (loop with count = 0
      with [] = nil
      for char across jukugo
      if (char= char #\[) do
        (setq [] t)
      else if (char= char #\]) do
        (setq [] nil)
        (incf count)
      else if (not []) do
        (incf count)
      end
      finally (return count)))

#||

(sort *shingen-jukugo* #'(lambda (x y) (> (jukugo-length x) (jukugo-length y))) :key #'car)

||#
