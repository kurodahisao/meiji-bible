%%% -*- mode: LaTeX; coding: utf-8 -*-
%%% -*- mode: LaTeX; coding: iso-2022-jp-2 -*-
\documentclass[a5j,10pt,openany,twocolumn]{tbook}% mentuke tombo myjsbook ,oneside,tbook,treport
\usepackage[deluxe,expert,multi]{otf}
%% \usepackage{atbegshi}
%% \AtBeginShipoutFirst{\special{pdf:tounicode 90ms-RKSJ-UCS2}}
%% \usepackage{makeidx}
%% \usepackage[dvipdfmx,bookmarks=true,bookmarksnumbered=true]{hyperref}
\usepackage[dvipdfmx]{bxglyphwiki}
\usepackage{kochu}
\usepackage{furikana}
\usepackage{furiknkt}
\usepackage{jdkintou}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{ipamjm}
\usepackage{pxjaschart}
\usepackage{uline--}
\pagestyle{bothstyle}
\setlength\intextsep{0.8zw}
\setlength\textfloatsep{0.8zw}
\kanjiskip0.2pt plus 0.4pt minus 0.4pt
\xkanjiskip\kanjiskip
\AtBeginDvi{\special{papersize=\the\paperwidth,\the\paperheight}}
%%% \setlength{\evensidemargin}{\oddsidemargin}
%% \newcommand{\pseudochapter}[1]{\chapter*{#1}}
%% \newcommand{\pseudosection}[1]{\section*{#1}}
%% \makeatletter
%% \def\section{\@startsection {section}{1}{\z@}{-0ex plus -0ex minus -.0ex}{0 ex plus .0ex}{\normalsize}}
%% \makeatother 
%% \makeindex
%% \title{耶蘇降生千八百九十六年 \\ {\Huge 新約全書} \\ 明治二十九年}
%% \author{大日本聖書舘 \\ 横濱二十六番}
%% \date{令和元年皐月 \\ \href{https://bitbucket.org/kurodahisao/meiji-bible}{\LaTeX 組版生成}}
%% \setcounter{tocdepth}{1}
%% \renewcommand{\contentsname}{新約全書目録}
%% \makeatletter
%% \renewcommand{\tableofcontents}{% TOCはいつでもTwoColumn
%%   \if@twocolumn\@restonecolfalse
%%   \else\@restonecoltrue\twocolumn\fi
%%   \chapter*{\contentsname
%%     \@mkboth{\contentsname}{\contentsname}%
%%   }\@starttoc{toc}%
%%   \if@restonecol\onecolumn\fi
%% }
%% \renewenvironment{theindex}
%%   {\if@twocolumn\@restonecolfalse\else\@restonecoltrue\fi
%%    \columnseprule\z@ \columnsep 35\p@
%%    \twocolumn%
%%    \@mkboth{\indexname}{\indexname}%
%%    \thispagestyle{jpl@in}\parindent\z@
%%    \parskip\z@ \@plus .3\p@\relax
%%    \let\item\@idxitem}
%%   {\if@restonecol\onecolumn\else\clearpage\fi}
%% \makeatother
\begin{document}
\topmargin-0.6in
\newcommand{\LRkanJI}[2]{%
  \tbaselineshift=0.3zw  % 0pt
        \parbox<y>{1zw}{%
            \scalebox{0.5}[1]{%
                \makebox[2zw]{#1\hspace{-0.1zw}#2}}}}
\def\katano#1{\linebreak[3]\kern0.5zw\raisebox{0.5zw}[0pt][0pt]{\tiny#1}\kern0.1zw}
\makeatletter
\def\kochuuname{}
\def\arabicchuucite#1{\rensuji{\chuucite{#1}}}
\def\@kochuulabel#1{\rensuji{#1}}
\makeatother
%% \maketitle
%% \tableofcontents
\chapter*{\Large \Kanakt{新,約,全,書}{しん,やく,ぜん,しよ}\Kanakt{使,徒}{し,と}ユダの\kana{書}{ふみ}}
%% \addcontentsline{toc}{chapter}{猶太書}

\markboth{書太猶}{猶太書}
\noindent\katano{一} \protect\nolinebreak  イエス・キリストの\kanakt[3]{僕}{しもべ}ユダ\kanakt{即}{すなは}ちヤコブの\kanakt[3]{兄弟}{きやうだい}\kana{書}{ふみ}を\kana{召}{めさ}れたる\kana{者}{もの}すなはち\kana{父}{ちち}なる\kana{神}{かみ}に\kana{愛}{あい}せられ\kana{且}{かつ}イエス・キリストの\kana{爲}{ため}に\kana{守}{まも}らるる\kanakt{衆人}{ひとびと}に\kana{贈}{おく}る
%% \refstepcounter{section}\addcontentsline{toc}{section}{ {\tiny  イエス・キリストの僕ユダ即ちヤコブの兄弟書を}}
\katano{二} \protect\nolinebreak  \kanakt{願}{ねがは}くは\kanakt{爾曹}{なんぢら}に\kanakt{慈悲}{あはれみ}と\kana{平康}{やすき}と\kanakt{仁愛}{いつくしみ}の\kana{増}{まさ}んことを


\indent
\katano{三} \protect\nolinebreak  \kana{愛}{あい}する\kana{者}{もの}よ\kana{我}{われ}\kanakt{心}{こころ}を\kanakt{熱}{あつく}して\kana{共}{とも}に\kanakt{與}{あづか}る\kanakt{所}{ところ}の\kanakt{救}{すくひ}の\kana{事}{こと}を\kanakt{爾曹}{なんぢら}に\kana{書}{かき}おくらんと\kanakt{思}{おもひ}ゐたりしが\kana{今}{いま}なんぢらに\kana{書}{ふみ}を\kana{贈}{おく}りて\kana{聖徒}{せいと}が\kana{一}{ひと}たび\kanakt{傳}{つたへ}られし\kanakt{信仰}{しんかう}の\kana{道}{みち}の\kana{爲}{ため}に\kanakt{力}{ちから}を\kana{盡}{つく}して\kanakt{戰}{たたか}はん\kana{事}{こと}を\kanakt{爾曹}{なんぢら}に\kanakt{勸}{すすめ}ざるを\kanakt{得}{え}ず
\katano{四} \protect\nolinebreak  そは\kana{神}{かみ}を\kanakt{敬}{うやま}はず\kana{我儕}{われら}の\kana{神}{かみ}の\kanakt{恩}{めぐみ}を\kana{易}{かへ}て\kanakt{色慾}{しきよく}を\kanakt{放縱}{ほしいまま}にするの\kana{縁}{えん}となし\kana{惟}{ただ}\kanakt{一}{ひとり}の\kana{主}{しゆ}なる\kana{神}{かみ}と\kana{我儕}{われら}の\kana{主}{しゆ}イエス・キリストを\kana{棄}{すつ}るもの\kana{數人}{すにん}\kanakt{潛}{ひそか}に\kanakt{教會}{けうくわい}に\kana{入}{いり}たればなり\kana{彼等}{かれら}が\kana{此}{この}\kana{審判}{さばき}を\kana{受}{うく}ることに\kanakt{定}{さだめ}られたる\kana{事}{こと}は\kanakt{昔}{むかし}より\kanakt{預}{あらか}じめ\kana{録}{しる}されたり
\katano{五} \protect\nolinebreak  なんぢら\kana{素}{もと}より\kana{知}{しれ}る\kana{事}{こと}なれど\kana{我}{われ}なほ\kanakt{爾曹}{なんぢら}に\kanakt[3]{憶}{おもひ}\kana{起}{いだ}させんとする\kana{事}{こと}は\kana{主}{しゆ}その\kana{民}{たみ}をエジプトの\kanakt{地}{ち}より\kanakt[3]{救}{すくひ}\kana{出}{いだ}ししのち\kana{信}{しん}ぜざる\kana{者}{もの}を\kana{滅}{ほろ}ぼし\kana{給}{たま}ひし\kana{事}{こと}と
\katano{六} \protect\nolinebreak  \kana{己}{おの}が\kana{本位}{ほんゐ}を\kana{守}{まも}らずして\kana{其}{その}\kana{住}{すめ}る\kanakt{所}{ところ}を\kana{離}{はな}れたる\kanakt{天使}{てんのつかひ}を\kanakt{限}{かぎり}なく\kanakt{繫}{つなぎ}て\kanakt{大}{おほい}なる\kanakt{日}{ひ}の\kana{審判}{さばき}まで\kanakt{幽暗}{くらやみ}の\kana{中}{なか}に\kana{守}{まも}り\kana{置}{おき}たまひし\kana{事}{こと}と
\katano{七} \protect\nolinebreak  ソドム、ゴモラ\kana{及}{およ}び\kana{其}{その}\kana{比隣}{となり}の\kana{邑}{まち}かれらと\kanakt{同}{おなじ}く\kanakt{姦淫}{かんいん}をなし\kana{且}{かつ}\kanakt{男色}{なんしよく}を\kanakt{行}{おこな}ふにより\kanakt{限}{かぎり}なく\kanakt{火}{ひ}の\kana{罰}{ばつ}を\kana{受}{うけ}て\kana{鑑戒}{かがみ}に\kana{立}{たて}られし\kana{事}{こと}となり
\katano{八} \protect\nolinebreak  この\kana{夢}{ゆめ}みる\kana{者}{もの}も\kana{亦}{また}\kanakt{肉體}{にくたい}を\kana{汚}{けが}し\kana{主}{しゆ}たる\kana{者}{もの}を\kana{藐忽}{かろん}じ\kanakt[3]{尊}{たふとき}\kana{者}{もの}を\kana{謗}{そし}れり
\katano{九} \protect\nolinebreak  それ\kanakt{天使}{てんのつかひ}の\kana{長}{をさ}ミカエル\kana{惡魔}{あくま}とモーセの\kanakt{屍}{しかばね}を\kanakt{爭}{あらそ}ひ\kana{論}{ろん}ぜしとき\kana{彼}{かれ}なほ\kana{之}{これ}を\kana{謗}{そし}りて\kanakt{訴}{うつた}へざりき\kana{惟}{ただ}\kana{主}{しゆ}なんぢを\kana{責}{せむ}べしと\kana{曰}{いへ}り
\katano{一〇} \protect\nolinebreak  \kana{然}{しか}るに\kana{彼等}{かれら}は\kana{知}{しら}ざる\kanakt{所}{ところ}の\kana{事}{こと}を\kana{謗}{そし}れり\kana{其}{その}\kanakt{本性}{うまれつき}しる\kanakt{所}{ところ}は\kanakt[3]{無知}{わきまへなき}\kanakt{獸}{けもの}の\kana{知}{しる}ところと\kana{同}{おな}じ\kana{彼等}{かれら}は\kana{之}{これ}を\kanakt{以}{も}て\kanakt{己}{おのれ}を\kanakt{亡}{ほろぼ}せり
\katano{一一} \protect\nolinebreak  \kanakt{禍}{わざはひ}なる\kana{哉}{かな}\kana{彼等}{かれら}はカインの\kana{途}{みち}にゆき\kanakt{利}{り}の\kana{爲}{ため}にバラムの\kanakt{迷謬}{あやまり}に\kana{馳}{はせ}またコラの\kanakt{逆}{さから}ひし\kanakt{如}{ごとく}して\kana{亡}{ほろ}びたり
\katano{一二} \protect\nolinebreak  \kana{彼等}{かれら}は\kanakt{爾曹}{なんぢら}の\kana{愛}{あい}の\kanakt{筵席}{ふるまひ}の\kana{磐}{いは}なり\kanakt{憚}{はばか}る\kanakt{所}{ところ}なく\kana{同}{とも}に\kana{其}{その}\kanakt{筵席}{ふるまひ}に\kanakt{與}{あづか}りて\kanakt{自己}{みづから}を\kanakt{養}{やしな}へり\kana{彼等}{かれら}は\kana{風}{かぜ}に\kana{逐}{おは}るる\kana{雨}{あめ}なき\kana{雲}{くも}\kana{枯}{かれ}て\kana{再}{また}かれ\kanakt{根}{ね}を\kana{拔}{ぬか}るる\kanakt{果}{み}のなき\kana{秋}{あき}の\kanakt{樹}{き}
\katano{一三} \protect\nolinebreak  その\kanakt{穢}{けがれ}を\kana{湧}{わき}\kana{出}{いだ}す\kana{海}{うみ}の\kanakt[3]{猛}{あらき}\kana{浪}{なみ}\kana{道}{みち}をはなれたる\kana{星}{ほし}なり\kana{之}{これ}が\kana{爲}{ため}に\kanakt{黒暗}{くらやみ}を\kanakt{限}{かぎり}なく\kana{留}{とめ}\kana{置}{おか}れたり
\katano{一四} \protect\nolinebreak  アダムより\kana{七}{しち}\kana{代}{だい}に\kana{當}{あた}れるエノク\kana{此}{この}\kanakt{輩}{ともがら}の\kana{事}{こと}を\kana{預言}{よげん}して\kana{曰}{いひ}けるは\kanakt{視}{み}よ\kana{主}{しゆ}\kana{其}{その}\kanakt[3]{聖}{きよき}\kanakt{萬軍}{ばんぐん}と\kana{偕}{とも}に\kana{來}{きた}りて
\katano{一五} \protect\nolinebreak  \kanakt[3]{衆}{すべての}\kana{人}{ひと}を\kana{鞫}{さば}き\kana{凡}{すべ}て\kana{神}{かみ}を\kanakt{敬}{うやま}はざる\kana{者}{もの}の\kana{神}{かみ}を\kanakt{敬}{うやま}はずして\kanakt{行}{おこな}ひし\kanakt[3]{惡}{あしき}\kana{行}{わざ}と\kana{神}{かみ}を\kanakt{敬}{うやま}はざる\kanakt{罪人}{つみびと}の\kana{主}{しゆ}に\kanakt{逆}{さから}ひて\kana{語}{かた}れる\kanakt{諸}{すべて}の\kanakt[3]{惡}{あしき}\kanakt{言}{ことば}を\kana{責}{せめ}\kana{給}{たま}ふべしと
\katano{一六} \protect\nolinebreak  \kana{此}{この}\kanakt{輩}{ともがら}は\kanakt{怨言}{つぶやく}もの\kana{足}{たる}ことを\kana{知}{しら}ざる\kana{者}{もの}おのれの\kana{慾}{よく}に\kanakt{從}{したが}ひて\kana{行}{ある}き\kana{其}{その}\kana{口}{くち}は\kana{誇}{ほこ}ることを\kana{語}{かた}り\kanakt{利}{り}の\kana{爲}{ため}に\kana{人}{ひと}に\kanakt{諂}{へつら}ふ\kana{者}{もの}なり
\katano{一七} \protect\nolinebreak  \kana{愛}{あい}する\kana{者}{もの}よ\kanakt{爾曹}{なんぢら}わが\kana{主}{しゆ}イエス・キリストの\kanakt{使徒}{しと}\kana{等}{たち}の\kana{曩}{さき}に\kana{語}{かた}りし\kana{言}{こと}を\kanakt[3]{憶}{おもひ}\kana{起}{いだ}すべし
\katano{一八} \protect\nolinebreak  \kanakt{即}{すなは}ち\kanakt{爾曹}{なんぢら}に\kanakt{語}{かたり}ていふ\kanakt{末期}{すゑのとき}に\kanakt{戲謔}{あざける}\kana{者}{もの}おこり\kana{己}{おの}が\kanakt{横逆}{よこしま}なる\kana{慾}{よく}に\kanakt{從}{したが}ひて\kanakt{行}{あゆま}んと
\katano{一九} \protect\nolinebreak  \kana{彼等}{かれら}は\kanakt{自}{みづか}ら\kana{區別}{わかち}をなす\kana{者}{もの}また\kana{肉}{にく}に\kana{屬}{つけ}る\kana{者}{もの}にして\kanakt{靈}{みたま}のなき\kana{者}{もの}なり
\katano{二〇} \protect\nolinebreak  \kana{愛}{あい}する\kana{者}{もの}よ\kanakt{爾曹}{なんぢら}その\kana{徳}{とく}を\kana{至}{いと}\kana{潔}{きよ}き\kanakt{信仰}{しんかう}の\kana{上}{うへ}に\kanakt{建}{た}て\kanakt{聖靈}{せいれい}に\kana{感}{かん}じて\kana{祈}{いの}り
\katano{二一} \protect\nolinebreak  \kanakt{自己}{みづから}を\kana{守}{まも}りて\kana{神}{かみ}の\kana{愛}{あい}の\kana{中}{うち}に\kana{居}{をり}われらの\kana{主}{しゆ}イエス・キリストの\kanakt[3]{永}{かぎりなき}\kanakt{生}{いのち}を\kana{賜}{たま}ふ\kana{其}{その}\kanakt{矜恤}{あはれみ}を\kana{待}{まつ}べし
\katano{二二} \protect\nolinebreak  \kana{彼等}{かれら}のうち\kana{或}{ある}\kana{者}{もの}をば\kana{論}{ろん}じて\kana{口}{くち}を\kanakt{噤}{つぐま}しめ
\katano{二三} \protect\nolinebreak  \kana{或}{ある}\kana{者}{もの}をば\kanakt{火}{ひ}より\kana{取}{とり}\kana{出}{いだ}して\kana{救}{すく}ひ\kana{或}{ある}\kana{者}{もの}をば\kana{畏懼}{おそれ}を\kanakt{以}{も}て\kanakt{憐}{あはれ}むべし\kana{其}{その}\kana{惡}{あく}は\kana{肉}{にく}の\kana{慾}{よく}に\kana{染}{そみ}たる\kanakt{衣}{ころも}までも\kana{惡}{にく}むことをせよ


\indent
\katano{二四、二五} \protect\nolinebreak  \kana{我儕}{われら}の\kanakt[3]{救}{すくひ}\kana{主}{ぬし}なる\kana{獨一}{ひとり}の\kana{神}{かみ}すなはち\kanakt{爾曹}{なんぢら}を\kanakt{躓}{つまづ}かせじと\kana{保}{まも}り\kanakt{爾曹}{なんぢら}をして\kana{汚}{しみ}なく\kanakt{歡}{よろこ}びて\kana{其}{その}\kanakt{榮光}{えいくわう}の\kana{前}{まへ}に\kana{立}{たつ}ことを\kanakt{得}{え}しむる\kana{者}{もの}は\kanakt{世}{よ}の\kanakt{始}{はじめ}の\kana{前}{まへ}より\kana{今}{いま}また\kana{後}{のち}も\kanakt{世々}{よよ}\kanakt{永遠}{かぎりなく}われらの\kana{主}{しゆ}イエス・キリストに\kana{由}{より}て\kanakt{榮}{さかえ}と\kanakt{威光}{いくわう}と\kana{大能}{ちから}と\kana{權}{けん}を\kana{有}{たも}ち\kana{給}{たま}ふなりアメン


\vspace*{\fill}
\noindent
\Kanakt{新,約,全,書}{しん,やく,ぜん,しよ}\Kanakt{猶,太}{ゆ,だ}\kana[3]{書}{のふみ}\kana[3]{終}{をはり}
