;;; -*- mode:emacs-lisp; lexical-binding:t; coding:utf-8 -*-

(require 'cl)

;;; 手順:
;;; Open shingen.wiki
;;; M-x hex-to-char
;;; M-x set-buffer-file-coding-system utf-8
;;; Save as shingen.utf8
;;; M-x furikana-shingen-jukugo
;;; Save as shingen.furikana
;;; M-x kanakt-shingen-jukugo
;;; Save as shingen.kanakt
;;; M-x put-line-on-katakana
;;; Save as shingen.kanakt
;;; M-x latex-shingen-section
;;; M-x latex-shingen-subsection
;;; M-x latex-shingen-white-space
;;; ....
;;; M-x utf-to-jis    ; 正字體變換 (引數 C-u をつけるとIPAFont利用)
;;; ....
;;; Open shingen.utf8
;;; M-x char-to-hex
;;; M-x set-buffer-file-coding-system iso-2022-jp
;;; Save as shingen.wiki

(require :utf-to-jis)

(defun shingen-wiki-to-latex (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (shingen-wiki-to-furikana start end)
  (shingen-furikana-to-latex nil))

(defun shingen-wiki-to-furikana (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (load-file "shingen.jukugo")
  (hex-to-char)
  (set-buffer-file-coding-system 'utf-8)
  (furikana-shingen-jukugo 'shingen *shingen-jukugo* start end)
  (goto-char (point-min)))

(defun shingen-furikana-to-latex (ipa)
  (interactive "P")
  (convert-shingen-subsection)
  (kanakt-shingen-jukugo)
  (latex-shingen-section)
  (latex-shingen-subsection)
  (latex-shingen-white-space)
  (utf-to-jis-2 ipa))

(defun convert-shingen-subsection ()
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "==== \\([0-9]+\\):\\([0-9]+\\) \\([0-9]+\\):\\([0-9]+\\) ====" nil t)
    (replace-match " \\1:\\2 \\3:\\4" nil nil))
  (goto-char (point-min))
  (while (re-search-forward "==== \\([0-9]+\\):\\([0-9]+\\) ====" nil t)
    (replace-match " \\1:\\2 " nil nil)))

(defun read-file (filename)
  (save-excursion
    (let ((current (current-buffer))
          (new (find-file filename)))
      (switch-to-buffer new)
      ;; (mark-whole-buffer)
      (let* ((contents (buffer-substring (point-min) (point-max)))
             (value (ignore-errors (read-from-string contents))))
        (kill-buffer new)
        (switch-to-buffer current)
        (car value)))))

(defun greater-jukugo (x y)
  (> (count-shingen-jukugo x)
     (count-shingen-jukugo y)))

(defun sort-jisho (jisho &optional copyp)
  (when copyp
    (let* ((cjisho (copy-list jisho))
           (rjisho (cl-remove-duplicates cjisho :test #'equal :key #'car :from-end t)))
      (setq jisho rjisho)))
  (sort jisho #'greater-jukugo))

(defvar *jukugo* nil)

(defun furikana-shingen-jukugo (name jisho sstart send)
  (interactive "P")
  (cl-loop initially (goto-char (point-min))
        for section from sstart to send
        for startexp = (format "[ |]%d[:|][0-9]+" section)
        for endexp = (format "[ |]%d[:|][0-9]+" (1+ section))
        for start = (re-search-forward startexp nil t)
        for end = (or (re-search-forward endexp nil t) (point-max))
        for xjisho = (read-file (format "%s.jukugo.%d" name section))
        for sxjisho = (sort-jisho (append xjisho jisho) t)
        while start do
         (setq *jukugo* nil) ; 章毎の熟語を保存する (redo-jukugo で利用)
         (setq end (furikana-shingen-jukugo-aux sxjisho start end nil))
         (setq end (furikana-shingen-jukugo-aux sxjisho start end nil))
         ;; (warn "%d %s %d %d" section xjisho start end)
         (goto-char start)))

(defun furikana-shingen-jukugo-aux (jisho start end redo)
  ;; ふり假名をふる
  ;; ダビデの子イスラエルの王ソロモンの箴言 →
  ;; ダビデの子{こ}イスラエルの王{わう}ソロモンの箴言{しん,げん}
  (cl-loop for (jukugo . furikana) in jisho
        for pos = (string-match-p "[\\^[あ-んア-ン]" jukugo) ; 漢字の前
        for poskanji = (string-match-p "[^^あ-んア-ン]" jukugo) ; 漢字
        for pos2 = (string-match-p "[\\^[あ-んア-ン]" jukugo poskanji) ; 漢字の後
        for regexp = (if pos jukugo ; 熟語が假名混りであれば其をその儘regexpとし、
                       ; 漢字だけのときは前後が {} で圍まれてゐないものをregexpとする
                       (format "\\(\\(^\\|[} あ-ん、（ア-ン]\\)%s\\|%s\\($\\|[{ あ-ん、）ア-ン]\\)\\)" jukugo jukugo))
        for backexp = (if (null pos) jukugo
                        (if (= pos 0) (cl-subseq jukugo poskanji pos2) ; 頭がカタカナひらがな
                          (cl-subseq jukugo 0 pos)))
    do (cl-loop initially (goto-char start)
         for pos1 = (or (re-search-forward regexp nil t nil) (point-max))
         while (< pos1 end) do
          (when (null redo) ; 使つた熟語を登録しておく（後のredo-jukugoで利用のため）
            (pushnew (cons jukugo furikana) *jukugo* :test #'equal))
          (search-backward backexp  nil t nil)
          (insert "{") (cl-incf end)
          (search-forward backexp  nil t nil)
          (insert "}") (cl-incf end)
          (let ((string (format "{%s}" furikana)))
            (insert string) (cl-incf end (length string)))))
    ;; 更新された章の最終positionを返す
    end)

(defun kanakt-shingen-jukugo ()
  ;; ふり假名をふる (肩付ルビ)
  ;; ダビデの{子}{こ}イスラエルの{王}{わう}ソロモンの{箴言}{しん,げん}
  ;; ダビデの\kanakt{子}{こ}イスラエルの\\kanakt{王}{わう}ソロモンの\\Kanakt{箴,言}{しん,げん}
  (interactive)
  (cl-loop initially (goto-char (point-min))
        for pos1 = (re-search-forward "{[あ-ん]*}" nil t nil)
        while pos1 do
        (backward-sexp)
        (backward-sexp)
        (insert "\\kanakt")
        (forward-sexp)
        (forward-sexp))
  (cl-loop initially (goto-char (point-min))
        for pos1 = (re-search-forward "{[　あ-ん]*,[　あ-ん,]*}" nil t nil)
        while pos1 do
        (backward-sexp)
        (backward-sexp)
        (insert "\\Kanakt")
        (forward-char 1)
        (cl-loop for point from (point) ; 漢字の間に,を插入
           for c = (char-after point)
           while (not (or (<= 0 c 255) (<= ?あ c ?ん))) do
           (forward-char)
           (cl-incf point)
           (insert ","))
        (backward-delete-char 1)
        (forward-char 1)
        (forward-sexp)))

(defvar *latex-header*
  "%%% -*- mode: LaTeX; coding: iso-2022-jp-2 -*-
\\documentclass[a4j,10pt,openany,twocolumn]{tbook}% mentuke tombo myjsbook ,oneside,tbook,treport
\\usepackage[deluxe,expert,multi]{otf}
%% \\usepackage{atbegshi}
%% \\AtBeginShipoutFirst{\\special{pdf:tounicode 90ms-RKSJ-UCS2}}
%% \\usepackage{makeidx}
%% \\usepackage[dvipdfmx,bookmarks=true,bookmarksnumbered=true]{hyperref}
\\usepackage[dvipdfmx]{bxglyphwiki}
\\usepackage{kochu}
\\usepackage{furikana}
\\usepackage{furiknkt}
\\usepackage{jdkintou}
\\usepackage{graphicx}
\\usepackage{wrapfig}
\\usepackage{ipamjm}
\\usepackage{pxjaschart}
\\usepackage{uline--}
\\pagestyle{bothstyle}
\\setlength\\intextsep{0.8zw}
\\setlength\\textfloatsep{0.8zw}
\\kanjiskip0.2pt plus 0.4pt minus 0.4pt
\\xkanjiskip\\kanjiskip
\\AtBeginDvi{\\special{papersize=\\the\\paperwidth,\\the\\paperheight}}
%%% \\setlength{\\evensidemargin}{\\oddsidemargin}
%% \\newcommand{\\pseudochapter}[1]{\\chapter*{#1}}
%% \\newcommand{\\pseudosection}[1]{\\section*{#1}}
%% \\makeatletter
%% \\def\\section{\\@startsection {section}{1}{\\z@}{-0ex plus -0ex minus -.0ex}{0 ex plus .0ex}{\\normalsize}}
%% \\makeatother 
%% \\makeindex
%% \\title{{\\Huge 舊約聖書} \\\\ 大正三年}
%% \\author{米國聖書協會 \\\\ 東京市京橋區銀座}
%% \\date{令和五年皐月 \\\\ \\href{https://bitbucket.org/kurodahisao/meiji-bible}{\\LaTeX 組版生成}}
%% \\setcounter{tocdepth}{1}
%% \\renewcommand{\\contentsname}{舊約聖書目録}
%% \\makeatletter
%% \\renewcommand{\\tableofcontents}{% TOCはいつでもTwoColumn
%%   \\if@twocolumn\\@restonecolfalse
%%   \\else\\@restonecoltrue\\twocolumn\\fi
%%   \\chapter*{\\contentsname
%%     \\@mkboth{\\contentsname}{\\contentsname}%
%%   }\\@starttoc{toc}%
%%   \\if@restonecol\\onecolumn\\fi
%% }
%% \\renewenvironment{theindex}
%%   {\\if@twocolumn\\@restonecolfalse\\else\\@restonecoltrue\\fi
%%    \\columnseprule\\z@ \\columnsep 35\\p@
%%    \\twocolumn%
%%    \\@mkboth{\\indexname}{\\indexname}%
%%    \\thispagestyle{jpl@in}\\parindent\\z@
%%    \\parskip\\z@ \\@plus .3\\p@\\relax
%%    \\let\\item\\@idxitem}
%%   {\\if@restonecol\\onecolumn\\else\\clearpage\\fi}
%% \\makeatother
\\begin{document}
\\topmargin-0.6in
\\newcommand{\\LRkanJI}[2]{%
  \\tbaselineshift=0.3zw  % 0pt
        \\parbox<y>{1zw}{%
            \\scalebox{0.5}[1]{%
                \\makebox[2zw]{#1\\hspace{-0.1zw}#2}}}}
\\def\\katano#1{\\linebreak[3]\\kern0.5zw\\raisebox{0.5zw}[0pt][0pt]{\\tiny#1}\\kern0.1zw}
\\makeatletter
\\def\\kochuuname{}
\\def\\arabicchuucite#1{\\rensuji{\\chuucite{#1}}}
\\def\\@kochuulabel#1{\\rensuji{#1}}
\\makeatother
%% \\maketitle
%% \\tableofcontents
\\chapter*{箴言}
%%% \\addcontentsline{toc}{chapter}{箴言}}
")

(defvar *mark-both* "\\markboth{%s}{%s}")

(defvar *section-figure*
  "
\\goodbreak\n\\noindent
\\begin{wrapfigure}[2]{l}{0pt}\n \\vbox to 2zw {\\hbox to 3zw {\\large \\kintou{4zw}{\\bf 第%s章}}}\n \\end{wrapfigure}
\\noindent")

(defvar *subsection-figure*
  "\\katano{%s} \\protect\\nolinebreak ")

(defun arabic-to-kanji (string)
  (cl-concatenate 'string
                  (cl-loop for char across string
                     collect (cl-case char
                               (?0 ?〇)
                               (?1 ?一)
                               (?2 ?二)
                               (?3 ?三)
                               (?4 ?四)
                               (?5 ?五)
                               (?6 ?六)
                               (?7 ?七)
                               (?8 ?八)
                               (?9 ?九)))))

(defun latex-shingen-section ()
  (interactive)
  (goto-char (point-min))
  (search-forward "==") (previous-line) (kill-region 1 (point))
  (cl-loop initially (goto-char (point-min))
        for pos1 = (re-search-forward "== *\\(第[0-9]+章\\) *==" nil t nil)
        while pos1 do
        (replace-match "\\1")
        (let* ((pos2 (progn (backward-word) (point)))
               (pos3 (progn (backward-word) (point)))
               (num (buffer-substring pos2 pos3)))
          (delete-char (- pos2 pos3))   ; 1, 2, 3,,,
          (delete-char -1)              ; 第
          (let ((mark (format "箴言第%s章" (arabic-to-kanji num))))
            (insert (format *mark-both* (reverse mark) mark)))
          (insert (format *section-figure* (arabic-to-kanji num)))
          (kill-line 1)))
  (progn (goto-char (point-max))
         (previous-line 3)
         (beginning-of-line 1)
         (kill-line 3)))

(defun latex-shingen-subsection ()
  (interactive)
  (cl-loop initially (goto-char (point-min))
        for pos1 = (re-search-forward "[0-9]+:[0-9]+ [0-9]+:[0-9]+" nil t nil)
        while pos1 do
        (let* ((pos2 (progn (backward-word) (point)))
               (num1 (prog1 (buffer-substring pos1 pos2) (backward-word) (backward-char)))
               (pos3 (point))
               (pos4 (progn (backward-word) (point)))
               (num2 (buffer-substring pos3 pos4)))
          (delete-char (- pos1 pos3))
          (backward-kill-sexp)
          (insert (format *subsection-figure* (format "%s、%s" (arabic-to-kanji num2) (arabic-to-kanji num1))))
          (kill-line 1)))
  (cl-loop initially (goto-char (point-min))
        for pos1 = (re-search-forward "[0-9]+:[0-9]+" nil t nil)
        while pos1 do
        (let* ((pos2 (progn (backward-word) (point)))
               (num (buffer-substring pos1 pos2)))
          (delete-char (- pos1 pos2))
          (backward-kill-sexp)
          (insert (format *subsection-figure* (arabic-to-kanji num)))
          (kill-line 1))))

(defun latex-shingen-white-space ()
  (interactive)
  (cl-loop initially (goto-char (point-min))
        for pos1 = (re-search-forward "\\katano{[〇一二三四五六七八九]+\\(\\|、[〇一二三四五六七八九]+\\)}" nil t nil)
        while pos1 do
        (forward-sexp) (forward-char)
        (cl-loop for point from (point)
              for char = (char-after point)
              until (= 10 char) do
              (forward-char)
              (when (= 32 char)
                (backward-delete-char 1)
                (insert "、"))
              (when (= #x3000 char)
                (backward-delete-char 1)
                (insert "○")))
        (kill-line))
  (progn (goto-char (point-min))
         (insert *latex-header*)))

(defun hex-to-char (&optional min max)
  ;; 汝らのほろび&#x98ba;風の如くきたり → 汝らのほろび颺風の如くきたり
  (interactive)
  (unless min (setq min (point-min)))
  (unless max (setq max (point-max)))
  (cl-loop initially (goto-char min)
        for pos = (search-forward "&" nil t nil)
        for poscol = (search-forward ";" nil t nil)
        while (and pos (< pos max)) do 
          (let ((hex (buffer-substring pos (1- poscol))))
            (backward-delete-char (1+ (- poscol pos)))
            (insert (car (read-from-string hex))))))

(defun char-to-hex ()
  ;; 汝らのほろび颺風の如くきたり → 汝らのほろび&#x98ba;風の如くきたり
  (interactive)
  (cl-loop initially (goto-char (point-min))
        for char = (char-after)
        while char do
        (let ((charset (char-charset char)))
          (if (or (eql charset 'japanese-jisx0208)
                  (eql charset 'ascii))
              (forward-char)
            (progn
              (delete-char 1)
              (insert (format "&#x%x;" char)))))))

(defun count-shingen-jukugo (jukugo)
  (let ((count 0)
        (kakko nil))
    (cl-loop for char across (car jukugo) do
          (if (= char 91)               ; found "["
              (progn (setq kakko t) (cl-incf count 1))
            (if (= char 91)             ; found "]"
                (setq kakko nil)
              (when (null kakko)
                (if (<= ?あ char ?ん)
                    (cl-incf count 1)
                  (cl-incf count 2))))))
    count))

;; (sort *shingen-jukugo* #'(lambda (x y) (> (count-shingen-jukugo x) (count-shingen-jukugo y))))

(defvar *katakana-revert-table* nil)    ; ブック毎各々定義

(defun revert-katakana-exception-2 ()
  (interactive)
  (process-hentai-kana-table-2 *katakana-revert-table*))

(defun process-hentai-kana-table-2 (table)
  (cl-loop for (key . value) in table
        do (cl-loop initially (beginning-buffer)
                 for pos = (re-search-forward key nil t nil)
                 while pos do
                 (replace-match value))))

(defun utf-to-jis-2 (ipa)
  (interactive "P")
  (put-line-on-katakana)
  (canonicalize-3-kanakt)
  (convert-kunoji-word)
  (convert-kurikahesi-hirakana ipa)
  (revert-katakana-exception-2)
  (beginning-buffer)
  (when (search-forward "%%% -*- mode: LaTeX; coding: utf-8 -*-" nil t nil)
    (beginning-buffer)
    (kill-line 1))
  (jis-to-cid ipa)
  (unicode-to-charcode ipa))

(defun canonicalize-3-kanakt ()
  (interactive)
  (beginning-buffer)
  (while (re-search-forward "\\\\kanakt{\\(.\\),\\(.\\),\\(.\\),\\(.\\)}{\\([あ-ん][あ-ん]+\\),\\([あ-ん][あ-ん][あ-ん]+\\),\\([あ-ん]+\\),\\([あ-ん]+\\)}" nil t)
    ;; ("長女季女" . "あね,むすめ,いも,むすめ")
    (replace-match "\\\\Kanakt[3]{\\1,\\2}{\\5,\\6}\\\\Kanakt{\\3,\\4}{\\7,\\8}" nil nil))
  (beginning-buffer)
  (while (re-search-forward "\\\\kanakt{\\(.\\),\\(.\\),\\(.\\)}{\\([あ-ん][あ-ん]+\\),\\([あ-ん][あ-ん][あ-ん]+\\),\\([あ-ん]+\\)}" nil t)
    ;; ("驅使者" . "おひ,つかふ,もの"), {とりあげ,をんな,ども}
    (replace-match "\\\\Kanakt[3]{\\1,\\2}{\\4,\\5}\\\\kanakt{\\3}{\\6}" nil nil))
  (beginning-buffer)
  (while (re-search-forward "\\\\kanakt{\\(.\\),\\(.\\),\\(.\\)}{\\([あ-ん][あ-ん][あ-ん]+\\),\\([あ-ん][あ-ん]+\\),\\([あ-ん][あ-ん]+\\)}" nil t)
    ;; ("贖罪所" . "しよく,ざい,しよ")
    (replace-match "\\\\Kanakt[3]{\\1,\\2}{\\4,\\5}\\\\kanakt{\\3}{\\6}" nil nil))
  (beginning-buffer)
  (while (re-search-forward "\\\\kanakt{\\(.\\),\\(.\\),\\(.\\)}{\\([あ-ん][あ-ん]+\\),\\([あ-ん][あ-ん]+\\),\\([あ-ん][あ-ん][あ-ん]+\\)}" nil t)
    ;; ("黄緑玉" . "きみ,どり,のたま")
    (replace-match "\\\\Kanakt[3]{\\1,\\2}{\\4,\\5}\\\\kanakt{\\3}{\\6}" nil nil))
  (beginning-buffer)
  (while (re-search-forward "\\\\kanakt{\\(.\\),\\(.\\)}{\\([あ-ん][あ-ん][あ-ん]+\\),\\([あ-ん][あ-ん]+\\)}" nil t) ; {子,女}{むすこ,むすめ}
    (replace-match "\\\\kanakt[3]{\\1}{\\3}\\\\kanakt{\\2}{\\4}" nil nil))
  (cl-loop initially (beginning-buffer)
        while (re-search-forward "kanakt{.}{[あ-ん][あ-ん][あ-ん]+}\\\\" nil t nil) ; {塊}{かたまり}
        do (backward-sexp 3)
           (insert "[3]"))
  (cl-loop initially (beginning-buffer)
        while (re-search-forward "kanakt{.,*.}{[あ-ん][あ-ん],*[あ-ん],*[あ-ん][あ-ん]+}\\\\" nil t nil) ; {兄弟}{きやうだい}
        do (backward-sexp 3)
           (insert "[3]")))
;;;
(provide :shingen)
