;;; -*- mode:emacs-lisp; lexical-binding:t; coding:utf-8 -*-

(require 'cl)

(defun init-meiji-bible ()
  (interactive)
  (add-hook 'comint-output-filter-functions 'comint-truncate-buffer)
  (if (fboundp 'fi:common-lisp)
      (progn
        (fi:common-lisp)
        (switch-to-buffer "*common-lisp*")
        (insert ":pushd ~/project/meiji-bible/")
        (fi:inferior-lisp-newline)
        (insert ":ld init")
        (fi:inferior-lisp-newline))
    (progn
      (load-file "~/projects/meiji-bible/utf-to-jis.el")
      (let ((buffer (find-file "~/projects/meiji-bible/")))
        (switch-to-buffer buffer))
      (slime)
      (switch-to-buffer "*slime-repl sbcl*")
      (sleep-for 3)
      (insert "(load \"init\")")
      (slime-repl-return))))

(setq *assoc-meiji-pdf-name*
  '(("meiji-new-testament.txt" "meiji-new-testament" "新約全書")
    ("multicol-new-testament.txt" "multicol-new-testament" "新約全書對譯")
    ("meiji-matthew.txt" "meiji-matthew" "馬太傳福音書")
    ("multicol-matthew.txt" "multicol-matthew" "馬太傳對譯")
    ("meiji-mark.txt" "meiji-mark" "馬可傳福音書")
    ("multicol-mark.txt" "multicol-mark" "馬可傳對譯")
    ("meiji-luke.txt" "meiji-luke" "路加傳福音書")
    ("multicol-luke.txt" "multicol-luke" "路加傳對譯")
    ("meiji-john.txt" "meiji-john" "約翰傳福音書")
    ("multicol-john.txt" "multicol-john" "約翰傳對譯")
    ("meiji-acts.txt" "meiji-acts" "使徒行傳")
    ("multicol-acts.txt" "multicol-acts" "使徒行傳對譯")
    ("meiji-romans.txt" "meiji-romans" "羅馬書")
    ("multicol-romans.txt" "multicol-romans" "羅馬書對譯")
    ("meiji-i-corinthians.txt" "meiji-i-corinthians" "哥林多前書")
    ("multicol-i-corinthians.txt" "multicol-i-corinthians" "哥林多前書對譯")
    ("meiji-ii-corinthians.txt" "meiji-ii-corinthians" "哥林多後書")
    ("multicol-ii-corinthians.txt" "multicol-ii-corinthians" "哥林多後書對譯")
    ("meiji-galatians.txt" "meiji-galatians" "加拉太書")
    ("multicol-galatians.txt" "multicol-galatians" "加拉太書對譯")
    ("meiji-ephesians.txt" "meiji-ephesians" "以弗所書")
    ("multicol-ephesians.txt" "multicol-ephesians" "以弗所書對譯")
    ("meiji-philippians.txt" "meiji-philippians" "腓立比書")
    ("multicol-philippians.txt" "multicol-philippians" "腓立比書對譯")
    ("meiji-colossians.txt" "meiji-colossians" "哥羅西書")
    ("multicol-colossians.txt" "multicol-colossians" "哥羅西書對譯")
    ("meiji-i-thessalonians.txt" "meiji-i-thessalonians" "帖撒羅尼迦前書")
    ("multicol-i-thessalonians.txt" "multicol-i-thessalonians" "帖撒羅尼迦前書對譯")
    ("meiji-ii-thessalonians.txt" "meiji-ii-thessalonians" "帖撒羅尼迦後書")
    ("multicol-ii-thessalonians.txt" "multicol-ii-thessalonians" "帖撒羅尼迦後書對譯")
    ("meiji-i-timothy.txt" "meiji-i-timothy" "提摩太前書")
    ("multicol-i-timothy.txt" "multicol-i-timothy" "提摩太前書對譯")
    ("meiji-ii-timothy.txt" "meiji-ii-timothy" "提摩太後書")
    ("multicol-ii-timothy.txt" "multicol-ii-timothy" "提摩太後書對譯")
    ("meiji-titus.txt" "meiji-titus" "提多書")
    ("multicol-titus.txt" "multicol-titus" "提多書對譯")
    ("meiji-philemon.txt" "meiji-philemon" "腓利門書")
    ("multicol-philemon.txt" "multicol-philemon" "腓利門書對譯")
    ("meiji-hebrews.txt" "meiji-hebrews" "希伯來書")
    ("multicol-hebrews.txt" "multicol-hebrews" "希伯來書對譯")
    ("meiji-james.txt" "meiji-james" "雅各書")
    ("multicol-james.txt" "multicol-james" "雅各書對譯")
    ("meiji-i-peter.txt" "meiji-i-peter" "彼得前書")
    ("multicol-i-peter.txt" "multicol-i-peter" "彼得前書對譯")
    ("meiji-ii-peter.txt" "meiji-ii-peter" "彼得後書")
    ("multicol-ii-peter.txt" "multicol-ii-peter" "彼得後書對譯")
    ("meiji-i-john.txt" "meiji-i-john" "約翰第一書")
    ("multicol-i-john.txt" "multicol-i-john" "約翰第一書對譯")
    ("meiji-ii-john.txt" "meiji-ii-john" "約翰第二書")
    ("multicol-ii-john.txt" "multicol-ii-john" "約翰第二書對譯")
    ("meiji-iii-john.txt" "meiji-iii-john" "約翰第三書")
    ("multicol-iii-john.txt" "multicol-iii-john" "約翰第三書對譯")
    ("meiji-jude.txt" "meiji-jude" "猶太書")
    ("multicol-jude.txt" "multicol-jude" "猶太書對譯")
    ("meiji-revelation.txt" "meiji-revelation" "約翰默示録")
    ("multicol-revelation.txt" "multicol-revelation" "約翰默示録對譯")))

(defun assoc-meiji-pdf-name (buffer-name args)
  (let ((assoc (assoc buffer-name *assoc-meiji-pdf-name*))) ; #'string=
    (if (not args) (second assoc) (third assoc))))

(defun multicol-volume-p (buffer-name)
  (cl-search "multicol-" buffer-name))

(defun new-testament-p (buffer-name)
  (cl-search "new-testament" buffer-name))

(defun make-meiji-volume (buffer-name args)
  (if (multicol-volume-p buffer-name)
      (if (new-testament-p buffer-name)
          (multicol-new-testament args)
        (multicol-separate-volume args))
    (if (new-testament-p buffer-name)
        (meiji-new-testament args)
      (meiji-separate-volume args))))

(defvar tex-zap-file)

(defun do-oyamoji-zorohe (current-name args)
  (ignore-errors (read))         ; wait until tex compile end manually
  (let* ((sbclbuffer (get-buffer "*slime-repl sbcl*"))
         (lisp-buffer (or sbclbuffer (get-buffer "*common-lisp*")))
         (newline (if sbclbuffer 'slime-repl-return 'fi:inferior-lisp-newline))
         (pdf-name (assoc-meiji-pdf-name current-name args)))
    (if (multicol-volume-p current-name) ; コピーするだけ
        (let ((insert-string (format "mv %s.pdf %s.pdf" tex-zap-file pdf-name)))
          (switch-to-buffer "*tex-shell*")
          (insert insert-string)
          (comint-send-input))
      (let ((insert-string              ; 親文字揃へ
             (if (new-testament-p current-name)
                 (format "(pdf:pdf-adjust-oyamoji \"%s.pdf\" \"%s.pdf\" :bottom-1 -458 :bottom-2 -458)" tex-zap-file pdf-name)
               (format "(pdf:pdf-adjust-oyamoji \"%s.pdf\" \"%s.pdf\")" tex-zap-file pdf-name))))
        (switch-to-buffer lisp-buffer) (end-buffer)
        (insert insert-string)
        (funcall newline)))))

(defun make-meiji-pdf (ipa)
  ;; 明治譯分册をつくる (自動化)
  (interactive "P")
  (let ((current-name (buffer-name (current-buffer))))
    (unless (or (get-buffer "*slime-repl sbcl*") (get-buffer "*common-lisp*"))
      (init-meiji-bible)
      (switch-to-buffer current-name))
    (make-meiji-volume current-name ipa)
    (tex-buffer)
    (switch-to-buffer "*tex-shell*") (end-buffer)
    (set-buffer-process-coding-system 'unix 'unix)
    (comint-previous-input 1) ; 二回コンパイルしないとindexがうまくできない
    (comint-send-input)
    (switch-to-buffer "*tex-shell*") (end-buffer)
    (ignore-errors (read))       ; wait until tex compile end manually
    (insert (format "mendex %s" tex-zap-file))
    (completion-at-point)
    (delete-backward-char 1)
    (comint-send-input)
    (ignore-errors (read))       ; wait until tex compile end manually
    (find-file (format "%s.ind" tex-zap-file))
    (ignore-errors (insert-eject-index))
    (save-buffer)
    (kill-buffer)
    (switch-to-buffer "*tex-shell*") (end-buffer)
    (insert (format "platex \\\\nonstopmode\\\\input %s.tex > /dev/null" tex-zap-file))
    (comint-send-input)
    (comint-previous-input 1) ; 二回コンパイルしないとindexがうまくできない
    (comint-send-input)
    (ignore-errors (read))       ; wait until tex compile end manually
    (let ((map (if (not ipa) "kozukapr6n.map" "ipa.map")))
      (comint-send-input)
      (insert (format "dvipdfmx -f %s %s.dvi" map tex-zap-file))
      (comint-send-input))
    (do-oyamoji-zorohe current-name ipa)
    (switch-to-buffer current-name)
    (revert-buffer t t t)
    (find-file-other-window "")))

(defun meiji-separate-volume (ipa)
  ;; 明治譯分册をつくる
  (interactive "P")
  (insert-lf-in-long-title)
  (utf-to-jis ipa)                      ; if ipa=true use IPAmj else Adobe-Japan
  (uncomment-makeindex nil)             ; 表紙なし
  (make-kana-index)
  (remove-index-from-title)
  (remove-index-from-addcontents)
  (set-buffer-file-coding-system 'iso-2022-jp-2))

(defun meiji-new-testament (ipa)
  ;; 明治譯新約全書をつくる
  (interactive "P")
  (buffer-disable-undo)
  (utf-to-jis ipa)                      ; if ipa=true use IPAmj else Adobe-Japan
  (uncomment-makeindex t)               ; 表紙あり
  (make-kana-index)
  (remove-index-from-title)
  (remove-index-from-addcontents)
  (buffer-enable-undo)
  (set-buffer-file-coding-system 'iso-2022-jp-2))

(defun multicol-separate-volume (ipa)
  ;; 明治譯英和對譯分册をつくる
  (interactive "P")
  (utf-to-jis ipa)                      ; if ipa=true use IPAmj else Adobe-Japan
  (uncomment-makeindex t)               ; 表紙あり
  (make-kana-index)
  (set-buffer-file-coding-system 'iso-2022-jp-2))

(defun multicol-new-testament (ipa)
  ;; 明治譯英和對譯新約全書をつくる
  (interactive "P")
  (buffer-disable-undo)
  (utf-to-jis ipa)                      ; if ipa=true use IPAmj else Adobe-Japan
  (uncomment-makeindex t)               ; 表紙あり
  (make-kana-index)
  (buffer-enable-undo)
  (set-buffer-file-coding-system 'iso-2022-jp-2))

(defun beginning-buffer ()
  (goto-char (point-min)))

(defun end-buffer ()
  (goto-char (point-max)))

(defun init-log-and-begin-buffer (current log)
  (switch-to-buffer log)
  (erase-buffer)
  (switch-to-buffer current)
  (beginning-buffer))

(defun utf-to-jis (ipa)
  (interactive "P")
  (convert-kunoji-word)
  (convert-hentai-kana-2)
  (canonicalize-24-ruby-kana)
  (canonicalize-23-ruby-kana)
  (canonicalize-3-ruby-kana)
  (canonicalize-3-ruby-kanakt)
  (convert-kurikahesi-hirakana ipa)
  (convert-hentai-kana-rubi)
  (convert-hentai-kana-1)
  (revert-hentai-kana-exception)
  (put-line-on-katakana)
  (revert-katakana-exception)
  ;; (convert-kurikahesi-katakana ipa)
  (beginning-buffer)
  (when (search-forward "%%% -*- mode: LaTeX; coding: utf-8 -*-" nil t nil)
    (beginning-buffer)
    (kill-line 1))
  (unicode-to-charcode ipa)
  (jis-to-cid ipa))

(defun uncomment-makeindex (args)
  (interactive "P")
  ;; TwoColumn -> SingleColumn
  (let ((tbook (progn (beginning-buffer)
                      (search-forward "{tbook}" nil t nil))))
    (when args                          ; 表紙あり
      (let ((keys (if tbook
                      '(",openany" ",twocolumn" "\\topmargin-0.6in")
                    '(",notitlepage"))))
        (cl-loop initially (beginning-buffer)
              for key in keys do
              (search-forward key nil t nil)
              (backward-delete-char-untabify (length key))
              (when (string= key ",openany")
                (insert ",openleft")))))
    (cl-loop initially (beginning-buffer)
          with key = "^%%[ ]+"
          for pos1 = (re-search-forward key nil t nil)
          while pos1 do
          (move-beginning-of-line 1)
          (delete-char 3))
    (unless args
      (beginning-buffer)
      (re-search-forward "^\\\\maketitle")
      (move-beginning-of-line 1)
      (insert "%% ")
      (re-search-forward "^\\\\tableofcontents")
      (move-beginning-of-line 1)
      (insert "%% "))
    (end-buffer) (insert "\n")
    (insert "\\pseudochapter{}\n\\thispagestyle{empty}\n")
    (insert "\\addcontentsline{toc}{chapter}{索引}\n")
    (when args                         ; 表紙あり
      (insert "\\begin{center} \\Huge 索引 \\end{center}\n"))
    (insert "\\printindex\n")))

(defun make-kana-index ()
  (interactive)
  (make-kana-1-index)
  (make-kana-2-index)
  (make-kana-3-index))

(defun make-kana-1-index ()
  (interactive)
  (cl-loop initially (beginning-buffer)
        with key = "\\oline{"
        with len = (length key)
        for pos1 = (search-forward key nil t nil)
        while pos1 do
        (progn (backward-char len) (kill-sexp 2))
        (progn (yank) (insert "\\index{")
               (yank) (insert "}"))
        (refresh-screen)))

(defun make-kana-2-index ()
  (interactive)
  (cl-loop initially (beginning-buffer)
        with key = "\\oline[lines=2]"
        with len = (length key)
        for pos1 = (search-forward key nil t nil)
        while pos1 do
        (progn (backward-char len) (kill-sexp 3))
        (progn (yank) (insert "\\index{")
               (yank) (insert "}"))
        (refresh-screen)))

(defun make-kana-3-index ()
  (interactive)
  (cl-loop for key in (append others-name-table others-name-table-2)
        for regexp = (format "[^{ア-ン]%s[^}ア-ン]" key)
        for len = (length key)
     do (cl-loop initially (beginning-buffer)
              for pos1 = (re-search-forward regexp nil t nil)
              while pos1 do
              (progn (backward-char (1+ len)) (kill-forward-chars len))
              (progn (yank) (insert "\\index{")
                     (yank) (insert "}"))
              (refresh-screen))))

(defun insert-lf-in-long-title ()  ; 長いtitleの途中に改行を入れる
  (interactive)
  (beginning-buffer)
  (cl-loop for pos = (re-search-forward "{新,約,全,書}{しん,やく,ぜん,しよ}\\\\Kanakt{使,徒}{し,と}パウロ、" nil t nil)
        while pos
        do (backward-char) (delete-char 1) (insert "\\\\　　")))

(defun remove-index-from-title () ; 長いtitleの中のindexを取り去る
  (interactive)
  (cl-loop for pos1 = (search-forward "\\chapter*{" nil t nil)
        for pos2 = (progn (move-end-of-line 1) (point))
        while pos1 do
        (cl-loop initially (move-beginning-of-line 1)
              for pos = (search-forward "\\index" nil t nil)
              while (< pos pos2) do
              (backward-kill-sexp)
              (kill-sexp))))

(defun remove-index-from-addcontents ()
  (interactive)
  (cl-loop initially (beginning-buffer) ; 目次からindex除去
        with regexp = "\\addcontentsline{toc}{section}{.+\\index{.+$"
        for pos = (re-search-forward regexp nil t nil)
        while pos do
        (search-backward "\\index")
        (kill-sexp)
        (kill-sexp)
        (move-beginning-of-line 1))
  (cl-loop initially (beginning-buffer) ; 目次からoline除去
        with regexp = "\\addcontentsline{toc}{section}{.+\\oline\\[lines=2\\]{.+$"
        for pos = (re-search-forward regexp nil t nil)
        while pos do
        (search-backward "\\oline[lines=2]")
        (kill-sexp)
        (kill-sexp)
        (delete-char 1)
        (forward-sexp)
        (delete-char 1)
        (move-beginning-of-line 1))
  (cl-loop initially (beginning-buffer) ; 目次からoline除去
        with regexp = "\\addcontentsline{toc}{section}{.+\\oline{.+$"
        for pos = (re-search-forward regexp nil t nil)
        while pos do
        (search-backward "\\oline")
        (kill-sexp)
        (delete-char 1)
        (forward-sexp)
        (delete-char 1)
        (move-beginning-of-line 1)))

(defun canon-addcontents-characters ()
  (interactive)
  (cl-loop for regexp in '("\\addcontentsline{toc}{section}.*\\MJMZM"
                        "\\addcontentsline{toc}{chapter}.*\\MJMZM"
                        "\\addcontentsline{toc}{chapter}.*\\CID"
                        "\\addcontentsline{toc}{chapter}.*\\UTF")
    do (cl-loop initially (beginning-buffer)
          for pos1 = (re-search-forward regexp nil t nil)
          while pos1 do ; addcontentslineがCID,UTF,MJMZMを食へないため
            (progn (backward-word) (backward-char 1) (kill-sexp 2))
            (let* ((code (car kill-ring))
                   (code-string (format "%s" code))
                   (char (car (or (rassoc code-string jitai-convert-table)
                                  (rassoc code-string char-to-mjm-table)))))
              (insert char))
            (previous-line))))

(defun insert-eject-index ()
  (interactive)
  (beginning-buffer)
  (search-forward "oline[")
  (move-beginning-of-line 1)
  (open-line 1)
  (insert "\\pseudosection{}\n")
  (insert "\\addcontentsline{toc}{section}{Lands}") ; 地名
  (search-forward "oline{")
  (move-beginning-of-line 1)
  (open-line 1)
  (insert "\\eject\n")  ; 地名、人名の間にejectを入れる
  (insert "\\pseudosection{}\n")
  (insert "\\addcontentsline{toc}{section}{Names}") ; 人名
  (re-search-forward "\\item [ア-ン]")
  (move-beginning-of-line 1)
  (open-line 1)
  (insert "\\eject\n")  ; 人名、其の他の間にejectを入れる
  (insert "\\pseudosection{}\n")
  (insert "\\addcontentsline{toc}{section}{Others}") ; 他
  )

(defun remove-section-toc ()
  (interactive)
  (cl-loop initially (beginning-buffer)
     for pos = (search-forward "{section}" nil t nil)
     while pos
     do (move-beginning-of-line 1)
        (kill-line 1)))


;;;;

(setq unicode-to-mjm-table
      '( ;; Glyph missing in font.
        (?\撙 . "\\MJMZM{012765}")
        (?\稺 . "\\MJMZM{019129}")
        (?\襜 . "\\MJMZM{024153}")
        (?\讟 . "\\MJMZM{024865}")
        (?\龥 . "\\MJMZM{030170}")
        (?\諉 . "\\MJMZM{024569}")
        (?\僨 . "\\MJMZM{056951}")
        (?\嚷 . "\\MJMZM{008776}")
        (?\暌 . "\\MJMZM{013339}")
        (?\榨 . "\\MJMZM{014292}")
        (?\漰 . "\\MJMZM{015861}")
        (?\艄 . "\\MJMZM{021412}")
        (?\誶 . "\\MJMZM{024546}")
        (?\譭 . "\\MJMZM{024806}")        ; MJ058766
        (?\阱 . "\\MJMZM{027550}")
        (?\麰 . "\\MJMZM{029843}")        ; MJ029844
        (?\洏 . "\\MJMZM{015273}")
        (?\蠉 . "\\MJMZM{023773}")
        (?\醡 . "\\MJMZM{026532}")
        ;; 草冠
        (?\儆 . "\\MJMZM{007136}")
        (?\蒺 . "\\MJMZM{022512}")
        (?\謊 . "\\MJMZM{024662}")
        (?\厲 . "\\MJMZM{007965}")
        (?\爇 . "\\MJMZM{016692}")
        (?\蔯 . "\\MJMZM{022729}")
        (?\蠆 . "\\MJMZM{023769}")
        ))

(defun unicode-to-charcode (args)
  (interactive "P")
  (cl-loop initially (beginning-buffer)
        for char = (char-after)
        while char do
        (let ((charset (char-charset char))
              (assoc))
          (if (or (eql charset 'japanese-jisx0208)
                  (eql charset 'ascii))
              (forward-char 1)
            (progn
              (delete-char 1)
              (cond ((= char #x9eab)    ; パンの字、例外処理  
                     ;; ** WARNING ** No character mapping available.
                     ;;  CMap name: UniJIS-UTF16-H
                     ;; input str: <9eab>
                     (insert "\\UTF{9eaa}")) ; 麪
                    ((= char #x3bf6)         ; 
                     ;; dvipdfmx:warning: No character mapping available.
                     ;; CMap name: UniJIS-UTF16-H
                     ;; input str: <3bf6>
                     (insert "\\UTF{6936}")) ; 椶
                    ((= char #x6e71)         ; 氵砉の字、例外処理
                     ;; ** WARNING ** No character mapping available.
                     ;;  CMap name: UniJIS-UTF16-H
                     ;; input str: <6e71>
                     ;; (insert "\\LRkanJI{\\UTF{6c35}}{\\UTF{7809}}")
                     (insert "\\MJMZM{015644}"))
                    ((and args ; dvipdfmx:warning: Glyph missing in font. (ipa.map)
                          (setq assoc (assoc char unicode-to-mjm-table)))
                     (insert (cdr assoc)))
                    (t
                     (insert (format "\\UTF{%x}" char))))
              (refresh-screen))))))

(defun charcode-to-unichar ()
  (interactive)
  (cl-loop initially (beginning-buffer)
        for pos1 = (search-forward "\\UTF{" nil t nil)
        while pos1 do
        (kill-word 1)                   ; charcode
        (yank)                          ; undo
        (backward-word)
        (let* ((word (car kill-ring))
               (char (car (read-from-string (format "#x%s" word))))
               (charset (char-charset char)))
          (when (eql charset 'japanese-jisx0208)
            (kill-word 1)
            (delete-char 1)
            (backward-delete-char-untabify 5)
            (insert char)))))

(defun jis-to-cid (args)
  (interactive "P")
  (cl-loop initially (beginning-buffer)
        for char = (char-after)
        while char do
        (let ((cid (if args (assoc char char-to-mjm-table)
                     (assoc char jitai-convert-table))))
          (if cid
              (progn
                (delete-char 1)
                (insert (cdr cid))
                (refresh-screen))
            (forward-char 1)))))

(setq jitai-convert-table
  '( ;; \UTF{5F34} とん
    ;; \UTF{63F7} さす
    ;; "\\UTF{8B43}" {いつはる}{うそ} 第4水準2-88-74
    ;; "\\UTF{626D}" {ねじまはす}
    ;; "\\UTF{5E1F}" {ひらとばり}
    ;; "\\UTF{6A9E}" {かし}の木
    ;; "\\UTF{60DD}\\UTF{6033}" {しやうけい}
    ;; (?\讐 . "\\CID{6729}")
    ;; (?\僊 . "\\CID{7982}")
    (?\初 . "\\GWI{u521d-v}")       ; bxglyphwikiの利用
    (?\又 . "\\GWI{u53c8-var-001}") ; bxglyphwikiの利用
    (?\𤧚 . "\\GWI{u249da-jv}")     ; bxglyphwikiの利用 (\UTFで出ない)
    (?\麆 . "\\GWI{u9e86}")         ; bxglyphwikiの利用 (\UTFで出ない)
    (?\鸆 . "\\GWI{u9e06}")         ; bxglyphwikiの利用 (\UTFで出ない)
    (?\鸅 . "\\MJMZM{029740}")      ; mjmzm (\UTFで出ない)
    (?\朕 . "\\CID{13936}")
    ;; ? 度熱初炎牝尺 ?
    (?\都 . "\\UTF{FA26}") (?\緒 . "\\CID{8595}") (?\者 . "\\CID{13349}") (?\著 . "\\CID{13367}")  (?\堵 . "\\CID{7753}") (?\箸 . "\\CID{7775}") (?\煮 . "\\CID{13347}") (?\暑 . "\\CID{13352}") (?\署 . "\\CID{13353}") (?\諸 . "\\CID{8622}") (?\儲 . "\\CID{7798}") (?\屠 . "\\CID{7754}") (?\曙 . "\\CID{7699}") (?\渚 . "\\CID{7700}") (?\瀦 . "\\CID{7741}") (?\猪 . "\\CID{8548}") (?\薯 . "\\CID{7701}") (?\藷 . "\\CID{7702}") (?\賭 . "\\CID{7756}")
    (?\敵 . "\\MJMZM{013019}") (?\竟 . "\\MJMZM{019331}") (?\章 . "\\MJMZM{019333}") (?\支 . "\\MJMZM{012941}") (?\技 . "\\MJMZM{012195}") (?\錬 . "\\MJMZM{027019}") (?\障 . "\\MJMZM{027670}")
    ;; 寛
    (?\寛 . "\\UTF{5BEC}")
    (?\突 . "\\UTF{FA55}") (?\器 . "\\CID{13333}") (?\類 . "\\CID{13396}") (?\臭 . "\\CID{13350}")
    ;; 音
    (?\音 . "\\CID{13664}") (?\暗 . "\\CID{13638}") (?\意 . "\\CID{13639}") (?\龍 . "\\CID{14087}") (?\帝 . "\\CID{13943}") (?\商 . "\\CID{13830}") (?\刻 . "\\MJMZM{007517}") (?\憶 . "\\MJMZM{011957}") ; 倍傍僮億僻壁劈剖噫噺培境壟妾嬖宰寵嶂幟幢彰憧擘拉接撞新旁臂膀胱臆朧柆梓椄榜樟橦檍檗槞殕毅泣童障親刻
    ;; 黒
    (?\黒 . "\\UTF{9ED1}") (?\練 . "\\UTF{FA57}") (?\薫 . "\\UTF{85B0}") (?\勳 . "\\CID{4291}") (?\煉 . "\\CID{7810}") (?\墨 . "\\CID{13387}") (?\黛 . "\\CID{7729}") (?\蘭 . "\\CID{14084}") (?\欄 . "\\CID{13392}") 
    ;; 負
    (?\危 . "\\CID{13696}") (?\兔 . "\\CID{14103}") (?\絶 . "\\CID{13882}") (?\兎 . "\\CID{13949}") (?\負 . "\\CID{14006}") ; (?\負 . "\\CID{14005}")
    (?\冤 . "\\CID{7817}")              ; 14121
    (?\頼 . "\\CID{8627}") (?\瀬 . "\\CID{8534}")
    ;; 頓
    (?\頓 . "\\CID{7764}") (?\噸 . "\\CID{7762}")
    ;; 文
    (?\吏 . "\\CID{13513}") (?\雙 . "\\CID{13525}") (?\文 . "\\CID{20131}") (?\蚊 . "\\CID{20212}") (?\交 . "\\CID{13439}") (?\便 . "\\CID{13506}") (?\較 . "\\CID{20226}") (?\史 . "\\CID{13451}") (?\使 . "\\CID{13450}") (?\紋 . "\\CID{14060}") (?\更 . "\\CID{13441}") (?\丈 . "\\CID{13463}") (?\更 . "\\CID{13441}") (?\校 . "\\CID{13442}") (?\父 . "\\CID{13497}") (?\絞 . "\\CID{13444}") (?\斐 . "\\CID{13493}") (?\建 . "\\CID{13436}") ; 攻攸放故政救敖敗敬散敦枚牧畋致赦馭
    (?\庭 . "\\CID{13481}")
    (?\全 . "\\CID{13890}") (?\詮 . "\\CID{7720}") (?\斜 . "\\CID{13805}")
    (?\敢 . "\\CID{13425}") (?\橄 . "\\CID{13554}")
    (?\誹 . "\\CID{13495}") (?\徘 . "\\CID{13538}") ; (?\俳 . "\\CID{13538}")
    (?\悲 . "\\CID{13491}")
    (?\排 . "\\CID{13487}")
    (?\腓 . "\\CID{13585}")
    (?\榧 . "\\CID{13553}")
    (?\琲 . "\\CID{13564}")
    (?\緋 . "\\CID{13494}")
    (?\罪 . "\\CID{13449}")
    (?\菲 . "\\CID{13589}")
    (?\蜚 . "\\CID{13592}")
    (?\裴 . "\\CID{13596}")
    (?\輩 . "\\CID{13488}")
    (?\耕 . "\\CID{13770}")
    (?\籍 . "\\CID{13878}")
    (?\耗 . "\\CID{14058}")
    ;; 兆  ?銚誂挑晁佻
    (?\兆 . "\\CID{13477}") (?\姚 . "\\CID{13530}") (?\桃 . "\\CID{13484}") (?\眺 . "\\CID{13478}") (?\窕 . "\\CID{13573}") (?\跳 . "\\CID{13480}")
    ;; 月
    (?\月 . "\\CID{13746}") (?\前 . "\\CID{13889}") (?\勝 . "\\CID{13829}") (?\箭 . "\\CID{7974}") (?\揃 . "\\CID{7975}") (?\萌 . "\\CID{14026}") (?\鵬 . "\\CID{14030}") (?\愉 . "\\CID{14067}") (?\明 . "\\CID{14052}") (?\朋 . "\\CID{14022}") (?\有 . "\\CID{14071}") (?\朝 . "\\CID{13931}") (?\潮 . "\\CID{13932}") (?\崩 . "\\CID{14020}") (?\棚 . "\\CID{7736}") (?\服 . "\\CID{14007}") (?\煎 . "\\CID{7718}") (?\繃 . "\\CID{14185}") (?\廟 . "\\CID{7786}") (?\嘲 . "\\CID{7821}") (?\藤 . "\\CID{13957}") (?\騰 . "\\CID{13961}") (?\期 . "\\CID{13702}")
    ;; 的
    (?\的 . "\\CID{13945}") (?\釣 . "\\CID{13941}") (?\約 . "\\CID{14062}") (?\凡 . "\\CID{14041}") (?\恐 . "\\CID{13721}") (?\酌 . "\\CID{13810}") (?\杓 . "\\CID{7695}") (?\灼 . "\\CID{7696}") (?\豹 . "\\CID{20222}") (?\帆 . "\\CID{13987}")
    (?\訊 . "\\CID{13860}")             ; 13861 
    (?\築 . "\\CID{13921}")
    (?\蔑 . "\\CID{14013}")
    ;; 黄
    (?\横 . "\\CID{8503}") (?\徳 . "\\UTF{5FB7}") (?\黄 . "\\UTF{9EC3}") (?\腰 . "\\CID{13777}")
    ;; 漢
    (?\漢 . "\\CID{13332}") (?\嘆 . "\\CID{13366}") (?\歎 . "\\CID{13915}") (?\難 . "\\CID{13374}") (?\謹 . "\\CID{13339}") (?\勤 . "\\UTF{FA34}") (?\灘 . "\\CID{7767}") (?\要 . "\\CID{14079}") (?\僅 . "\\CID{7662}")
    ;; 悦
    (?\悦 . "\\UTF{6085}") (?\増 . "\\UTF{589E}") (?\脱 . "\\UTF{812B}") (?\説 . "\\CID{13880}") (?\憎 . "\\CID{13363}") (?\益 . "\\CID{8571}") (?\溢 . "\\CID{7635}") (?\噂 . "\\CID{7642}") (?\僧 . "\\CID{13360}") (?\咲 . "\\CID{13788}") (?\鋭 . "\\CID{13652}") (?\税 . "\\CID{13875}") (?\閲 . "\\CID{13653}") (?\噌 . "\\CID{7721}") (?\層 . "\\CID{13361}") (?\贈 . "\\CID{13364}")
    (?\隊 . "\\CID{13907}") (?\尊 . "\\CID{13901}") ; 13902
    (?\猶 . "\\CID{14072}")                         ; 14073
    (?\甑 . "\\CID{13778}")                         ; 20284 7684
    (?\酋 . "\\CID{7698}")
    (?\樽 . "\\CID{7738}")
    (?\鄭 . "\\CID{7748}")
    (?\楢 . "\\CID{7768}")
    (?\鱒 . "\\CID{7796}")
    (?\猷 . "\\CID{14074}")             ; 7804
    (?\墜 . "\\CID{13937}")
    ;; 示偏
    (?\示 . "\\CID{19130}")
    (?\神 . "\\UTF{FA19}") (?\福 . "\\UTF{FA1B}") (?\祷 . "\\CID{7758}") (?\社 . "\\UTF{FA4C}") (?\祈 . "\\UTF{FA4E}") (?\祖 . "\\UTF{FA50}") (?\祝 . "\\UTF{FA51}") (?\視 . "\\CID{13346}") (?\祐 . "\\CID{13391}") (?\祥 . "\\CID{8581}") (?\祉 . "\\CID{13345}") (?\禍 . "\\CID{13325}") (?\祁 . "\\CID{7668}") (?\祇 . "\\CID{7659}") (?\禎 . "\\CID{13371}") (?\禰 . "\\CID{7769}") (?\榊 . "\\CID{7686}")
    ;; 羽
    (?\羽 . "\\UTF{FA1E}") (?\習 . "\\CID{13817}") (?\翻 . "\\CID{14040}") (?\弱 . "\\CID{13811}") (?\扇 . "\\CID{13883}") (?\翌 . "\\CID{14081}") (?\摺 . "\\CID{7713}") (?\擢 . "\\CID{7749}") (?\曜 . "\\CID{14077}") (?\濯 . "\\CID{7731}") (?\燿 . "\\CID{13562}") (?\翁 . "\\CID{13662}") (?\翠 . "\\CID{7712}") (?\翫 . "\\CID{7657}") (?\翰 . "\\CID{7656}") (?\翼 . "\\CID{14082}") (?\耀 . "\\CID{7806}") (?\謬 . "\\CID{7785}") (?\躍 . "\\CID{14063}") (?\溺 . "\\CID{7750}") (?\鰯 . "\\CID{7636}") 
    ;; 戸
    (?\戸 . "\\UTF{6236}") (?\篇 . "\\CID{7979}") (?\所 . "\\CID{13826}") (?\房 . "\\CID{14035}") (?\偏 . "\\CID{14014}") (?\涙 . "\\CID{13395}") (?\煽 . "\\CID{7972}") (?\戻 . "\\CID{13390}") (?\肩 . "\\CID{13752}") (?\扉 . "\\CID{7779}") (?\蝙 . "\\CID{8000}") (?\粐 . "\\CID{7849}") (?\雇 . "\\CID{13758}") (?\顧 . "\\CID{13759}") (?\騙 . "\\CID{8003}") (?\褊 . "\\CID{7864}") (?\捩 . "\\CID{7829}") (?\綟 . "\\CID{7853}") (?\編 . "\\CID{14015}") (?\翩 . "\\CID{7998}") (?\扁 . "\\CID{7991}") (?\芦 . "\\CID{7961}") (?\啓 . "\\CID{13738}") (?\扈 . "\\CID{7874}") (?\諞 . "\\CID{7866}")
    ;; 受、采       曖→?
    (?\受 . "\\CID{13813}") (?\采 . "\\CID{7685}") (?\菜 . "\\CID{13786}") (?\採 . "\\CID{13784}") (?\緩 . "\\CID{13690}") (?\彩 . "\\CID{13783}") (?\乳 . "\\CID{13968}") (?\浮 . "\\CID{14004}") (?\妥 . "\\CID{13903}") (?\暖 . "\\CID{13918}") (?\授 . "\\CID{13814}") (?\淫 . "\\CID{7637}") (?\援 . "\\CID{13655}") (?\媛 . "\\CID{7784}") (?\爵 . "\\CID{13808}") (?\埓 . "\\CID{4496}")
    ;; 麻
    (?\磨 . "\\CID{14042}") (?\摩 . "\\CID{14039}") (?\麻 . "\\CID{14044}") (?\魔 . "\\CID{14043}") (?\術 . "\\CID{13821}")
    ;; 之繞
    (?\通 . "\\CID{13939}") (?\道 . "\\CID{13963}") (?\進 . "\\CID{13855}") (?\樋 . "\\CID{7780}") (?\送 . "\\CID{13895}") (?\逗 . "\\CID{7711}") (?\逸 . "\\UTF{FA67}") (?\速 . "\\CID{13899}") (?\達 . "\\CID{13912}") (?\遠 . "\\CID{13658}") (?\追 . "\\CID{13938}") (?\迂 . "\\CID{7638}") (?\辻 . "\\CID{8267}") (?\近 . "\\CID{13730}") (?\週 . "\\CID{13819}") (?\迎 . "\\CID{13742}") (?\返 . "\\CID{14016}") (?\述 . "\\CID{13822}") (?\途 . "\\CID{13950}") (?\連 . "\\CID{14097}") (?\違 . "\\CID{13641}") (?\退 . "\\CID{13905}") (?\運 . "\\CID{13649}") (?\逃 . "\\CID{13959}") (?\過 . "\\CID{13669}") (?\迫 . "\\CID{13978}") (?\造 . "\\CID{13897}") (?\遂 . "\\CID{13864}") (?\避 . "\\CID{13991}") (?\選 . "\\CID{13887}") (?\蓮 . "\\CID{7811}") (?\遣 . "\\CID{13754}") (?\適 . "\\CID{13946}") (?\遮 . "\\CID{7694}") (?\遍 . "\\CID{14017}") (?\迦 . "\\CID{7647}") (?\蓬 . "\\CID{7794}") (?\逐 . "\\CID{13924}") (?\縫 . "\\CID{14024}") (?\遇 . "\\CID{13736}") (?\迄 . "\\CID{7980}") (?\遊 . "\\CID{14076}") (?\迷 . "\\CID{14054}") (?\逆 . "\\CID{13709}") (?\透 . "\\CID{13960}") (?\腿 . "\\CID{7728}") (?\槌 . "\\CID{7744}") (?\漣 . "\\CID{7809}") (?\謎 . "\\CID{7766}") (?\込 . "\\CID{13779}") (?\迅 . "\\CID{13862}") (?\辿 . "\\CID{7735}") (?\迪 . "\\CID{7871}") (?\迭 . "\\CID{13947}") (?\這 . "\\CID{7772}") (?\逝 . "\\CID{7714}") (?\逮 . "\\CID{13906}") (?\遁 . "\\CID{7763}") (?\逼 . "\\CID{7783}") (?\遡 . "\\CID{7722}") (?\遜 . "\\CID{7726}") (?\遭 . "\\CID{13896}") (?\遺 . "\\CID{13642}") (?\遵 . "\\CID{13824}") (?\遷 . "\\CID{13888}") (?\遼 . "\\CID{7808}") (?\還 . "\\CID{13692}") (?\鎚 . "\\CID{7745}") (?\鑓 . "\\CID{7801}") (?\巡 . "\\CID{13823}") (?\逢 . "\\CID{8266}") (?\導 . "\\CID{13962}")
    ;; 海
    (?\海 . "\\UTF{FA45}") (?\梅 . "\\CID{13375}") (?\敏 . "\\UTF{FA41}") (?\毎 . "\\UTF{6BCF}") (?\繁 . "\\UTF{FA59}") (?\悔 . "\\CID{13326}") (?\侮 . "\\CID{13382}") (?\晦 . "\\CID{7650}")
    ;; 青
    (?\青 . "\\CID{8695}") (?\靖 . "\\UTF{FA1C}") (?\晴 . "\\UTF{FA12}") (?\清 . "\\UTF{6DF8}") (?\情 . "\\CID{13842}") (?\錆 . "\\CID{7690}") (?\精 . "\\CID{8590}") (?\請 . "\\CID{13872}") (?\鯖 . "\\CID{7689}") (?\瀞 . "\\CID{7761}") 
    ;; 平
    (?\坪 . "\\CID{13940}") (?\平 . "\\CID{14011}") (?\半 . "\\CID{13986}") (?\消 . "\\CID{13836}") (?\伴 . "\\CID{13984}") (?\鞘 . "\\CID{7708}") (?\宵 . "\\CID{13831}") (?\梢 . "\\CID{7705}")  (?\削 . "\\CID{13789}") (?\判 . "\\CID{13985}") (?\邦 . "\\CID{14028}") (?\硝 . "\\CID{13837}") (?\評 . "\\CID{13999}") (?\肖 . "\\CID{13838}") (?\哨 . "\\CID{7703}") (?\尚 . "\\CID{13835}") (?\鎖 . "\\CID{13781}") (?\叛 . "\\CID{7978}") (?\屑 . "\\CID{7666}") (?\蛸 . "\\CID{7733}") (?\秤 . "\\CID{7773}") (?\廠 . "\\CID{7704}") (?\拳 . "\\CID{7969}") (?\倦 . "\\CID{7674}") (?\捲 . "\\CID{7676}") (?\弊 . "\\CID{14012}") (?\蔽 . "\\CID{7789}") (?\瞥 . "\\CID{7790}") (?\畔 . "\\CID{13988}")
    (?\虚 . "\\CID{13336}") (?\嘘 . "\\CID{7963}") (?\譜 . "\\CID{20218}") (?\繍 . "\\CID{7697}") (?\普 . "\\CID{20139}") (?\戯 . "\\CID{19381}")
    ;; 分
    (?\分 . "\\CID{13499}") (?\粉 . "\\CID{13502}") (?\紛 . "\\CID{13503}") (?\穴 . "\\CID{13434}") (?\公 . "\\CID{13440}") (?\控 . "\\CID{13766}") (?\空 . "\\CID{13735}") (?\鉛 . "\\CID{13417}") (?\雰 . "\\CID{13504}") (?\盆 . "\\CID{13508}") (?\頒 . "\\CID{13490}") (?\船 . "\\CID{13886}") (?\貧 . "\\CID{13496}") (?\沿 . "\\CID{13416}") (?\訟 . "\\CID{13462}")
    (?\捨 . "\\CID{13804}")
    ;; 包  ?忌把
    (?\改 . "\\CID{21678}") (?\包 . "\\CID{14019}") (?\抱 . "\\CID{14021}") (?\砲 . "\\CID{14023}") (?\胞 . "\\CID{14025}") (?\飽 . "\\CID{14029}") (?\泡 . "\\CID{7793}") (?\鞄 . "\\CID{7653}") (?\萢 . "\\CID{7999}") (?\庖 . "\\CID{7792}") (?\巷 . "\\CID{7679}") (?\撰 . "\\CID{7716}")  (?\巽 . "\\CID{7734}") (?\杞 . "\\CID{14140}") (?\港 . "\\CID{13769}") (?\配 . "\\CID{20236}") (?\起 . "\\CID{13704}")
    (?\紀 . "\\MJMZM{019956}") (?\妃 . "\\MJMZM{009561}") (?\記 . "\\MJMZM{024358}")
    ;; 及
    (?\及 . "\\CID{13710}") (?\吸 . "\\CID{13711}") (?\級 . "\\CID{13713}") (?\扱 . "\\CID{13637}") (?\汲 . "\\CID{7966}") (?\笈 . "\\CID{7967}") ; 20263
    ;; 歴
    (?\歴 . "\\CID{13398}") (?\暦 . "\\CID{13397}")
    ;; 直
    (?\直 . "\\CID{13934}") (?\値 . "\\CID{13919}") (?\置 . "\\CID{13920}") (?\殖 . "\\CID{13846}") (?\植 . "\\CID{13845}") (?\埴 . "\\CID{13464}") (?\稙 . "\\CID{13572}")
    (?\苅 . "\\CID{13687}")
    ;; 葛
    (?\葛 . "\\CID{7652}") (?\喝 . "\\CID{7651}") (?\褐 . "\\CID{13331}") (?\渇 . "\\CID{13330}") (?\謁 . "\\CID{13321}") (?\掲 . "\\CID{13340}")
    (?\考 . "\\CID{13445}")
    ;; 呉
    (?\呉 . "\\CID{13760}")
    (?\娯 . "\\CID{13761}")
    (?\誤 . "\\CID{13762}")
    (?\虞 . "\\CID{13734}")
    ;; 成
    (?\成 . "\\CID{13867}") (?\城 . "\\CID{13841}") (?\盛 . "\\CID{13868}") (?\誠 . "\\CID{13871}") (?\晟 . "\\CID{14136}") ; 13548 14137
    (?\姉 . "\\CID{14449}") (?\御 . "\\CID{20117}")
    (?\衆 . "\\CID{13818}")
    ;; 兼
    (?\嫌 . "\\CID{7675}") (?\兼 . "\\CID{13748}") (?\謙 . "\\CID{13753}") (?\鎌 . "\\CID{13686}") (?\廉 . "\\CID{14095}") (?\簾 . "\\CID{7981}") ; 20265 
    ;; 講
    (?\講 . "\\CID{13773}") (?\溝 . "\\CID{7681}") (?\構 . "\\CID{13767}") (?\篝 . "\\CID{7997}") (?\購 . "\\CID{13774}") (?\冓 . "\\CID{13521}") (?\媾 . "\\CID{7824}") (?\搆 . "\\CID{7830}") (?\覯 . "\\CID{7865}") (?\遘 . "\\CID{7873}")
    ;; 化
    (?\化 . "\\CID{13665}") (?\花 . "\\CID{13666}") (?\囮 . "\\CID{20099}") (?\埖 . "\\CID{20100}") (?\貨 . "\\CID{13668}") (?\錵 . "\\CID{20238}") (?\靴 . "\\CID{7667}")
    ;; 郎
    (?\郎 . "\\UTF{90DE}") (?\朗 . "\\CID{14098}") (?\廊 . "\\CID{13401}") (?\榔 . "\\CID{7812}") (?\螂 . "\\CID{7862}") 
    ;; 即
    (?\即 . "\\CID{13365}") (?\節 . "\\CID{13879}") (?\概 . "\\CID{13679}")
    (?\卿 . "\\CID{7661}")              ; 13719
    (?\慨 . "\\CID{13676}")             ; 13328
    (?\既 . "\\CID{13701}")             ; 13334
    (?\櫛 . "\\CID{13737}") (?\郷 . "\\CID{13725}") (?\厩 . "\\CID{7640}") 
    (?\響 . "\\CID{13726}")             ; 20245, 20246
    ;; 食
    (?\食 . "\\UTF{2EDD}") (?\飯 . "\\CID{20261}") (?\飼 . "\\UTF{FA2B}") (?\飴 . "\\CID{7634}") (?\蝕 . "\\CID{7709}") (?\喰 . "\\CID{7664}") (?\飾 . "\\CID{13844}") (?\餌 . "\\CID{13650}") (?\餓 . "\\CID{13672}")
    (?\館 . "\\CID{8702}")                           ; UTF{8212}
    (?\餐 . "\\CID{7970}") (?\饗 . "\\CID{13727}")   ; 20249 7968
    ;; 緑
    (?\緑 . "\\UTF{7DA0}") (?\塚 . "\\CID{8422}") (?\録 . "\\CID{13402}") (?\縁 . "\\CID{13322}") 
    ;; 歩
    (?\歩 . "\\UTF{6B65}") (?\渉 . "\\CID{13354}") (?\頻 . "\\CID{7788}") (?\賓 . "\\CID{13380}") (?\捗 . "\\CID{7743}") (?\瀕 . "\\CID{7787}")
    ;; 冬
    (?\冬 . "\\CID{13954}") (?\終 . "\\CID{13816}") (?\寒 . "\\CID{13688}") (?\柊 . "\\CID{7781}") (?\疼 . "\\CID{20175}") (?\螽 . "\\CID{14213}") (?\鮗 . "\\CID{7883}") (?\於 . "\\CID{13660}")
    ;; 内
    (?\内 . "\\CID{13966}") (?\肉 . "\\CID{13967}") (?\納 . "\\CID{13972}") (?\丙 . "\\CID{14009}") (?\病 . "\\CID{14001}") (?\柄 . "\\CID{20145}")
    ;;
    (?\券 . "\\CID{13749}") (?\契 . "\\CID{13739}") ; 20104 
    (?\喫 . "\\CID{13707}")                         ; 20092
    ;;
    (?\状 . "\\CID{13355}")
    ;;
    (?\巣 . "\\UTF{5DE2}")
    ;; 牙
    (?\牙 . "\\CID{7965}") (?\邪 . "\\CID{13806}") (?\冴 . "\\CID{13787}") (?\穿 . "\\CID{7973}") (?\芽 . "\\CID{13670}") (?\訝 . "\\CID{20268}") (?\雅 . "\\CID{13671}") (?\鴉 . "\\CID{14272}")
    ;; 彦
    (?\彦 . "\\CID{13996}") (?\諺 . "\\CID{7678}") (?\産 . "\\CID{13790}") (?\薩 . "\\CID{7688}") (?\隆 . "\\CID{13393}") ; 8686
    ;; 次
    (?\次 . "\\CID{13799}") (?\姿 . "\\CID{13792}") (?\諮 . "\\CID{13795}") (?\資 . "\\CID{13797}") (?\茨 . "\\CID{7962}") 
    ;; 亡
    (?\亡 . "\\CID{14031}") (?\匹 . "\\CID{13994}") (?\忘 . "\\CID{14033}") (?\忙 . "\\CID{14034}") (?\荒 . "\\CID{13772}") (?\芒 . "\\CID{20292}")
    (?\妄 . "\\CID{14055}") (?\慌 . "\\CID{13764}") (?\盲 . "\\CID{14057}") (?\網 . "\\CID{20189}") ; (?\魍 . "\\CID{13629}")
    ;; 匚構えへ
    (?\匚 . "\\CID{04307}")
    (?\區 . "\\MJMZM{007833}") (?\慝 . "\\MJMZM{011833}") (?\歐 . "\\MJMZM{014802}") (?\毆 . "\\MJMZM{014930}") ;  (?\區 . "\\CID{13524}")
    (?\匿 . "\\MJMZM{007831}")    ; "\\CID{20087}"
    (?\鴎 . "\\MJMZM{029691}")    ; "\\CID{7646}"
    (?\躯 . "\\MJMZM{025532}")    ; "\\CID{7663}"
    (?\望 . "\\CID{14037}") (?\聖 . "\\CID{13869}") (?\害 . "\\CID{13675}") (?\程 . "\\CID{13944}")
    (?\冒 . "\\CID{14038}") (?\帽 . "\\CID{14032}")
    ;; 刃
    (?\刃 . "\\CID{13858}")             ; 4250
    (?\忍 . "\\CID{13969}") 
    (?\籾 . "\\CID{7800}")              ; 20293
    (?\認 . "\\CID{13970}")
    (?\丹 . "\\CID{13914}") (?\垂 . "\\UTF{57C0}")
    ;; 急
    (?\急 . "\\CID{13712}") (?\婦 . "\\CID{14002}") (?\尋 . "\\CID{13859}") (?\雪 . "\\CID{13881}") (?\鱈 . "\\CID{7737}") (?\掃 . "\\CID{13891}") (?\慧 . "\\CID{13741}") (?\侵 . "\\CID{13851}") (?\浸 . "\\CID{13853}")
    ;; 周
    (?\周 . "\\CID{13815}") (?\彫 . "\\CID{13928}") (?\調 . "\\CID{13933}") (?\鯛 . "\\CID{13908}") (?\凋 . "\\CID{7742}") (?\告 . "\\CID{13775}") (?\鵠 . "\\CID{7683}")
    (?\敷 . "\\CID{14003}") (?\薄 . "\\CID{13977}") (?\博 . "\\CID{13976}") (?\簿 . "\\CID{14018}") (?\縛 . "\\CID{13979}")
    ;; 主
    (?\主 . "\\CID{13812}") (?\往 . "\\CID{13661}") (?\住 . "\\CID{13820}") (?\柱 . "\\CID{13925}") (?\註 . "\\CID{7740}") (?\駐 . "\\CID{13927}") (?\注 . "\\CID{13926}")
    ;; 巨
    (?\巨 . "\\CID{13714}") (?\距 . "\\CID{13716}") (?\拒 . "\\CID{13715}") (?\矩 . "\\CID{13732}") (?\苣 . "\\CID{14200}") ; 炬
    ;; 并
    (?\并 . "\\CID{19346}") (?\屏 . "\\CID{7826}") (?\瓶 . "\\CID{8564}") (?\併 . "\\CID{13383}") (?\塀 . "\\CID{13384}") (?\絣 . "\\CID{14180}") (?\駢 . "\\CID{14266}") (?\胼 . "\\CID{14194}") (?\研 . "\\UTF{784F}")
    ;; 灰
    (?\灰 . "\\CID{13674}") (?\炭 . "\\CID{13916}") (?\恢 . "\\CID{20269}")
    ;; 免
    (?\晩 . "\\CID{13377}") (?\卑 . "\\CID{13378}") (?\碑 . "\\CID{13379}") (?\勉 . "\\CID{13385}") (?\免 . "\\CID{13389}") (?\牌 . "\\CID{7771}") (?\挽 . "\\CID{7778}") (?\稗 . "\\CID{7782}") (?\娩 . "\\CID{7791}") ; (?\晩 . "\\CID{13989}")
    ;; ?
    (?\煙 . "\\CID{13657}")
    (?\溌 . "\\CID{7776}") (?\涜 . "\\CID{7760}")
    (?\強 . "\\CID{13720}")
    (?\那 . "\\CID{7765}")
    (?\箪 . "\\CID{7739}")
    (?\掴 . "\\CID{7747}")
    (?\唖 . "\\CID{7633}")
    (?\瞬 . "\\CID{13458}")
    (?\姫 . "\\CID{13997}")
    (?\剥 . "\\CID{7774}")
    (?\痩 . "\\CID{13893}")             ; 7725
    (?\温 . "\\CID{13324}")
    (?\撃 . "\\CID{13341}")
    (?\穀 . "\\CID{13343}")
    (?\殺 . "\\CID{13344}")
    (?\教 . "\\CID{8471}")
    (?\徴 . "\\CID{13368}") (?\懲 . "\\CID{13369}")
    (?\闘 . "\\CID{7297}")
    (?\虜 . "\\CID{13394}")
    (?\焔 . "\\CID{7644}")
    (?\涼 . "\\CID{4241}")
    (?\頬 . "\\CID{7795}") 
    (?\派 . "\\CID{13974}")
    (?\蝋 . "\\CID{7813}")
    (?\貌 . "\\CID{6752}")
    (?\微 . "\\CID{13992}")
    (?\旅 . "\\CID{14088}")
    (?\虐 . "\\CID{13708}")
    (?\憲 . "\\CID{20120}")
    (?\掻 . "\\CID{7724}")
    (?\尨 . "\\CID{7988}")
    (?\逞 . "\\CID{14230}")
    (?\噛 . "\\UTF{5699}")
    (?\顛 . "\\CID{7752}")
    (?\痳 . "\\CID{5755}")
    (?\嚔 . "\\CID{4445}")
    (?\諭 . "\\CID{14068}")
    (?\輸 . "\\CID{14069}")
    (?\愈 . "\\CID{7802}")
    (?\癒 . "\\CID{7803}")
    (?\鹸 . "\\CID{7677}")
    (?\啄 . "\\CID{7730}")
    (?\延 . "\\CID{13654}") (?\誕 . "\\CID{13917}")
    ;; JIS2004 蟹釜牽膏 棘橙祟靄 
    (?\襖 . "\\CID{7645}")
    (?\徽 . "\\CID{7658}")
    (?\灸 . "\\CID{20270}")
    (?\粂 . "\\CID{20272}")
    (?\隙 . "\\CID{20273}")
    (?\楯 . "\\CID{13460}")
    (?\叟 . "\\CID{14111}")
    (?\囀 . "\\CID{14116}")             ; 20096
    (?\甕 . "\\CID{7843}")              ; 20267
    (?\竈 . "\\CID{20285}")
    (?\艘 . "\\CID{14196}")
    (?\蜃 . "\\CID{20296}")
    (?\蠅 . "\\CID{14212}")             ; 20295
    (?\隔 . "\\CID{13683}")
    (?\歳 . "\\CID{13785}")
    (?\絋 . "\\CID{8359}")
    (?\屡 . "\\CID{7693}")
    (?\軟 . "\\CID{17144}")
    (?\穎 . "\\CID{1266}")
    (?\填 . "\\CID{7751}")
    (?\羈 . "\\CID{18658}")
    ;; (?\蓋 . "\\UTF{8462}")
    ;; (?\剪 . "\\CID{6197}")
    ;; (?\拐 . "\\CID{7649}")
    ;; (?\匂 . "\\UTF{5300}")
    ;; (?\叉 . "\\CID{20281}")
    ;; (?\筵 . "\\CID{20266}")
    (?\廻 . "\\CID{17605}")
    (?\健 . "\\CID{13435}")
    ;; (?\鍵 . "\\CID{20276}")
    ;; (?\梗 . "\\CID{20279}")
    ;; (?\鮫 . "\\CID{20280}")
    ;; (?\杖 . "\\CID{20282}")
    ;; (?\挺 . "\\CID{20286}")
    ;; (?\鞭 . "\\CID{03628}")
    ;; (?\咬 . "\\CID{20277}")
    ;; (?\狡 . "\\CID{20278}")
    ;; (?\甦 . "\\CID{05707}")
    ;; (?\腱 . "\\CID{20275}")
    ;; (?\虔 . "\\CID{20274}")
    ;; (?\爺 . "\\CID{20294}")
    ;; (?\駁 . "\\CID{20288}")
    ;; (?\斧 . "\\CID{20289}")
    ;; (?\靱 . "\\CID{13624}")           ; 7710
    (?\象 . "\\CID{20220}")
    (?\像 . "\\MJMZM{007063}")
    ;; (?\像 . "\\CID{13474}") ; ?
    (?\率 . "\\CID{14085}")
    (?\並 . "\\CID{20074}")
    (?\飢 . "\\CID{13705}")
    (?\諂 . "\\CID{15143}")
    ;; 吉
    (?\吉 . "\\CID{13706}")
    ;; 窓
    (?\窓 . ?\窗)
    ;; 明治元譯新約全書のみの特例
    (?\与 . ?\予)
    (?\収 . ?\收)           ; 時々に使ひ分けられてゐるため注意のこと
    ))

;;; Just for util（使ってゐない）
(setq char-to-utf-table
  '((?\都 . "\\UTF{FA26}")
    (?\寛 . "\\UTF{5BEC}")
    (?\突 . "\\UTF{FA55}")
    (?\黒 . "\\UTF{9ED1}") (?\練 . "\\UTF{FA57}") (?\薫 . "\\UTF{85B0}")
    (?\徳 . "\\UTF{5FB7}") (?\黄 . "\\UTF{9EC3}")
    (?\勤 . "\\UTF{FA34}")
    (?\悦 . "\\UTF{6085}") (?\増 . "\\UTF{589E}") (?\脱 . "\\UTF{812B}")
    (?\神 . "\\UTF{FA19}") (?\福 . "\\UTF{FA1B}") (?\社 . "\\UTF{FA4C}") (?\祈 . "\\UTF{FA4E}") (?\祖 . "\\UTF{FA50}") (?\祝 . "\\UTF{FA51}")
    (?\羽 . "\\UTF{FA1E}")
    (?\戸 . "\\UTF{6236}") (?\逸 . "\\UTF{FA67}")
    (?\海 . "\\UTF{FA45}") (?\敏 . "\\UTF{FA41}") (?\毎 . "\\UTF{6BCF}") (?\繁 . "\\UTF{FA59}")
    (?\靖 . "\\UTF{FA1C}") (?\晴 . "\\UTF{FA12}") (?\清 . "\\UTF{6DF8}")
    (?\郎 . "\\UTF{90DE}")
    (?\食 . "\\UTF{2EDD}") (?\飼 . "\\UTF{FA2B}")
    (?\緑 . "\\UTF{7DA0}")
    (?\歩 . "\\UTF{6B65}")
    (?\巣 . "\\UTF{5DE2}")
    (?\垂 . "\\UTF{57C0}")
    (?\研 . "\\UTF{784F}")
    (?\噛 . "\\UTF{5699}")
    ))

(setq char-to-mjm-table
      '((?\初 . "\\GWI{u521d-v}")       ; bxglyphwikiの利用
        (?\又 . "\\GWI{u53c8-var-001}") ; "\\MJMZM{007986}"
        (?\𤧚 . "\\GWI{u249da-jv}")     ; bxglyphwikiの利用 (\UTFで出ない)
        (?\鸆 . "\\GWI{u9e06}")         ; https://glyphwiki.org/wiki/u9e06
        (?\鸅 . "\\MJMZM{029740}")
        (?\朕 . "\\MJMZM{013539}")
        (?\都 . "\\MJMZM{030235}")      ; 以下UTFコードを持つ
        (?\寛 . "\\MJMZM{010237}")      ; MJ010236
        (?\突 . "\\MJMZM{030283}")      ; MJ030283
        (?\黒 . "\\MJMZM{029892}")
        (?\練 . "\\MJMZM{030286}")      ; MJ030286
        (?\薫 . "\\MJMZM{022986}")
        (?\徳 . "\\MJMZM{011356}")
        (?\黄 . "\\MJMZM{029868}")
        (?\勤 . "\\MJMZM{030248}")
        (?\悦 . "\\MJMZM{011574}")      ; MJ011574
        (?\増 . "\\MJMZM{009289}")
        (?\脱 . "\\MJMZM{021075}")
        (?\神 . "\\MJMZM{030211}")
        (?\福 . "\\MJMZM{030213}")
        (?\社 . "\\MJMZM{030274}")      ; MJ030274
        (?\祈 . "\\MJMZM{030276}")      ; MJ030276
        (?\祖 . "\\MJMZM{030278}")      ; MJ030278
        (?\祝 . "\\MJMZM{030279}")      ; MJ030279
        (?\羽 . "\\MJMZM{030216}")
        (?\戸 . "\\MJMZM{012107}")
        (?\逸 . "\\MJMZM{030303}")
        (?\達 . "\\MJMZM{026061}")      ; MJ026061
        (?\海 . "\\MJMZM{030266}")      ; MJ030266
        (?\敏 . "\\MJMZM{030262}")
        (?\毎 . "\\MJMZM{014940}")
        (?\繁 . "\\MJMZM{030288}")      ; MJ030288
        (?\靖 . "\\MJMZM{030214}")
        (?\晴 . "\\MJMZM{030199}")
        (?\清 . "\\MJMZM{015507}")
        (?\郎 . "\\MJMZM{026309}")
        (?\食 . "\\MJMZM{028339}")      ; MJ028339
        (?\餓 . "\\MJMZM{028422}")      ; MJ028422
        (?\飼 . "\\MJMZM{030241}")
        (?\緑 . "\\MJMZM{020170}")
        (?\歩 . "\\MJMZM{014827}")
        (?\巣 . "\\MJMZM{010769}")
        (?\垂 . "\\MJMZM{009047}")
        (?\研 . "\\MJMZM{018495}")
        (?\噛 . "\\MJMZM{008740}")
        (?\緒 . "\\MJMZM{020239}")
        (?\絶 . "\\MJMZM{020072}")      ; MJ020072
        (?\頼 . "\\MJMZM{025096}")
        (?\瀬 . "\\MJMZM{016185}")      ; MJ016185
        (?\眺 . ?\眺)           ; https://glyphwiki.org/wiki/aj1-13478
        (?\凡 . ?\凡)           ; https://glyphwiki.org/wiki/aj1-14041
        (?\帆 . ?\帆)           ; https://glyphwiki.org/wiki/aj1-13987
        (?\説 . "\\MJMZM{024533}")      ; MJ024533
        (?\鋭 . "\\MJMZM{026853}")      ; MJ026853
        (?\税 . "\\MJMZM{019067}")
        (?\閲 . "\\MJMZM{027465}")
        (?\示 . ?\示)           ; https://glyphwiki.org/wiki/aj1-19130
        (?\祷 . "\\MJMZM{018972}")      ; MJ018972
        (?\涙 . "\\MJMZM{015472}")      ; MJ015472
        (?\受 . ?\受)           ; https://glyphwiki.org/wiki/aj1-13813
        (?\授 . ?\授)           ; https://glyphwiki.org/wiki/aj1-13814
        (?\青 . "\\MJMZM{027896}")
        (?\尚 . "\\MJMZM{010295}")      ; MJ010295
        (?\虚 . "\\MJMZM{023293}")      ; MJ023293
        (?\戯 . "\\MJMZM{012100}")      ; MJ012100
        (?\嘘 . "\\MJMZM{008663}")
        (?\繍 . "\\MJMZM{020415}")
        (?\普 . ?\普)           ; https://glyphwiki.org/wiki/aj1-20139
        (?\歴 . "\\MJMZM{014848}")
        (?\暦 . "\\MJMZM{013423}")      ; MJ013423
        (?\渇 . "\\MJMZM{015573}")
        (?\掲 . "\\MJMZM{012573}")
        (?\呉 . "\\MJMZM{008108}")
        (?\娯 . "\\MJMZM{009717}")
        (?\姉 . "\\MJMZM{009633}")
        (?\郷 . "\\MJMZM{026367}")      ; MJ026366, MJ026368, MJ026369
        (?\録 . "\\MJMZM{026948}")      ; MJ026948
        (?\渉 . "\\MJMZM{015392}")
        (?\病 . ?\病)           ; https://glyphwiki.org/wiki/aj1-14001
        (?\状 . "\\MJMZM{016907}")
        (?\彦 . "\\MJMZM{011257}")
        (?\産 . "\\MJMZM{017621}")
        (?\告 . "\\MJMZM{008122}")
        (?\并 . "\\MJMZM{010950}")      ; MJ010950
        (?\瓶 . "\\MJMZM{017573}")      ; MJ017573
        (?\併 . "\\MJMZM{006816}")      ; MJ006816
        (?\駢 . "\\MJMZM{028723}")      ; MJ028723
        (?\胼 . "\\MJMZM{021098}")
        (?\晩 . "\\MJMZM{013283}")
        (?\溌 . "\\MJMZM{015891}")
        (?\涜 . "\\MJMZM{016125}")
        (?\強 . "\\MJMZM{011199}")
        (?\箪 . "\\MJMZM{019684}")
        (?\掴 . "\\MJMZM{012675}")
        (?\唖 . "\\MJMZM{008406}")
        (?\姫 . "\\MJMZM{009671}")      ; MJ009671
        (?\剥 . "\\MJMZM{007547}")
        (?\痩 . "\\MJMZM{017889}")      ; MJ017889
        (?\温 . "\\MJMZM{015705}")      ; MJ015705
        (?\撃 . "\\MJMZM{012817}")
        (?\教 . "\\MJMZM{012975}")      ; MJ012975
        (?\徴 . "\\MJMZM{011354}")      ; MJ011354
        (?\闘 . "\\MJMZM{028989}")      ; MJ028989
        (?\焔 . "\\MJMZM{016452}")
        (?\涼 . "\\MJMZM{007390}")      ; MJ007390
        (?\頬 . "\\MJMZM{028209}")
        (?\鴎 . "\\MJMZM{029691}")
        (?\蝋 . "\\MJMZM{023805}")
        (?\貌 . "\\MJMZM{024977}")      ; MJ024977
        (?\掻 . "\\MJMZM{012609}")
        (?\痳 . "\\MJMZM{017830}")      ; MJ017830
        (?\鹸 . "\\MJMZM{029776}")      ; MJ029776
        (?\絋 . "\\MJMZM{019997}")
        (?\屡 . "\\MJMZM{010376}")
        (?\軟 . "\\MJMZM{025656}")      ; MJ025656
        (?\穎 . "\\MJMZM{028215}")      ; MJ028215
        (?\填 . "\\MJMZM{009224}")
        (?\羈 . "\\MJMZM{024216}")   ; MJ024216
        (?\者 . "\\MJMZM{030290}")   ; 以下異字体を持つがUTFコード無し
        (?\著 . "\\MJMZM{022327}")   ; MJ030294
        (?\煮 . "\\MJMZM{030269}") 
        (?\暑 . "\\MJMZM{030264}") 
        (?\署 . "\\MJMZM{030289}") 
        (?\諸 . "\\MJMZM{030226}") 
        (?\渚 . "\\MJMZM{030267}") 
        (?\猪 . "\\MJMZM{030208}") 
        (?\器 . "\\MJMZM{030252}") 
        (?\類 . "\\MJMZM{030188}") 
        (?\臭 . "\\MJMZM{030291}") 
        (?\龍 . "\\MJMZM{030124}")      ; MJ030124
        (?\墨 . "\\MJMZM{030254}") 
        (?\欄 . "\\MJMZM{030183}") 
        (?\漢 . "\\MJMZM{030268}") 
        (?\嘆 . "\\MJMZM{030251}") 
        (?\難 . "\\MJMZM{030304}") 
        (?\謹 . "\\MJMZM{030298}") 
        (?\憎 . "\\MJMZM{030259}") 
        (?\益 . "\\MJMZM{030209}") 
        (?\僧 . "\\MJMZM{030245}") 
        (?\層 . "\\MJMZM{030255}") 
        (?\贈 . "\\MJMZM{030301}") 
        (?\視 . "\\MJMZM{030296}") 
        (?\祐 . "\\MJMZM{030277}") 
        (?\祥 . "\\MJMZM{030212}") 
        (?\祉 . "\\MJMZM{030275}") 
        (?\禍 . "\\MJMZM{030280}") 
        (?\禎 . "\\MJMZM{030281}") 
        (?\梅 . "\\MJMZM{030265}") 
        (?\悔 . "\\MJMZM{030257}") 
        (?\侮 . "\\MJMZM{030244}") 
        (?\精 . "\\MJMZM{030215}") 
        (?\喝 . "\\MJMZM{030250}") 
        (?\褐 . "\\MJMZM{030295}") 
        (?\謁 . "\\MJMZM{030297}") 
        (?\朗 . "\\MJMZM{030185}") 
        (?\廊 . "\\MJMZM{030184}") 
        (?\節 . "\\MJMZM{030285}") 
        (?\慨 . "\\MJMZM{030258}") 
        (?\既 . "\\MJMZM{030263}") 
        (?\響 . "\\MJMZM{030305}") 
        (?\飯 . "\\MJMZM{030240}") 
        (?\館 . "\\MJMZM{030242}") 
        (?\塚 . "\\MJMZM{030194}") 
        (?\頻 . "\\MJMZM{030307}") 
        (?\賓 . "\\MJMZM{030299}") 
        (?\隆 . "\\MJMZM{030190}") 
        (?\塀 . "\\MJMZM{030253}") 
        (?\卑 . "\\MJMZM{030249}") 
        (?\碑 . "\\MJMZM{030272}") 
        (?\勉 . "\\MJMZM{030247}") 
        (?\免 . "\\MJMZM{030246}") 
        (?\穀 . "\\MJMZM{030282}") 
        (?\殺 . "\\MJMZM{030187}") 
        (?\懲 . "\\MJMZM{030260}") 
        (?\虜 . "\\MJMZM{030186}")
        (?\堵 . "\\MJMZM{009177}")
        (?\箸 . "\\MJMZM{019568}")
        (?\儲 . "\\MJMZM{007191}")
        (?\屠 . "\\MJMZM{010374}")
        (?\曙 . "\\MJMZM{013448}")
        (?\瀦 . "\\MJMZM{016176}")
        (?\薯 . "\\MJMZM{022985}")
        (?\藷 . "\\MJMZM{023117}")
        (?\賭 . "\\MJMZM{025088}")
        (?\音 . "\\MJMZM{028123}")
        (?\暗 . "\\MJMZM{013357}")
        (?\意 . "\\MJMZM{011734}")
        (?\帝 . "\\MJMZM{010834}")
        (?\商 . "\\MJMZM{008377}")      ; MJ008377
        (?\憶 . "\\MJMZM{011957}")
        (?\勳 . "\\MJMZM{007733}")      ; MJ007733
        (?\煉 . "\\MJMZM{016475}")
        (?\黛 . "\\MJMZM{029906}")
        (?\蘭 . "\\MJMZM{023222}")
        (?\危 . "\\MJMZM{007887}")
        (?\兔 . "\\MJMZM{007241}")
        (?\兎 . "\\MJMZM{007231}")
        (?\負 . "\\MJMZM{025000}")
        (?\冤 . "\\MJMZM{007346}")      ; MJ007346
        (?\頓 . "\\MJMZM{028173}")
        (?\噸 . "\\MJMZM{008705}")
        (?\文 . "\\MJMZM{013041}")
        (?\蚊 . "\\MJMZM{023342}")
        (?\交 . "\\MJMZM{006469}")
        (?\便 . "\\MJMZM{006750}")
        (?\較 . "\\MJMZM{025606}")
        (?\史 . "\\MJMZM{008042}")
        (?\使 . "\\MJMZM{006688}")
        (?\紋 . "\\MJMZM{019978}")
        (?\更 . "\\MJMZM{013483}")
        (?\丈 . "\\MJMZM{006304}")
        (?\更 . "\\MJMZM{013483}")
        (?\校 . "\\MJMZM{013847}")
        (?\父 . "\\MJMZM{016750}")
        (?\絞 . "\\MJMZM{020086}")
        (?\斐 . "\\MJMZM{013053}")
        (?\建 . "\\MJMZM{011110}")
        (?\庭 . "\\MJMZM{011005}")
        (?\全 . "\\MJMZM{007267}")
        (?\詮 . "\\MJMZM{024459}")
        (?\斜 . "\\MJMZM{013064}")
        (?\敢 . "\\MJMZM{057594}")      ; MJ057594
        (?\橄 . "\\MJMZM{014489}")      ; MJ014489
        (?\誹 . "\\MJMZM{024551}")
        (?\徘 . "\\MJMZM{011316}")
        (?\悲 . "\\MJMZM{011625}")
        (?\排 . "\\MJMZM{012472}")
        (?\腓 . "\\MJMZM{021120}")
        (?\榧 . "\\MJMZM{014291}")
        (?\琲 . "\\MJMZM{017304}")
        (?\緋 . "\\MJMZM{020229}")
        (?\罪 . "\\MJMZM{020556}")
        (?\菲 . "\\MJMZM{022163}")
        (?\蜚 . "\\MJMZM{023499}")
        (?\裴 . "\\MJMZM{024030}")
        (?\輩 . "\\MJMZM{025651}")
        (?\兆 . "\\MJMZM{007218}")
        (?\姚 . "\\MJMZM{009651}")
        (?\窕 . "\\MJMZM{019241}")
        (?\跳 . "\\MJMZM{025315}")
        (?\月 . "\\MJMZM{013519}")
        (?\前 . "\\MJMZM{007531}")
        (?\勝 . "\\MJMZM{007700}")
        (?\騰 . "\\MJMZM{028769}")
        (?\箭 . "\\MJMZM{019555}")
        (?\揃 . "\\MJMZM{012521}")
        (?\萌 . "\\MJMZM{022213}") ; https://glyphwiki.org/wiki/aj1-14026
        (?\鵬 . "\\MJMZM{029562}")
        (?\愉 . "\\MJMZM{011725}")
        (?\明 . "\\MJMZM{013202}")
        (?\朋 . "\\MJMZM{013524}")
        (?\有 . "\\MJMZM{013522}")
        (?\朝 . "\\MJMZM{013557}")      ; MJ013557
        (?\潮 . "\\MJMZM{015936}")
        (?\崩 . "\\MJMZM{010573}")
        (?\棚 . "\\MJMZM{014059}")
        (?\服 . "\\MJMZM{013527}")
        (?\煎 . "\\MJMZM{016480}")
        (?\繃 . "\\MJMZM{020379}")
        (?\廟 . "\\MJMZM{011069}")
        (?\嘲 . "\\MJMZM{008622}")
        (?\期 . "\\MJMZM{013563}")
        (?\藤 . "\\MJMZM{023080}")      ; MJ023080
        (?\的 . "\\MJMZM{017994}")
        (?\釣 . "\\MJMZM{026616}")
        (?\約 . "\\MJMZM{019966}")
        (?\恐 . "\\MJMZM{057504}")
        (?\酌 . "\\MJMZM{026437}")
        (?\杓 . "\\MJMZM{013616}")
        (?\灼 . "\\MJMZM{016282}")
        (?\豹 . "\\MJMZM{024955}")
        (?\訊 . "\\MJMZM{024341}")
        (?\築 . "\\MJMZM{019593}")
        (?\蔑 . "\\MJMZM{022669}")      ; MJ022669
        (?\横 . "\\MJMZM{014456}")
        (?\腰 . "\\MJMZM{021152}")
        (?\歎 . "\\MJMZM{014799}")      ; MJ014799
        (?\灘 . "\\MJMZM{016238}")
        (?\要 . "\\MJMZM{024200}")
        (?\僅 . "\\MJMZM{007049}")
        (?\溢 . "\\MJMZM{015694}")
        (?\噂 . "\\MJMZM{008643}")
        (?\咲 . "\\MJMZM{008235}")
        (?\噌 . "\\MJMZM{008657}")
        (?\隊 . "\\MJMZM{027644}")
        (?\尊 . "\\MJMZM{010277}")
        (?\猶 . "\\MJMZM{017025}")
        (?\甑 . "\\MJMZM{017602}")
        (?\酋 . "\\MJMZM{026434}")      ; MJ026434
        (?\樽 . "\\MJMZM{014480}")
        (?\鄭 . "\\MJMZM{026398}")
        (?\楢 . "\\MJMZM{014215}")
        (?\鱒 . "\\MJMZM{029360}")      ; MJ029360
        (?\猷 . "\\MJMZM{017027}")
        (?\墜 . "\\MJMZM{009285}")
        (?\祁 . "\\MJMZM{018763}")
        (?\祇 . "\\MJMZM{018771}")
        (?\禰 . "\\MJMZM{018969}")      ; MJ018969
        (?\榊 . "\\MJMZM{014256}")
        (?\習 . "\\MJMZM{020682}")
        (?\翻 . "\\MJMZM{020739}")
        (?\弱 . "\\MJMZM{011190}")
        (?\扇 . "\\MJMZM{012129}")
        (?\翌 . "\\MJMZM{020675}")
        (?\摺 . "\\MJMZM{012732}")
        (?\擢 . "\\MJMZM{012842}")
        (?\曜 . "\\MJMZM{013453}")
        (?\濯 . "\\MJMZM{016099}")
        (?\燿 . "\\MJMZM{016682}")
        (?\翁 . "\\MJMZM{020659}")      ; MJ020659
        (?\翠 . "\\MJMZM{020696}")
        (?\翫 . "\\MJMZM{020718}")
        (?\翰 . "\\MJMZM{020725}")
        (?\翼 . "\\MJMZM{020743}")      ; MJ020743
        (?\耀 . "\\MJMZM{020749}")
        (?\謬 . "\\MJMZM{024714}")
        (?\躍 . "\\MJMZM{025470}")
        (?\溺 . "\\MJMZM{015723}")
        (?\鰯 . "\\MJMZM{029312}")
        (?\篇 . "\\MJMZM{019589}")
        (?\所 . "\\MJMZM{012119}")
        (?\房 . "\\MJMZM{012116}")      ; MJ012116
        (?\偏 . "\\MJMZM{006907}")
        (?\諞 . "\\MJMZM{024598}")      ; MJ024598
        (?\煽 . "\\MJMZM{016532}")      ; MJ016532
        (?\戻 . "\\MJMZM{012111}")
        (?\肩 . "\\MJMZM{020948}")
        (?\扉 . "\\MJMZM{012134}")      ; MJ012134
        (?\蝙 . "\\MJMZM{023572}")
        (?\粐 . "\\MJMZM{019828}")
        (?\雇 . "\\MJMZM{027725}")
        (?\顧 . "\\MJMZM{028272}")
        (?\騙 . "\\MJMZM{028741}")
        (?\褊 . "\\MJMZM{024055}")
        (?\捩 . "\\MJMZM{012427}")
        (?\綟 . "\\MJMZM{020169}")
        (?\編 . "\\MJMZM{020261}")
        (?\翩 . "\\MJMZM{020712}")
        (?\扁 . "\\MJMZM{012121}")
        (?\芦 . "\\MJMZM{021563}")
        (?\啓 . "\\MJMZM{008391}")
        (?\扈 . "\\MJMZM{012131}")
        (?\采 . "\\MJMZM{026587}")
        (?\菜 . "\\MJMZM{022110}")      ; MJ022111
        (?\採 . "\\MJMZM{012488}")
        (?\緩 . "\\MJMZM{020263}")
        (?\彩 . "\\MJMZM{011260}")
        (?\乳 . "\\MJMZM{006416}")
        (?\浮 . "\\MJMZM{015361}")
        (?\妥 . "\\MJMZM{009598}")
        (?\暖 . "\\MJMZM{013354}")
        (?\淫 . "\\MJMZM{015492}")
        (?\援 . "\\MJMZM{012581}")
        (?\媛 . "\\MJMZM{009851}")
        (?\爵 . "\\MJMZM{016748}")
        (?\埓 . "\\MJMZM{009069}")
        (?\磨 . "\\MJMZM{018654}")
        (?\摩 . "\\MJMZM{012708}")
        (?\麻 . "\\MJMZM{029854}")
        (?\魔 . "\\MJMZM{029039}")
        (?\術 . "\\MJMZM{023870}")      ; MJ023870
        (?\通 . "\\MJMZM{025933}")
        (?\道 . "\\MJMZM{026059}")
        (?\進 . "\\MJMZM{025980}")
        (?\樋 . "\\MJMZM{014417}")
        (?\送 . "\\MJMZM{025881}")
        (?\逗 . "\\MJMZM{025927}")
        (?\速 . "\\MJMZM{025945}")
        (?\遠 . "\\MJMZM{026087}")
        (?\追 . "\\MJMZM{025872}")
        (?\迂 . "\\MJMZM{025773}")
        (?\辻 . "\\MJMZM{025761}")
        (?\近 . "\\MJMZM{025799}")
        (?\週 . "\\MJMZM{025978}")
        (?\迎 . "\\MJMZM{025794}")
        (?\返 . "\\MJMZM{025806}")
        (?\述 . "\\MJMZM{025852}")      ; MJ025852
        (?\途 . "\\MJMZM{025920}")
        (?\連 . "\\MJMZM{025954}")
        (?\違 . "\\MJMZM{026065}")
        (?\退 . "\\MJMZM{025879}")
        (?\運 . "\\MJMZM{026035}")
        (?\逃 . "\\MJMZM{025885}")
        (?\過 . "\\MJMZM{026042}")      ; MJ026042
        (?\迫 . "\\MJMZM{025841}")
        (?\造 . "\\MJMZM{025947}")
        (?\遂 . "\\MJMZM{026017}")      ; MJ026017
        (?\避 . "\\MJMZM{026164}")
        (?\選 . "\\MJMZM{026148}")
        (?\蓮 . "\\MJMZM{022606}")      ; MJ022606
        (?\遣 . "\\MJMZM{026095}")
        (?\適 . "\\MJMZM{026107}")
        (?\遮 . "\\MJMZM{026116}")      ; MJ026116
        (?\遍 . "\\MJMZM{026040}")
        (?\迦 . "\\MJMZM{025830}")
        (?\蓬 . "\\MJMZM{022600}")      ; MJ022600
        (?\逐 . "\\MJMZM{025912}")
        (?\縫 . "\\MJMZM{020345}")
        (?\遇 . "\\MJMZM{026027}")
        (?\迄 . "\\MJMZM{025778}")
        (?\遊 . "\\MJMZM{026033}")
        (?\迷 . "\\MJMZM{025862}")
        (?\逆 . "\\MJMZM{025892}")
        (?\透 . "\\MJMZM{025909}")
        (?\腿 . "\\MJMZM{021169}")
        (?\槌 . "\\MJMZM{014341}")
        (?\漣 . "\\MJMZM{015845}")
        (?\謎 . "\\MJMZM{024670}")
        (?\込 . "\\MJMZM{025763}")
        (?\迅 . "\\MJMZM{025780}")
        (?\辿 . "\\MJMZM{025768}")
        (?\迪 . "\\MJMZM{025839}")
        (?\迭 . "\\MJMZM{025845}")
        (?\這 . "\\MJMZM{025931}")
        (?\逝 . "\\MJMZM{025938}")
        (?\逮 . "\\MJMZM{025973}")
        (?\遁 . "\\MJMZM{026014}")
        (?\逼 . "\\MJMZM{026003}")
        (?\遡 . "\\MJMZM{026090}")
        (?\遜 . "\\MJMZM{026079}")
        (?\遭 . "\\MJMZM{026113}")
        (?\遺 . "\\MJMZM{026152}")
        (?\遵 . "\\MJMZM{026137}")
        (?\遷 . "\\MJMZM{026145}")
        (?\遼 . "\\MJMZM{026156}")
        (?\還 . "\\MJMZM{026181}")      ; MJ026181
        (?\鎚 . "\\MJMZM{027109}")
        (?\鑓 . "\\MJMZM{027327}")
        (?\巡 . "\\MJMZM{010768}")
        (?\逢 . "\\MJMZM{025952}")
        (?\導 . "\\MJMZM{010283}")
        (?\晦 . "\\MJMZM{013297}")
        (?\情 . "\\MJMZM{011647}")
        (?\錆 . "\\MJMZM{026950}")
        (?\請 . "\\MJMZM{024571}")
        (?\鯖 . "\\MJMZM{029194}")
        (?\瀞 . "\\MJMZM{016159}")
        (?\坪 . "\\MJMZM{008966}")
        (?\平 . "\\MJMZM{010942}")
        (?\半 . "\\MJMZM{007843}")
        (?\消 . "\\MJMZM{015390}")
        (?\伴 . "\\MJMZM{006601}")
        (?\鞘 . "\\MJMZM{027991}")
        (?\宵 . "\\MJMZM{010163}")
        (?\梢 . "\\MJMZM{013997}")
        (?\削 . "\\MJMZM{007527}")
        (?\判 . "\\MJMZM{007492}")
        (?\邦 . "\\MJMZM{026245}")
        (?\硝 . "\\MJMZM{018502}")
        (?\評 . "\\MJMZM{024429}")
        (?\肖 . "\\MJMZM{020927}")
        (?\哨 . "\\MJMZM{008279}")
        (?\鎖 . "\\MJMZM{027103}")
        (?\叛 . "\\MJMZM{008009}")      ; MJ008009
        (?\屑 . "\\MJMZM{010355}")
        (?\蛸 . "\\MJMZM{023458}")
        (?\秤 . "\\MJMZM{019033}")
        (?\廠 . "\\MJMZM{011071}")
        (?\拳 . "\\MJMZM{012303}")
        (?\倦 . "\\MJMZM{006858}")
        (?\捲 . "\\MJMZM{012437}")
        (?\弊 . "\\MJMZM{011136}")
        (?\蔽 . "\\MJMZM{022745}")
        (?\瞥 . "\\MJMZM{018321}")
        (?\畔 . "\\MJMZM{017675}")
        (?\譜 . "\\MJMZM{024783}")
        (?\分 . "\\MJMZM{007456}")
        (?\粉 . "\\MJMZM{019820}")
        (?\紛 . "\\MJMZM{020003}")
        (?\穴 . "\\MJMZM{019204}")
        (?\公 . "\\MJMZM{007276}")
        (?\訟 . "\\MJMZM{024369}")
        (?\控 . "\\MJMZM{012496}")
        (?\空 . "\\MJMZM{019211}")
        (?\鉛 . "\\MJMZM{026759}")
        (?\雰 . "\\MJMZM{027784}")
        (?\盆 . "\\MJMZM{018073}")
        (?\頒 . "\\MJMZM{028172}")
        (?\船 . "\\MJMZM{021402}")
        (?\貧 . "\\MJMZM{025008}")
        (?\沿 . "\\MJMZM{015188}")
        (?\捨 . "\\MJMZM{012424}")
        (?\包 . "\\MJMZM{007757}")
        (?\抱 . "\\MJMZM{012241}")
        (?\砲 . "\\MJMZM{018470}")
        (?\胞 . "\\MJMZM{021002}")
        (?\飽 . "\\MJMZM{028383}")
        (?\泡 . "\\MJMZM{015226}")
        (?\鞄 . "\\MJMZM{027970}")
        (?\萢 . "\\MJMZM{022244}")
        (?\庖 . "\\MJMZM{010980}")
        (?\巷 . "\\MJMZM{010789}")
        (?\撰 . "\\MJMZM{012791}")
        (?\巽 . "\\MJMZM{010802}")
        (?\杞 . "\\MJMZM{013632}")
        (?\港 . "\\MJMZM{015566}")
        (?\配 . "\\MJMZM{026440}")      ; MJ026440
        (?\起 . "\\MJMZM{025179}")
        (?\及 . "\\MJMZM{007989}")
        (?\吸 . "\\MJMZM{008113}")
        (?\級 . "\\MJMZM{020001}")
        (?\扱 . "\\MJMZM{012179}")
        (?\汲 . "\\MJMZM{015106}")
        (?\笈 . "\\MJMZM{019380}")      ; MJ019380
        (?\苅 . "\\MJMZM{021628}")      ; MJ021628
        (?\葛 . "\\MJMZM{022339}")
        (?\考 . "\\MJMZM{020754}")
        (?\誤 . "\\MJMZM{024522}")
        (?\虞 . "\\MJMZM{023296}")
        (?\成 . "\\MJMZM{012064}")
        (?\城 . "\\MJMZM{009063}")
        (?\盛 . "\\MJMZM{018094}")
        (?\誠 . "\\MJMZM{024518}")
        (?\晟 . "\\MJMZM{013289}")
        (?\御 . "\\MJMZM{011325}")      ; MJ011325
        (?\衆 . "\\MJMZM{058675}")      ; MJ058675
        (?\嫌 . "\\MJMZM{009909}")
        (?\兼 . "\\MJMZM{056985}")      ; or MJ007296
        (?\謙 . "\\MJMZM{024686}")
        (?\鎌 . "\\MJMZM{027092}")
        (?\廉 . "\\MJMZM{011038}")
        (?\簾 . "\\MJMZM{019723}")
        (?\講 . "\\MJMZM{024693}")
        (?\溝 . "\\MJMZM{015686}")
        (?\構 . "\\MJMZM{014338}")
        (?\篝 . "\\MJMZM{019610}")
        (?\購 . "\\MJMZM{025106}")      ; MJ025106
        (?\冓 . "\\MJMZM{057016}")
        (?\媾 . "\\MJMZM{009887}")
        (?\搆 . "\\MJMZM{012592}")
        (?\覯 . "\\MJMZM{024257}")
        (?\遘 . "\\MJMZM{026070}")      ; MJ026070
        (?\化 . "\\MJMZM{007778}")
        (?\花 . "\\MJMZM{021592}")      ; MJ021592
        (?\囮 . "\\MJMZM{008837}")
        (?\埖 . "\\MJMZM{057226}")
        (?\貨 . "\\MJMZM{025010}")
        (?\錵 . "\\MJMZM{027003}")
        (?\靴 . "\\MJMZM{027951}")
        (?\榔 . "\\MJMZM{014268}")
        (?\螂 . "\\MJMZM{023621}")      ; MJ023621
        (?\即 . "\\MJMZM{007892}")
        (?\概 . "\\MJMZM{014251}")      ; or MJ014250
        (?\卿 . "\\MJMZM{007912}")
        (?\櫛 . "\\MJMZM{014672}")
        (?\厩 . "\\MJMZM{007954}")
        (?\飴 . "\\MJMZM{028370}")
        (?\蝕 . "\\MJMZM{023567}")
        (?\喰 . "\\MJMZM{008494}")
        (?\飾 . "\\MJMZM{028386}")
        (?\餌 . "\\MJMZM{028412}")
        (?\餐 . "\\MJMZM{028415}")
        (?\饗 . "\\MJMZM{028570}")
        (?\縁 . "\\MJMZM{020256}")      ; MJ020256
        (?\捗 . "\\MJMZM{012413}")
        (?\瀕 . "\\MJMZM{016143}")
        (?\冬 . "\\MJMZM{007358}")
        (?\終 . "\\MJMZM{020046}")
        (?\寒 . "\\MJMZM{010200}")
        (?\柊 . "\\MJMZM{013750}")
        (?\疼 . "\\MJMZM{017778}")
        (?\螽 . "\\MJMZM{023681}")
        (?\鮗 . "\\MJMZM{029120}")
        (?\内 . "\\MJMZM{007266}")      ; MJ007266
        (?\納 . "\\MJMZM{019982}")
        (?\丙 . "\\MJMZM{006328}") ; https://glyphwiki.org/wiki/aj1-14009
        (?\柄 . "\\MJMZM{013743}") ; https://glyphwiki.org/wiki/aj1-20145
        (?\券 . "\\MJMZM{007513}")
        (?\契 . "\\MJMZM{009492}")
        (?\喫 . "\\MJMZM{008488}")
        (?\牙 . "\\MJMZM{016801}")      ; MJ016801
        (?\邪 . "\\MJMZM{026254}")      ; MJ026254
        (?\冴 . "\\MJMZM{007367}")      ; MJ007367
        (?\穿 . "\\MJMZM{019216}")      ; MJ019216
        (?\芽 . "\\MJMZM{021615}")      ; MJ021615
        (?\訝 . "\\MJMZM{024363}")      ; MJ024363
        (?\雅 . "\\MJMZM{027721}")      ; MJ027721
        (?\鴉 . "\\MJMZM{029451}")      ; MJ029451
        (?\諺 . "\\MJMZM{024640}")
        (?\薩 . "\\MJMZM{022967}")      ; MJ022967
        (?\次 . "\\MJMZM{014748}")
        (?\姿 . "\\MJMZM{009690}")
        (?\諮 . "\\MJMZM{024622}")
        (?\資 . "\\MJMZM{025046}")
        (?\茨 . "\\MJMZM{021801}")
        (?\亡 . "\\MJMZM{006462}")
        (?\匹 . "\\MJMZM{007817}")
        (?\忘 . "\\MJMZM{011392}")
        (?\忙 . "\\MJMZM{011394}")
        (?\荒 . "\\MJMZM{021898}")      ; MJ021898
        (?\芒 . "\\MJMZM{021526}")
        (?\妄 . "\\MJMZM{009563}")
        (?\慌 . "\\MJMZM{011811}")
        (?\匿 . "\\MJMZM{007831}")
        (?\慝 . "\\MJMZM{011833}")
        (?\匚 . "\\MJMZM{007816}")
        (?\區 . "\\MJMZM{007833}")      ; MJ007833
        (?\毆 . "\\MJMZM{014930}")
        (?\歐 . "\\MJMZM{014802}")
        (?\躯 . "\\MJMZM{025532}")      ; MJ025532
        (?\盲 . "\\MJMZM{018120}")
        (?\網 . "\\MJMZM{020194}")      ; MJ020194
        (?\望 . "\\MJMZM{013549}")      ; MJ013549
        (?\聖 . "\\MJMZM{020847}")
        (?\害 . "\\MJMZM{010161}")
        (?\程 . "\\MJMZM{019074}")
        (?\冒 . "\\MJMZM{007318}")
        (?\帽 . "\\MJMZM{010867}")
        (?\刃 . "\\MJMZM{007450}")
        (?\忍 . "\\MJMZM{011378}")
        (?\籾 . "\\MJMZM{019805}")
        (?\認 . "\\MJMZM{024493}")
        (?\丹 . "\\MJMZM{056826}")      ; CID+13914
        (?\急 . "\\MJMZM{011472}")
        (?\婦 . "\\MJMZM{009796}")
        (?\尋 . "\\MJMZM{010278}")
        (?\雪 . "\\MJMZM{027774}")
        (?\鱈 . "\\MJMZM{029347}")
        (?\掃 . "\\MJMZM{012455}")
        (?\慧 . "\\MJMZM{011847}")      ; MJ011847
        (?\侵 . "\\MJMZM{006736}")
        (?\浸 . "\\MJMZM{015372}")
        (?\周 . "\\MJMZM{008158}")
        (?\彫 . "\\MJMZM{011266}")
        (?\調 . "\\MJMZM{024558}")
        (?\鯛 . "\\MJMZM{029201}")
        (?\凋 . "\\MJMZM{007393}")
        (?\鵠 . "\\MJMZM{029547}")
        (?\敷 . "\\MJMZM{013021}")      ; MJ013021
        (?\薄 . "\\MJMZM{022888}")      ; or MJ022887
        (?\博 . "\\MJMZM{059401}")      ; or MJ059400
        (?\簿 . "\\MJMZM{019724}")
        (?\縛 . "\\MJMZM{020319}")
        (?\主 . "\\MJMZM{006363}")
        (?\往 . "\\MJMZM{011290}")
        (?\住 . "\\MJMZM{006633}")
        (?\柱 . "\\MJMZM{013795}")
        (?\註 . "\\MJMZM{024401}")
        (?\駐 . "\\MJMZM{028660}")
        (?\注 . "\\MJMZM{015236}")
        (?\巨 . "\\MJMZM{010776}")
        (?\距 . "\\MJMZM{025288}")
        (?\拒 . "\\MJMZM{012276}")
        (?\矩 . "\\MJMZM{018402}")
        (?\苣 . "\\MJMZM{021674}")
        (?\屏 . "\\MJMZM{010368}")      ; MJ010368
        (?\絣 . "\\MJMZM{020092}")
        (?\灰 . "\\MJMZM{016266}")
        (?\炭 . "\\MJMZM{016334}")
        (?\恢 . "\\MJMZM{011539}")
        (?\牌 . "\\MJMZM{016783}")
        (?\挽 . "\\MJMZM{012376}")
        (?\稗 . "\\MJMZM{019088}")
        (?\娩 . "\\MJMZM{009734}")
        (?\煙 . "\\MJMZM{016496}")
        (?\那 . "\\MJMZM{026238}")
        (?\瞬 . "\\MJMZM{018330}")      ; MJ018330
        (?\派 . "\\MJMZM{015324}")
        (?\微 . "\\MJMZM{011344}")      ; MJ011344
        (?\旅 . "\\MJMZM{013110}")
        (?\虐 . "\\MJMZM{023278}")
        (?\憲 . "\\MJMZM{011950}")
        (?\尨 . "\\MJMZM{010309}")
        (?\逞 . "\\MJMZM{025940}")      ; MJ025940
        (?\顛 . "\\MJMZM{028259}")
        (?\嚔 . "\\MJMZM{008731}")      ; MJ008731
        (?\諭 . "\\MJMZM{024619}")
        (?\輸 . "\\MJMZM{025668}")
        (?\愈 . "\\MJMZM{011723}")
        (?\癒 . "\\MJMZM{017937}")
        (?\啄 . "\\MJMZM{008373}")
        (?\延 . "\\MJMZM{011100}")
        (?\誕 . "\\MJMZM{024504}")
        (?\襖 . "\\MJMZM{024146}")
        (?\徽 . "\\MJMZM{011363}")
        (?\灸 . "\\MJMZM{016277}")      ; MJ016277
        (?\粂 . "\\MJMZM{019812}")      ; MJ019812
        (?\隙 . "\\MJMZM{027665}")      ; MJ027665
        (?\楯 . "\\MJMZM{014230}")      ; MJ014230
        (?\叟 . "\\MJMZM{008019}")      ; MJ008019
        (?\囀 . "\\MJMZM{008783}")      ; MJ008783
        (?\甕 . "\\MJMZM{017608}")      ; MJ017608
        (?\竈 . "\\MJMZM{019305}")      ; MJ019305
        (?\艘 . "\\MJMZM{021435}")      ; MJ021435
        (?\蜃 . "\\MJMZM{023472}")      ; MJ023472
        (?\蠅 . "\\MJMZM{023767}")      ; MJ023767
        (?\隔 . "\\MJMZM{027656}")
        (?\歳 . "\\MJMZM{059760}")
        (?\廻 . "\\MJMZM{035473}")      ; MJ011114, MJ035473
        (?\鞭 . "\\MJMZM{028016}")
        (?\甦 . "\\MJMZM{017625}")      ; MJ017625
        (?\象 . "\\MJMZM{049391}")      ; MJ049391
        (?\率 . "\\MJMZM{017115}")
        (?\並 . "\\MJMZM{006340}")
        (?\飢 . "\\MJMZM{028342}")
        (?\技 . "\\MJMZM{012195}")
        (?\雙 . "\\MJMZM{027749}")
        (?\妃 . "\\MJMZM{009561}")
        (?\支 . "\\MJMZM{012941}")
        (?\吏 . "\\MJMZM{008071}")      ; MJ008071
        (?\敵 . "\\MJMZM{013019}")
        (?\竟 . "\\MJMZM{019331}")
        (?\章 . "\\MJMZM{019333}")
        (?\障 . "\\MJMZM{027670}")
        (?\改 . "\\MJMZM{012955}")      ; MJ012955
        (?\紀 . "\\MJMZM{019958}")
        (?\記 . "\\MJMZM{024358}")
        (?\蓋 . "\\MJMZM{022550}")      ; MJ022550
        (?\拐 . "\\MJMZM{012273}")      ; MJ012273
        (?\直 . ?\直)                   ; CID+13934
        (?\植 . ?\植)                   ; CID+13845
        (?\埴 . ?\埴)                   ; CID+13464
        (?\値 . ?\値)                   ; 異字體はCIDのみ
        (?\稙 . ?\稙)                   ; 異字體はCIDのみ
        (?\置 . ?\置)           ; https://glyphwiki.org/wiki/aj1-13920
        (?\殖 . ?\殖)           ; https://glyphwiki.org/wiki/aj1-13846
        (?\肉 . ?\肉)           ; 異字體はCIDのみ
        (?\桃 . ?\桃)           ; 異字體はCIDのみ
        ;; 以下變換不要?
        (?\叉 . "\\MJMZM{007988}")      ; MJ007988
        (?\筵 . "\\MJMZM{019494}")      ; MJ019494
        (?\剪 . ?\剪)
        (?\匂 . ?\匂)
        (?\健 . "\\MJMZM{006934}")      ; MJ006934
        (?\鍵 . "\\MJMZM{027071}")      ; MJ027071
        (?\梗 . "\\MJMZM{013983}")      ; MJ013983
        (?\鮫 . "\\MJMZM{029141}")      ; MJ029141
        (?\杖 . "\\MJMZM{013622}")      ; MJ013622
        (?\挺 . "\\MJMZM{012372}")      ; MJ012372
        (?\咬 . "\\MJMZM{008228}")      ; MJ008228
        (?\狡 . "\\MJMZM{016940}")      ; MJ016940
        (?\腱 . "\\MJMZM{021153}")      ; MJ021153
        (?\虔 . "\\MJMZM{023285}")      ; MJ023285
        (?\爺 . "\\MJMZM{016755}")      ; MJ016755
        (?\駁 . "\\MJMZM{028644}")      ; MJ028644
        (?\皎 . "\\MJMZM{018007}")
        (?\斧 . "\\MJMZM{013078}")      ; MJ013078
        (?\靱 . "\\MJMZM{027946}")      ; MJ027946 CID 13624,7710
        (?\魍 . "\\MJMZM{029029}")      ; MJ029029
        (?\讐 . ?\讐)
        (?\僊 . "\\MJMZM{007054}")      ; MJ007054
        (?\駛 . "\\MJMZM{028671}")      ; MJ028671
        (?\耕 . "\\MJMZM{020772}")      ; MJ020772
        (?\籍 . "\\MJMZM{019738}")      ; MJ019738
        (?\耗 . "\\MJMZM{020776}")      ; MJ020776
        ;; 文字情報基盤のみ
        (?\像 . "\\MJMZM{007063}")      ; MJ007063
        (?\惠 . "\\MJMZM{059623}")      ; MJ059623
        (?\敬 . "\\MJMZM{013007}")      ; MJ013007
        (?\藉 . "\\MJMZM{023032}")      ; MJ023032
        (?\龕 . "\\MJMZM{030146}")      ; MJ030146
        (?\藐 . "\\MJMZM{023050}")      ; MJ023050
        (?\護 . "\\MJMZM{024820}")      ; MJ024820
        (?\錨 . "\\MJMZM{026990}")      ; MJ026990
        (?\勸 . "\\MJMZM{007741}")      ; MJ007741
        (?\灌 . "\\MJMZM{016227}")      ; MJ016227
        (?\觀 . "\\MJMZM{024279}")      ; MJ024279
        (?\葡 . "\\MJMZM{022352}")      ; MJ022352
        (?\萄 . "\\MJMZM{022199}")      ; MJ022199
        (?\慕 . "\\MJMZM{011822}")      ; MJ011822
        (?\蒙 . "\\MJMZM{022453}")      ; MJ022453
        (?\藜 . "\\MJMZM{023067}")      ; MJ023067
        (?\幕 . "\\MJMZM{010898}")      ; MJ010898
        (?\蓄 . "\\MJMZM{022533}")      ; MJ022533
        (?\蕩 . "\\MJMZM{022839}")      ; MJ022839
        (?\舊 . "\\MJMZM{021341}")      ; MJ021341
        (?\嘩 . "\\MJMZM{008611}")      ; MJ008611
        (?\暮 . "\\MJMZM{013384}")      ; MJ013384
        (?\蘆 . "\\MJMZM{023148}")      ; MJ023148
        (?\薈 . "\\MJMZM{022901}")      ; MJ022901
        (?\華 . "\\MJMZM{022152}")      ; MJ022152
        (?\寞 . "\\MJMZM{010218}")      ; MJ010218
        (?\萬 . "\\MJMZM{022255}")      ; MJ022255
        (?\穫 . "\\MJMZM{019195}")      ; MJ019195
        (?\苟 . "\\MJMZM{021665}")      ; MJ021665
        (?\萠 . "\\MJMZM{022240}")      ; MJ022240
        (?\苗 . "\\MJMZM{021649}")      ; MJ021649
        (?\蒙 . "\\MJMZM{022453}")      ; MJ022453
        (?\薦 . "\\MJMZM{022960}")      ; MJ022960
        (?\薪 . "\\MJMZM{022970}")      ; MJ022970
        (?\吝 . "\\MJMZM{008084}")      ; MJ008084
        (?\錬 . "\\MJMZM{027019}")      ; MJ027019
        (?\諂 . "\\MJMZM{024699}")      ; MJ024699
        (?\藏 . "\\MJMZM{023046}")      ; MJ023046
        (?\草 . "\\MJMZM{021876}")      ; MJ021876
        (?\備 . "\\MJMZM{006998}")      ; MJ006998
        (?\夢 . "\\MJMZM{009447}")      ; MJ009447
        (?\募 . "\\MJMZM{007704}")      ; MJ007704
        (?\塔 . "\\MJMZM{009206}")      ; MJ009206
        (?\墓 . "\\MJMZM{009280}")      ; MJ009280
        (?\模 . "\\MJMZM{014444}")      ; MJ014444
        (?\荷 . "\\MJMZM{021935}")      ; MJ021935
        ;; 謨
        ;; 菩
        ;; 漠
        (?\莫 . "\\MJMZM{022035}")      ; MJ022035
        ;; 獏
        ;; 貘
        (?\若 . "\\MJMZM{021679}")      ; MJ021679
        (?\惹 . "\\MJMZM{011705}")      ; MJ011705
        ;; 憊
        ;; 描
        ;; 猫
        ;; 曚
        (?\矇 . "\\MJMZM{018363}")      ; MJ018363
        ;; 朦
        ;; 檬
        ;; 濛
        ;; 艨
        ;; 莽
        (?\芥 . "\\MJMZM{021560}")      ; MJ021560
        ;; 瀟
        ;; 甍
        (?\薙 . "\\MJMZM{022938}")      ; MJ022938
        ;; 蔟
        ;; 茜
        ;; 芋
        ;; 葵
        ;; 英
        ;; 萩
        ;; 荻
        (?\獲 . "\\MJMZM{017092}")      ; MJ017092
        ;; 茅
        ;; 菌
        (?\芹 . "\\MJMZM{021609}")      ; MJ021609
        ;; 茄
        ;; 芸
        (?\權 . "\\MJMZM{014724}")      ; MJ014724
        (?\莊 . "\\MJMZM{021971}")      ; MJ021971
        (?\勵 . "\\MJMZM{007737}")      ; MJ007737
        (?\荏 . "\\MJMZM{021890}")      ; MJ021890
        (?\蔬 . "\\MJMZM{022724}")      ; MJ022724
        (?\荊 . "\\MJMZM{021878}")      ; MJ021878
        (?\躪 . "\\MJMZM{025504}")      ; MJ025504
        (?\歡 . "\\MJMZM{014822}")      ; MJ014822
        (?\苛 . "\\MJMZM{021656}")      ; MJ021656
        (?\苦 . "\\MJMZM{021682}")      ; MJ021682
        (?\落 . "\\MJMZM{022283}")      ; MJ022283
        (?\茵 . "\\MJMZM{021833}")      ; MJ021833
        (?\葉 . "\\MJMZM{022302}")      ; MJ022302
        (?\蒼 . "\\MJMZM{022517}")      ; MJ022517
        (?\芳 . "\\MJMZM{021599}")      ; MJ021599
        (?\葦 . "\\MJMZM{022368}")      ; MJ022368
        (?\葬 . "\\MJMZM{022379}")      ; MJ022379
        (?\蕃 . "\\MJMZM{022761}")      ; MJ022761
        (?\蔭 . "\\MJMZM{022726}")      ; MJ022726
        (?\懽 . "\\MJMZM{012042}")      ; MJ012042
        (?\茴 . "\\MJMZM{021831}")      ; MJ021831
        (?\藩 . "\\MJMZM{023097}")      ; MJ023097
        (?\躇 . "\\MJMZM{025462}")      ; MJ025462
        (?\藥 . "\\MJMZM{023087}")      ; MJ023087
        (?\英 . "\\MJMZM{021701}")      ; MJ021701
        (?\茲 . "\\MJMZM{021827}")      ; MJ021827
        (?\邁 . "\\MJMZM{026171}")      ; MJ026171
        ;; 吉
        (?\吉 . "\\MJMZM{032129}")
        ;; 窓
        (?\窓 . ?\窗)
        ;; 明治元譯新約全書のみの特例
        (?\与 . ?\予)
        (?\収 . ?\收)         ; 時々に使ひ分けられてゐるため注意のこと
        ))

(setq ruby-convert-24-table
      '(("ぐんちう" . "ぐん,ちう")      ; 郡中
        ("わうごん" . "わう,ごん")      ; 黄金
        ("みちすぢ" . "みち,すぢ")      ; 路線
        ;; ("いただき" . "いただき")       ; 頂上
        ("いつてん" . "いつ,てん")      ; 一點
        ("しれもの" . "しれ,もの")      ; 狂妄
        ("なげいれ" . "なげ,いれ")      ; 投入
        ("おほきみ" . "おほ,きみ")      ; 大王
        ;; ("いないな" . "いないな")       ; 否々
        ("こうえき" . "こう,えき")      ; 公役
        ;; ("まつたく" . "まつたく")       ; 完全
        ("かほいろ" . "かほ,いろ")      ; 顏色
        ;; ("ちりぢり" . "ちりぢり")      ; 流離
        ;; ("おとなし" . "おとなし")       ; 馴良
        ("とりあげ" . "とり,あげ")      ; 挈上
        ("くうきよ" . "くう,きよ")      ; 空虚
        ("はえいで" . "はえ,いで")      ; 萠出
        ("たねまき" . "たね,まき")      ; 播種
        ("あだびと" . "あだ,びと")      ; 敵人
        ("かりいれ" . "かり,いれ")      ; 收穫
        ;; ("つまづき" . "つまづき")      ; 躓礙
        ;; ("ざじやう" . "ざじやう")       ; 座上
        ("わだなか" . "わだ,なか")      ; 海中
        ("こうがう" . "こう,がう")      ; 苟口
        ("とうせつ" . "とう,せつ")      ; 盜竊
        ("ゆふやけ" . "ゆふ,やけ")      ; 夕紅
        ("あさやけ" . "あさ,やけ")      ; 朝紅
        ("てんかん" . "てん,かん")      ; 癲癇
        ;; ("めぐれる" . "めぐれる")       ; 周流
        ;; ("りやうめ" . "りやうめ")       ; 兩眼
        ;; ("あなどる" . "あなどる")       ; 輕視
        ("せんまん" . "せん,まん")      ; 千萬
        ("ひきおひ" . "ひき,おひ")      ; 負債
        ("つれなき" . "つれ,なき")      ; 不情
        ("きのえだ" . "きの,えだ")      ; 樹枝
        ("れいふく" . "れい,ふく")      ; 禮服
        ("うみやま" . "うみ,やま")      ; 水陸
        ("いんよく" . "いん,よく")      ; 淫欲
        ("よむもの" . "よむ,もの")      ; 讀者
        ;; ("ちじやう" . "ちじやう")       ; 地上
        ;; ("ちゆうぎ" . "ちゆうぎ")       ; 忠義
        ("はうばい" . "はう,ばい")      ; 朋輩
        ("うるもの" . "うる,もの")      ; 賣者
        ("てんこく" . "てん,こく")      ; 天國
        ("いつせん" . "いつ,せん")      ; 一千
        ;; ("ともども" . "ともども")       ; 共々
        ("なりゆき" . "なり,ゆき")      ; 結局
        ("うつもの" . "うつ,もの")      ; 撃者
        ("つみなき" . "つみ,なき")      ; 無辜
        ("あかいろ" . "あか,いろ")      ; 絳色
        ("いきたえ" . "いき,たえ")      ; 氣絶
        ("あけがた" . "あけ,がた")      ; 黎明
        ;; ("けごろも" . "けごろも")       ; 毛衣
        ("かはおび" . "かは,おび")      ; 皮帶
        ("あくかう" . "あく,かう")      ; 惡口
        ("ろくじふ" . "ろく,じふ")      ; 六十
        ;; ("いにしへ" . "いにしへ")       ; 往昔
        ("せんにん" . "せん,にん")      ; 千人
        ("をりよき" . "をり,よき")      ; 機會
        ("かふもの" . "かふ,もの")      ; 牧者
        ;; ("さびしき" . "さびしき")       ; 寂寞
        ;; ("にひやく" . "にひやく")       ; 二百
        ;; ("くみぐみ" . "くみぐみ")       ; 組々
        ("ひとごと" . "ひと,ごと")      ; 毎人
        ;; ("すること" . "すること")      ; 行事
        ("あくねん" . "あく,ねん")      ; 惡念
        ("はうとく" . "はう,とく")      ; 謗讟
        ("けうがう" . "けう,がう")      ; 驕傲
        ("たべくづ" . "たべ,くづ")      ; 遺屑
        ("たんそく" . "たん,そく")      ; 歎息
        ;; ("あらたむ" . "あらたむ")       ; 復振
        ("あるひと" . "ある,ひと")      ; 一人
        ;; ("なげたふ" . "なげたふ")       ; 傾跌
        ;; ("かろがろ" . "かろがろ")      ; 輕易
        ("いつぱい" . "いつ,ぱい")      ; 一杯
        ;; ("りやうて" . "りやうて")       ; 兩手
        ("かたあし" . "かた,あし")      ; 一足
        ("じふにん" . "じふ,にん")      ; 十人
        ;; ("しづまれ" . "しづまれ")       ; 緘默
        ("こしかけ" . "こし,かけ")      ; 椅子
        ("さかぶね" . "さか,ぶね")      ; 酒榨
        ("をやいし" . "をや,いし")      ; 首石
        ("ことはり" . "こと,はり")      ; 道理
        ("まへかた" . "まへ,かた")      ; 以前
        ;; ("すくなく" . "すくなく")       ; 減少
        ("かどぐち" . "かど,ぐち")      ; 門口
        ;; ("たばかり" . "たばかり")       ; 詭計
        ("いづかた" . "いづ,かた")      ; 何處
        ("したには" . "した,には")      ; 下庭
        ("にはぐち" . "には,ぐち")      ; 庭門
        ;; ("ちいさき" . "ちいさき")       ; 年少
        ("じふいち" . "じふ,いち")      ; 十一
        ;; ("いしやう" . "いしやう")       ; 異象
        ;; ("はらみご" . "はらみご")       ; 胎孕
        ;; ("つかひめ" . "つかひめ")       ; 使女
        ;; ("よろづよ" . "よろづよ")       ; 萬世
        ;; ("いきほひ" . "いきほひ")       ; 權柄
        ("よきもの" . "よき,もの")      ; 美食
        ("うみどき" . "うみ,どき")      ; 産期
        ("やまざと" . "やま,ざと")      ; 山地
        ;; ("いひふら" . "いひふら")      ; 傳播
        ("しのかげ" . "しの,かげ")      ; 死蔭
        ("いへすぢ" . "いへ,すぢ")      ; 宗族
        ("てんぐん" . "てん,ぐん")      ; 天軍
        ("やまばと" . "やま,ばと")      ; 斑鳩
        ("としごと" . "とし,ごと")      ; 毎年
        ;; ("けはしき" . "けはしき")       ; 崎嶇
        ("まちをり" . "まち,をり")      ; 懷望
        ;; ("かんがへ" . "かんがへ")       ; 忖度
        ("さんじふ" . "さん,じふ")      ; 三十
        ;; ("まかされ" . "まかされ")       ; 委任
        ("ひれふさ" . "ひれ,ふさ")      ; 拜跪
        ;; ("たふとま" . "たふとま")       ; 敬重
        ;; ("しうとめ" . "しうとめ")       ; 妻母
        ("さりゆく" . "さり,ゆく")      ; 離去
        ;; ("みまはし" . "みまはし")       ; 環視
        ;; ("なやむる" . "なやむる")       ; 虐遇
        ("うつばり" . "うつ,ばり")      ; 梁木
        ;; ("ちひさき" . "ちひさき")       ; 至微
        ;; ("ごひやく" . "ごひやく")       ; 五百
        ("かしぬし" . "かし,ぬし")      ; 債主
        ("あしがせ" . "あし,がせ")      ; 桎梏
        ;; ("おしあひ" . "おしあひ")      ; 擁擠
        ("よくじつ" . "よく,じつ")      ; 翌日
        ;; ("たちまち" . "たちまち")      ; 忽然
        ;; ("まがれる" . "まがれる")       ; 悖逆
        ;; ("ひきつけ" . "ひきつけ")      ; 拘攣
        ;; ("てきたは" . "てきたは")      ; 敵抗
        ("むらびと" . "むら,びと")      ; 郷人
        ("もちぬし" . "もち,ぬし")      ; 稼主
        ;; ("ゑしやく" . "ゑしやく")       ; 問候
        ("しちじふ" . "しち,じふ")      ; 七十
        ("しるもの" . "しる,もの")      ; 識者
        ("せいしん" . "せい,しん")      ; 精神
        ;; ("はたごや" . "はたごや")       ; 旅邸
        ("かいはう" . "かい,はう")      ; 介抱
        ;; ("はたらか" . "はたらか")       ; 勞動
        ("よきかた" . "よき,かた")      ; 善業
        ("どんよく" . "どん,よく")      ; 貪欲
        ;; ("あかるき" . "あかるき")       ; 光明
        ("やのうへ" . "やの,うへ")      ; 屋上
        ("ゆゐもつ" . "ゆゐ,もつ")      ; 遺業
        ("たんしん" . "たん,しん")      ; 貪心
        ("あんしん" . "あん,しん")      ; 安心
        ("すんいん" . "すん,いん")      ; 寸陰
        ("えいぐわ" . "えい,ぐわ")      ; 榮華
        ;; ("あてがは" . "あてがは")       ; 給與
        ("おしころ" . "おし,ころ")      ; 壓死
        ;; ("かがまり" . "かがまり")       ; 傴僂
        ("てきたい" . "てき,たい")      ; 敵對
        ;; ("たびだて" . "たびだて")      ; 旅行
        ("めんどり" . "めん,どり")      ; 母鷄
        ;; ("しよくじ" . "しよくじ")       ; 食事
        ("どうせき" . "どう,せき")      ; 同席
        ("しんるゐ" . "しん,るゐ")      ; 親戚
        ;; ("さうぢよ" . "さうぢよ")       ; 掃除
        ("ゑんごく" . "ゑん,ごく")      ; 遠國
        ("たびだち" . "たび,だち")      ; 旅行
        ("まめがら" . "まめ,がら")      ; 豆莢
        ;; ("あつかひ" . "あつかひ")       ; 會計
        ("かきつけ" . "かき,つけ")      ; 券書
        ;; ("むらさき" . "むらさき")       ; 紫袍
        ("ひきうす" . "ひき,うす")      ; 磨石
        ;; ("たがやし" . "たがやし")       ; 樹藝
        ;; ("やづくり" . "やづくり")       ; 構造
        ;; ("きをおと" . "きをおと")      ; 沮喪
        ("ざいにん" . "ざい,にん")      ; 罪人
        ;; ("のちのよ" . "のちのよ")       ; 來世
        ;; ("なぶられ" . "なぶられ")       ; 戲弄
        ("みのたけ" . "みの,たけ")      ; 身量
        ;; ("くはのき" . "くはのき")       ; 桑樹
        ;; ("りやうち" . "りやうち")       ; 領地
        ("なにほど" . "なに,ほど")      ; 幾何
        ;; ("ろばのこ" . "ろばのこ")       ; 驢駒
        ("うけとら" . "うけ,とら")      ; 受收
        ("あとつぎ" . "あと,つぎ")      ; 嗣子
        ("もくねん" . "もく,ねん")      ; 默然
        ("あいさつ" . "あい,さつ")      ; 問安
        ;; ("じやうざ" . "じやうざ")       ; 上坐
        ("しんだい" . "しん,だい")      ; 所有
        ("はうなふ" . "はう,なふ")      ; 奉納
        ("ほういう" . "ほう,いう")      ; 朋友
        ("けいばつ" . "けい,ばつ")      ; 刑罰
        ;; ("まちなや" . "まちなや")      ; 俟惱
        ("しんどう" . "しん,どう")      ; 震動
        ("なかには" . "なか,には")      ; 中庭
        ;; ("じやうか" . "じやうか")       ; 城下
        ("あざわら" . "あざ,わら")      ; 嘲哂
        ("てうろう" . "てう,ろう")      ; 嘲弄
        ;; ("よからぬ" . "よからぬ")       ; 不是
        ;; ("ひやうぎ" . "ひやうぎ")       ; 評議
        ("かうもつ" . "かう,もつ")      ; 香物
        ;; ("うろたへ" . "うろたへ")       ; 躊躇
        ("やくにん" . "やく,にん")      ; 有司
        ;; ("おどろか" . "おどろか")       ; 驚駭
        ("みつぶさ" . "みつ,ぶさ")      ; 蜜房
        ("こんえん" . "こん,えん")      ; 婚筵
        ("いしがめ" . "いし,がめ")      ; 石甕
        ;; ("あきなひ" . "あきなひ")       ; 貿易
        ("みづがめ" . "みづ,がめ")      ; 水瓶
        ;; ("をりをり" . "をりをり")       ; 時々
        ("おほぜい" . "おほ,ぜい")      ; 衆多
        ("さいせん" . "さい,せん")      ; 賽錢
        ("こつじき" . "こつ,じき")      ; 乞食
        ("かどもり" . "かど,もり")      ; 門守
        ("がうたう" . "がう,たう")      ; 強盜
        ;; ("とりかこ" . "とりかこ")      ; 環圍
        ("いでむか" . "いで,むか")      ; 出迎
        ("とりのけ" . "とり,のけ")      ; 移去
        ("くにぢう" . "くに,ぢう")      ; 擧國
        ("ひとつぶ" . "ひと,つぶ")      ; 一粒
        ;; ("おひつか" . "おひつか")       ; 追及
        ("かねいれ" . "かね,いれ")      ; 金嚢
        ("きりとり" . "きり,とり")      ; 剪除
        ("てうちん" . "てう,ちん")      ; 提燈
        ("へいぜい" . "へい,ぜい")      ; 平生
        ("やくしよ" . "やく,しよ")      ; 公廳
        ("しきいし" . "しき,いし")      ; 鋪石
        ("すてふだ" . "すて,ふだ")      ; 罪標
        ("うみわた" . "うみ,わた")      ; 海絨
        ;; ("ろくわい" . "ろくわい")       ; 蘆薈
        ;; ("そなへび" . "そなへび")       ; 備日
        ;; ("てぬぐひ" . "てぬぐひ")       ; 手巾
        ("くれがた" . "くれ,がた")      ; 暮時
        ("ぜんこく" . "ぜん,こく")      ; 全國
        ("ただなか" . "ただ,なか")      ; 眞中
        ;; ("つかふる" . "つかふる")       ; 奉事
        ;; ("おびやか" . "おびやか")       ; 恐喝
        ;; ("さわだち" . "さわだち")      ; 喧嘩
        ("いくぶん" . "いく,ぶん")      ; 幾分
        ("みやもり" . "みや,もり")      ; 殿司
        ;; ("てあらき" . "てあらき")       ; 強暴
        ;; ("ひろまり" . "ひろまり")       ; 傳播
        ("とつぜん" . "とつ,ぜん")      ; 突然
        ("さんげふ" . "さん,げふ")      ; 産業
        ;; ("おしのけ" . "おしのけ")      ; 拒却
        ;; ("きよきち" . "きよきち")       ; 聖地
        ("ぐんぜい" . "ぐん,ぜい")      ; 軍勢
        ;; ("なやまさ" . "なやまさ")       ; 窘迫
        ("わかもの" . "わか,もの")      ; 少年
        ;; ("むらむら" . "むらむら")      ; 諸邑
        ("だいじん" . "だい,じん")      ; 大臣
        ;; ("ほふりば" . "ほふりば")       ; 屠場
        ("むらざと" . "むら,ざと")      ; 邑郷
        ("なんによ" . "なん,によ")      ; 男女
        ("のみくひ" . "のみ,くひ")      ; 飮食
        ;; ("おどろき" . "おどろき")       ; 駭異
        ("しよはう" . "しよ,はう")      ; 諸方
        ;; ("ちゆうぶ" . "ちゆうぶ")       ; 癱瘋
        ("とめおか" . "とめ,おか")      ; 配置
        ;; ("しんじん" . "しんじん")      ; 信心
        ("しんぞく" . "しん,ぞく")      ; 親族
        ;; ("ためらは" . "ためらは")       ; 猶豫
        ;; ("つらつら" . "つらつら")       ; 熟々
        ("よつあし" . "よつ,あし")      ; 四足
        ("よきひと" . "よき,ひと")      ; 善人
        ("そのころ" . "その,ころ")      ; 當時
        ("ひたすら" . "ひた,すら")      ; 懇切
        ;; ("まへのよ" . "まへのよ")       ; 前夜
        ;; ("かがやく" . "かがやく")       ; 照輝
        ;; ("いひはり" . "いひはり")      ; 力言
        ;; ("したしみ" . "したしみ")       ; 親睦
        ("わけもち" . "わけ,もち")      ; 分封
        ;; ("またのな" . "またのな")       ; 一名
        ;; ("つよきて" . "つよきて")       ; 勁手
        ;; ("さからひ" . "さからひ")       ; 爭辯
        ;; ("くるしめ" . "くるしめ")       ; 窘迫
        ("あしなへ" . "あし,なへ")      ; 跛者
        ("かみたち" . "かみ,たち")      ; 諸神
        ;; ("おのれら" . "おのれら")       ; 己等
        ("あきびと" . "あき,びと")      ; 商人
        ;; ("うらなひ" . "うらなひ")       ; 卜占
        ("たいぜい" . "たい,ぜい")      ; 大勢
        ("ひれふし" . "ひれ,ふし")      ; 俯伏
        ("うちきず" . "うち,きず")      ; 杖傷
        ("おほやけ" . "おほ,やけ")      ; 公然
        ("したやく" . "した,やく")      ; 下吏
        ;; ("さへづる" . "さへづる")       ; 嘐啁
        ;; ("うやまふ" . "うやまふ")       ; 敬拜
        ;; ("ほりつけ" . "ほりつけ")      ; 刻書
        ("ぜんめん" . "ぜん,めん")      ; 全面
        ;; ("くらかり" . "くらかり")       ; 蒙昧
        ;; ("みすぐし" . "みすぐし")       ; 不問
        ("ちかごろ" . "ちか,ごろ")      ; 新近
        ("かんあく" . "かん,あく")      ; 奸惡
        ("なのこと" . "なの,こと")      ; 名字
        ;; ("つぎつぎ" . "つぎつぎ")      ; 逐次
        ("べんさい" . "べん,さい")      ; 辯才
        ("ときあか" . "とき,あか")      ; 説明
        ("うけいれ" . "うけ,いれ")      ; 接容
        ("いひふせ" . "いひ,ふせ")      ; 辨折
        ("かうだう" . "かう,だう")      ; 講堂
        ("あせふき" . "あせ,ふき")      ; 汗布
        ("まへだれ" . "まへ,だれ")      ; 襜衣
        ;; ("しよしよ" . "しよしよ")      ; 諸所
        ("ひとかた" . "ひと,かた")      ; 容易
        ("ぎんがん" . "ぎん,がん")      ; 銀龕
        ("みちづれ" . "みち,づれ")      ; 同行
        ("おほかた" . "おほ,かた")      ; 大半
        ("ぐんじふ" . "ぐん,じふ")      ; 群集
        ("さんがい" . "さん,がい")      ; 三階
        ("いでたて" . "いで,たて")      ; 出立
        ;; ("くはだて" . "くはだて")       ; 詭謀
        ;; ("いへいへ" . "いへいへ")       ; 家々
        ("まちごと" . "まち,ごと")      ; 毎邑
        ("いでたち" . "いで,たち")      ; 出立
        ;; ("いちいち" . "いちいち")       ; 一々
        ("いくまん" . "いく,まん")      ; 幾萬
        ;; ("さわだた" . "さわだた")      ; 聳動
        ("わるもの" . "わる,もの")      ; 凶徒
        ("ひるごろ" . "ひる,ごろ")      ; 日中
        ("かはむち" . "かは,むち")      ; 革鞭
        ("みんせき" . "みん,せき")      ; 民籍
        ("がうもん" . "がう,もん")      ; 拷問
        ("くひのみ" . "くひ,のみ")      ; 食飮
        ("まちぶせ" . "まち,ぶせ")      ; 埋伏
        ("たいへい" . "たい,へい")      ; 太平
        ("せんけん" . "せん,けん")      ; 先見
        ;; ("きよまり" . "きよまり")       ; 潔淨
        ;; ("ぎくわい" . "ぎくわい")       ; 議會
        ;; ("たすくる" . "たすくる")       ; 供給
        ("にんこく" . "にん,こく")      ; 任國
        ;; ("あくるひ" . "あくるひ")       ; 次日
        ("こうだう" . "こう,だう")      ; 公堂
        ("ことさら" . "こと,さら")      ; 殊更
        ("ざいあん" . "ざい,あん")      ; 罪案
        ("はくがく" . "はく,がく")      ; 博學
        ;; ("きやうき" . "きやうき")       ; 狂氣
        ("かたすみ" . "かた,すみ")      ; 方隅
        ;; ("たやすく" . "たやすく")       ; 容易
        ;; ("たやすき" . "たやすき")       ; 容易
        ;; ("もてなし" . "もてなし")       ; 供應
        ("かざした" . "かざ,した")      ; 風下
        ("だんじき" . "だん,じき")      ; 斷食
        ;; ("あやうき" . "あやうき")       ; 危險
        ("ふなぬし" . "ふな,ぬし")      ; 船主
        ;; ("ほどなく" . "ほどなく")       ; 未幾
        ("かざしも" . "かざ,しも")      ; 風下
        ("ひきあげ" . "ひき,あげ")      ; 援上
        ("おほぶね" . "おほ,ぶね")      ; 大舟
        ("そんがい" . "そん,がい")      ; 損害
        ;; ("かみのけ" . "かみのけ")       ; 頭髮
        ("いりうみ" . "いり,うみ")      ; 海灣
        ("かぢづな" . "かぢ,づな")      ; 舵纜
        ("あめふり" . "あめ,ふり")      ; 降雨
        ;; ("りびやう" . "りびやう")       ; 痢病
        ;; ("つぎのひ" . "つぎのひ")       ; 次日
        ("へいたい" . "へい,たい")      ; 兵隊
        ("しゆへい" . "しゆ,へい")      ; 守兵
        ;; ("おもだち" . "おもだち")       ; 尊重
        ("よびあつ" . "よび,あつ")      ; 召集
        ;; ("しゆうし" . "しゆうし")       ; 宗旨
        ("ひとこと" . "ひと,こと")      ; 一言
        ;; ("あらため" . "あらため")       ; 悔改
        ("せいぜん" . "せい,ぜん")      ; 聖善
        ("れいせい" . "れい,せい")      ; 靈性
        ("ことがら" . "こと,がら")      ; 事情
        ;; ("くちはて" . "くちはて")       ; 朽壞
        ;; ("くちはつ" . "くちはつ")       ; 朽壞
        ("あくとく" . "あく,とく")      ; 惡慝
        ("ばうこん" . "ばう,こん")      ; 暴很
        ("こくはく" . "こく,はく")      ; 刻薄
        ("ざんがい" . "ざん,がい")      ; 讒害
        ("がうまん" . "がう,まん")      ; 傲慢
        ("はいやく" . "はい,やく")      ; 背約
        ;; ("かたより" . "かたより")       ; 徧視
        ;; ("さとれる" . "さとれる")       ; 明達
        ("れんたつ" . "れん,たつ")      ; 練達
        ("じんしや" . "じん,しや")      ; 仁者
        ;; ("なやめる" . "なやめる")       ; 困苦
        ;; ("むなしき" . "むなしき")       ; 虚空
        ;; ("くるしむ" . "くるしむ")       ; 勞苦
        ;; ("ちやくし" . "ちやくし")       ; 嫡子
        ;; ("おとうと" . "おとうと")       ; 幼子
        ;; ("あはれむ" . "あはれむ")       ; 憐憫
        ;; ("あはれま" . "あはれま")       ; 憐憫
        ;; ("あやまち" . "あやまち")       ; 錯失
        ("こつにく" . "こつ,にく")      ; 骨肉
        ("うけもど" . "うけ,もど")      ; 收納
        ;; ("うるほひ" . "うるほひ")       ; 汁漿
        ;; ("すくひて" . "すくひて")       ; 救者
        ;; ("なすべき" . "なすべき")      ; 當然
        ;; ("たひらか" . "たひらか")       ; 公平
        ;; ("あつきひ" . "あつきひ")       ; 熱炭
        ("たうてつ" . "たう,てつ")      ; 饕餐
        ;; ("かろしむ" . "かろしむ")       ; 藐視
        ("なにびと" . "なに,びと")      ; 何人
        ;; ("かろんず" . "かろんず")       ; 藐視
        ;; ("したがは" . "したがは")       ; 順從
        ("としごろ" . "とし,ごろ")      ; 年來
        ("しつぼく" . "しつ,ぼく")      ; 質朴
        ;; ("したがへ" . "したがへ")       ; 順從
        ;; ("ひろがり" . "ひろがり")       ; 傳揚
        ("しんせき" . "しん,せき")      ; 親戚
        ;; ("ひとりの" . "ひとりの")       ; 獨一
        ;; ("かしこき" . "かしこき")       ; 睿智
        ("がくしや" . "がく,しや")      ; 學者
        ("ろんしや" . "ろん,しや")      ; 論者
        ("でんだう" . "でん,だう")      ; 傳道
        ;; ("かろしめ" . "かろしめ")       ; 藐視
        ("いしずゑ" . "いし,ずゑ")      ; 基礎
        ("すぎこし" . "すぎ,こし")      ; 逾越
        ;; ("あそびめ" . "あそびめ")       ; 娼妓
        ("いんかう" . "いん,かう")      ; 淫行
        ;; ("かかはり" . "かかはり")       ; 關係
        ("くひもの" . "くひ,もの")      ; 食物
        ("のみもの" . "のみ,もの")      ; 飮物
        ;; ("つぶやき" . "つぶやき")       ; 怨言
        ("ていはつ" . "てい,はつ")      ; 薙髮
        ("ばんさん" . "ばん,さん")      ; 晩餐
        ;; ("みにくき" . "みにくき")       ; 不美
        ;; ("ととのへ" . "ととのへ")       ; 調和
        ;; ("いつまで" . "いつまで")      ; 永久
        ;; ("よのなか" . "よのなか")       ; 世間
        ("いちまん" . "いち,まん")      ; 一萬
        ("ほんやく" . "ほん,やく")      ; 繙譯
        ;; ("かたはら" . "かたはら")       ; 旁邊
        ("はうげん" . "はう,げん")      ; 方言
        ;; ("あやふき" . "あやふき")       ; 危險
        ;; ("またたく" . "またたく")       ; 瞬息
        ("よきをり" . "よき,をり")      ; 便時
        ;; ("まごころ" . "まごころ")       ; 丹心
        ;; ("ゆるやか" . "ゆるやか")       ; 寛容
        ("こんらん" . "こん,らん")      ; 混亂
        ("そへぶみ" . "そへ,ぶみ")      ; 薦書
        ;; ("みとむる" . "みとむる")       ; 注目
        ;; ("しののり" . "しののり")      ; 死法
        ;; ("ながらふ" . "ながらふ")       ; 長存
        ("きうやく" . "きう,やく")      ; 舊約
        ("こんにち" . "こん,にち")      ; 今日
        ("ほろ,ぶる" . "ほろ,ぶる")     ; 沈淪
        ;; ("かぎりな" . "かぎりな")       ; 永遠
        ("だいぜん" . "だい,ぜん")      ; 臺前
        ("この,のち" . "この,のち")     ; 以後
        ;; ("うたるる" . "うたるる")       ; 責打
        ("ほねをる" . "ほね,をる")      ; 勤勞
        ;; ("うれふる" . "うれふる")       ; 憂愁
        ;; ("ますます" . "ますます")       ; 益々
        ("もちもの" . "もち,もの")      ; 所有
        ;; ("たらざる" . "たらざる")       ; 不足
        ;; ("ひとしく" . "ひとしく")       ; 平均
        ;; ("しかのみ" . "しかのみ")       ; 第此
        ("きよねん" . "きよ,ねん")      ; 去年
        ;; ("しようこ" . "しようこ")       ; 證據
        ;; ("いよいよ" . "いよいよ")       ; 愈々
        ("きふれう" . "きふ,れう")      ; 給料
        ("たうぞく" . "たう,ぞく")      ; 盜賊
        ("どうぞく" . "どう,ぞく")      ; 同族
        ("かいちう" . "かい,ちう")      ; 海中
        ("けんげん" . "けん,げん")      ; 顯現
        ;; ("とりこむ" . "とりこむ")       ; 牢籠
        ("ざんげん" . "ざん,げん")      ; 讒言
        ;; ("ひとしき" . "ひとしき")       ; 相若
        ;; ("みぎのて" . "みぎのて")       ; 右手
        ;; ("すゑずゑ" . "すゑずゑ")       ; 裔々
        ;; ("とぢこめ" . "とぢこめ")       ; 拘幽
        ("ぜんげふ" . "ぜん,げふ")      ; 全業
        ("ぱんだね" . "ぱん,だね")      ; 麪酵
        ;; ("ふじゆつ" . "ふじゆつ")       ; 巫術
        ("きうこん" . "きう,こん")       ; 仇恨
        ("ぶんさう" . "ぶん,さう")       ; 分爭
        ("けつたう" . "けつ,たう")       ; 結黨
        ("すゐしゆ" . "すゐ,しゆ")       ; 醉酒
        ("じんあい" . "じん,あい")       ; 仁愛
        ("をんじう" . "をん,じう")       ; 温柔
        ("いうえき" . "いう,えき")       ; 有益
        ;; ("ならはし" . "ならはし")       ; 風俗
        ("しよけん" . "しよ,けん")       ; 諸權
        ("ひといへ" . "ひと,いへ")       ; 全屋
        ("くみたて" . "くみ,たて")       ; 搆合
        ("ぜんだい" . "ぜん,だい")       ; 前代
        ;; ("たまもの" . "たまもの")       ; 恩賜
        ;; ("ひろやか" . "ひろやか")       ; 寛容
        ;; ("ただよは" . "ただよは")       ; 蕩漾
        ;; ("つらなり" . "つらなり")       ; 聯緒
        ;; ("かたまり" . "かたまり")       ; 鞏固
        ;; ("もとれる" . "もとれる")       ; 背逆
        ("ちちはは" . "ちち,はは")       ; 父母
        ("けつにく" . "けつ,にく")       ; 血肉
        ("べんめい" . "べん,めい")       ; 辨明
        ("ひつえう" . "ひつ,えう")       ; 必要
        ;; ("まじはり" . "まじはり")       ; 交際
        ;; ("をののき" . "をののき")       ; 戰慄
        ;; ("あらそふ" . "あらそふ")       ; 爭辯
        ;; ("きふさぎ" . "きふさぎ")       ; 憂悶
        ("ばんぶつ" . "ばん,ぶつ")       ; 萬物
        ;; ("いやしき" . "いやしき")       ; 貧賤
        ("とりやり" . "とり,やり")       ; 授受
        ("けんぞく" . "けん,ぞく")       ; 眷屬
        ("ちのうへ" . "ちの,うへ")       ; 地上
        ;; ("たくはへ" . "たくはへ")       ; 蓄積
        ("くうげん" . "くう,げん")       ; 空言
        ;; ("みちたれ" . "みちたれ")       ; 充足
        ("ちうかん" . "ちう,かん")       ; 中間
        ("ついたち" . "つい,たち")       ; 月朔
        ;; ("へりくだ" . "へりくだ")       ; 謙卑
        ("せうがく" . "せう,がく")       ; 小學
        ;; ("をくわい" . "をくわい")       ; 汚穢
        ("あくよく" . "あく,よく")       ; 惡欲
        ("たんらん" . "たん,らん")       ; 貪婪
        ("はうどく" . "はう,どく")       ; 謗讟
        ("しうげん" . "しう,げん")       ; 醜言
        ("いつたい" . "いつ,たい")       ; 一體
        ;; ("みちたら" . "みちたら")       ; 充足
        ;; ("めのまへ" . "めのまへ")       ; 眼前
        ("こうへい" . "こう,へい")       ; 公平
        ;; ("あじつく" . "あじつく")       ; 調和
        ;; ("もろもろ" . "もろもろ")       ; 衆人
        ;; ("ときのま" . "ときのま")       ; 暫時
        ("ふたたび" . "ふた,たび")       ; 兩次
        ;; ("むなしく" . "むなしく")       ; 徒然
        ("がうれい" . "がう,れい")       ; 號令
        ;; ("なみする" . "なみする")       ; 藐視
        ("ぜんれい" . "ぜん,れい")       ; 全靈
        ("ぜんせい" . "ぜん,せい")       ; 全生
        ("ぜんしん" . "ぜん,しん")       ; 全身
        ;; ("まどはし" . "まどはし")       ; 詭理
        ("ざいあく" . "ざい,あく")       ; 罪惡
        ("ばんせい" . "ばん,せい")       ; 萬世
        ;; ("よむこと" . "よむこと")       ; 誦讀
        ;; ("ゆるかせ" . "ゆるかせ")       ; 忽畧
        ;; ("としより" . "としより")       ; 老人
        ("ばうしつ" . "ばう,しつ")       ; 媢嫉
        ("さうたう" . "さう,たう")       ; 爭鬪
        ("さうろん" . "さう,ろん")       ; 爭論
        ;; ("いしよく" . "いしよく")       ; 衣食
        ("めつばう" . "めつ,ばう")       ; 滅亡
        ("ちんりん" . "ちん,りん")       ; 沈淪
        ("かんにん" . "かん,にん")       ; 堪忍
        ("あんしゆ" . "あん,しゆ")       ; 按手
        ;; ("あきらか" . "あきらか")       ; 明著
        ("へいそつ" . "へい,そつ")       ; 兵卒
        ;; ("よのわざ" . "よのわざ")       ; 世事
        ("おひもと" . "おひ,もと")       ; 追求
        ;; ("やはらか" . "やはらか")       ; 和平
        ;; ("ののしり" . "ののしり")       ; 詬誶
        ;; ("ふじやう" . "ふじやう")       ; 不情
        ("ざんこく" . "ざん,こく")       ; 殘刻
        ;; ("たのしみ" . "たのしみ")       ; 佚樂
        ("けいけん" . "けい,けん")       ; 敬虔
        ;; ("おこなひ" . "おこなひ")       ; 品行
        ("みちのり" . "みち,のり")       ; 途程
        ("しよもつ" . "しよ,もつ")       ; 書籍
        ("ことわけ" . "こと,わけ")       ; 事由
        ("せんけう" . "せん,けう")       ; 宣教
        ;; ("まちまち" . "まちまち")       ; 各邑
        ("しんじや" . "しん,じや")       ; 信者
        ;; ("かるがる" . "かるがる")       ; 輕易
        ;; ("ただしく" . "ただしく")       ; 公義
        ("べんばく" . "べん,ばく")       ; 辯駁
        ("かつれい" . "かつ,れい")       ; 割禮
        ("らうじん" . "らう,じん")       ; 老人
        ("ていけつ" . "てい,けつ")       ; 貞潔
        ;; ("うやうや" . "うやうや")       ; 端莊
        ;; ("いまのよ" . "いまのよ")       ; 今世
        ("べんろん" . "べん,ろん")       ; 辯論
        ("さうとう" . "さう,とう")       ; 爭鬪
        ;; ("なぐさめ" . "なぐさめ")       ; 安慰
        ;; ("てづから" . "てづから")       ; 親手
        ;; ("めしうど" . "めしうど")       ; 囚人
        ;; ("かがやき" . "かがやき")       ; 光輝
        ;; ("なほざり" . "なほざり")       ; 等閑
        ;; ("あざむき" . "あざむき")       ; 誘惑
        ;; ("ひさしき" . "ひさしき")       ; 多年
        ;; ("かたくな" . "かたくな")       ; 剛愎
        ;; ("ふしぶし" . "ふしぶし")       ; 筋節
        ;; ("みわくる" . "みわくる")       ; 鑒察
        ;; ("すくひう" . "すくひう")       ; 救得
        ("よしあし" . "よし,あし")       ; 善惡
        ;; ("しのわざ" . "しのわざ")       ; 死行
        ("すひこみ" . "すひ,こみ")       ; 吸入
        ;; ("つぐもの" . "つぐもの")       ; 嗣者
        ("さきがけ" . "さき,がけ")       ; 前驅
        ;; ("いとたか" . "いとたか")       ; 至高
        ;; ("ぶんどり" . "ぶんどり")       ; 所獲
        ;; ("とりなさ" . "とりなさ")       ; 懇求
        ("かんえう" . "かん,えう")       ; 肝要
        ("くにびと" . "くに,びと")       ; 邦人
        ;; ("そのとき" . "そのとき")       ; 當時
        ;; ("あらたま" . "あらたま")       ; 振興
        ("ゆゐしよ" . "ゆゐ,しよ")       ; 遺書
        ;; ("あらはれ" . "あらはれ")       ; 顯現
        ("はんさい" . "はん,さい")       ; 燔祭
        ("ざいさい" . "ざい,さい")       ; 罪祭
        ;; ("しばしば" . "しばしば")       ; 屡々
        ("あしだい" . "あし,だい")       ; 足凳
        ("あいしん" . "あい,しん")       ; 愛心
        ;; ("よきわざ" . "よきわざ")       ; 善行
        ("いけにへ" . "いけ,にへ")       ; 犧牲
        ;; ("やきつく" . "やきつく")       ; 焚滅
        ;; ("ふみつけ" . "ふみつけ")       ; 蹂躙
        ;; ("よのつね" . "よのつね")       ; 尋常
        ;; ("なげすつ" . "なげすつ")       ; 投棄
        ;; ("うけつぐ" . "うけつぐ")       ; 承繼
        ("ことくに" . "こと,くに")       ; 異邦
        ("ふるさと" . "ふる,さと")       ; 家郷
        ;; ("いきかへ" . "いきかへ")       ; 復活
        ("がいこつ" . "がい,こつ")       ; 骸骨
        ("ふたおや" . "ふた,おや")       ; 父母
        ;; ("うるはし" . "うるはし")       ; 美都
        ("こうかい" . "こう,かい")       ; 紅海
        ("かんじや" . "かん,じや")       ; 偵者
        ;; ("やすらか" . "やすらか")       ; 平安
        ;; ("あざけり" . "あざけり")       ; 嬉笑
        ("めんやう" . "めん,やう")       ; 棉羊
        ;; ("ともしく" . "ともしく")       ; 窮乏
        ;; ("さまよひ" . "さまよひ")       ; 周流
        ("たへしの" . "たへ,しの")       ; 耐忍
        ;; ("みちびき" . "みちびき")       ; 先導
        ;; ("かくしご" . "かくしご")       ; 私子
        ;; ("こらしめ" . "こらしめ")       ; 懲治
        ("たんれん" . "たん,れん")       ; 鍛錬
        ;; ("やはらぐ" . "やはらぐ")       ; 和睦
        ;; ("にがきね" . "にがきね")       ; 苦根
        ("いつぱん" . "いつ,ぱん")       ; 一飯
        ("ひきかへ" . "ひき,かへ")       ; 挽回
        ("くろくも" . "くろ,くも")       ; 密雲
        ;; ("ちよろづ" . "ちよろづ")       ; 千萬
        ;; ("あつまり" . "あつまり")       ; 聚集
        ;; ("ちやうし" . "ちやうし")       ; 長子
        ("しんやく" . "しん,やく")       ; 新約
        ("なかだち" . "なか,だち")       ; 中保
        ;; ("もてなす" . "もてなす")       ; 接待
        ("こうがふ" . "こう,がふ")       ; 苟合
        ;; ("ほどこし" . "ほどこし")       ; 施捨
        ("けいやく" . "けい,やく")       ; 契約
        ("へいあん" . "へい,あん")       ; 平安
        ("よきこと" . "よき,こと")       ; 善事
        ("なにもの" . "なに,もの")       ; 何物
        ;; ("さだまり" . "さだまり")       ; 定準
        ;; ("いたづら" . "いたづら")       ; 徒然
        ;; ("みなしご" . "みなしご")       ; 孤子
        ;; ("いやしめ" . "いやしめ")       ; 藐視
        ;; ("しひたげ" . "しひたげ")       ; 凌虐
        ("にちよう" . "にち,よう")       ; 日用
        ;; ("をののけ" . "をののけ")       ; 戰慄
        ("かぢとり" . "かぢ,とり")       ; 舵子
        ("ぜんたい" . "ぜん,たい")       ; 全體
        ("はふもの" . "はふ,もの")       ; 昆蟲
        ;; ("しのどく" . "しのどく")       ; 死毒
        ("しほみづ" . "しほ,みづ")       ; 鹹水
        ;; ("あらそひ" . "あらそひ")       ; 爭競
        ("だんぢよ" . "だん,ぢよ")       ; 男女
        ;; ("ひととせ" . "ひと,とせ")       ; 一年
        ("いつさい" . "いつ,さい")       ; 一切
        ("おしたづ" . "おし,たづ")       ; 推究
        ("おやいし" . "おや,いし")       ; 首石
        ;; ("やどれる" . "やどれる")       ; 寄寓
        ;; ("かへりみ" . "かへりみ")       ; 眷顧
        ("しゆじん" . "しゆ,じん")       ; 主人
        ("かんとく" . "かん,とく")       ; 監督
        ;; ("おだやか" . "おだやか")       ; 恬靜
        ;; ("よりたの" . "よりたの")       ; 依頼
        ;; ("さまたげ" . "さまたげ")       ; 阻礙
        ;; ("やはらぎ" . "やはらぎ")       ; 和睦
        ("ねつしん" . "ねつ,しん")       ; 熱心
        ("ゆゑよし" . "ゆゑ,よし")       ; 縁由
        ("ひとたび" . "ひと,たび")       ; 一次
        ("はこぶね" . "はこ,ぶね")       ; 方舟
        ("ちんめん" . "ちん,めん")       ; 沈湎
        ("しゆゑん" . "しゆ,ゑん")       ; 酒宴
        ("はうたう" . "はう,たう")       ; 放蕩
        ("のべつた" . "のべ,つた")       ; 宣傳
        ("なにごと" . "なに,ごと")       ; 何事
        ("せつたい" . "せつ,たい")       ; 接待
        ;; ("ことごと" . "ことごと")       ; 毎事
        ("たちいり" . "たち,いり")       ; 干渉
        ("ぼくしや" . "ぼく,しや")       ; 牧者
        ;; ("かんむり" . "かんむり")       ; 冠冕
        ("けんそん" . "けん,そん")       ; 謙遜
        ;; ("たかぶる" . "たかぶる")       ; 驕傲
        ("たいのう" . "たい,のう")       ; 大能
        ("てのした" . "ての,した")       ; 手下
        ;; ("つつしめ" . "つつしめ")       ; 謹愼
        ;; ("へめぐり" . "へめぐり")       ; 徧行
        ("くちつけ" . "くち,つけ")       ; 接吻
        ("せいしつ" . "せい,しつ")       ; 性質
        ("そんせつ" . "そん,せつ")       ; 撙節
        ;; ("つつしみ" . "つつしみ")       ; 敬虔
        ("たうぜん" . "たう,ぜん")       ; 當然
        ;; ("ゐくわう" . "ゐくわう")       ; 威光
        ;; ("むさぼる" . "むさぼる")       ; 貪婪
        ("こうずゐ" . "こう,ずゐ")       ; 洪水
        ;; ("わがまま" . "わがまま")       ; 自放
        ;; ("むさぼり" . "むさぼり")       ; 貪婪
        ;; ("こどもら" . "こどもら")       ; 子輩
        ("にくよく" . "にく,よく")       ; 肉慾
        ("しんじつ" . "しん,じつ")       ; 眞實
        ;; ("いましめ" . "いましめ")       ; 命令
        ;; ("すゑのひ" . "すゑのひ")       ; 末日
        ("せんねん" . "せん,ねん")       ; 千年
        ("たいしつ" . "たい,しつ")       ; 體質
        ("あんぜん" . "あん,ぜん")       ; 安然
        ("せいしよ" . "せい,しよ")       ; 聖書
        ("ねんごろ" . "ねん,ごろ")       ; 懇切
        ;; ("たかぶり" . "たかぶり")       ; 驕傲
        ;; ("わかきこ" . "わかきこ")       ; 孺子
        ;; ("すゑのよ" . "すゑのよ")       ; 季世
        ;; ("ただしき" . "ただしき")       ; 公義
        ("めいれい" . "めい,れい")       ; 命令
        ("なにゆゑ" . "なに,ゆゑ")       ; 何故
        ;; ("ともしき" . "ともしき")       ; 窮乏
        ;; ("よのひと" . "よのひと")       ; 世人
        ;; ("ひとりご" . "ひとりご")       ; 獨子
        ;; ("まつたき" . "まつたき")       ; 全備
        ;; ("まつたう" . "まつたう")       ; 全備
        ;; ("まことの" . "まことの")       ; 眞理
        ;; ("をさなご" . "をさなご")       ; 小子
        ;; ("すこやか" . "すこやか")       ; 康強
        ;; ("よろこび" . "よろこび")       ; 喜樂
        ("たびびと" . "たび,びと")       ; 賓旅
        ("ともだち" . "とも,だち")       ; 諸友
        ("しきよく" . "しき,よく")       ; 色慾
        ("にくたい" . "にく,たい")       ; 肉體
        ;; ("あやまり" . "あやまり")       ; 迷謬
        ;; ("ふるまひ" . "ふるまひ")       ; 筵席
        ("くらやみ" . "くら,やみ")       ; 黒暗
        ("ばんぐん" . "ばん,ぐん")       ; 萬軍
        ("つみびと" . "つみ,びと")       ; 罪人
        ;; ("つぶやく" . "つぶやく")       ; 怨言
        ;; ("あざける" . "あざける")       ; 戲謔
        ;; ("よこしま" . "よこしま")       ; 横逆
        ("せいれい" . "せい,れい")       ; 聖靈
        ;; ("みづから" . "みづから")       ; 自己
        ;; ("あはれみ" . "あはれみ")       ; 矜恤
        ;; ("いくわう" . "いくわう")       ; 威光
        ;; ("みくらゐ" . "みくらゐ")       ; 寶座
        ("おほみづ" . "おほ,みづ")       ; 大水
        ("ほねをり" . "ほね,をり")       ; 勞苦
        ("あくにん" . "あく,にん")       ; 惡人
        ;; ("まづしき" . "まづしき")       ; 貧乏
        ;; ("いはゆる" . "いはゆる")       ; 所謂
        ("やきもの" . "やき,もの")       ; 陶瓦
        ;; ("けしおと" . "けしおと")       ; 塗抹
        ;; ("こころみ" . "こころみ")       ; 試煉
        ;; ("すみやか" . "すみやか")       ; 迅速
        ("ちうしん" . "ちう,しん")       ; 忠信
        ("ざうくわ" . "ざう,くわ")       ; 造化
        ;; ("めぐすり" . "めぐすり")       ; 目藥
        ("うちそと" . "うち,そと")       ; 内外
        ;; ("みこころ" . "みこころ")       ; 意旨
        ;; ("せんせん" . "せんせん")       ; 千々
        ("あかむま" . "あか,むま")       ; 赤馬
        ("くろむま" . "くろ,むま")       ; 黒馬
        ("おほむぎ" . "おほ,むぎ")       ; 大麥
        ;; ("あをざめ" . "あをざめ")       ; 灰色
        ("まうじう" . "まう,じう")       ; 猛獸
        ("さいだん" . "さい,だん")       ; 祭壇
        ("おほかぜ" . "おほ,かぜ")       ; 大風
        ("まきもの" . "まき,もの")       ; 卷物
        ;; ("やまやま" . "やまやま")       ; 諸山
        ;; ("しまじま" . "しまじま")       ; 諸島
        ;; ("たふとび" . "たふとび")       ; 尊敬
        ("かんなん" . "かん,なん")       ; 艱難
        ("ふういん" . "ふう,いん")       ; 封印
        ("はんとき" . "はん,とき")       ; 半時
        ("あをくさ" . "あを,くさ")       ; 青草
        ("しにふね" . "しに,ふね")       ; 死船
        ("いんちん" . "いん,ちん")       ; 茵蔯
        ("さんにん" . "さん,にん")       ; 三人
        ;; ("いくさば" . "いくさば")       ; 戰塲
        ;; ("まんまん" . "まんまん")       ; 萬々
        ;; ("まぼろし" . "まぼろし")       ; 異象
        ("ありさま" . "あり,さま")       ; 形状
        ("むねあて" . "むね,あて")       ; 胸當
        ("たうせつ" . "たう,せつ")       ; 盜竊
        ("かうだん" . "かう,だん")       ; 香壇
        ;; ("ふみあら" . "ふみあら")       ; 蹂躙
        ("かんらん" . "かん,らん")       ; 橄欖
        ("とうだい" . "とう,だい")       ; 燈臺
        ("いくたび" . "いく,たび")       ; 幾囘
        ("れいもつ" . "れい,もつ")       ; 禮物
        ("やりとり" . "やり,とり")       ; 贈答
        ("かんしや" . "かん,しや")       ; 感謝
        ("やくそく" . "やく,そく")       ; 約束
        ;; ("つかさど" . "つかさど")       ; 主理
        ("いくばく" . "いく,ばく")       ; 幾時
        ("せんぼう" . "せん,ぼう")       ; 僭妄
        ("だいせう" . "だい,せう")       ; 大小
        ("うりかひ" . "うり,かひ")       ; 貿易
        ("ちうわう" . "ちう,わう")       ; 中央
        ("しよぞく" . "しよ,ぞく")       ; 諸族
        ("ふくいん" . "ふく,いん")       ; 福音
        ("おほごゑ" . "おほ,ごゑ")       ; 大聲
        ("よるひる" . "よる,ひる")       ; 夜晝
        ("しんかう" . "しん,かう")       ; 信仰
        ("にんたい" . "にん,たい")       ; 忍耐
        ;; ("はたらき" . "はたらき")       ; 勞苦
        ("かりどき" . "かり,どき")       ; 刈時

        ("こくもつ" . "こく,もつ")       ; 穀物
        ("ばんみん" . "ばん,みん")       ; 萬民
        ("たいやう" . "たい,やう")       ; 太陽
        ("たいねつ" . "たい,ねつ")       ; 大熱
        ("しゆもつ" . "しゆ,もつ")       ; 腫物
        ("おほかは" . "おほ,かは")       ; 大河
        ("ぬすびと" . "ぬす,びと")       ; 盜賊
        ("いかづち" . "いか,づち")       ; 迅雷
        ("いなづま" . "いな,づま")       ; 閃電
        ("このかた" . "この,かた")       ; 以來

        ("せんばう" . "せん,ばう")       ; 僭妄
        ("そこなき" . "そこ,なき")       ; 無底
        ("しよみん" . "しよ,みん")       ; 庶民
        ("しよいん" . "しよ,いん")       ; 諸音
        ;; ("さぶしく" . "さぶしく")       ; 荒墟
        ("によわう" . "によ,わう")       ; 女王
        ("いちにち" . "いち,にち")       ; 一日
        ;; ("かなしみ" . "かなしみ")       ; 悲哀
        ("きんぎん" . "きん,ぎん")       ; 金銀
        ("かうぼく" . "かう,ぼく")       ; 香木
        ("しんちう" . "しん,ちう")       ; 眞鍮
        ("らふせき" . "らふ,せき")       ; 蝋石
        ("にくけい" . "にく,けい")       ; 肉桂
        ("かうれう" . "かう,れう")       ; 香料
        ("もつやく" . "もつ,やく")       ; 沒藥
        ("にうかう" . "にう,かう")       ; 乳香
        ("くわこく" . "くわ,こく")       ; 果穀
        ("はうせき" . "はう,せき")       ; 寶石
        ;; ("きえうせ" . "きえうせ")       ; 消滅
        ("ふなをさ" . "ふな,をさ")       ; 舟長
        ;; ("ひとびと" . "ひとびと")       ; 人々
        ;; ("すぎはひ" . "すぎはひ")       ; 生業
        ;; ("まきちら" . "まきちら")       ; 散布
        ;; ("ひととき" . "ひととき")       ; 一時
        ("こうじん" . "こう,じん")       ; 工人
        ;; ("あきうど" . "あきうど")       ; 商人
        ("いんらん" . "いん,らん")       ; 淫亂
        ("いきもの" . "いき,もの")       ; 活物
        ("こんいん" . "こん,いん")       ; 婚姻
        ("いつぴき" . "いつ,ぴき")       ; 一匹
        ;; ("たたかひ" . "たたかひ")       ; 戰爭
        ("しよぐん" . "しよ,ぐん")       ; 諸軍
        ("しよしゆ" . "しよ,しゆ")       ; 諸主
        ("くうちう" . "くう,ちう")       ; 空中
        ("ぐんたい" . "ぐん,たい")       ; 軍隊
        ("しろむま" . "しろ,むま")       ; 白馬
        ("らうじや" . "らう,じや")       ; 老蛇
        ("しよこく" . "しよ,こく")       ; 諸國
        ;; ("しばらく" . "しばらく")       ; 暫時
        ;; ("くにぐに" . "くにぐに")       ; 列邦
        ("ぢんえい" . "ぢん,えい")       ; 陣營
        ;; ("くるしみ" . "くるしみ")       ; 痛苦
        ("はなむこ" . "はな,むこ")       ; 新郎
        ("ばんもつ" . "ばん,もつ")       ; 萬物
        ;; ("わざはひ" . "わざはひ")       ; 災殃
        ("かなまり" . "かな,まり")       ; 金椀
        ("しちにん" . "しち,にん")       ; 七人
        ;; ("きよらか" . "きよらか")       ; 清潔
        ("いしがき" . "いし,がき")       ; 石垣
        ;; ("さまざま" . "さまざま")       ; 各樣
        ("だいいち" . "だい,いち")       ; 第一
        ("だいさん" . "だい,さん")       ; 第三
        ("だいろく" . "だい,ろく")       ; 第六
        ("だいしち" . "だい,しち")       ; 第七
        ("だいはち" . "だい,はち")       ; 第八
        ("みづいろ" . "みづ,いろ")       ; 水色
        ("だいじふ" . "だい,じふ")       ; 第十
        ("しんじゆ" . "しん,じゆ")       ; 眞珠
        ("ぎやまん" . "ぎや,まん")       ; 玻璃
        ("ぜんのう" . "ぜん,のう")       ; 全能
        ("じつげつ" . "じつ,げつ")       ; 日月
        ;; ("ともしび" . "ともしび")       ; 月燈
        ("しよわう" . "しよ,わう")       ; 諸王
        ;; ("ひねもす" . "ひねもす")       ; 終日
        ;; ("たふとき" . "たふとき")       ; 尊貴
        ("ばんこく" . "ばん,こく")       ; 萬國
        ;; ("たましひ" . "たましひ")       ; 靈魂
        ("あしもと" . "あし,もと")       ; 足下
        ;; ("ふぎなる" . "ふぎなる")       ; 不義
        ;; ("きたなき" . "きたなき")       ; 汚穢
        ;; ("おのおの" . "おの,おの")      ; 各人
        ("いやさき" . "いや,さき")      ; 首先
        ("いやはて" . "いや,はて")      ; 末後
        ;; ("まじゆつ" . "まじゆつ")       ; 魔術
        ("かんいん" . "かん,いん")      ; 奸淫
        ("ぐうざう" . "ぐう,ざう")      ; 偶像
        ;; ("いつはり" . "いつはり")       ; 虚妄
        ;; ("なんぢら" . "なんぢら")       ; 爾曹
        ("はなよめ" . "はな,よめ")      ; 新婦
        ))

(setq ruby-convert-23-table
      '(("しだい" . "し,だい")
        ("ちすぢ" . "ち,すぢ")
        ("こせき" . "こ,せき")
        ("うひご" . "うひ,ご")
        ("みきき" . "み,きき")
        ("なんし" . "なん,し")
        ("さんび" . "さん,び")
        ("みつか" . "みつ,か")
        ("いつか" . "いつ,か")
        ("けうし" . "けう,し")
        ("ざいゐ" . "ざい,ゐ")
        ("じふご" . "じふ,ご")
        ("うちば" . "うち,ば")
        ("しじふ" . "し,じふ")
        ("てんか" . "てん,か")
        ("ふつか" . "ふつ,か")
        ("うみべ" . "うみ,べ")
        ("きたう" . "き,たう")
        ("ぜんび" . "ぜん,び")
        ("しにん" . "し,にん")
        ("かれい" . "か,れい")
        ("おくぎ" . "おく,ぎ")
        ("あくま" . "あく,ま")
        ("ひれふ" . "ひれ,ふ")
        ("ちろう" . "ち,ろう")
        ("いしや" . "い,しや")
        ("ごせん" . "ご,せん")
        ("ごじふ" . "ご,じふ")
        ("じふに" . "じふ,に")
        ("やうか" . "やう,か")
        ("よなか" . "よ,なか")
        ("はくか" . "はく,か")
        ("やさい" . "や,さい")
        ("せんぞ" . "せん,ぞ")
        ("すまん" . "す,まん")
        ("ぎぜん" . "ぎ,ぜん")
        ("ぢごく" . "ぢ,ごく")
        ("きふじ" . "きふ,じ")
        ("とちう" . "と,ちう")
        ("ことし" . "こ,とし")
        ("むいか" . "むい,か")
        ("まつざ" . "まつ,ざ")
        ("かみざ" . "かみ,ざ")
        ("でんぢ" . "でん,ぢ")
        ("しまい" . "し,まい")
        ("わぼく" . "わ,ぼく")
        ("たねん" . "た,ねん")
        ("ふさい" . "ふ,さい")
        ("こむぎ" . "こ,むぎ")
        ("せうじ" . "せう,じ")
        ("だいじ" . "だい,じ")
        ("むえき" . "む,えき")
        ("いわう" . "い,わう")
        ("たはた" . "た,はた")
        ("ちうや" . "ちう,や")
        ("らくだ" . "らく,だ")
        ("このよ" . "この,よ")
        ("しはう" . "し,はう")
        ("あいし" . "あい,し")
        ("のうふ" . "のう,ふ")
        ("せいじ" . "せい,じ")
        ("かうざ" . "かう,ざ")
        ("ぢしん" . "ぢ,しん")
        ("ききん" . "き,きん")
        ("けんゐ" . "けん,ゐ")
        ("てんち" . "てん,ち")
        ("ぎんす" . "ぎん,す")
        ("さいふ" . "さい,ふ")
        ("よげん" . "よ,げん")
        ("しそつ" . "し,そつ")
        ("あくじ" . "あく,じ")
        ("いつき" . "いつ,き")
        ("あをき" . "あを,き")
        ("かれき" . "かれ,き")
        ("いふく" . "い,ふく")
        ("たにん" . "た,にん")
        ("みくに" . "み,くに")
        ("まなか" . "ま,なか")
        ("もなか" . "も,なか")          ; 眞中
        ("ぎじん" . "ぎ,じん")
        ("ぎゐん" . "ぎ,ゐん")
        ("なぬか" . "なぬ,か")
        ("ひとり" . "ひと,り")
        ("さいし" . "さい,し")
        ("しざい" . "し,ざい")
        ("ふたり" . "ふた,り")
        ("みたり" . "みた,り")
        ("てあし" . "て,あし")
        ("これら" . "これ,ら")
        ("かれら" . "かれ,ら")
        ("けいづ" . "けい,づ")      ; 系圖
        ("のみつ" . "の,みつ")      ; 野蜜
        ("にうわ" . "にう,わ")      ; 柔和
        ("ごたい" . "ご,たい")      ; 五體
        ("いちり" . "いち,り")      ; 一里
        ("あんぴ" . "あん,ぴ")      ; 安否
        ("くらう" . "く,らう")      ; 苦勞
        ("かたて" . "かた,て")      ; 一手
        ("いしぢ" . "いし,ぢ")      ; 磽地
        ("みいだ" . "み,いだ")      ; 見出
        ("けしき" . "け,しき")      ; 景色
        ("ばんじ" . "ばん,じ")      ; 萬事
        ("かため" . "かた,め")      ; 一眼
        ("つまこ" . "つま,こ")      ; 妻孥
        ("じじん" . "じ,じん")      ; 寺人
        ("だいに" . "だい,に")      ; 第二
        ("まきん" . "ま,きん")      ; 馬芹
        ("ふはふ" . "ふ,はふ")      ; 不法
        ("ごにん" . "ご,にん")      ; 五人
        ("かりね" . "かり,ね")      ; 假寐
        ("にせん" . "に,せん")      ; 二千
        ("なだか" . "な,だか")      ; 名高
        ("みぎて" . "みぎ,て")      ; 右手
        ("さんじ" . "さん,じ")      ; 三時
        ("じいう" . "じ,いう")      ; 自由
        ("じゆう" . "じ,ゆう")      ; 自由 (原本:明示二十九年新約全書)
        ("でいり" . "で,いり")      ; 出入
        ("よつか" . "よつ,か")      ; 四日
        ("むゆか" . "むゆ,か")      ; 六日
        ("しゆろ" . "しゆ,ろ")      ; 椶櫚
        ("ふうし" . "ふう,し")      ; 夫子
        ("こぶね" . "こ,ぶね")      ; 小舟
        ("あまた" . "あま,た")      ; 許多
        ("すじつ" . "す,じつ")      ; 數日
        ("すねん" . "す,ねん")      ; 數年
        ("ばしよ" . "ば,しよ")      ; 塲處
        ("よにん" . "よ,にん")      ; 四人
        ("にちや" . "にち,や")      ; 日夜
        ("すにん" . "す,にん")      ; 數人
        ("きけつ" . "き,けつ")      ; 詭譎
        ("あくき" . "あく,き")      ; 惡鬼
        ("しせん" . "し,せん")      ; 四千
        ("ぎろん" . "ぎ,ろん")      ; 議論
        ("むほん" . "む,ほん")      ; 謀反
        ("りえん" . "り,えん")      ; 離緣
        ("りゑん" . "り,ゑん")      ; 離緣
        ("はかせ" . "はか,せ")      ; 博士
        ("ししや" . "し,しや")      ; 使者
        ("ぶんり" . "ぶん,り")      ; 分釐
        ("いはう" . "い,はう")      ; 異邦
        ("へんげ" . "へん,げ")      ; 變化
        ("いはう" . "い,はう")      ; 異邦
        ("だうし" . "だう,し")      ; 導師
        ("ちしや" . "ち,しや")      ; 智者
        ("あれち" . "あれ,ち")      ; 荒地
        ("みたび" . "み,たび")      ; 三次
        ("ゆきき" . "ゆき,き")      ; 往來
        ("せいと" . "せい,と")      ; 聖徒
        ("ようい" . "よう,い")      ; 用意
        ("しつと" . "しつ,と")      ; 嫉妒
        ("じさつ" . "じ,さつ")      ; 自殺
        ("めあき" . "め,あき")      ; 目明
        ("へいき" . "へい,き")      ; 兵器
        ("すみび" . "すみ,び")      ; 炭火
        ("ぶだう" . "ぶ,だう")      ; 葡萄
        ("ふけん" . "ふ,けん")      ; 不虔
        ("しよく" . "し,よく")      ; 嗜慾
        ("ひばう" . "ひ,ばう")      ; 毀謗
        ("ふかう" . "ふ,かう")      ; 不孝
        ("かふぶ" . "かふ,ぶ")      ; 狎侮
        ("しんく" . "しん,く")      ; 辛苦
        ("ぎぶん" . "ぎ,ぶん")      ; 儀文
        ("しそん" . "し,そん")      ; 子孫
        ("きばう" . "き,ばう")      ; 希望
        ("おんし" . "おん,し")      ; 恩賜
        ("したい" . "し,たい")      ; 肢體
        ("いんぷ" . "いん,ぷ")      ; 淫婦
        ("こじま" . "こ,じま")      ; 小島
        ("せかい" . "せ,かい")      ; 世界
        ("しうし" . "しう,し")      ; 宗旨
        ("みつか" . "みつ,か")      ; 三日
        ("ちうぎ" . "ちう,ぎ")      ; 忠義
        ("しはい" . "し,はい")      ; 支配
        ("ぢしよ" . "ぢ,しよ")      ; 地所
        ("むはふ" . "む,はふ")      ; 無法
        ("むがく" . "む,がく")      ; 無學
        ("ねだい" . "ね,だい")      ; 寢牀
        ("まくや" . "まく,や")      ; 幕屋
        ("さつき" . "さつ,き")      ; 殺氣
        ("じこく" . "じ,こく")      ; 時刻
        ("よすみ" . "よ,すみ")      ; 四隅
        ("ないじ" . "ない,じ")      ; 内侍
        ("ふなで" . "ふな,で")      ; 船出
        ("ますぐ" . "ま,すぐ")      ; 眞直
        ("どれい" . "ど,れい")      ; 奴隷
        ("いうし" . "いう,し")      ; 有司
        ("かぞく" . "か,ぞく")      ; 家族
        ("きしん" . "き,しん")      ; 鬼神
        ("うちう" . "う,ちう")      ; 宇宙
        ("しじん" . "し,じん")      ; 詩人
        ("ぎゑん" . "ぎ,ゑん")      ; 戲園
        ("かげつ" . "か,げつ")      ; ヶ月
        ("がげつ" . "が,げつ")      ; ヶ月
        ("つみに" . "つみ,に")      ; 積荷
        ("ふなぢ" . "ふな,ぢ")      ; 船路
        ("きへい" . "き,へい")      ; 騎兵
        ("とをか" . "とを,か")      ; 十日
        ("ぜんち" . "ぜん,ち")      ; 全地
        ("たじつ" . "た,じつ")      ; 他日
        ("ふなぐ" . "ふな,ぐ")      ; 船具
        ("すさき" . "す,さき")      ; 洲崎
        ("てんり" . "てん,り")      ; 天理
        ("ふしん" . "ふ,しん")      ; 不信
        ("ふたご" . "ふた,ご")      ; 二子
        ("こうし" . "こう,し")      ; 工師
        ("じしゆ" . "じ,しゆ")      ; 自主
        ("ちしき" . "ち,しき")      ; 知識
        ("はせば" . "はせ,ば")      ; 馳場
        ("はうび" . "はう,び")      ; 襃美
        ("いたん" . "い,たん")      ; 異端
        ("ひれい" . "ひ,れい")      ; 非禮
        ("もくし" . "もく,し")      ; 默示
        ("くうき" . "くう,き")      ; 空氣
        ("けつき" . "けつ,き")      ; 血氣
        ("ひつし" . "ひつ,し")      ; 必死
        ("せきひ" . "せき,ひ")      ; 石碑
        ("にくひ" . "にく,ひ")      ; 肉碑
        ("さいう" . "さ,いう")      ; 左右
        ("はせん" . "は,せん")      ; 破船
        ("たびぢ" . "たび,ぢ")      ; 旅路
        ("はうし" . "はう,し")      ; 放恣
        ("きらく" . "き,らく")      ; 喜樂
        ("へいわ" . "へい,わ")      ; 平和
        ("ぼくし" . "ぼく,し")      ; 牧師
        ("しつじ" . "しつ,じ")      ; 執事
        ("じだい" . "じ,だい")      ; 時代
        ("ふんど" . "ふん,ど")      ; 糞土
        ("いちじ" . "いち,じ")      ; 一事
        ("いちど" . "いち,ど")      ; 一度
        ("りがく" . "り,がく")      ; 理學
        ("せつき" . "せつ,き")      ; 節期
        ("しげふ" . "し,げふ")      ; 嗣業
        ("きおく" . "き,おく")      ; 記憶
        ("しゆい" . "しゆ,い")      ; 主意
        ("ふふく" . "ふ,ふく")      ; 不服
        ("ふけい" . "ふ,けい")      ; 不敬
        ("ふけつ" . "ふ,けつ")      ; 不潔
        ("ゆうき" . "ゆう,き")      ; 勇氣
        ("らんだ" . "らん,だ")      ; 懶惰
        ("ばうぎ" . "ばう,ぎ")      ; 妄疑
        ("だつそ" . "だつ,そ")      ; 脱疽
        ("らうふ" . "らう,ふ")      ; 老婦
        ("ぜんか" . "ぜん,か")      ; 全家
        ("もろは" . "もろ,は")      ; 兩刃
        ("あかご" . "あか,ご")      ; 赤子
        ("らいせ" . "らい,せ")      ; 來世
        ("れいぎ" . "れい,ぎ")      ; 禮儀
        ("かうろ" . "かう,ろ")      ; 香鑪
        ("われら" . "われ,ら")      ; 我儕
        ("みつき" . "み,つき")      ; 三月
        ("くなん" . "く,なん")      ; 苦難
        ("めなう" . "め,なう")      ; 瑪瑙
        ("しばう" . "し,ばう")      ; 死亡
        ("だいし" . "だい,し")      ; 第四
        ("だいご" . "だい,ご")      ; 第五
        ("だいく" . "だい,く")      ; 第九
        ("きにん" . "き,にん")      ; 貴人
        ("ひいろ" . "ひ,いろ")      ; 火色
        ("くつう" . "く,つう")      ; 苦痛
        ("けんご" . "けん,ご")      ; 堅固
        ("きいろ" . "き,いろ")      ; 黄色
        ("すけん" . "す,けん")      ; 數件
        ("ゆうし" . "ゆう,し")      ; 勇士
        ("ひんぷ" . "ひん,ぷ")      ; 貧富
        ("さいち" . "さい,ち")      ; 才知
        ("ざうげ" . "ざう,げ")      ; 象牙
        ("むぎこ" . "むぎ,こ")      ; 麥粉
        ("くわび" . "くわ,び")      ; 華美
        ("べんし" . "べん,し")      ; 辯士
        ("かしこ" . "かし,こ")      ; 彼處
        ("いづこ" . "いづ,こ")      ; 何處
        ("いづく" . "いづ,く")      ; 何處
        ("いんじ" . "いん,じ")      ; 淫辭
        ("よつぎ" . "よ,つぎ")      ; 世嗣
        ("ねつき" . "ねつ,き")      ; 熱氣
        ("すみか" . "すみ,か")      ; 住處
        ("しふぎ" . "しふ,ぎ")      ; 集議
        ("したぎ" . "した,ぎ")      ; 裏衣
        ("うはぎ" . "うは,ぎ")      ; 外服
        ("よきち" . "よき,ち")      ; 沃壤
        ("ほどへ" . "ほど,へ")      ; 歴久
        ("しばし" . "しば,し")      ; 暫時
        ("はかば" . "はか,ば")      ; 墓間
        ("いかん" . "いか,ん")      ; 如何
        ("いかが" . "いか,が")      ; 如何
        ("いかに" . "いか,に")      ; 如何
        ("たくみ" . "たく,み")      ; 木匠
        ("さばき" . "さば,き")      ; 審判
        ("さばく" . "さば,く")      ; 審判
        ("さばか" . "さば,か")      ; 審判
        ("まもり" . "まも,り")      ; 保護
        ("いくつ" . "いく,つ")      ; 幾何
        ("いちば" . "いち,ば")      ; 街市
        ("つたへ" . "つた,へ")      ; 遺傳
        ("おふし" . "おふ,し")      ; 唖者
        ("しるし" . "しる,し")      ; 休徴
        ("とほく" . "とほ,く")      ; 遠處
        ("めしひ" . "めし,ひ")      ; 瞽者
        ("いのち" . "いの,ち")      ; 生命
        ("むかふ" . "むか,ふ")      ; 對面
        ("みやこ" . "みや,こ")      ; 城邑
        ("あるじ" . "ある,じ")      ; 主人
        ("をしへ" . "をし,へ")      ; 教誨、教訓
        ("ころも" . "ころ,も")      ; 衣服
        ("やもめ" . "やも,め")      ; 寡婦
        ("なやみ" . "なや,み")      ; 患難
        ("いのり" . "いの,り")      ; 祈祷
        ("つかひ" . "つか,ひ")      ; 使者
        ("いなご" . "いな,ご")      ; 蝗蟲
        ("さけび" . "さけ,び")      ; 喊叫
        ("きこえ" . "きこ,え")      ; 聲名
        ("つかへ" . "つか,へ")      ; 供事
        ("こころ" . "ここ,ろ")      ; 聖意
        ("けがし" . "けが,し")      ; 褻涜
        ("けがさ" . "けが,さ")      ; 謗讟
        ("たから" . "たか,ら")      ; 貨財
        ("よろづ" . "よろ,づ")      ; 百樣
        ("にぶき" . "にぶ,き")      ; 愚頑
        ("あまり" . "あま,り")      ; 有奇
        ("たとひ" . "たと,ひ")      ; 假令
        ("まどひ" . "まど,ひ")      ; 誘惑
        ("とりて" . "とり,て")      ; 逮捕
        ("ちから" . "ちか,ら")      ; 大權
        ("いかば" . "いか,ば")      ; 幾何
        ("まつり" . "まつ,り")      ; 節筵; 祭祀
        ("いつも" . "いつ,も")      ; 恆例
        ("しるべ" . "しる,べ")      ;
        ("つひえ" . "つひ,え")      ; 糜費
        ("かたみ" . "かた,み")      ; 記念
        ("おきて" . "おき,て")      ; 禮儀
        ("ねがひ" . "ねが,ひ")      ; 祈祷
        ("つとめ" . "つと,め")      ; 職事
        ("をとめ" . "をと,め")      ; 處女
        ("をどり" . "をど,り")      ; 跳動
        ("たすけ" . "たす,け")      ; 扶持
        ("すくひ" . "すく,ひ")      ; 拯救　
        ("めぐみ" . "めぐ,み")      ; 仁恵
        ("くらき" . "くら,き")      ; 幽暗
        ("およな" . "およ,な")      ; 老女
        ("いはひ" . "いは,ひ")      ; 節筵
        ("さとき" . "さと,き")      ; 知慧
        ("こたへ" . "こた,へ")      ; 應對
        ("おほせ" . "おほ,せ")      ; 命令
        ("すべて" . "すべ,て")      ; 四方
        ("まがり" . "まが,り")      ; 屈曲
        ("みつぎ" . "みつ,ぎ")      ; 税銀
        ("あひだ" . "あひ,だ")      ; 諸日
        ("まはり" . "まは,り")      ; 四方
        ("そだち" . "そだ,ち")      ; 長育
        ("おさへ" . "おさ,へ")      ; 壓制
        ("とほり" . "とほ,り")      ; 徑行
        ("おほく" . "おほ,く")      ; 許多
        ("けがす" . "けが,す")      ; 褻涜
        ("おほい" . "おほ,い")      ; 豐盛
        ("ただち" . "ただ,ち")      ; 立刻
        ("むくい" . "むく,い")      ; 賞賜
        ("ながれ" . "なが,れ")      ; 横流
        ("やぶれ" . "やぶ,れ")      ; 頽壞
        ("あざみ" . "あざ,み")      ; 蒺藜　
        ("つんぼ" . "つん,ぼ")      ; 聾者
        ("すぐれ" . "すぐ,れ")      ; 卓越
        ("わらべ" . "わら,べ")      ; 童子
        ("もたぬ" . "もた,ぬ")      ; 無有
        ("うけぬ" . "うけ,ぬ")      ; 不接
        ("ほとり" . "ほと,り")      ; 四圍
        ("たふし" . "たふ,し")      ; 傾跌
        ("あるひ" . "ある,ひ")      ; 一日
        ("あたひ" . "あた,ひ")      ; 工錢
        ("しらべ" . "しら,べ")      ; 調査
        ("をさめ" . "をさ,め")      ; 管理
        ("はじめ" . "はじ,め")      ; 初次
        ("かたへ" . "かた,へ")      ; 右方、左方
        ("あしき" . "あし,き")      ; 不善
        ("からだ" . "から,だ")      ; 身體
        ("はやて" . "はや,て")      ; 颶風
        ("すゐき" . "すゐ,き")      ; 腹脹
        ("まちゐ" . "まち,ゐ")      ; 佇望
        ("あがめ" . "あが,め")      ; 尊崇
        ("おろか" . "おろ,か")      ; 無知
        ("もてる" . "もて,る")      ; 所有
        ("かくれ" . "かく,れ")      ; 隱沒
        ("あかし" . "あか,し")      ; 證明
        ("さとり" . "さと,り")      ; 智識
        ("そだつ" . "そだ,つ")      ; 生長
        ("そなへ" . "そな,へ")      ; 預備
        ("わかた" . "わか,た")      ; 分爭
        ("ほのほ" . "ほの,ほ")      ; 火炎
        ("うばひ" . "うば,ひ")      ; 強索
        ("だまれ" . "だま,れ")      ; 默止
        ("あらは" . "あら,は")      ; 顯明
        ("ふくさ" . "ふく,さ")      ; 手巾
        ("だまり" . "だま,り")      ; 默止
        ("さけぶ" . "さけ,ぶ")      ; 號呼
        ("あるく" . "ある,く")      ; 遊行
        ("かざれ" . "かざ,れ")      ; 修飾
        ("をはり" . "をは,り")      ; 末期
        ("もとる" . "もと,る")      ; 敵對
        ("おそれ" . "おそ,れ")      ; 危懼
        ("わかれ" . "わか,れ")      ; 支派
        ("はげみ" . "はげ,み")      ; 極力
        ("むすめ" . "むす,め")      ; 女子
        ("にぶく" . "にぶ,く")      ; 昏迷
        ("ほろび" . "ほろ,び")      ; 沈淪
        ("けがれ" . "けが,れ")      ; 汚穢
        ("やすき" . "やす,き")      ; 平安
        ("かぎり" . "かぎ,り")      ; 限量
        ("きのふ" . "きの,ふ")      ; 昨日
        ("しらぶ" . "しら,ぶ")      ; 探索
        ("しもべ" . "しも,べ")      ; 奴隸
        ("おとな" . "おと,な")      ; 年長
        ("をがみ" . "をが,み")      ; 禮拜
        ("すまひ" . "すま,ひ")      ; 第宅
        ("たとへ" . "たと,へ")      ; 譬喩
        ("あるき" . "ある,き")      ; 遊行
        ("うつは" . "うつ,は")      ; 器皿
        ("のこり" . "のこ,り")      ; 餘遺
        ("ただし" . "ただ,し")      ; 公平
        ("かなた" . "かな,た")      ; 彼岸
        ("さわぎ" . "さわ,ぎ")      ; 忙亂
        ("そだて" . "そだ,て")      ; 成長
        ("ねたみ" . "ねた,み")      ; 嫉妬
        ("をんな" . "をん,な")      ; 婦女
        ("ひそか" . "ひそ,か")      ; 僻靜
        ("たふれ" . "たふ,れ")      ; 傾圯
        ("たもち" . "たも,ち")      ; 保護
        ("かため" . "かた,め")      ; 警固
        ("みだれ" . "みだ,れ")      ; 紛亂
        ("せむる" . "せむ,る")      ; 窘迫
        ("おひめ" . "おひ,め")      ; 負債
        ("あたり" . "あた,り")      ; 近傍
        ("つるべ" . "つる,べ")      ;
        ("ことば" . "こと,ば")      ; 方言
        ("うはべ" . "うは,べ")      ;
        ("ありか" . "あり,か")      ;
        ("あらす" . "あら,す")      ; 殘暴
        ("たしか" . "たし,か")      ; 確據
        ("すまは" . "すま,は")      ; 住居
        ("おもふ" . "おも,ふ")      ; 逆料
        ("つよく" . "つよ,く")      ; 健勁
        ("ありし" . "あり,し")      ; 所遇
        ("なやま" . "なや,ま")      ; 困苦
        ("みとめ" . "みと,め")      ; 諦視
        ("なげき" . "なげ,き")      ; 嘆息
        ("くらゐ" . "くら,ゐ")      ; 座位
        ("おどし" . "おど,し")      ; 兇言
        ("ほろぼ" . "ほろ,ぼ")      ; 殘害
        ("ここち" . "ここ,ち")      ; 心地
        ("のぞみ" . "のぞ,み")      ; 願望
        ("しもめ" . "しも,め")      ; 下婢
        ("くされ" . "くさ,れ")      ; 朽壤
        ("ゆたか" . "ゆた,か")      ; 豐穰
        ("からう" . "から,う")      ; 苦辛
        ("くづれ" . "くづ,れ")      ; 破壞
        ("なはめ" . "なは,め")      ; 械繋
        ("よのま" . "よの,ま")      ; 夜間
        ("をとこ" . "をと,こ")      ; 男子
        ("さぐり" . "さぐ,り")      ; 揣摩
        ("すくな" . "すく,な")      ; 僅少
        ("もとめ" . "もと,め")      ; 需用
        ("しのび" . "しの,び")      ; 耐心
        ("しめし" . "しめ,し")      ; 現示
        ("たやす" . "たや,す")      ; 容易
        ("くだけ" . "くだ,け")      ; 碎木
        ("ゑびす" . "ゑび,す")      ; 夷人
        ("なさけ" . "なさ,け")      ; 情分
        ("もてな" . "もて,な")      ; 待遇
        ("はやく" . "はや,く")      ; 從前
        ("たえず" . "たえ,ず")      ; 不斷
        ("かたう" . "かた,う")      ; 堅固
        ("おもひ" . "おも,ひ")      ; 思念
        ("くらく" . "くら,く")      ; 蒙昧
        ("さだめ" . "さだ,め")      ; 判定
        ("いかり" . "いか,り")      ; 震怒
        ("さかえ" . "さか,え")      ; 榮光
        ("へだて" . "へだ,て")      ; 區別
        ("なだめ" . "なだ,め")      ; 挽囘
        ("そそげ" . "そそ,げ")      ; 灌漑
        ("きよき" . "きよ,き")      ; 聖潔
        ("いたみ" . "いた,み")      ; 哀痛
        ("だまる" . "だま,る")      ; 緘默
        ("つつみ" . "つつ,み")      ; 包容
        ("たかぶ" . "たか,ぶ")      ; 驕傲
        ("すごさ" . "すご,さ")      ; 生活
        ("あらひ" . "あら,ひ")      ; 洗滌
        ("ぬすみ" . "ぬす,み")      ; 盜竊
        ("うばふ" . "うば,ふ")      ; 勒索
        ("おぼえ" . "おぼ,え")      ; 記憶
        ("はかる" . "はか,る")      ; 思議
        ("すぐる" . "すぐ,る")      ; 經過
        ("みつる" . "みつ,る")      ; 滿足
        ("にぎは" . "にぎ,は")      ; 賑恤
        ("ひくき" . "ひく,き")      ; 卑微
        ("えらび" . "えら,び")      ; 選擇
        ("もとき" . "もと,き")      ; 原樹
        ("はつほ" . "はつ,ほ")      ; 薦薪
        ("おもん" . "おも,ん")      ; 敬重
        ("ねたま" . "ねた,ま")      ; 嫉妬
        ("よみぢ" . "よみ,ぢ")      ; 陰府
        ("めぐま" . "めぐ,ま")      ; 矜恤
        ("ちかひ" . "ちか,ひ")      ; 盟約
        ("とりな" . "とり,な")      ; 禱告
        ("よわく" . "よわ,く")      ; 荏弱
        ("なやめ" . "なや,め")      ; 虐遇
        ("むくひ" . "むく,ひ")      ; 報賞
        ("やまひ" . "やま,ひ")      ; 疾病
        ("なぶり" . "なぶ,り")      ; 凌辱
        ("いかで" . "いか,で")      ; 如何
        ("かまへ" . "かま,へ")      ; 構造
        ("かなは" . "かな,は")      ; 應成
        ("うれひ" . "うれ,ひ")      ; 憂慮
        ("いきた" . "いき,た")      ; 氣絶
        ("あがむ" . "あが,む")      ; 讚美
        ("あゆみ" . "あゆ,み")      ; 歩行
        ("かたる" . "かた,る")      ; 説話
        ("さけん" . "さけ,ん")      ; 喧呼
        ("もらは" . "もら,は")      ; 受領
        ("せめら" . "せめ,ら")      ; 迫害
        ("たふさ" . "たふ,さ")      ; 趺倒
        ("つよし" . "つよ,し")      ; 剛毅
        ("したふ" . "した,ふ")      ; 思慕
        ("したひ" . "した,ひ")      ; 戀慕
        ("やすく" . "やす,く")      ; 安逸
        ("くるし" . "くる,し")      ; 困苦
        ("とりで" . "とり,で")      ; 營壘
        ("はなれ" . "はな,れ")      ; 暌違
        ("すこし" . "すこ,し")      ; 少許
        ("ただす" . "ただ,す")      ; 規正
        ("くつる" . "くつ,る")      ; 敗壞
        ("さはぎ" . "さは,ぎ")      ; 喧嚷
        ("ねがふ" . "ねが,ふ")      ; 祈求
        ("そねみ" . "そね,み")      ; 猜忌
        ("ひろめ" . "ひろ,め")      ; 傳播
        ("ついで" . "つい,で")      ; 次序
        ("かたき" . "かた,き")      ; 堅固
        ("おのれ" . "おの,れ")      ; 自己
        ("すすめ" . "すす,め")      ; 勤勉
        ("くつご" . "くつ,ご")      ; 口籠
        ("ほこり" . "ほこ,り")      ; 矜誇
        ("かぢや" . "かぢ,や")      ; 銅匠
        ("かたく" . "かた,く")      ; 堅立
        ("そむき" . "そむ,き")      ; 違逆
        ("やすみ" . "やす,み")      ; 安息
        ("はげむ" . "はげ,む")      ; 黽勉
        ("まつた" . "まつ,た")      ; 完全
        ("ふたつ" . "ふた,つ")      ; 二件
        ("きよく" . "きよ,く")      ; 聖潔
        ("まつれ" . "まつ,れ")      ; 奉事
        ("まつら" . "まつ,ら")      ; 奉事
        ("はげま" . "はげ,ま")      ; 激勵
        ("さとら" . "さと,ら")      ; 曉得
        ("まさり" . "まさ,り")      ; 愈美
        ("めぐり" . "めぐ,り")      ; 環巡
        ("よわき" . "よわ,き")      ; 荏弱
        ("ひとや" . "ひと,や")      ; 囹圄
        ("きよき" . "きよ,き")      ; 聖潔
        ("はやち" . "はや,ち")      ; 暴風
        ("うごか" . "うご,か")      ; 搖蕩
        ("ひかり" . "ひか,り")      ; 光明
        ("みまひ" . "みま,ひ")      ; 眷顧
        ("よきな" . "よき,な")      ; 美名
        ("きよめ" . "きよ,め")      ; 聖潔
        ("うらみ" . "うら,み")      ; 怨恨
        ("ほまれ" . "ほま,れ")      ; 嘉稱
        ("よきひ" . "よき,ひ")      ; 佳日
        ("おどす" . "おど,す")      ; 威嚇
        ("まもれ" . "まも,れ")      ; 儆醒
        ("そしり" . "そし,り")      ; 謗讟
        ("あらし" . "あら,し")      ; 狂風
        ("むかし" . "むか,し")      ; 上古
        ("まなこ" . "まな,こ")      ; 眼目
        ("きたる" . "きた,る")      ; 降臨
        ("ほんゐ" . "ほん,ゐ")      ; 本位
        ("となり" . "とな,り")      ; 比隣
        ("かがみ" . "かが,み")      ; 鑑戒
        ("かろん" . "かろ,ん")      ; 藐忽
        ("わかち" . "わか,ち")      ; 區別
        ("なげか" . "なげ,か")      ; 哀哭
        ("ぬるく" . "ぬる,く")      ; 温然
        ("たがひ" . "たが,ひ")      ; 彼此
        ("はかり" . "はか,り")      ; 權衡
        ("つるぎ" . "つる,ぎ")      ; 刀劔
        ("しづか" . "しづ,か")      ; 静謐
        ("ひがし" . "ひが,し")      ; 東方
        ("はだか" . "はだ,か")      ; 裸裎
        ("おごり" . "おご,り")      ; 奢侈
        ("かざり" . "かざ,り")      ; 修飾
        ("もとゐ" . "もと,ゐ")      ; 基址
        ("のろひ" . "のろ,ひ")      ; 呪詛
        ("わづか" . "わづ,か")      ; 僅々
        ("あぶら" . "あぶ,ら")      ; 香膏
        ("むしろ" . "むし,ろ")      ; 筵席
        ("おぼろ" . "おぼ,ろ")      ; 昏然
        ("あくた" . "あく,た")      ; 汚穢
        ("むなし" . "むな,し")      ; 徒然
        ("ふるき" . "ふる,き")      ; 舊樣
        ("やくめ" . "やく,め")      ;
        ("つかれ" . "つか,れ")      ;
        ("すがた" . "すが,た")      ;
        ("うはさ" . "うは,さ")      ;
        ("つかさ" . "つか,さ")      ;
        ("ひるげ" . "ひる,げ")      ;
        ("ゆふげ" . "ゆふ,げ")      ;
        ("このひ" . "この,ひ")      ;
        ("かたは" . "かた,は")      ;
        ("あなた" . "あな,た")      ;
        ("こなた" . "こな,た")      ;
        ("きんす" . "きん,す")      ; 
        ("みなみ" . "みな,み")      ; 南方
        ("のたま" . "の,たま")      ; 宣給
        ("きもの" . "き,もの")      ; 衣服
        ("こども" . "こ,ども")      ; 兒女
        ("まがき" . "ま,がき")      ; 
        ("もはや" . "も,はや")      ;
        ("てびき" . "て,びき")      ; 相者
        ("みまは" . "み,まは")      ; 環視
        ("よつぎ" . "よ,つぎ")      ;
        ("よあけ" . "よ,あけ")      ; 早晨
        ("はがみ" . "は,がみ")      ; 切齒
        ("もより" . "も,より")      ; 附近
        ("こよひ" . "こ,よひ")      ; 今夜
        ("えもの" . "え,もの")      ;
        ("しかた" . "し,かた")      ;
        ("ひごと" . "ひ,ごと")      ;
        ("ちまた" . "ち,また")      ; 岐路
        ("まむし" . "ま,むし")      ;
        ("しわざ" . "し,わざ")      ;
        ("ゐなか" . "ゐ,なか")      ; 田間
        ("まこと" . "ま,こと")      ; 確實
        ("やから" . "や,から")      ;
        ("ふしぎ" . "ふ,しぎ")      ; 奇跡、奇異
        ("いばら" . "い,ばら")      ; 荊棘
        ("てだし" . "て,だし")      ;
        ("へめぐ" . "へ,めぐ")      ; 經行
        ("しうと" . "し,うと")      ; 外舅
        ("みおと" . "み,おと")      ; 遺漏
        ("やしき" . "や,しき")      ; 公廨
        ("ゐつき" . "ゐ,つき")      ; 膠定
        ("てがみ" . "て,がみ")      ; 書信
        ("しぜん" . "し,ぜん")      ; 自然
        ("どだい" . "ど,だい")      ; 土基
        ("こたち" . "こ,たち")      ; 諸子
        ("めあて" . "め,あて")      ; 標準
        ("めのと" . "め,のと")      ; 乳母
        ("かたち" . "か,たち")      ; 形體
        ("みもち" . "み,もち")      ; 品行
        ("みらい" . "み,らい")      ; 未來
        ("だらく" . "だ,らく")      ; 墮落
        ("みもの" . "み,もの")      ; 觀玩
        ("まみづ" . "ま,みづ")      ; 淡水
        ("いくさ" . "い,くさ")      ; 戰鬪
        ("けぬの" . "け,ぬの")      ; 毛布
        ("よたり" . "よ,たり")      ; 四人
        ("たかき" . "たか,き")      ; 尊大
        ))

;; ("そうぢよ" . "さう,ぢよ") ; 掃除

(setq ruby-noconvert-23-table '())

(setq ruby-convert-table-3
      `(("よげんしや" . "よ,げん,しや")
        ("みつかめ" . "みつ,か,め")         ; 第三日
        ("いつかめ" . "いつ,か,め")         ; 第五日
        ("やうかめ" . "やう,か,め")         ; 第八日
        ("ぶだうしゆ" . "ぶ,だう,しゆ")     ; 葡萄酒
        ("じふじか" . "じふ,じ,か")         ; 十字架
        ("あんそくにち" . "あん,そく,にち") ; 安息日
        ("はちじふし" . "はち,じふ,し")     ; 八十四
        ("さいせんばこ" . "さい,せん,ばこ") ; 賽錢箱
        ("はうなふもの" . "はう,なふ,もの") ; 奉納物
        ("じふにじ" . "じふ,に,じ")         ; 十二時
        ("じふよだい" . "じふ,よ,だい")     ; 十四代
        ("いはうじん" . "い,はう,じん")     ; 異邦人
        ("ぎぜんしや" . "ぎ,ぜん,しや")     ; 僞善者
        ("しふぎしよ" . "しふ,ぎ,しよ")     ; 集議所
        ("けうはふし" . "けう,はふ,し")     ; 教法師
        ("ぜんせかい" . "ぜん,せ,かい")     ; 全世界
        ("ふしんじや" . "ふ,しん,じや")     ; 不信者
        ("りがくしや" . "り,がく,しや")     ; 理學者
        ("ふしんかう" . "ふ,しん,かう")     ; 不信仰
        ("はうけいし" . "はう,けい,し")     ; 保惠師
        ("いわういろ" . "い,わう,いろ")     ; 硫黄色
        ("はうりつか" . "はう,りつ,か")     ; 法律家
        ("かりいれば" . "かり,いれ,ば")     ; 收稼場
        ("さいばんにん" . "さい,ばん,にん") ; 裁判人
        ("ひとまはり" . "ひと,まは,り")     ; 七日間
        ("いつごろ" . "い,つ,ごろ")         ; 幾何時
        ("いつまで" . "い,つ,まで")         ; 幾何時
        ("かはせざ" . "かは,せ,ざ")         ; 兌錢肆
        ("みちづれ" . "み,ち,づれ")         ; 同行人
        ("かきいた" . "か,き,いた")         ; 寫字板
        ("ぶだうのき" . "ぶ,だう,のき")     ; 葡萄樹
        ("ともしきもの" . "とも,しき,もの") ; 窮乏者
        ("ぎじくわん" . "ぎ,じ,くわん")     ; 議事官
        ("のちのもの" . "のち,の,もの")     ; 後至者
        ("かんらんざん" . "かん,らん,ざん") ; 橄欖山
        ("いやしきもの" . "いや,しき,もの") ; 卑賤者
        ("またたきのま" . "また,たき,のま") ; 一瞬間
        ("せかいぢう" . "せ,かい,ぢう")     ; 全世界
        ("せんぞたち" . "せん,ぞ,たち")     ; 先祖等
        ("あなどるもの" . "あな,どる,もの") ; 藐忽者
        ("さいばんしよ" . "さい,ばん,しよ") ; 裁判所
        ("でんだうしや" . "でん,だう,しや") ; 傳道者
        ("ことくにびと" . "こと,くに,びと") ; 異邦人
        ("たねなきぱん" . "たね,なき,ぱん") ; 無酵麪
        ("ふたたびめ" . "ふた,たび,め")     ; 第二次
        ("やどれるもの" . "やど,れる,もの") ; 寄寓者
        ("ほろぶるもの" . "ほろ,ぶる,もの") ; 淪亡者
        ("ぢよしつじ" . "ぢよ,しつ,じ")     ; 女執事
        ("あんしゆれい" . "あん,しゆ,れい") ; 按手禮
        ("はふりつか" . "はふ,りつ,か")     ; 法律家
        ("うけあひびと" . "うけ,あひ,びと") ; 保証人
        ("まつるもの" . "まつ,る,もの")     ; 獻祭者
        ("ものみびと" . "もの,み,びと")     ; 見證人
        ("だいぼくしや" . "だい,ぼく,しや") ; 大牧者
        ("たかぶるもの" . "たか,ぶる,もの") ; 驕傲者
        ("ざうぶつしや" . "ざう,ぶつ,しや") ; 造物者
        ("こんがうせき" . "こん,がう,せき") ; 金剛石
        ("ほそきぬの" . "ほそ,き,ぬの")     ; 細麻布
        ("かたは" . "か,た,は")             ; 殘缺者
        ("こどもら" . "こ,ども,ら")         ; 兒童輩
        ("そなへび" . "そな,へ,び")         ; 備節日
        ("すぎこし" . "すぎ,こ,し")         ; 逾越節
        ("かりびと" . "か,り,びと")         ; 負債人
        ;; ("ばんとう" . "ばんとう")           ; 操會者
        ("ちのみご" . "ち,のみ,ご")     ; 哺乳兒
        ("くちざる" . "く,ち,ざる")     ; 不朽壞
        ("みたびめ" . "み,たび,め")     ; 第三次
        ("うしろみ" . "うし,ろ,み")     ; 受託者
        ("なぬかめ" . "なぬ,か,め")     ; 第七日
        ;; ("いちじく" . "いちじく")           ; 無花果
        ("いのりのば" . "いの,りの,ば") ; 祈祷所
        ))

(setq ruby-noconvert-table-3
      `("なぐさむるもの" "ぶだうばたけ" "にはとりなくころ" "らいびやうにん"  "せかいぢゆう"  "りやうがへするもの"  "りやうがへや"  "いとちひさきもの"  "りえんじやう"  "にはとりなくころ" "いとたかきもの"  "ゆり"  "みやきよめのいはひ"  "なぐさむるもの"  "いちじるしきひ"  "ちきやうだい"  "しよくみんち" "しよきくわん"  "たねいれぬぱんのいはひ"  "しよくわいだう"  "ものをつくりしかみ"  "つくられしもの" "ぶだうばたけ" "うきたるこころ"  "ちやうらうくわい"  "なくてならぬもの"  "しよくざいしよ"  "いときよきところ"  "すぎこしのいはひ"  "へりくだるもの"))

(defun canonicalize-24-ruby-kana ()
  (interactive)
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (cl-loop initially (init-log-and-begin-buffer current log)
          for pos = (re-search-forward "kanakt{..}{[あ-ん][あ-ん][あ-ん][あ-ん]}" nil t nil)
          while pos do
            (let ((kanj1 (char-after (- pos 9)))
                  (kanj2 (char-after (- pos 8)))
                  (hira1 (char-after (- pos 5)))
                  (hira2 (char-after (- pos 4)))
                  (hira3 (char-after (- pos 3)))
                  (hira4 (char-after (- pos 2))))
              (let* ((yomi (format "%c%c%c%c" hira1 hira2 hira3 hira4))
                     (assoc (assoc yomi ruby-convert-24-table)))
                (when assoc
                  (backward-char 16)    ; kanakt{郡中}{ぐんちう}
                  (delete-char 1)
                  (insert "K")          ; Kanakt{郡中}{ぐんちう}
                  (forward-char 7)
                  (insert ",")          ; Kanakt{郡,中}{ぐんちう}
                  (forward-char 3)
                  (delete-char 4)       ; Kanakt{郡,中}{}
                  (insert (cdr assoc))  ; Kanakt{郡,中}{ぐん,ちう}
                  (switch-to-buffer log)
                  (insert kanj1) (insert kanj2) (insert " ")
                  (insert (cdr assoc)) (insert "\n")
                  (switch-to-buffer current)
                  (refresh-screen)))))))

(defun canonicalize-23-ruby-kana ()
  (interactive)
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (cl-loop initially (init-log-and-begin-buffer current log)
          for pos = (re-search-forward "kana{..}{[あ-ん][あ-ん][あ-ん]}" nil t nil)
          while pos do
            (let ((kanj1 (char-after (- pos 8)))
                  (kanj2 (char-after (- pos 7)))
                  (hira1 (char-after (- pos 4)))
                  (hira2 (char-after (- pos 3)))
                  (hira3 (char-after (- pos 2))))
              (let* ((yomi (format "%c%c%c" hira1 hira2 hira3))
                     (assoc (assoc yomi ruby-convert-23-table)))
                (when assoc
                  (backward-char 13)    ; kana{知識}{ちしき}
                  (delete-char 1)
                  (insert "K")          ; Kana{知識}{ちしき}
                  (forward-char 3)
                  (insert "k")
                  (insert "t")          ; Kanakt{知識}{ちしき}
                  (forward-char 2)
                  (insert ",")          ; Kanakt{知,識}{ちしき}
                  (forward-char 3)
                  (delete-char 3)       ; Kanakt{知,識}{}
                  (insert (cdr assoc))  ; Kanakt{知,識}{ち,しき}
                  (switch-to-buffer log)
                  (insert kanj1) (insert kanj2) (insert " ")
                  (insert hira1) (insert hira2) (insert hira3) (insert "\n")
                  (switch-to-buffer current)
                  (refresh-screen)))))))

(defun canonicalize-3-ruby-kanakt ()
  (interactive)
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (cl-loop initially (init-log-and-begin-buffer current log)
          while (re-search-forward "kanakt{...}{[あ-ん]*}" nil t nil)
          do (backward-sexp)
             (let* ((point (point))
                    (kanj1 (char-after (- point 4)))
                    (kanj2 (char-after (- point 3)))
                    (kanj3 (char-after (- point 2)))
                    (yomi (cl-loop for hpos from (1+ point)
                                for char = (char-after hpos)
                                while (/= char ?\})
                                collect char into hira
                                finally (return (mapconcat #'string hira nil)))))
               (let ((assoc (assoc yomi ruby-convert-table-3)))
                 (when assoc
                   (backward-sexp)
                   (backward-char 6)
                   (delete-char 1)
                   (insert "K")
                   (forward-char 7)
                   (insert ",")
                   (forward-char 1)
                   (insert ",")
                   (forward-char 3)
                   (kill-word 1)
                   (insert (cdr assoc))
                   (switch-to-buffer log)
                   (insert kanj1) (insert kanj2) (insert kanj3) (insert " ")
                   (insert yomi) (insert "\n")
                   (switch-to-buffer current)
                   (refresh-screen)))))))

(defun canonicalize-3-ruby-kana ()
  (interactive)
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (cl-loop initially (init-log-and-begin-buffer current log)
          while (re-search-forward "kana{...}{[あ-ん]*}" nil t nil)
          do (backward-sexp)
             (let* ((point (point))
                    (kanj1 (char-after (- point 4)))
                    (kanj2 (char-after (- point 3)))
                    (kanj3 (char-after (- point 2)))
                    (yomi (cl-loop for hpos from (1+ point)
                                for char = (char-after hpos)
                                while (/= char ?\})
                                collect char into hira
                                finally (return (mapconcat #'string hira nil)))))
               (let ((assoc (assoc yomi ruby-convert-table-3)))
                 (when assoc
                   (backward-sexp)
                   (backward-char 4)
                   (delete-char 1)
                   (insert "K")
                   (forward-char 3)
                   (insert "kt")
                   (forward-char 2)
                   (insert ",")
                   (forward-char 1)
                   (insert ",")
                   (forward-char 3)
                   (kill-word 1)
                   (insert (cdr assoc))
                   (switch-to-buffer log)
                   (insert kanj1) (insert kanj2) (insert kanj3) (insert " ")
                   (insert yomi) (insert "\n")
                   (switch-to-buffer current)
                   (refresh-screen)))))))

(defun find-22-yomi ()
  (interactive)
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (cl-loop initially (init-log-and-begin-buffer current log)
          for pos = (re-search-forward "{..}{[あ-ん][あ-ん][あ-ん][あ-ん]}" nil t nil)
          while pos do
          (let* ((kanj1 (char-after (- pos 9)))
                 (kanj2 (char-after (- pos 8)))
                 (hira1 (char-after (- pos 5)))
                 (hira2 (char-after (- pos 4)))
                 (hira3 (char-after (- pos 3)))
                 (hira4 (char-after (- pos 2)))
                 (yomi (format "%c%c%c%c" hira1 hira2 hira3 hira4)))
            (unless (or (assoc yomi ruby-convert-23-table)
                        (member yomi ruby-noconvert-23-table))
              (switch-to-buffer "*yomi-log*")
              (insert kanj1) (insert kanj2) (insert " ")
              (insert yomi) (insert "\n")
              (switch-to-buffer current)
              (refresh-screen))))))

(defun find-23-yomi ()
  (interactive)
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (cl-loop initially (init-log-and-begin-buffer current log)
          for pos = (re-search-forward "{..}{[あ-ん][あ-ん][あ-ん]}" nil t nil)
          while pos do
          (let* ((kanj1 (char-after (- pos 8)))
                 (kanj2 (char-after (- pos 7)))
                 (hira1 (char-after (- pos 4)))
                 (hira2 (char-after (- pos 3)))
                 (hira3 (char-after (- pos 2)))
                 (yomi (format "%c%c%c" hira1 hira2 hira3)))
            (unless (or (assoc yomi ruby-convert-23-table)
                        (member yomi ruby-noconvert-23-table))
              (switch-to-buffer "*yomi-log*")
              (insert kanj1) (insert kanj2) (insert " ")
              (insert hira1) (insert hira2) (insert hira3) (insert "\n")
              (switch-to-buffer current)
              (refresh-screen))))))

(defun find-3-yomi ()
  (interactive)
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (cl-loop initially (init-log-and-begin-buffer current log)
          while (re-search-forward "{...}{[あ-ん]*}" nil t nil)
          do (backward-sexp)
             (let* ((point (point))
                    (kanj1 (char-after (- point 4)))
                    (kanj2 (char-after (- point 3)))
                    (kanj3 (char-after (- point 2)))
                    (yomi (cl-loop for hpos from (1+ point)
                                for char = (char-after hpos)
                                while (/= char ?\})
                                collect char into hira
                                finally (return (mapconcat #'string hira nil)))))
               (unless (or (assoc yomi ruby-convert-table-3)
                           (member yomi ruby-noconvert-table-3))
                 (switch-to-buffer "*yomi-log*")
                 (insert kanj1) (insert kanj2) (insert kanj3) (insert " ")
                 (insert yomi) (insert "\n")
                 (switch-to-buffer current)
                 (refresh-screen))))))

(defun find-242-yomi ()
  (interactive)
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (cl-loop initially (init-log-and-begin-buffer current log)
          while (re-search-forward "{..}{....}\\\\kana{者}{もの}" nil t nil)
          do (set-mark-command nil)
          (backward-sexp)
          (backward-sexp)
          (backward-sexp)
          (backward-sexp)
          (backward-sexp)
          (backward-word)
          (backward-char)
          (kill-ring-save nil nil t)
          (move-end-of-line 1)
          (switch-to-buffer "*yomi-log*")
          (yank 1)
          (insert "\n")
          (switch-to-buffer current)
          (refresh-screen))))

;;;
;;; 踊り字の處理
;;;

(defun dakuten-hirakana-p (char)
  (cl-position char "がぎぐげござじずぜぞだぢづでどばびぶべぼ"))

(defun dakuten-hirakana (char)
  (let ((position (cl-position char "かきくけこさしすせそたちつてとはひふへほ")))
    (when position
      (aref "がぎぐげござじずぜぞだぢづでどばびぶべぼ" position))))

(defun convert-kurikahesi-hirakana (args)
  (interactive "P")
  (cl-loop initially (beginning-buffer)
        with odori = (if args "\\MJMZM{000003}" "\\UTF{303B}")
        for point = (point)
        for pchar = nil then char
        for char = (char-after point)
        while char
        do (if (<= ?\あ char ?\ん)
               (cond ((eql pchar char)
                      (delete-char 1)
                      (if (dakuten-hirakana-p pchar)
                          (insert (format "\\D{%s}゛" odori))
                        (insert odori))
                      (refresh-screen))
                     ((eql (dakuten-hirakana pchar) char)
                      (delete-char 1)
                      (insert (format "\\D{%s}゛" odori))
                      (refresh-screen))
                     ((eql pchar (dakuten-hirakana char))
                      (delete-char 1)
                      (insert odori)
                      (refresh-screen))
                     (t (forward-char 1)))
             (forward-char 1))))

(defun dakuten-katakana-p (char)
  (cl-position char "ガギグゲゴザジズゼゾダヂヅデドバビブベボ"))

(defun dakuten-katakana (char)
  (let ((position (cl-position char "カキクケコサシスセソタチツテトハヒフヘホ")))
    (when position
      (aref "ガギグゲゴザジズゼゾダヂヅデドバビブベボ" position))))

(defun handaku-katakana-p (char)
  (cl-position char "パピプペポ"))

;; (defun convert-kurikahesi-katakana (args)
;;   (interactive "P")
;;   (cl-loop initially (beginning-buffer)
;;         for point = (point)
;;         for pchar = nil then char
;;         for char = (char-after point)
;;         while char
;;         do (if (and (<= ?\ア char ?\ン)
;;                     (not (handaku-katakana-p char)))
;;                (cond ((eql pchar char)
;;                       (delete-char 1)
;;                       (if (dakuten-katakana-p pchar)
;;                           (insert "\\UTF{30FE}")
;;                         (insert "\\UTF{30FD}")))
;;                      ((eql (dakuten-katakana pchar) char)
;;                       (delete-char 1)
;;                       (insert "\\UTF{30FE}"))
;;                      ((eql pchar (dakuten-katakana char))
;;                       (delete-char 1)
;;                       (insert "\\UTF{30FD}"))
;;                      (t (forward-char 1)))
;;              (forward-char 1))))

(defun convert-kurikahesi-katakana (katakana)
  (cl-loop for i from 0 below (length katakana)
        for pchar = t then char
        for char = (aref katakana i)
        append (if (or (<= ?\ア char ?\ン)
                       (= char ?\ー))
                   (cond ((handaku-katakana-p char)
                          (list char))
                         ((eql pchar char)
                          (list
                           (if (dakuten-katakana-p pchar)
                               ?\ヾ
                             ?\ヽ)))
                         ((eql (dakuten-katakana pchar) char)
                          (list ?\ヾ))
                         ((eql pchar (dakuten-katakana char))
                          (list ?\ヽ))
                         (t (list char)))
                 (error "Illegal Katakana: %s." katakana))
        into characters
        finally (return (apply 'string characters))))

(defun convert-kunoji-word ()
  (interactive)
  (unless (progn
            (beginning-buffer)
            (search-forward "usepackage{multicolpar}" nil t nil))
    ;; 「〜はしばしば」のやうな例に對應するため end-buffer から見て行く
    ;; 但し、「おのおのおのが〜」のやうな例があると崩壞
    (cl-loop initially (progn (end-buffer) (backward-char 1))
          with kunoji = "\\ajKunoji{}"
          with dkunoji = "\\ajDKunoji{}"
          for point = (point)
          for char6 = 6 then char5
          for char5 = 5 then char4
          for char4 = 4 then char3
          for char3 = 3 then char2
          for char2 = 2 then char1
          for char1 = (char-after point)
          while (> point 1)
          do (if (<= ?\あ char1 ?\ん)
                 (cond ((and (eql char1 char4)
                             (eql char2 char5)
                             (eql char3 char6))
                        (forward-char 3)
                        (delete-char 3)
                        (insert kunoji)
                        (setq char1 1 char2 2 char3 3 ; reset chars
                              char4 4 char5 5 char6 6)
                        (refresh-screen))
                       ((and (eql (dakuten-hirakana char1) char4)
                             (eql char2 char5)
                             (eql char3 char6))
                        (forward-char 3)
                        (delete-char 3)
                        (insert dkunoji)
                        (setq char1 1 char2 2 char3 3 ; reset chars
                              char4 4 char5 5 char6 6)
                        (refresh-screen))
                       ((and (eql char1 char3)
                             (eql char2 char4))
                        (forward-char 2)
                        (delete-char 2)
                        (insert kunoji)
                        (setq char1 1 char2 2 ; reset chars
                              char3 3 char4 4)
                        (refresh-screen))
                       ((and (eql (dakuten-hirakana char1) char3)
                             (eql char2 char4))
                        (forward-char 2)
                        (delete-char 2)
                        (insert dkunoji)
                        (setq char1 1 char2 2 ; reset chars
                              char3 3 char4 4)
                        (refresh-screen))
                       (t (backward-char 1)))
               (backward-char 1)))))

;;;
;;; 變態假名ルール:
;;;   ルビ内:
;;;     ルビの先頭の「し」は「志」
;;;     ルビ内の「ゆ」は「由」
;;;     ルビ内の「ば」は「者゛」
;;;     ルビ内の「ぱ」は「者゜」
;;;
(defun goto-kanji-kaishi ()
  (interactive)
  (and (re-search-forward "\\\\[Kk]ana" nil t nil)
       (search-forward "{" nil t nil)
       (progn (backward-char 1) (point))))

(defun replace-word (after)
  (delete-char 1)
  (insert after)
  (length after))

(defun convert-hentai-kana-rubi ()
  (interactive)
  (cl-loop initially (beginning-buffer)
        with si = "\\MJMZM{090075}"
        with yu = "\\MJMZM{090239}"
        with ba = "\\D{\\MJMZM{090171}}{゛}"
        with pa = "\\D{\\MJMZM{090171}}{゜}"
        for pos1 = (goto-kanji-kaishi) ; 漢字開始
        for pos2 = (and pos1 (progn (forward-sexp) (1+ (point)))) ; ルビ開始
        for pos3 = (and pos2 (progn (forward-sexp) (1- (point)))) ; ルビ終了
        while pos1 do
        (if (eql ?\し (char-after pos2)) ; ルビ先頭が「し」
            (progn
              (goto-char pos2)
              (cl-incf pos3 (1- (replace-word si)))
              (refresh-screen))
          (goto-char pos2))
        (cl-loop for point = (point)
              for char = (char-after point)
              while (<= point pos3) do
              (cond ((eql ?\ゆ char)
                     (cl-incf pos3 (1- (replace-word yu)))
                     (refresh-screen))
                    ((eql ?\ば char)
                     (cl-incf pos3 (1- (replace-word ba)))
                     (refresh-screen))
                    ((eql ?\ぱ char)
                     (cl-incf pos3 (1- (replace-word pa)))
                     (refresh-screen))
                    (t (forward-char 1))))))

;;;
;;; 變態假名ルール:
;;;   ルビ外 (1) :
;;;     「は」は「ハ」
;;;     「ば」は「ハ゛」
;;;     「ぱ」は「ハ゜」
;;;     「そ」は「曾」
;;;     「ぞ」は「曾゛」
;;;
(defun convert-hentai-kana-1 ()
  (interactive)
  (cl-loop initially (beginning-buffer)
        with ha = "\\hakern{\\MJMZM{090163}}"
        with so = "\\MJMZM{090093}"
        with ba = "\\bakern{\\MJMZM{090163}}{゛}"
        with zo = "\\D{\\MJMZM{090093}}{゛}"
        ;; for pos1 = (goto-kanji-kaishi) then (goto-char pos4) ; 漢字開始
        ;; for pos2 = (progn (forward-sexp) (1+ (point))) ; ルビ開始
        for pos3 = (progn (forward-sexp) (1- (point))) ; ルビ終了
        for pos4 = (goto-kanji-kaishi) ; 次の漢字開始
        while pos4 do
        (cl-loop initially (goto-char pos3)
              for point = (point)
              for char = (char-after point)
              while (< point pos4)
              do (cond ((eql ?\は char)
                        (cl-incf pos4 (1- (replace-word ha)))
                        (refresh-screen))
                       ((eql ?\そ char)
                        (cl-incf pos4 (1- (replace-word so)))
                        (refresh-screen))
                       ((eql ?\ば char)
                        (cl-incf pos4 (1- (replace-word ba)))
                        (refresh-screen))
                       ((eql ?\ぞ char)
                        (cl-incf pos4 (1- (replace-word zo)))
                        (refresh-screen))
                       (t (forward-char 1))))))

;;;
;;; 變態假名ルール:
;;;   ルビ外 (2) :
;;;     「さばきのひに」の「の」は「能」、「に」は「爾」
;;;     「なんぢ[はにを]」の「な」は「奈」
;;;     「爾が母」の「が」は「加゛」MJ090025
;;;     「なんぢらが」の「が」は「加゛」MJ090025
;;;     「人の子」の「の」は「能」
;;;     「今の世」の「の」は「能」
;;;     「みなみの女王」の「の」は「能」
;;;      「水滿んとして」の「と」は「等」MJMZM{090130}
;;;     etc
(setq hentai-kana-table-2               ; ルビ外
      '(("なんぢは" . "\\MJMZM{090134}んぢは")
        ("なんぢに" . "\\MJMZM{090134}んぢに")
        ("なんぢを" . "\\MJMZM{090134}んぢを")
        ("なんぢらに" . "\\MJMZM{090134}んぢらに")
        ("なんぢらが" . "なんぢら\\D{\\MJMZM{090025}}{゛}")
        ("ニネベの" . "ニネベ\\MJMZM{090161}")
        ("ソロモンの" . "ソロモン\\MJMZM{090161}")
        ;; ("イエスの" . "イエス\\MJMZM{090161}")
        ("ヨハネの" . "ヨハネ\\MJMZM{090161}")
        ("ボソロの" . "ボソロ\\MJMZM{090161}")
        ("バラムの" . "バラム\\MJMZM{090161}")
        ("カインの" . "カイン\\MJMZM{090161}")
        ("コラの" . "コラ\\MJMZM{090161}")
        ("サタンの" . "サタン\\MJMZM{090161}")
        ("ダビデの" . "ダビデ\\MJMZM{090161}")
        ("こと}をなす" . "こと}を\\MJMZM{090134}す")
        ("{ごと}きの" . "{ごと}き\\MJMZM{090161}")
        ("{事}{こと}の\\" . "{事}{こと}\\MJMZM{090161}\\")
        ("{ひと}の\\kanakt{子}" . "{ひと}\\MJMZM{090161}\\kanakt{子}")
        ("{みな,み}の" . "{みな,み}\\MJMZM{090161}")
        ("{いま}の\\kanakt{世}{よ}の\\kana{人}{ひと}の" . "{いま}\\MJMZM{090161}\\kanakt{世}{よ}\\MJMZM{090161}\\kana{人}{ひと}\\MJMZM{090161}")
        ("{さばき}の\\kanakt{日}{ひ}に\\kana{共}{とも}に" . "{さばき}\\MJMZM{090161}\\kanakt{日}{ひ}\\MJMZM{090144}\\kana{共}{とも}\\MJMZM{090144}")
        ("{あらた}なる" . "{あらた}\\MJMZM{090134}る")
        ("{なんぢ}が\\kana{母}" . "{なんぢ}\\D{\\MJMZM{090025}}{゛}\\kana{母}")
        ("{みち}んとして" . "{みち}ん\\MJMZM{090130}して")
        ("にのせ" . "に\\MJMZM{090161}せ")
        ("}しる\\" . "}\\MJMZM{090075}る\\")
        ("しらざる" . "\\MJMZM{090075}らざる")
        ("し\\bakern{\\MJMZM{090163}}{゛}らく" . "\\MJMZM{090075}\\bakern{\\MJMZM{090163}}{゛}らく")
        ("かな\\hakern{\\MJMZM{090163}}せ" . "か\\MJMZM{090134}\\hakern{\\MJMZM{090163}}せ")
        ("なす}なかれ" . "なす}\\MJMZM{090134}かれ")
        ("{見}{み}な" . "{見}{み}\\MJMZM{090134}")
        ("}なり\\" . "}\\MJMZM{090134}り\\")
        ("}なす\\" . "}\\MJMZM{090134}す\\")
        ("なれど" . "\\MJMZM{090134}れど")
        ("}なるに\\" . "}\\MJMZM{090134}るに\\")
;;         ("}なし
;; " . "}\\MJMZM{090134}し
;; ")
        ("}なり。" . "}\\MJMZM{090134}り。")
        ("}なり
" . "}\\MJMZM{090134}り
")
        ("}しば\\ajKunoji" . "}\\MJMZM{090075}\\bakern{\\MJMZM{090163}}{゛}\\ajKunoji")
        ("しば\\ajKunoji{}\\" . "\\MJMZM{090075}\\bakern{\\MJMZM{090163}}{゛}\\ajKunoji{}\\")
        ;; ("すなはち" . "す\\MJMZM{090134}はち")
        ("}かつ\\" . "}\\MJMZM{090025}つ\\")
        ))

(defun process-hentai-kana-table (table)
  (cl-loop for (key . value) in table
        do (cl-loop initially (beginning-buffer)
                 for pos = (search-forward key nil t nil)
                 while pos do
                 (backward-delete-char (length key))
                 (insert value)
                 (refresh-screen))))

(defun convert-hentai-kana-2 ()
  (interactive)
  (process-hentai-kana-table hentai-kana-table-2))

;;;
;;; 變態假名變換後例外は元に戻す
;;;
(setq hentai-kana-revert-table
      '(("\\hakern{\\MJMZM{090163}}げしく" . "はげしく")
        ("\\hakern{\\MJMZM{090163}}や\\k" . "はや\\k")
        ("\\hakern{\\MJMZM{090163}}やく" . "はやく")
        ("\\hakern{\\MJMZM{090163}}るか" . "はるか")
        ("\\hakern{\\MJMZM{090163}}らみ" . "はらみ")
        ("\\hakern{\\MJMZM{090163}}りさけ" . "はりさけ")
        ("\\hakern{\\MJMZM{090163}}なれて" . "\\MJMZM{090171}なれて")
        ("\\hakern{\\MJMZM{090163}}なるれ" . "はなるれ")
        ("\\bakern{\\MJMZM{090163}}{゛}\\UTF{303B}や" . "\\bakern{\\MJMZM{090163}}{゛}はや")
        ("\\bakern{\\MJMZM{090163}}{゛}\\MJMZM{000003}や" . "\\bakern{\\MJMZM{090163}}{゛}はや") ; MJMZM版
        ("\\hakern{\\MJMZM{090163}}\\UTF{303B}や" . "\\hakern{\\MJMZM{090163}}はや")
        ("\\hakern{\\MJMZM{090163}}\\MJMZM{000003}や" . "\\hakern{\\MJMZM{090163}}はや") ; MJMZM版
        ("よ\\MJMZM{090093}\\hakern{\\MJMZM{090163}}せ" . "よ\\MJMZM{090093}はせ")
        ("\\hakern{\\MJMZM{090163}}じめ" . "はじめ")
        ;; ("を\\hakern{\\MJMZM{090163}}らん" . "をはらん")
        ("\\hakern{\\MJMZM{090163}}め" . "はめ")
        ("いひ\\UTF{303B}ら". "いひひら")
        ("いひ\\MJMZM{000003}ら". "いひひら") ; MJMZM版
        ("}\\bakern{\\MJMZM{090163}}{゛}かり" . "}ばかり")
        ("すくひ\\D{\\UTF{303B}}゛と" . "すくひびと")
        ("すくひ\\D{\\MJMZM{000003}}゛と" . "すくひびと") ; MJMZM版
        ("\\hakern{\\MJMZM{090163}}しり\\kana{出}" . "はしり\\kana{出}")
        ("{かたく}\\MJMZM{090134}り\\" . "{かたく}なり\\")
        ("\\UTF{303B}も\\ajDKunoji" . "とも\\ajDKunoji")
        ("\\MJMZM{000003}も\\ajDKunoji" . "とも\\ajDKunoji") ; MJMZM版
        ("\\hakern{\\MJMZM{090163}}\\D{\\UTF{303B}}゛からず" . "は\\D{\\UTF{303B}}゛からず")
        ("\\hakern{\\MJMZM{090163}}\\D{\\MJMZM{000003}}゛からず" . "は\\D{\\MJMZM{000003}}゛からず") ; MJMZM版
        ("\\hakern{\\MJMZM{090163}}ずなり" . "はずなり")
        ("}\\hakern{\\MJMZM{090163}}た\\" . "}はた\\")
        ("}\\hakern{\\MJMZM{090163}}ゆる\\" . "}はゆる\\")
        ("\\hakern{\\MJMZM{090163}}な\\hakern{\\MJMZM{090163}}だ" . "はなはだ")
        ("\\hakern{\\MJMZM{090163}}からずも" . "\\MJMZM{090171}からずも")
        ("\\hakern{\\MJMZM{090163}}たらく" . "はたらく")
        ("\\hakern{\\MJMZM{090163}}たらき" . "はたらき")
        ("\\hakern{\\MJMZM{090163}}なれ\\" . "はなれ\\")
        ("\\hakern{\\MJMZM{090163}}え\\kana{出}" . "はえ\\kana{出}")
        ("\\hakern{\\MJMZM{090163}}えいで" . "はえいで")
        ("\\hakern{\\MJMZM{090163}}じむ" . "はじむ")
        ("\\hakern{\\MJMZM{090163}}つる" . "はつる")
        ("\\hakern{\\MJMZM{090163}}しり\\" . "はしり\\")
        ("に\\hakern{\\MJMZM{090163}}せて" . "にはせて")
        ("を\\hakern{\\MJMZM{090163}}ぎ" . "をはぎ")
        ("\\hakern{\\MJMZM{090163}}しれども" . "はしれども")
        ;; ("あた\\hakern{\\MJMZM{090163}}ず" . "あたはず")
        ))

(defun revert-hentai-kana-exception ()
  (interactive)
  (process-hentai-kana-table hentai-kana-revert-table))

;;;
;;; 片假名にラインをつける
;;;

(setq person-name-table
      '("アイネア" "アウグスト" "アカイコ" "アカズ" "アガボ" "アガボス" "アキム" "アクラ" "アケラヲ" "アグリッパ" "アサ" "アスキキリト" "アセル" "アゾル" "アダム" "アツデ" "アナニア" "アナニヤ" "アバドン" "アバザデ" "アビア" "アビアタル" "アビウデ" "アピア" "アブラハム" "アベル" "アペレ" "アポリオン" "アポロ" "アミナダブ" "アモス" "アモン" "アラム" "アリスタルコ" "アリストプロ" "アルキポ" "アルテマス" "アルテミス" "アルパイ" "アルパヨ" "アレキサンデル" "アレタ" "アロン" "アンテパス" "アンデレ" "アンデロニコ" "アンナ" "アンナス" "アンピリアト" "イエザベル" "イエス" "イサカル" "イサク" "イザヤ" "イピタ" "インマヌエル" "ウツズヤ" "ウリヤ" "ウルバノ" "エサウ" "ヱスリ" "エスロン" "エツサイ" "エノク" "ヱノス" "エバ" "エパイネト" "エパフラス" "エパフロデト" "エホヤキン" "エラスト" "エリアキン" "エリアザル" "エリウデ" "ヱリヱゼル" "エリサベツ" "エリシヤ" "エリヤ" "エルマス" "ヱルモダム" "エレミヤ" "オネシポロ" "オネシモ" "オベデ" "オルンパ" "カイザル" "カイナン" "カイン" "カヤパ" "カルポ" "カンダケ" "ガイヲス" "ガド" "ガブリエル" "ガマリエル" "ガヨス" "ガヨス" "ガリヨ" "キス" "キリスト" "ギデオン" "クラウデア" "クラウデオ" "クラウデヲ" "クリア" "クリスポ" "クレオパ" "クレスケンス" "クレニオ" "クレメンス" "クロエ" "クロパ" "クワルト" "クーザ" "ケパ" "コサム" "コラ" "コルネリヲ" "サウロ" "サタン" "サツピラ" "サムエル" "サムソン" "サラ" "サルク" "サルモン" "サロメ" "ザアカイ" "ザカリア" "ザカリヤ" "ザドク" "ザラ" "シアテル" "シメオン" "シメヲン" "シモン" "シラス" "シルワノ" "ジユニヤ" "ジユリヤ" "スケワ" "スザンナ" "スタク" "ステパナ" "ステパノ" "スントケ" "セクンド" "セツ" "セム" "セメイ" "セルギヲ" "ゼウス" "ゼナス" "ゼブルン" "ゼベダイ" "ゼルバベル" "ゼロテ" "ソシパテロ" "ソステネ" "ソパテル" "ソロモン" "タツダイ" "タビタ" "タマル" "ダニエル" "ダビデ" "ダマリス" "チウダ" "テキコ" "テトス" "テベリオ" "テマイ" "テモテ" "テモン" "テラ" "テラノス" "テリテオ" "テルトルス" "テルパイナ" "テルポサ" "テヨピロ" "デオヌシオ" "デドモ" "デマス" "デメテリヲ" "デルシラ" "デヲスクリ" "デヲテレペス" "トマス" "トロピモ" "ドルカス" "ナアソン" "ナオム" "ナコル" "ナソン" "ナタナエル" "ナタン" "ナフタリ" "ナムガイ" "ナルキソ" "ナーマン" "ニカノル" "ニゲル" "ニコデモ" "ニコラ" "ニコライ" "ニネベ" "ヌンパス" "ネリ" "ネリオ" "ノア" "ハガル" "バアル" "バラキア" "バラク" "バラバ" "バラム" "バリエス" "バルサバ" "バルテマイ" "バルトロマイ" "バルナバ" "パウロ" "パトロバ" "パヌエル" "バルメナ" "パレク" "パレス" "パロ" "ヒメナヨ" "ヒメナヨ" "ピラト" "ピリゴン" "ピリポ" "ピレト" "ピレモン" "ピロロコ" "フイベ" "フゲロ" "ブラスト" "プデス" "ププリヲ" "プリスキラ" "プロコロ" "プロス" "ヘゼキヤ" "ヘベル" "ヘリ" "ヘルメス" "ヘルモゲネ" "ヘレマ" "ヘレメ" "ヘロデ" "ヘロデオナ" "ヘロデヤ" "ベニヤミン" "ベリアル" "ベルゼブル" "ベルニケ" "ペストス" "ペテロ" "ペリクス" "ペルシー" "ホセヤ" "ボアズ" "ボアネルゲ" "ボソロ" "ポルキス" "ポルトナト" "ポンテオ" "ポンテヲ" "マアツ" "マイナン" "マコ" "マタイ" "マタツタ" "マタテヤ" "マツタテ" "マツタン" "マツテア" "マトサラ" "マナエン" "マナセ" "マリア" "マルコス" "マルタ" "マレレヱル" "ミカエル" "メルキ" "メルキセデク" "メレア" "モロク" "モーセ" "ヤイロ" "ヤコブ" "ヤソン" "ヤレド" "ヤンナ" "ヤンネ" "ヤンブレ" "ユウオデヤ" "ユウリアス" "ユスト"
        "ユダ"              ; Hebrews7-14 「ユダより出し」
        "ユテコ" "ユニケ" "ユブル" "ヨエル" "ヨオレム" "ヨサパテ" "ヨシア" "ヨシユア" "ヨセ" "ヨセフ" "ヨタム" "ヨナ" "ヨナン" "ヨハネ" "ヨハンナ" "ヨブ" "ヨラム" "ラガヲ" "ラケル" "ラザロ" "ラハブ" "ラメク" "リノス" "リベカ" "ルカ" "ルキ" "ルキヲ" "ルサニア" "ルシアス" "ルツ" "ルデア" "ルデヤ" "ルフ" "ルベン" "ルポ" "レギヨン" "レサ" "レツバイ" "レハベアム" "レパン"
        "レビ"              ; Luke10-32, John1-19 「レビの人」傍線なし 地名にもあり
        "レプタ" "ロイス" "ロト" "ローダ" "ヱパフラス" "ヱラスト" "ヱリアキム" "ヱル" "ヱレミヤ" "ヱンモル" "ヲネシポロ"
        ;; 箴言
        "ヒゼキヤ" "ヤケ" "アグル" "イテエル" "ウカル" "レムエル" "ヱホバ"
        ;; 出埃及
        "ヱムエル" "ヱテロ" "ゲルシヨン" "リブニ" "シメイ" "メラリ" "マヘリ" "ムシ" "ホル" "プワ" "パル" "ヘノク" "ヘヅロン" "カルミ"
        "ヤミン" "オハデ" "ヤキン" "ゾハル" "シヤウル" "ヌン" "イツサカル" "ダン" "シフラ" "リウエル" "チツポラ" "ゲルシヨム" "コハテ"
        "アムラム" "イヅハル" "ヘブロン" "ウジエル" "ヨケベデ" "ネベグ" "ジクリ" "ミサエル" "エルザパン" "シテリ" "ナダブ" "アビウ"
        "エレアザル" "イタマル" "アツシル" "エルカナ" "アビアサフ" "ナシヨン" "エリセバ" "プテエル" "ピネハス" "ウリ" "ミリアム" "エリエゼル"
        "ベザレル" "アヒサマク" "アホリアブ"
        ;; 創世記
        "ヱバ" "イラデ" "メホヤエル" "メトサエル" "レメク" "アダ" "チラ" "ヤバル" "ユバル" "トバルカイン" "ナアマ"
        "エノス" "マハラレル" "メトセラ" "ハム" "ヤペテ" "ゴメル" "マデア" "ヤワン" "トバル" "メセク" "テラス"
        "アシケナズ" "リパテ" "トガルマ" "タルシシ" "キツテム" "ドダニム"  "ミツライム" "フテ" "セバ"
        "サブタ" "ラアマ" "サブテカ" "シバ" "デダン" "ニムロデ" "ルデ" "アナミ" "レハビ" "ナフト"
        "バテロス" "カスル" "カフトリ" "エブス" "アルワデ" "ゼマリ" "ハマテ" "ヱベル" ; "ギルガシ"
        "アシユル" "アルパクサデ" "ルデ" "ウヅ" "ゲテル" "マシ" "シラ" "エベル" "ペレグ" "ヨクタン"
        "アルモダデ" "シヤレフ" "ハザルマウテ" "ヱラ" "ハドラム" "ウザル" "デクラ" "オバル" "アビマエル"
        "オフル" "ヨバブ" "リウ" "セルグ" "ナホル" "アブラム" "ハラン" "サライ" "ミルカ" "イスカ"
        "アムラペル"  "アリオク" "ケダラオメル" "テダル" "ベラ" "ビルシア" "シナブ" "セメベル"
        "エシコル" "アネル" "メルキゼデク" "イシマエル" "アタエルロイ" "ベニアミン" "ピコル"
        ;;  "カナン" "マゴグ" "ハビラ" 地名にもあり
        "ヱホバエレ"               ; 22:14 何故か一本線
        "ブヅ" "ケムヱル" "ケセデ" "ハゾ" "ピルダシ" "ヱデラフ" "ベトエル" "ルマ" "テバ" "ガハム"
        "タハシ" "マアカ" "エフロン" "ラバン" "ケトラ" "ジムラン" "ヨクシヤン" "メダン" "イシバク"
        "シユワ" "エペル" "アビダ" "エルダア" "ネバヨテ" "ケダル" "アデビエル" "ミブサム" "ミシマ" "ドマ"
        "マツサ"                       ; 25:14 地名にもあり
        "ハダデ" "テマ" "ヱトル" "ネフシ" "ケデマ" "アホザテ" "ベエリ" "ユデテ" "エロン" "バスマテ"
        "マハラテ" "レア" "ジルパ" "ビルハ" "アルキ" "セニ" "デナ" "ハモル" ; "マムレ"
        "デボラ" "アビメレク" "ベノニ" "ヂベオン" "アヤ" "アナ" "アホリバマ" "エリパズ" "ヱウシ" "ヤラム"  "テマン"
        "オマル" "ゼポ" "ガタム" "テムナ" "ナハテ" "ゼラ" "シヤンマ" "ミザ" "ロタン" "シヨバル" "デシヨン" "エゼル"
        "デシヤン" "ヘマム" "アルワン" "マナハテ" "エバル" "シポ" "オナム" "ヘムダン" "エシバン" "イテラン" "ケラン"
        "ビルハン" "ザワン" "ヤカン" "アラン" "ベオル" "ホシヤム" "ベダデ" "サムラ" "サウル" "アクボル"
        "バアルハナン" "ハダル" "メヘタベル" "マテレデ" "メザハブ" "アルワ" "エテテ" "エラ" "ピノン"
        "ミブザル" "マグデエル" "イラム" "ポテパル" "ヒラ" "シユア" "オナン" "ペレヅ" "ザフナテパネア"
        "アセナテ" "ハムル" "トラ" "シムロン" "セレデ" "ヤリエル" "ゼボン" "ハギ" "シユニ" "エヅポン"
        "アロデ" "アレリ" "ヱムナ" "イシワ" "イスイ" "ベリア" "マルキエル" "ベケル" "アシベル" "ナアマン"
        "エヒ" "ロシ" "ムツピム" "ホパム" "アルデ" "ホシム" "ヤジエル" "グニ" "ヱゼル" "シレム" "マキル"
        ;; 利末記
        "シロミテ" "アザゼル"
        ;; 民數紀略
        "シデウル" "エリヅル" "ツリシヤダイ" "シルミエル" "ツアル"
        "ネタニエル" "ヘロン" "エリアブ" "アミホデ" "エリシヤマ" "パダヅル" "ギデオニ" "アビダン"
        "アミシヤダイ" "アヒエゼル" "オクラン" "パギエル" "デウエル" "エリアサフ" "エナン" "アヒラ"
        ;; 申命記
        "シホン" "エデレイ" "オグ" "アナク" "ヱフンネ" "カルブ" "レバイム" "アンモン" "ヤイル"
        "バアルペオル" "ダタン" "アビラム"
        ))                              ; 人名

(setq land-name-table                   ;  "イスカリ[オヲ]テ"、"ゴルゴ[タダ]" 兩方ある
      '("アイノム" "アカヤ" "アシドド" "アジア" "アソス" "アツタリア" "アツピーポロム" "アテンス" "アデリア" "アドラミテオム" "アビレネ" "アポロニヤ" "アムピポリス" "アラビヤ" "アリマタヤ" "アレオ" "アレキサンデリア" "アンテオケ" "アンテパトリス" "イコニオム" "イスカリオテ" "イスカリヲテ" "イスラエル" "イタリヤ" "イツリア" "イドマヤ" "イルリコ" "エジプト" "エテヲピア" "エフライム" "エペソ" "エラム" "エリコ" "エルサレム" "カイザリヤ" "カナ" "カナン" "カパドキア" "カペナウム" "カペナウン" "カラン" "カルダヤ" "ガザ" "ガダラ" "ガラテヤ" "ガリラヤ" "キヨス" "キリキア" "キリキヤ" "ギリシヤ" "クニドス" "クプロ" "クラウダ" "クラニオン" "クレテ" "クレネ" "ケデロン" "ケンクレア" "ゲツセマネ" "ゲネサレ" "コス" "コラジン" "コリント" "コロサイ" "ゴグ" "ゴモラ" "ゴルゴダ" "ゴルゴタ" "サイロピニケ" "サマリア" "サマリヤ" "サモス" "サモトラケ" "サラミス" "サリム" "サルデス" "サルモネ" "サレム" "サレパタ" "サロン" "シオン" "シドン" "シナイ" "シロアム" "シヲン" "スカル" "スクテヤ" "スケム" "スムルナ" "スラクサ" "スリヤ" "セルキア" "ソドム" "タルソ" "ダマスコ" "ダルマテヤ" "ツロ" "テアテラ" "テサロニケ" "テベリア" "テラコニテ" "デカポリス" "デルベ" "トレマイ" "トロアス" "トログリヲム" "ナザレ" "ニコポリス" "ネアポリス" "ハルマゲドン" "バビロン" "パタラ" "パトモス" "パポス" "パムフリア" "パルテア" "ヒエラポリ" "ヒスパニヤ" "ヒラデルヒア" "ビテニア" "ピシデア" "ピニクス" "ピニケ" "ピリピ" "フルギア" "フルギヤ" "プテヲリ" "ヘブル" "ベタニヤ" "ベテサイダ" "ベテパゲ" "ベテレヘム" "ベレア" "ペルガモ" "ペルゲ" "ポント" "マグダラ" "マケドニヤ" "マゴグ" "ミテレネ" "ミデアン" "ミレトス" "ムシア" "ムラ" "メソポタミア" "メソポタミヤ" "メデア" "メリタ" "ユダヤ" "ユフラテ" "ヨツパ" "ヨルダン" "ラオデキヤ" "ラサイア" "ラマ" "ラヲデキヤ" "リブヱ" "ルカオニヤ" "ルキヤ" "ルステラ" "ルツダ" "レギヲ" "ロドス" "ロマ" "ヱマヲ" "ナイン" "ダルマヌタ"
        ;; 出埃及
        "ヱブス" "レピデム" "メラ" "ヘテ" "アモリ" "ヒビ" "ピトム" "ラメセス" "ホレブ" "ペリジ" "ゴセン" "スコテ" "エタム"
        "ヱホバニシ" "ミグドル" "ピハヒロテ" "バアルゼポン" "ペリシテ" "モアブ" "エドム" "エリム" "シユル" "シン" "メリバ" "アマレク" ; "マツサ" 人名にもあり
        ;; 創世記
        "エデン" "ピソン" "ハビラ" "ギホン" "クシ" "ヒデケル" "アツスリヤ" "ノド" "アララテ" "シナル" "バベル"
        "エレク" "アツカデ" "カルネ" "レホポテイリ" "カラ" "レセン" "ゲラル" "アデマ" "ゼボイム" "レシヤ"
        "メシヤ" "セバル" "カルデア" "ウル" "シケム" "モレ" "ベテル" "アイ" "ゾアル" "エラサル"
        "ゴイム" "シデム" "アシタロテ" "カルナイム" "レパイム" "ズジ" "シヤベキリアタイム"
        "エミ" "セイル" "ホリ" "エルパラン" "エンミシパテ" "カデシ" "ハザゾンタマル" "ホバ" "シヤベ" "ケニ"
        "ケナズ" "カデモニ" "ベエルラハイロイ" "ベレデ" "アンモニ" "ベエルシバ" "パラン" "モリア"
        "キリアテアルバ" "マクペラ" "ラハイロイ" "アツシユリ" "レトシ" "リウミ" "パダンアラム" "スリア"
        "エセク" "シテナ" "レホボテ" "ルズ" "エガルサハドタ" "ギレアデ" "ミヅパ" "マハナイム"
                                        ; "ニネベ" "ヘブロン" "ダン"
        "ヤボク" "ベニエル"  "マムレ" "エル" "エロヘ" "エルベテル" "ギルガシ" "エフラタ" "エダル" "デナバ"
        "マスレカ" "パウ" "ボヅラ" "アビテ" "ドタン" "アドラム" "クジブ" "エナイム" "オン"
        "アベルミツライム" "パダン" "アタデ"
        ;; 利末記
        "デブリ"
        ;; 申命記
        "トペル" "ハゼロテ" "デザハブ" "カデシバルネア" "ヘシボン" "バシヤン" "レバノン" "ホルマ"
        "アラバ" "エラテ" "エジオンゲベル" "アル" "ゼレデ" "カフトル" "アビ" "アルノン" "ケデモテ"
        "ヤハヅ" "アロエル" "ザムズミ" "アルゴブ" "ヘルモン" "シリオン" "セニル" "サルカ" "ラバ"
        "ゲシユル" "ハヲテヤイル" "キンネレテ" "ベテベオル" "ピスガ" "ベゼル" "ラモテ" "ゴラン"
        "ベテペオル" "タベラ" "キブロテハツタワ" "モセラ" "グデゴダ" "ヨテバ" "ゲリジム" "ギルガル"
        ))                              ; 土地

(setq others-name-table
      '("アケルダマ" "アバ" "アメン" "アメンハレルヤ" "アルパ" "エツパタ" "エピクリアン" "オメガ" "ガバタ" "キユビト" "キリステアン" "ケルビン" "コルバン" "サドカイ" "ストイク" "タラント" "タリタクミ" "デナリ" "ナルダ" "ナルド" "ハレルヤ" "バプテスマ" "パラダイス" "パリサイ" "パン" "ヒソプ" "ベテスダ" "ペンテコステ" "ホザナ" "マナ" "メツシヤ" "ユーロクルドン" "ラボニ" "リベルテン"
        ;; 出埃及
        "オメル" "ベカ" "ヒン" "エポデ" "アビブ" "シケル" "ケルブ" "ケルビム" "キユピト" "ウリム" "トンミム" "ゲラ" "ナタフ" "シケレテ" "ヘルベナ" "エパ" "アシラ"
        ;; 創世記
        "ブドラク" "ネピリム" "セヤ" "テラピム" "アロンバクテ" "シロ"
        ;; 利末記
        "ログ" "ヨベル" "ホメル"
        ))                              ; 他

(setq others-name-table-2 '("エリ" "ラビ" "ラマサバクタニ"))

;;; Not Yet Defined
'()


(setq others-index-table
      '("\\kanakt{除酵\\CID{13879}}{たねいれぬ\\D{\\MJMZM{090171}}{゜}んのいはひ}"
        "\\kanakt{逾越\\CID{13879}}{すぎこしのいはひ}"
        "\\kana{人}{ひと}\\MJMZM{090161}\\kanakt{子}{こ}"
        "\\kana{\\UTF{FA19}}{かみ}の\\kanakt{子}"
        "\\Kanakt{十,字,架}{じふ,じ,か}"
        "\\Kanakt{安,息,日}{あん,そく,にち}"
        "\\Kanakt{七,日}{なぬ,か}の\\kanakt{首}{はじめ}の\\kanakt{日}{ひ}"
        ;; "\\kana{十}{じふ}\\kanakt{二}{に}の\\kanakt{弟子}{でし}"
        ;; "\\kanakt{女王}{によわう}"
        ;; "\\Kanakt{惡,\\CID{14043}}{あく,ま}"
        ;; "\\Kanakt{惡,鬼}{あく,き}"
        ;; "\\kanakt{\\CID{13869}靈}{せいれい}"
        ;; "\\kanakt{學\\CID{13349}}{がくしや}"
        ;; "\\kana{預備日}{そなへび}"
        ;; "\\kanakt{備日}{そなへび}"
        ;; "\\kana{備\\CID{13879}日}{そなへび}"
        ))


;;;
;;; 片假名變換例外處理
;;;
(setq katakana-revert-table
      '(("\\oline{レビ}の\\kana{人}{ひと}" . "レビの\\kana{人}{ひと}")
                                        ; Luke-10.32, John-1.19
        ("\\oline{ユダ}より\\kana{出}{いで}し" . "\\oline[lines=2]{ユダ}より\\kana{出}{いで}し")
                                        ; Hebrews-7.14
        ("と\\oline{ユダ}の\\kana{家}{いへ}" . "と\\oline[lines=2]{ユダ}の\\kana{家}{いへ}")
                                        ; Hebrews-8.8
        ))

(defun revert-katakana-exception ()
  (interactive)
  (process-hentai-kana-table katakana-revert-table))

(defun insert-oline-katakana (katakana)
  (let* ((string
          (cond ((member katakana others-name-table)
                 katakana)
                ((member katakana person-name-table)
                 (format "\\oline{%s}"
                         (convert-kurikahesi-katakana katakana)))
                ((member katakana land-name-table)
                 (format "\\oline[lines=2]{%s}"
                         (convert-kurikahesi-katakana katakana))))))
    (unless (<= ?\ア (char-after (point)) ?\ン)
      (error "ah?"))
    (delete-char (length katakana))
    (cl-case (char-after (1- (point)))
      ((?\・ ?\、)
       (backward-delete-char 1)
       (insert "\\,")))
    (insert string)
    ;; (insert "\\,")
    (length string)))

(defun collect-katakana (point)
  (cl-loop for p from point
        for char = (char-after p)
        while (or (= char ?\ー)
                  (<= ?\ア char ?\ン))
        collect char into chars
        finally (return (apply 'string chars))))

(defun put-line-on-katakana ()
  (interactive)
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (cl-loop initially (init-log-and-begin-buffer current log)
        for point from 1
        for char = (char-after point)
        while char do
        (if (<= ?\ア char ?\ン)
            (let ((katakana (collect-katakana point)))
              (cond ((member katakana others-name-table-2)
                     ;; others-name-table-2は、
                     ;; 「ラビ、ラビ」「エリ、エリ、」のやうに、
                     ;; 間の「、」を出力したい際の片假名を登録しておくもの
                     (cl-incf point (length katakana)))
                    ((or (member katakana others-name-table)
                         (member katakana person-name-table)
                         (member katakana land-name-table))
                     (cl-incf point
                           (insert-oline-katakana katakana))
                     (refresh-screen))
                    (t                  ; 未定義の片假名はlogに出す
                     (switch-to-buffer "*yomi-log*")
                     (insert katakana)
                     (insert "\n")
                     (switch-to-buffer current)
                     (cl-incf point (length katakana))
                     (refresh-screen)))
              (goto-char (1+ point)))
          (forward-char 1)))))


(defun kanji-p (char)
  (not (or (<= 0 char 255) (<= ?\ァ char ?\ヺ) (<= ?\ぁ char ?\ゖ) (<= ?、 char ?。))))

(defun collect-kanji (point)
  (cl-loop for p from point
        for char = (char-after p)
        while (kanji-p char)
        collect char into chars
        finally (return (apply 'string chars))))

(defun collect-kanji-jukugo ()
  (interactive)
  (let ((current (current-buffer))
        (log (get-buffer-create "*yomi-log*")))
    (cl-loop initially (init-log-and-begin-buffer current log)
          for point from 1
          for char = (char-after point)
          while char do
          (if (kanji-p char)
              (let ((kanji (collect-kanji point)))
                (cond (t                ; 未定義の漢字はlogに出す
                       (switch-to-buffer "*yomi-log*")
                       (insert kanji)
                       (insert "\n")
                       (switch-to-buffer current)
                       (cl-incf point (length kanji))
                       (refresh-screen)))
                (goto-char (1+ point)))
            (forward-char 1)))))

;;;;
;;; "selection too long to send to terminal" なる warning (X11版では落ちる) を禦ぐために
;;; 適當なタイミングで screen update をする
(setq *counter* 0)

(defun refresh-screen (&optional arg)
  (when (= 0 (% (cl-incf *counter*) 1000))
    (when arg
      (warn "%s = %d" arg *counter*)
      (scroll-other-window))
    (sit-for 0)))

;;;;
;;;;
;;; 新約全書からLaTeX command を除く
;;; (本來、Webからはtextのみを抽出し、それを加工すべきであったが…)
(defun remove-latex-command-lines ()
  (interactive)
  (buffer-disable-undo)
  (latex-mode)
  (progn
    (beginning-buffer)
    (set-mark-command nil)
    (when (or (search-forward "\\chapter*{馬太傳福音書}" nil t nil) ; obsolete
              (search-forward "\\chapter*{\\Large \\Kanakt{新,約,全,書}" nil t nil)
              (search-forward "\\section*{\\mc MATTHEW 馬太傳福音書第一章}" nil t nil))
      (move-beginning-of-line nil)
      (previous-line)
      (move-end-of-line nil)
      (kill-region t t t))
    (deactivate-mark))
  (cl-loop initially (beginning-buffer)
        for pos = (re-search-forward "^%%" nil t nil)
        while pos do
        (move-beginning-of-line nil) (kill-line 1)
        (refresh-screen))
  (cl-loop initially (beginning-buffer)
        for pos = (search-forward "\\begin{wrapfigure}" nil t nil)
        while pos do
        (move-beginning-of-line nil) (kill-line 1)
        (search-forward "\\bf" nil t nil)
        (backward-kill-sexp) (delete-char 1) (backward-char)
        (forward-sexp) (kill-line) (backward-sexp)
        (set-mark-command nil) (move-beginning-of-line nil) (kill-region t t t)
        (forward-line) (move-beginning-of-line nil) (kill-line 1)
        (deactivate-mark)
        (refresh-screen))
  (cl-loop for key in '("\\indent" "\\goodbreak" "\\markboth" "\\vspace*" "\\begin" "\\end" "\\vskip" "\\thispagestyle")
        do (cl-loop initially (beginning-buffer)
                 for pos = (search-forward key nil t nil)
                 while pos do
                 (move-beginning-of-line nil) (kill-line 1)
                 (refresh-screen)))
  (buffer-enable-undo))

(defun remove-latex-command-words ()
  (interactive)
  (delete-other-windows)
  (latex-mode)
  (buffer-disable-undo)
  (cl-loop  initially (beginning-buffer)
         for key = "\\chapter*{}"
         for pos = (search-forward key nil t nil)
         while pos do
         (move-beginning-of-line nil) (kill-line)
         (refresh-screen))
  (cl-loop  initially (beginning-buffer)
         for key = "\\chapter*"
         for pos = (search-forward key nil t nil)
         while pos do
         (backward-delete-char (length key))
         (set-mark-command nil) (forward-sexp)
         (backward-char) (delete-char 1)
         (move-beginning-of-line nil) (delete-char 1)
         (deactivate-mark)
         (refresh-screen))
  (cl-loop for key in '("\\katano" "\\noindent" "\\kanakt" "\\kana" "\\protect" "\\nolinebreak" "[3]" "\\section*" "\\dropping" "{2}" "\\break" "\\mc " "\\Large ")
        do (cl-loop initially (beginning-buffer)
                 for pos = (search-forward key nil t nil)
                 while pos do
                 (backward-delete-char (length key)) ; (backward-kill-word 1) ← 遲い!
                 (refresh-screen)))
  (buffer-enable-undo))

(defun remove-latex-command-rms ()
  (interactive)
  (latex-mode)
  (cl-loop initially (beginning-buffer) ; {\rmfamily{} \HUGE ア} → ア
        for pos = (re-search-forward "\\\\rmfamily{} \\\\HUGE \\([あ-ん]\\|[ア-ン]\\)" nil t nil)
        while pos do
        (backward-char)
        (backward-kill-sexp 3) (backward-delete-char 1) (forward-char)
        (delete-char 1)
        (refresh-screen))
  (cl-loop initially (beginning-buffer) ; {\rmfamily{} \HUGE {當}{そのころ}}時 → {當時}{そのころ}
        for pos = (search-forward "\\rmfamily{} \\HUGE " nil t nil)
        while pos do
        (backward-up-list) (forward-sexp) (set-mark-command nil)
        (re-search-forward "\\([あ-ん]\\|[ア-ン]\\|[{}]\\)" nil t nil) (backward-char)
        (kill-region t t t) (backward-delete-char 1) (backward-sexp) (backward-char) (yank)
        (backward-up-list) (set-mark-command nil) (move-beginning-of-line nil) (kill-region t t t)
        (deactivate-mark)
        (refresh-screen))
  (cl-loop initially (beginning-buffer) ; {\rmfamily{} T} → 1. T
        for pos = (search-forward "\\rmfamily{} " nil t nil)
        while pos do
        (backward-kill-sexp 2) (backward-delete-char 1) (forward-char)
        (delete-char 1) (move-beginning-of-line nil) (insert "1. ")
        (refresh-screen)))

(defun remove-latex-command-ems ()
  (interactive)
  (latex-mode)
  (cl-loop initially (beginning-buffer) ; \textit{that had been the wife} → /that had been the wife/
        for key = "\\textit"
        for pos = (search-forward key nil t nil)
        while pos do
        (backward-delete-char (length key))        ; (backward-kill-word 1) ← 遲い!
        (insert "/") (set-mark-command nil) (forward-sexp) (insert "/")
        (backward-char 2) (delete-char 1) (exchange-point-and-mark) (delete-char 1)
        (deactivate-mark)
        (refresh-screen))
  (cl-loop initially (beginning-buffer) ; \textbf{It is written, .... God.} →*It is written, .... God.*
        for key = "\\textbf"
        for pos = (search-forward "\\textbf" nil t nil)
        while pos do
        (backward-delete-char (length key))        ; (backward-kill-word 1) ← 遲い!
        (insert "*") (set-mark-command nil) (forward-sexp) (insert "*")
        (backward-char 2) (delete-char 1) (exchange-point-and-mark) (delete-char 1)
        (deactivate-mark)
        (refresh-screen))
  (cl-loop initially (beginning-buffer) ; {\bfseries{} T}*AKE...* →*TAKE...*
        for key = "\\bfseries{} "
        for pos = (search-forward key nil t nil)
        while pos do
        (backward-kill-sexp 2) (backward-delete-char 1) (forward-char)
        (delete-char 1) (transpose-chars t)
        (move-beginning-of-line nil) (insert "1. ")
        (refresh-screen))
  (cl-loop initially (beginning-buffer)
        for key = "\\footnotesize "
        for pos = (search-forward key nil t nil)
        while pos do
        (backward-delete-char (length key))        ; (backward-kill-word 1) ← 遲い!
        (backward-delete-char 1) (forward-sexp) (delete-char 1)
        (refresh-screen)))

(defun remove-duplicate-lines ()
  (interactive)
  (let ((end (end-buffer)))
    (cl-loop initially (beginning-buffer)
          for pos = (re-search-forward "^$" nil t nil)
          while (and pos (< pos end)) do
          (forward-line)
          (cl-loop for char = (char-after)
                while (eql char 10) do     ; return
                (kill-line)
                (cl-decf end)))))
;;;;
;;;;
;;; 約翰默示録のデータに略字が甚しいので正字に戻すスクリプトを書いた
(defun ryaku2seiji (arg)
  (interactive "P")
  (cl-loop initially (beginning-buffer)
        for point = (point)
        for char = (char-after point)
        while char do
          (let ((pos (cl-position char ryaku)))
            (if (and pos (or (null arg)
                             (y-or-n-p (format "%c → %c?" char (aref seiji pos)))))
                (progn
                  (delete-char 1)
                  (insert (aref seiji pos)))
              (forward-char 1))))
  (beginning-buffer) (query-replace "欠" "缺")
  (beginning-buffer) (query-replace "弁" "辯")
  (beginning-buffer) (query-replace "弁" "辨")
  (beginning-buffer) (query-replace "弁" "瓣")
  (beginning-buffer) (query-replace "弁" "辮"))

(setq ryaku "鎮鋳届壷訳旧縦勧経済径奨励仮験覚辞応医続画満称剣慎没価売様乱偽顕随飲学弾従条礼獣尽挙釈辺当歯炉変雑巌銭与麦発伝巻万帰拝昼囲薬体残児聴奥帯髪鉄蔵戦争数将楽処労声来乗権栄属転為霊宝読国両会真実台悪観営諌気窃盗揺践猟抜寝荘歓況誉闘壮遅厳顔静騒潜懐ゃゅょっャュョッ豊証軽隠黙焼讃壊広拡寿芸酔傅窓効択秘剰賎蛮") ; 献回恒 欠弁
(setq seiji "鎭鑄屆壺譯舊縱勸經濟徑奬勵假驗覺辭應醫續畫滿稱劍愼沒價賣樣亂僞顯隨飮學彈從條禮獸盡擧釋邊當齒爐變雜巖錢與麥發傳卷萬歸拜晝圍藥體殘兒聽奧帶髮鐵藏戰爭數將樂處勞聲來乘權榮屬轉爲靈寶讀國兩會眞實臺惡觀營諫氣竊盜搖踐獵拔寢莊歡况譽鬪壯遲嚴顏靜騷潛懷やゆよつヤユヨツ豐證輕隱默燒讚壞廣擴壽藝醉傳窗效擇祕剩賤蠻") ; 獻囘恆 缺辨

;;; 以下注意
;; CL-USER(166): (code-char #x51b5)
;; #\况                                    ; いはんや
;; CL-USER(167): (code-char #x8ab6)
;; #\誶                                    ; そしり
;; CL-USER(168): (code-char #x5aa2)
;; #\媢                                    ; ねたみ
;; CL-USER(169): 

(defun check-okuri-kana (arg)
  (interactive "P")
  (let ((start ?\あ) (end ?\ん))
    (when arg
      (setq start arg))
    (cl-loop for char from start to end
          do (cl-loop initially (beginning-buffer)
                   for pos = (search-forward (format "%c}%c" char char) nil t nil)
                   while pos do
                   (sit-for 4)))))
;;;
(provide :utf-to-jis)
