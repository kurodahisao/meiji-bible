;;; emacs-lisp; lexical-binding:t; coding:utf-8 -*-

(require 'cl)

;;; 手順:
;;; Open numbers.wiki
;;; M-x numbers-wiki-to-furikana
;;; Save as numbers.furikana
;;; M-x numbers-furikana-to-latex    ; 正字體變換 (引數 C-u をつけるとIPAFont利用)
;;; Save as numbers.tex
;;; Compile LaTex
;;; $ dvipdfmx -f kozukapr6n.map _TZ_3170-penguin.dvi 
;;; CL-USER> (pdf:pdf-adjust-oyamoji "~/projects/meiji-bible/_TZ_3170-penguin.pdf" "numbers.pdf")

(require :utf-to-jis)
(require :shingen)
(require :exodus)
(require :genesis)
(require :leviticus)

(defun numbers-wiki-to-latex (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (numbers-wiki-to-furikana start end)
  (numbers-furikana-to-latex nil))

(defun numbers-furikana-to-latex (ipa)
  (interactive "P")
  (load-file "numbers.katakana")
  (convert-shingen-subsection)
  (kanakt-shingen-jukugo)
  (latex-shingen-section)
  (latex-shingen-subsection)
  (latex-shingen-white-space)
  (convert-numbers-title)
  (convert-numbers-kanakt)
  (utf-to-jis-2 ipa))

(defun numbers-jukugo ()
  (load-file "shingen.jukugo")
  (load-file "exodus.jukugo")
  (load-file "genesis.jukugo")
  (load-file "leviticus.jukugo")
  (load-file "numbers.jukugo")
  (setq *current-jukugo*
     (append *numbers-jukugo* *leviticus-jukugo* *genesis-jukugo* *exodus-jukugo* *shingen-jukugo*)))

(defun numbers-wiki-to-furikana (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (hex-to-char)
  (set-buffer-file-coding-system 'utf-8)
  (furikana-shingen-jukugo 'numbers (numbers-jukugo) start end)
  (goto-char (point-min))
  (kill-line 1)
  (insert "-*- coding:utf-8 -*-\n"))

(defun convert-numbers-title ()
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "箴言" nil t)
    (replace-match "民數紀略" nil nil))
  (goto-char (point-min))
  (while (re-search-forward "言箴" nil t)
    (replace-match "略紀數民" nil nil)))

(defun convert-numbers-kanakt ()
  ;; 辭書他で對應できない例外處理をここに書く
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "、" nil t)
    (replace-match " " nil nil))
  (goto-char (point-min))
  (while (re-search-forward "\\\\,\\\\oline" nil t)
    (backward-sexp)
    (forward-char 2)
    (insert " ")))

(provide :numbers)
