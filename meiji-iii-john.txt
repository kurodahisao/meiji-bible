%%% -*- mode: LaTeX; coding: utf-8 -*-
%%% -*- mode: LaTeX; coding: iso-2022-jp-2 -*-
\documentclass[a5j,10pt,openany,twocolumn]{tbook}% mentuke tombo myjsbook ,oneside,tbook,treport
\usepackage[deluxe,expert,multi]{otf}
%% \usepackage{atbegshi}
%% \AtBeginShipoutFirst{\special{pdf:tounicode 90ms-RKSJ-UCS2}}
%% \usepackage{makeidx}
%% \usepackage[dvipdfmx,bookmarks=true,bookmarksnumbered=true]{hyperref}
\usepackage[dvipdfmx]{bxglyphwiki}
\usepackage{kochu}
\usepackage{furikana}
\usepackage{furiknkt}
\usepackage{jdkintou}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{ipamjm}
\usepackage{pxjaschart}
\usepackage{uline--}
\pagestyle{bothstyle}
\setlength\intextsep{0.8zw}
\setlength\textfloatsep{0.8zw}
\kanjiskip0.2pt plus 0.4pt minus 0.4pt
\xkanjiskip\kanjiskip
\AtBeginDvi{\special{papersize=\the\paperwidth,\the\paperheight}}
%%% \setlength{\evensidemargin}{\oddsidemargin}
%% \newcommand{\pseudochapter}[1]{\chapter*{#1}}
%% \newcommand{\pseudosection}[1]{\section*{#1}}
%% \makeatletter
%% \def\section{\@startsection {section}{1}{\z@}{-0ex plus -0ex minus -.0ex}{0 ex plus .0ex}{\normalsize}}
%% \makeatother 
%% \makeindex
%% \title{耶蘇降生千八百九十六年 \\ {\Huge 新約全書} \\ 明治二十九年}
%% \author{大日本聖書舘 \\ 横濱二十六番}
%% \date{令和元年皐月 \\ \href{https://bitbucket.org/kurodahisao/meiji-bible}{\LaTeX 組版生成}}
%% \setcounter{tocdepth}{1}
%% \renewcommand{\contentsname}{新約全書目録}
%% \makeatletter
%% \renewcommand{\tableofcontents}{% TOCはいつでもTwoColumn
%%   \if@twocolumn\@restonecolfalse
%%   \else\@restonecoltrue\twocolumn\fi
%%   \chapter*{\contentsname
%%     \@mkboth{\contentsname}{\contentsname}%
%%   }\@starttoc{toc}%
%%   \if@restonecol\onecolumn\fi
%% }
%% \renewenvironment{theindex}
%%   {\if@twocolumn\@restonecolfalse\else\@restonecoltrue\fi
%%    \columnseprule\z@ \columnsep 35\p@
%%    \twocolumn%
%%    \@mkboth{\indexname}{\indexname}%
%%    \thispagestyle{jpl@in}\parindent\z@
%%    \parskip\z@ \@plus .3\p@\relax
%%    \let\item\@idxitem}
%%   {\if@restonecol\onecolumn\else\clearpage\fi}
%% \makeatother
\begin{document}
\topmargin-0.6in
\newcommand{\LRkanJI}[2]{%
  \tbaselineshift=0.3zw  % 0pt
        \parbox<y>{1zw}{%
            \scalebox{0.5}[1]{%
                \makebox[2zw]{#1\hspace{-0.1zw}#2}}}}
\def\katano#1{\linebreak[3]\kern0.5zw\raisebox{0.5zw}[0pt][0pt]{\tiny#1}\kern0.1zw}
\makeatletter
\def\kochuuname{}
\def\arabicchuucite#1{\rensuji{\chuucite{#1}}}
\def\@kochuulabel#1{\rensuji{#1}}
\makeatother
%% \maketitle
%% \tableofcontents
\chapter*{\Large \Kanakt{新,約,全,書}{しん,やく,ぜん,しよ}\Kanakt{使,徒}{し,と}ヨハネ\Kanakt{第,三,書}{だい,さん,しよ}}
%% \addcontentsline{toc}{chapter}{約翰第三書}

\markboth{書三第翰約}{約翰第三書}
\noindent\katano{一} \protect\nolinebreak  \kanakt[3]{長老}{ちやうらう}\kana{愛}{あい}するガヨス\kanakt{即}{すなは}ち\kanakt{我}{わ}が\kanakt{誠}{まこと}に\kana{愛}{あい}する\kanakt{所}{ところ}の\kana{者}{もの}に\kana{書}{ふみ}を\kana{贈}{おく}る
%% \refstepcounter{section}\addcontentsline{toc}{section}{ {\tiny  長老愛するガヨス即ち我が誠に愛する所の者に書}}
\katano{二} \protect\nolinebreak  \kana{愛}{あい}する\kana{者}{もの}よ\kanakt{爾}{なんぢ}が\kanakt{靈魂}{たましひ}の\kana{隆}{さか}んなる\kana{如}{ごと}く\kanakt{爾}{なんぢ}すべての\kana{事}{こと}につきて\kana{隆}{さか}んに\kana{又}{また}\kanakt{康強}{すこやか}ならんことを\kana{我}{われ}ねがふ
\katano{三} \protect\nolinebreak  \kanakt[3]{兄弟}{きやうだい}\kana{來}{きた}りて\kanakt{爾}{なんぢ}が\kana{眞理}{まこと}を\kanakt{有}{たもて}ること\kanakt{即}{すなは}ち\kanakt{爾}{なんぢ}が\kana{眞理}{まこと}に\kana{行}{あゆ}むことを\kanakt{證}{あかし}したれば\kana{我}{われ}\kanakt{甚}{はなは}だ\kanakt{喜}{よろこ}べり
\katano{四} \protect\nolinebreak  わが\kanakt{子}{こ}\kana{等}{たち}の\kana{眞理}{まこと}を\kana{行}{あゆ}むを\kana{聞}{きく}に\kana{愈}{まさ}れる\kanakt{大}{おほい}なる\kanakt{喜樂}{よろこび}は\kana{我}{われ}になし
\katano{五} \protect\nolinebreak  \kana{愛}{あい}する\kana{者}{もの}よ\kanakt{爾}{なんぢ}は\kanakt{賓旅}{たびびと}なる\kanakt{兄弟}{きやうだい}にまで\kana{凡}{すべ}て\kanakt{行}{おこな}ふに\kanakt{忠信}{ちゆうしん}をもて\kanakt{行}{おこな}へり
\katano{六} \protect\nolinebreak  かれら\kanakt{教會}{けうくわい}の\kana{前}{まへ}に\kana{在}{あり}て\kanakt{爾}{なんぢ}の\kana{愛}{あい}を\kanakt{證}{あかし}せり\kanakt{爾}{なんぢ}もし\kana{神}{かみ}に\kana{合}{かな}ふべく\kana{彼等}{かれら}の\kanakt{行路}{たび}を\kanakt{助}{たすけ}ば\kana{其}{その}\kanakt{行}{おこな}ふところ\kana{善}{ぜん}なり
\katano{七} \protect\nolinebreak  \kana{彼等}{かれら}は\kana{主}{しゆ}の\kanakt{名}{な}の\kana{爲}{ため}に\kana{出}{いで}て\kanakt{異邦人}{いはうじん}より\kana{何}{なに}をも\kana{受}{うけ}ざれば\kana{也}{なり}
\katano{八} \protect\nolinebreak  \kana{是}{この}\kana{故}{ゆゑ}に\kana{我儕}{われら}かくの\kana{如}{ごと}き\kana{人}{ひと}を\kana{助}{たす}くべし\kana{蓋}{そは}われらも\kana{彼等}{かれら}と\kana{偕}{とも}に\kana{眞理}{まこと}に\kanakt{働}{はたら}く\kana{者}{もの}とならん\kana{爲}{ため}なり
\katano{九} \protect\nolinebreak  われ\kana{曩}{さき}に\kana{書}{ふみ}を\kanakt{教會}{けうくわい}に\kana{贈}{おく}りしが\kana{彼等}{かれら}の\kana{中}{うち}に\kana{於}{おい}て\kana{長}{をさ}たらんことを\kana{欲}{この}むデヲテレペス\kana{我}{われ}を\kana{納}{うけ}ざりき
\katano{一〇} \protect\nolinebreak  \kana{我}{われ}もし\kana{往}{ゆか}ば\kana{其}{その}\kana{行}{なせ}る\kanakt{所}{ところ}を\kanakt{心}{こころ}に\kana{記}{とめ}\kana{置}{おか}ん\kana{彼}{かれ}は\kanakt[3]{惡}{あしき}\kanakt{言}{ことば}をもて\kanakt{妄}{みだり}に\kana{我儕}{われら}を\kana{論}{ろん}じ\kana{且}{かつ}これを\kanakt{以}{も}て\kana{足}{たれ}りとせず\kanakt{自}{みづか}ら\kanakt{兄弟}{きやうだい}を\kana{接}{うけ}ず\kana{其}{それ}を\kana{接}{うけ}んとする\kana{者}{もの}をも\kanakt{妨}{さまた}げて\kanakt{教會}{けうくわい}より\kanakt{黜}{しりぞ}けたり
\katano{一一} \protect\nolinebreak  \kana{愛}{あい}する\kana{者}{もの}よ\kana{惡}{あく}に\kana{效}{なら}ふ\kana{勿}{なか}れ\kanakt{即}{すなは}ち\kana{善}{ぜん}に\kana{效}{なら}へ\kana{善}{ぜん}を\kanakt{行}{おこな}ふ\kana{者}{もの}は\kana{神}{かみ}より\kana{出}{いで}\kana{惡}{あく}を\kanakt{行}{おこな}ふ\kana{者}{もの}は\kana{未}{いま}だ\kana{神}{かみ}を\kanakt{見}{み}ざる\kana{也}{なり}
\katano{一二} \protect\nolinebreak  デメテリヲは\kanakt{衆人}{ひとびと}と\kana{眞理}{まこと}とに\kanakt{證}{あかし}をせらる\kana{我儕}{われら}も\kanakt{證}{あかし}をす\kana{我儕}{われら}の\kanakt{證}{あかし}の\kana{眞實}{まこと}なるを\kanakt[3]{爾}{なんぢ}\kana{知}{しれ}り
\katano{一三} \protect\nolinebreak  \kana{我}{われ}なほ\kanakt{多}{おほく}の\kana{事}{こと}を\kanakt{爾}{なんぢ}に\kana{書}{かき}\kana{贈}{おく}らんと\kana{爲}{すれ}ども\kana{筆}{ふで}と\kana{墨}{すみ}とを\kanakt{以}{も}て\kana{書}{かき}おくるを\kanakt{欲}{このま}ず
\katano{一四} \protect\nolinebreak  \kanakt{速}{すみや}かに\kanakt{爾}{なんぢ}を\kanakt{見}{み}て\kana{口}{くち}を\kana{對}{むか}へ\kana{語}{かた}らんことを\kana{望}{のぞ}む\kanakt{願}{ねがは}くは\kanakt[3]{爾}{なんぢ}\kana{安}{やす}かれ\kanakt{多}{おほく}の\kana{友}{とも}なんぢの\kanakt{安}{やすき}を\kana{問}{とへ}り\kana{請}{こふ}なんぢ\kana{我}{われ}に\kanakt{代}{かはり}て\kanakt{諸友}{ともだち}おのおのに\kanakt{安}{やすき}を\kana{問}{とへ}

\vspace*{\fill}
\noindent
\Kanakt{新,約,全,書}{しん,やく,ぜん,しよ}\Kanakt{使,徒,約,翰}{し,と,よ,はね}\kana{第}{だい}\kana{三}{さん}\kana{書}{しよ}\kana[3]{終}{をはり}
