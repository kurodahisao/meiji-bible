;;; -*- mode:emacs-lisp; lexical-binding:t; coding:utf-8 -*-

(require 'cl)

;;; 手順:
;;; Open exodus.wiki
;;; M-x exodus-wiki-to-furikana
;;; Save as exodus.furikana
;;; M-x exodus-furikana-to-latex    ; 正字體變換 (引數 C-u をつけるとIPAFont利用)
;;; Save as exodus.tex
;;; Compile LaTex
;;; $ dvipdfmx -f kozukapr6n.map _TZ_3170-penguin.dvi 
;;; CL-USER> (pdf:pdf-adjust-oyamoji "~/projects/meiji-bible/_TZ_3170-penguin.pdf" "exodus.pdf")

(require :utf-to-jis)
(require :shingen)

(defun exodus-wiki-to-latex (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (exodus-wiki-to-furikana start end)
  (exodus-furikana-to-latex nil))

(defun exodus-furikana-to-latex (ipa)
  (interactive "P")
  (load-file "exodus.katakana")
  (convert-exodus-subsection)
  (kanakt-shingen-jukugo)
  (latex-shingen-section)
  (latex-shingen-subsection)
  (latex-shingen-white-space)
  (convert-exodus-title)
  (convert-exodus-kanakt)
  (utf-to-jis-2 ipa))

(defun exodus-wiki-to-furikana (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (load-file "shingen.jukugo")
  (load-file "exodus.jukugo")
  (hex-to-char)
  (set-buffer-file-coding-system 'utf-8)
  (furikana-shingen-jukugo 'exodus (append *exodus-jukugo* *shingen-jukugo*) start end)
  (convert-exodus-exception)
  (beginning-buffer)
  (kill-line 1)
  (insert "-*- coding:utf-8 -*-\n"))

(defun convert-exodus-exception ()
  ;; 辭書で對應できない例外處理
  (interactive)
  (beginning-buffer)
  (re-search-forward "{庭}{には}の{門}{かど}" nil t)
  (replace-match "{庭}{には}の{門}{もん}" nil nil))

(defun convert-exodus-subsection ()
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "{{verse|\\([0-9]+\\)|\\([0-9]+\\)}}" nil t)
    (replace-match " \\1:\\2 " nil nil)))

(defun convert-exodus-title ()
  (interactive)
  (beginning-buffer)
  (while (re-search-forward "箴言" nil t)
    (replace-match "出埃及記" nil nil))
  (beginning-buffer)
  (while (re-search-forward "言箴" nil t)
    (replace-match "記及埃出" nil nil)))

(defun convert-exodus-kanakt ()
  ;; 辭書他で對應できない例外處理をここに書く
  (interactive)
  (beginning-buffer)
  (while (re-search-forward "、" nil t)
    (replace-match " " nil nil))
  (beginning-buffer)                    ; カナが續くとき
  (while (re-search-forward "\\\\,\\\\oline" nil t)
    (backward-sexp)
    (forward-char 2)
    (insert " "))
  (beginning-buffer)
  (while (re-search-forward "刃" nil t) ; 
    (replace-match "\\\\CID{4250}" nil nil))
  (beginning-buffer)                    ; みち,みち
  (while (re-search-forward "みち,みち" nil t)
    (replace-match "みち,\\\\ajKunoji{}" nil nil))
  (beginning-buffer)
  (while (re-search-forward "\\(\\\\Kanakt\\)\\({贖,罪,所}{しよ,くざ,いしよ}\\)\\($\\| \\)" nil t)
    (replace-match "\\1[3]\\2")))

;;;
(provide :exodus)
