;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(in-package asdf)

(defsystem :bible-meiji
  :name "bible-meiji"
  :author "KURODA Hisao <littlelisper@gmail.com>"
  :maintainer "KURODA Hisao <littlelisper@gmail.com>"
  :description "BIBLE Meiji"
  :long-description "BIBLE Meiji"
  :components ((:file "bible-meiji" :depends-on ())
               (:file "bible-king" :depends-on ("bible-meiji")))
  :depends-on (:zpb-ttf2 :cl-pdf :cl-pdf-parser :ipa-font :drakma :cl-html-parse))
