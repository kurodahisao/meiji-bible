;;; emacs-lisp; lexical-binding:t; coding:utf-8 -*-

(require 'cl)

;;; 手順:
;;; Open leviticus.wiki
;;; M-x leviticus-wiki-to-furikana
;;; Save as leviticus.furikana
;;; M-x leviticus-furikana-to-latex    ; 正字體變換 (引數 C-u をつけるとIPAFont利用)
;;; Save as leviticus.tex
;;; Compile LaTex
;;; $ dvipdfmx -f kozukapr6n.map _TZ_3170-penguin.dvi 
;;; CL-USER> (pdf:pdf-adjust-oyamoji "~/projects/meiji-bible/_TZ_3170-penguin.pdf" "leviticus.pdf")

(require :utf-to-jis)
(require :shingen)
(require :exodus)
(require :genesis)

(defun leviticus-wiki-to-latex (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (leviticus-wiki-to-furikana start end)
  (leviticus-furikana-to-latex nil))

(defun leviticus-furikana-to-latex (ipa)
  (interactive "P")
  ;; (load-file "leviticus.katakana")
  (convert-shingen-subsection)
  (kanakt-shingen-jukugo)
  (latex-shingen-section)
  (latex-shingen-subsection)
  (latex-shingen-white-space)
  (convert-leviticus-title)
  (convert-leviticus-kanakt)
  (utf-to-jis-2 ipa))

(defun leviticus-jukugo ()
  (load-file "shingen.jukugo")
  (load-file "exodus.jukugo")
  (load-file "genesis.jukugo")
  (load-file "leviticus.jukugo")
  (setq *current-jukugo*
     (append *leviticus-jukugo* *genesis-jukugo* *exodus-jukugo* *shingen-jukugo*)))

(defun leviticus-wiki-to-furikana (start end)
  (interactive (list (read-minibuffer "START: ")
                     (read-minibuffer "END: ")))
  (hex-to-char)
  (set-buffer-file-coding-system 'utf-8)
  (furikana-shingen-jukugo 'leviticus (leviticus-jukugo) start end)
  (goto-char (point-min))
  (kill-line 1)
  (insert "-*- coding:utf-8 -*-\n"))

(defun convert-leviticus-title ()
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "箴言" nil t)
    (replace-match "利末記" nil nil))
  (goto-char (point-min))
  (while (re-search-forward "言箴" nil t)
    (replace-match "記末利" nil nil)))

(defun convert-leviticus-kanakt ()
  ;; 辭書他で對應できない例外處理をここに書く
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "、" nil t)
    (replace-match " " nil nil))
  (goto-char (point-min))
  (while (re-search-forward "\\\\,\\\\oline" nil t)
    (backward-sexp)
    (forward-char 2)
    (insert " "))
  (goto-char (point-min))
  (while (re-search-forward "\\( \\\\Kanakt\\)\\({任,職,祭}{にん,しよ,くさい} \\)" nil t)
    (replace-match "\\1[3]\\2")))

(provide :leviticus)
