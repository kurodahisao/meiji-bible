%%% -*- mode: LaTeX; coding: utf-8 -*-
%%% -*- mode: LaTeX; coding: iso-2022-jp-2 -*-
\documentclass[a5j,10pt,openany,twocolumn]{tbook}% mentuke tombo myjsbook ,oneside,tbook,treport
\usepackage[deluxe,expert,multi]{otf}
%% \usepackage{atbegshi}
%% \AtBeginShipoutFirst{\special{pdf:tounicode 90ms-RKSJ-UCS2}}
%% \usepackage{makeidx}
%% \usepackage[dvipdfmx,bookmarks=true,bookmarksnumbered=true]{hyperref}
\usepackage[dvipdfmx]{bxglyphwiki}
\usepackage{kochu}
\usepackage{furikana}
\usepackage{furiknkt}
\usepackage{jdkintou}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{ipamjm}
\usepackage{pxjaschart}
\usepackage{uline--}
\pagestyle{bothstyle}
\setlength\intextsep{0.8zw}
\setlength\textfloatsep{0.8zw}
\kanjiskip0.2pt plus 0.4pt minus 0.4pt
\xkanjiskip\kanjiskip
\AtBeginDvi{\special{papersize=\the\paperwidth,\the\paperheight}}
%%% \setlength{\evensidemargin}{\oddsidemargin}
%% \newcommand{\pseudochapter}[1]{\chapter*{#1}}
%% \newcommand{\pseudosection}[1]{\section*{#1}}
%% \makeatletter
%% \def\section{\@startsection {section}{1}{\z@}{-0ex plus -0ex minus -.0ex}{0 ex plus .0ex}{\normalsize}}
%% \makeatother 
%% \makeindex
%% \title{耶蘇降生千八百九十六年 \\ {\Huge 新約全書} \\ 明治二十九年}
%% \author{大日本聖書舘 \\ 横濱二十六番}
%% \date{令和元年皐月 \\ \href{https://bitbucket.org/kurodahisao/meiji-bible}{\LaTeX 組版生成}}
%% \setcounter{tocdepth}{1}
%% \renewcommand{\contentsname}{新約全書目録}
%% \makeatletter
%% \renewcommand{\tableofcontents}{% TOCはいつでもTwoColumn
%%   \if@twocolumn\@restonecolfalse
%%   \else\@restonecoltrue\twocolumn\fi
%%   \chapter*{\contentsname
%%     \@mkboth{\contentsname}{\contentsname}%
%%   }\@starttoc{toc}%
%%   \if@restonecol\onecolumn\fi
%% }
%% \renewenvironment{theindex}
%%   {\if@twocolumn\@restonecolfalse\else\@restonecoltrue\fi
%%    \columnseprule\z@ \columnsep 35\p@
%%    \twocolumn%
%%    \@mkboth{\indexname}{\indexname}%
%%    \thispagestyle{jpl@in}\parindent\z@
%%    \parskip\z@ \@plus .3\p@\relax
%%    \let\item\@idxitem}
%%   {\if@restonecol\onecolumn\else\clearpage\fi}
%% \makeatother
\begin{document}
\topmargin-0.6in
\newcommand{\LRkanJI}[2]{%
  \tbaselineshift=0.3zw  % 0pt
        \parbox<y>{1zw}{%
            \scalebox{0.5}[1]{%
                \makebox[2zw]{#1\hspace{-0.1zw}#2}}}}
\def\katano#1{\linebreak[3]\kern0.5zw\raisebox{0.5zw}[0pt][0pt]{\tiny#1}\kern0.1zw}
\makeatletter
\def\kochuuname{}
\def\arabicchuucite#1{\rensuji{\chuucite{#1}}}
\def\@kochuulabel#1{\rensuji{#1}}
\makeatother
%% \maketitle
%% \tableofcontents
\chapter*{\Large \Kanakt{新,約,全,書}{しん,やく,ぜん,しよ}\Kanakt{使,徒}{し,と}パウロ、ガラテヤ\kana{人}{びと}に\kana{贈}{おく}れる\kana{書}{ふみ}}
%% \addcontentsline{toc}{chapter}{加拉太書}

\markboth{章一第書太拉加}{加拉太書第一章}
\goodbreak
\noindent
\begin{wrapfigure}[2]{l}{0pt}
  \vbox to 2zw {\hbox to 3zw {\large \kintou{4zw}{\bf 第一章}}}
\end{wrapfigure}
\noindent\noindent\katano{一} \protect\nolinebreak  \kana{人}{ひと}よりに\kana{非}{あら}ず\kana{又}{また}\kana{人}{ひと}に\kana{由}{よら}ずイエス・キリストと\kana{彼}{かれ}を\kanakt{死}{し}より\kanakt{甦}{よみがへ}らしし\kana{父}{ちち}なる\kana{神}{かみ}に\kana{由}{より}て\kana{立}{たて}られたる\kanakt{使徒}{しと}パウロ
%% \refstepcounter{section}\addcontentsline{toc}{section}{第一章 {\tiny  人よりに非ず又人に由ずイエス・キリストと彼を}}
\katano{二} \protect\nolinebreak  \kana{及}{およ}び\kana{我}{われ}と\kana{偕}{とも}に\kana{在}{ある}すべての\kanakt[3]{兄弟}{きやうだい}ガラテヤの\kana{諸}{しよ}\kanakt{教會}{けうくわい}に\kana{書}{ふみ}を\kana{達}{おく}る
\katano{三} \protect\nolinebreak  なんぢら\kanakt{願}{ねがは}くは\kana{父}{ちち}なる\kana{神}{かみ}および\kana{我儕}{われら}の\kana{主}{しゆ}イエス・キリストより\kana{恩寵}{めぐみ}と\kana{平康}{やすき}を\kana{受}{うけ}よ
\katano{四} \protect\nolinebreak  キリストは\kana{我儕}{われら}の\kana{父}{ちち}なる\kana{神}{かみ}の\kana{旨}{むね}に\kanakt{循}{したが}ひ\kana{今}{いま}の\kanakt[3]{惡}{あしき}\kanakt{世}{よ}より\kana{我儕}{われら}を\kanakt[3]{救}{すくひ}\kana{出}{いだ}さんとて\kana{我儕}{われら}の\kana{罪}{つみ}の\kana{爲}{ため}に\kana{己}{おの}が\kanakt{身}{み}を\kana{捨}{すて}たまへり
\katano{五} \protect\nolinebreak  \kanakt{願}{ねがは}くは\kanakt[3]{榮}{さかえ}\kana{彼}{かれ}に\kanakt{歸}{き}して\kanakt{世々}{よよ}に\kana{至}{いた}れアメン


\indent
\katano{六} \protect\nolinebreak  キリストの\kanakt{恩}{めぐみ}をもて\kanakt{爾曹}{なんぢら}を\kana{召}{めし}たる\kana{者}{もの}を\kanakt{爾曹}{なんぢら}が\kanakt{如此}{かく}すみやかに\kana{離}{はな}れて\kana{異}{こと}なる\kanakt{福音}{ふくいん}に\kanakt{遷}{うつり}し\kana{事}{こと}を\kana{我}{われ}\kana{怪}{あや}しむ
\katano{七} \protect\nolinebreak  \kana{此}{これ}は\kanakt{福音}{ふくいん}に\kana{非}{あら}ず\kana{或}{ある}\kana{人}{ひと}ただ\kanakt{爾曹}{なんぢら}を\kana{擾}{みだ}しキリストの\kanakt{福音}{ふくいん}を\kana{更}{かへ}んとする\kana{也}{なり}
\katano{八} \protect\nolinebreak  \kana{我儕}{われら}にもせよ\kana{天}{てん}よりの\kana{使者}{つかひ}にもせよ\kana{若}{もし}われらが\kana{曾}{かつ}て\kanakt{爾曹}{なんぢら}に\kanakt{傳}{つたへ}し\kanakt{所}{ところ}に\kanakt{逆}{さから}ふ\kanakt{福音}{ふくいん}を\kanakt{爾曹}{なんぢら}に\kanakt{傳}{つたふ}る\kana{者}{もの}は\kanakt{詛}{のろは}るべし
\katano{九} \protect\nolinebreak  \kana{我儕}{われら}\kana{既}{すで}に\kana{言}{いひ}しが\kana{今}{いま}また\kana{我}{われ}その\kana{如}{ごと}く\kana{言}{いは}ん\kana{若}{もし}なんぢらが\kana{受}{うけ}し\kanakt{所}{ところ}に\kanakt{逆}{さから}ふ\kanakt{福音}{ふくいん}を\kanakt{爾曹}{なんぢら}に\kanakt{傳}{つたふ}る\kana{者}{もの}は\kanakt{詛}{のろは}るべし
\katano{一〇} \protect\nolinebreak  \kana{今}{いま}われ\kana{人}{ひと}の\kanakt{親}{したしみ}を\kanakt{得}{え}んことを\kanakt{要}{もとむ}るや\kana{神}{かみ}の\kanakt{親}{したしみ}を\kanakt{得}{え}んことを\kanakt{要}{もとむ}るや\kanakt{或}{あるひ}は\kana{人}{ひと}の\kanakt{心}{こころ}を\kanakt{得}{え}んことを\kana{求}{ねが}ふや\kana{若}{もし}われ\kana{人}{ひと}の\kanakt{心}{こころ}を\kanakt{得}{え}んことを\kana{求}{ねが}はばキリストの\kanakt{僕}{しもべ}に\kana{非}{あら}ざるべし


\indent
\katano{一一} \protect\nolinebreak  \kanakt{兄弟}{きやうだい}よ\kana{我}{われ}なんぢらに\kana{示}{しめ}す\kana{我}{われ}\kana{曾}{かつ}て\kanakt{爾曹}{なんぢら}に\kanakt{傳}{つたへ}し\kanakt{所}{ところ}の\kanakt{福音}{ふくいん}は\kana{人}{ひと}より\kana{出}{いづ}るに\kana{非}{あら}ず
\katano{一二} \protect\nolinebreak  \kana{蓋}{そは}われ\kana{之}{これ}を\kana{人}{ひと}より\kana{受}{うけ}ず\kana{亦}{また}\kanakt{教}{をしへ}られず\kana{惟}{ただ}イエス・キリストの\kana{默示}{しめし}に\kana{由}{より}て\kana{受}{うけ}たれば\kana{也}{なり}
\katano{一三} \protect\nolinebreak  わが\kana{曩}{さき}にユダヤ\kana{教}{けう}に\kana{在}{あり}しとき\kanakt{行}{おこな}ひたる\kana{事}{こと}を\kanakt{爾曹}{なんぢら}\kana{聞}{きけ}り\kanakt{即}{すなは}ち\kanakt{甚}{はなはだ}しく\kana{神}{かみ}の\kanakt{教會}{けうくわい}を\kana{窘}{せめ}かつ\kana{之}{これ}を\kana{殘賊}{ほろぼ}せり
\katano{一四} \protect\nolinebreak  \kana{我}{われ}また\kanakt{心}{こころ}を\kana{人}{ひと}よりも\kana{先祖}{せんぞ}\kana{等}{たち}の\kanakt{遺傳}{いひつたへ}に\kanakt{熱}{あつく}しユダヤ\kana{教}{けう}に\kana{在}{あり}ては\kanakt{我}{わ}が\kanakt{國人}{くにびと}のうち\kana{年}{とし}\kanakt{相若}{ひとしき}おほくの\kana{人}{もの}に\kana{超}{まさ}りたり
\katano{一五} \protect\nolinebreak  \kanakt{然}{しかれ}ども\kanakt{我}{わ}が\kana{母}{はは}の\kana{胎}{たい}を\kana{出}{いで}し\kana{時}{とき}より\kana{我}{われ}を\kana{簡}{えら}びおき\kanakt{恩}{めぐみ}をもて\kana{我}{われ}を\kana{召}{めし}\kana{給}{たま}ひし\kana{神}{かみ}
\katano{一六} \protect\nolinebreak  その\kanakt{子}{こ}を\kanakt{異邦人}{いはうじん}の\kana{中}{なか}に\kana{宣}{のべ}しめんがため\kanakt{心}{こころ}に\kana{善}{よし}として\kana{彼}{かれ}を\kana{我}{わが}\kanakt{心}{こころ}に\kana{示}{しめ}し\kana{給}{たま}へる\kana{其}{その}\kana{時}{とき}われ\kanakt{直}{ただち}に\kanakt{血肉}{けつにく}と\kana{謀}{はか}ることをせず
\katano{一七} \protect\nolinebreak  また\kana{我}{われ}より\kana{先}{さき}に\kanakt{使徒}{しと}と\kana{作}{なり}てエルサレムに\kana{在}{ある}ところの\kana{者}{もの}にも\kana{往}{ゆか}ずアラビヤに\kana{往}{ゆき}またダマスコに\kana{返}{かへ}れり
\katano{一八} \protect\nolinebreak  \kana{三}{さん}\kana{年}{ねん}を\kanakt{經}{へ}て\kana{後}{のち}ペテロを\kanakt{尋}{たづね}ん\kana{爲}{ため}にエルサレムに\kana{上}{のぼ}り\kana{十}{じふ}\kanakt{五}{ご}\kana{日}{にち}\kana{彼}{かれ}と\kana{偕}{とも}に\kana{居}{をり}しが
\katano{一九} \protect\nolinebreak  \kana{他}{ほか}の\kanakt{使徒}{しと}\kana{等}{たち}には\kana{主}{しゆ}の\kanakt[3]{兄弟}{きやうだい}ヤコブを\kanakt{除}{のぞき}ては\kana{誰}{たれ}にも\kana{遇}{あは}ざりき
\katano{二〇} \protect\nolinebreak  \kana{今}{いま}\kana{我}{わが}\kanakt{爾曹}{なんぢら}に\kana{書}{かき}\kana{遺}{おく}る\kanakt{所}{ところ}は\kana{神}{かみ}の\kana{前}{まへ}に\kanakt{謊}{いつは}れる\kana{言}{こと}なし
\katano{二一} \protect\nolinebreak  \kana{厥}{その}\kana{後}{のち}われスリヤ、キリキヤの\kanakt{地}{ち}に\kana{至}{いた}れり
\katano{二二} \protect\nolinebreak  \kanakt{然}{しかれ}どもユダヤに\kana{在}{ある}キリストの\kana{諸}{しよ}\kanakt{教會}{けうくわい}は\kanakt{我}{わ}が\kana{面}{かほ}を\kana{識}{しら}ざりき
\katano{二三} \protect\nolinebreak  \kana{只}{ただ}かれらは\kana{前}{さき}に\kanakt[3]{己}{おのれ}\kanakt{等}{ら}を\kana{窘}{せめ}しもの\kana{今}{いま}はその\kana{前}{さき}に\kanakt{滅}{ほろぼ}さんとしたる\kanakt{信仰}{しんかう}の\kana{道}{みち}を\kanakt{宣傳}{のべつた}ふと\kana{聞}{きき}
\katano{二四} \protect\nolinebreak  \kana{我}{わが}\kana{事}{こと}に\kana{因}{より}て\kana{神}{かみ}を\kanakt{崇}{あがむ}ることを\kanakt{爲}{せ}り



\markboth{章二第書太拉加}{加拉太書第二章}
\goodbreak
\noindent
\begin{wrapfigure}[2]{l}{0pt}
  \vbox to 2zw {\hbox to 3zw {\large \kintou{4zw}{\bf 第二章}}}
\end{wrapfigure}
\noindent\noindent\katano{一} \protect\nolinebreak  \kana{十}{じふ}\kanakt{四}{よ}\kana{年}{ねん}の\kana{後}{のち}われバルナバと\kana{偕}{とも}にテトスを\kanakt{伴}{ともな}ひて\kana{亦}{また}エルサレムに\kana{上}{のぼ}る
%% \refstepcounter{section}\addcontentsline{toc}{section}{第二章 {\tiny  十四年の後われバルナバと偕にテトスを伴ひて亦}}
\katano{二} \protect\nolinebreak  わが\kana{上}{のぼ}りしは\kana{默示}{しめし}に\kanakt{循}{したが}へるなり\kanakt{異邦人}{いはうじん}の\kana{中}{うち}に\kana{於}{おい}て\kanakt{我}{わ}が\kana{宣}{のべ}し\kanakt{所}{ところ}の\kanakt{福音}{ふくいん}を\kana{彼等}{かれら}に\kana{告}{つげ}また\kanakt{私}{ひそか}に\kanakt{名}{な}ある\kana{人}{ひと}\kana{等}{たち}に\kana{之}{これ}を\kana{告}{つげ}たり\kana{蓋}{そは}いま\kanakt{勤}{つとむ}る\kanakt{所}{ところ}また\kana{既}{すで}に\kana{勤}{つと}めし\kanakt{所}{ところ}の\kana{事}{こと}の\kanakt{徒然}{むなしく}ならざらんが\kana{爲}{ため}なり
\katano{三} \protect\nolinebreak  \kana{我}{われ}と\kana{偕}{とも}に\kana{在}{あり}しテトスはギリシヤ\kana{人}{びと}なるになほ\kana{強}{しひ}ては\kana{之}{これ}に\kanakt{割禮}{かつれい}を\kana{受}{うけ}させざりき
\katano{四} \protect\nolinebreak  そは\kanakt{私}{ひそか}に\kana{入}{いれ}られし\kanakt{僞}{いつはり}の\kanakt{兄弟}{きやうだい}あるに\kana{因}{より}てなり\kana{彼等}{かれら}の\kanakt{私}{ひそか}に\kana{入}{いり}しは\kana{我儕}{われら}がイエス・キリストに\kana{在}{あり}て\kanakt{有}{たもつ}ところの\kana{自由}{じゆう}を\kanakt{窺}{うかが}ひ\kana{我儕}{われら}を\kana{奴隸}{どれい}とせんが\kana{爲}{ため}なり
\katano{五} \protect\nolinebreak  われら\kanakt{一時}{ひととき}も\kana{之}{これ}に\kana{服}{ふく}する\kana{事}{こと}をせず\kanakt{此}{こ}は\kanakt{福音}{ふくいん}の\kanakt{眞}{まこと}つねに\kanakt{爾曹}{なんぢら}と\kana{偕}{とも}に\kana{在}{あら}んことを\kana{望}{のぞ}めば\kana{也}{なり}
\katano{六} \protect\nolinebreak  かの\kanakt{名}{な}ある\kana{者}{もの}より\kana{我}{われ}は\kana{受}{うけ}しことなし\kana{彼等}{かれら}は\kana{何}{いか}なる\kana{人}{ひと}なるにもせよ\kana{我}{われ}に\kana{於}{おい}て\kanakt{與}{あづか}る\kanakt{所}{ところ}なし\kana{神}{かみ}は\kanakt{偏}{かたよ}る\kana{者}{もの}に\kana{非}{あら}ず\kanakt{彼}{か}の\kanakt{名}{な}ある\kana{者}{もの}われに\kanakt{誨}{をしへ}を\kana{加}{そへ}しこと\kanakt{無}{な}きなり
\katano{七} \protect\nolinebreak  \kanakt{反}{かへつ}て\kana{彼等}{かれら}はペテロが\kanakt{割禮}{かつれい}を\kana{受}{うけ}たる\kana{者}{もの}に\kanakt{福音}{ふくいん}を\kanakt{傳}{つたふ}ることを\kanakt{託}{ゆだね}られし\kana{如}{ごと}く\kanakt{我}{わ}が\kanakt{割禮}{かつれい}を\kana{受}{うけ}ざる\kana{者}{もの}に\kanakt{福音}{ふくいん}を\kanakt{傳}{つたふ}ることを\kanakt{託}{ゆだね}られしを\kanakt{見}{み}
\katano{八} \protect\nolinebreak  （ペテロに\kana{能力}{ちから}を\kanakt{予}{あたへ}て\kanakt{割禮}{かつれい}を\kana{受}{うけ}たる\kana{人}{ひと}の\kanakt{使徒}{しと}と\kana{爲}{なし}し\kana{者}{もの}また\kana{我}{われ}にも\kana{能力}{ちから}を\kanakt{予}{あたへ}て\kanakt{異邦人}{いはうじん}の\kanakt{使徒}{しと}と\kana{爲}{なせ}り）
\katano{九} \protect\nolinebreak  また\kana{我}{われ}に\kanakt{賜}{たまひ}し\kanakt{所}{ところ}の\kanakt{恩}{めぐみ}を\kana{知}{しり}しにより\kanakt{柱}{はしら}と\kanakt{意}{おもは}るるヤコブ、ケパ、ヨハネも\kana{其}{その}\kanakt{右手}{みぎのて}を\kanakt{予}{あたへ}て\kana{我}{われ}とバルナバに\kanakt{交}{まじはり}を\kana{結}{むす}べり\kana{是}{これ}われらは\kanakt{異邦人}{いはうじん}に\kana{至}{いた}り\kana{彼等}{かれら}は\kanakt{割禮}{かつれい}を\kana{受}{うけ}たる\kana{者}{もの}に\kana{至}{いた}らん\kana{爲}{ため}なり
\katano{一〇} \protect\nolinebreak  \kana{彼等}{かれら}の\kana{惟}{ただ}ねがふ\kanakt{所}{ところ}は\kana{我儕}{われら}が\kanakt{貧民}{まづしきもの}を\kanakt{眷顧}{かへりみ}んことなり\kana{我儕}{われら}も\kana{亦}{また}この\kana{事}{こと}は\kana{素}{もと}より\kana{進}{すす}んで\kana{爲}{なさ}んとする\kanakt{所}{ところ}なり
\katano{一一} \protect\nolinebreak  ペテロ、アンテオケに\kana{至}{いた}りしとき\kana{彼}{かれ}に\kana{責}{せむ}べき\kanakt{所}{ところ}ありしに\kana{因}{より}われ\kanakt{當面}{まのあたり}これを\kanakt{詰}{いまし}めたり
\katano{一二} \protect\nolinebreak  \kana{蓋}{そは}ヤコブより\kana{來}{きた}る\kana{者}{もの}の\kana{未}{いま}だ\kana{至}{いた}らざる\kana{前}{さき}にはペテロ\kanakt{異邦人}{いはうじん}と\kana{同}{とも}に\kanakt{食}{しよく}したれども\kana{彼等}{かれら}が\kana{至}{いた}るに\kanakt{及}{および}て\kanakt{割禮}{かつれい}を\kana{受}{うけ}たる\kana{者}{もの}を\kana{懼}{おそ}れ\kanakt{退}{しりぞ}きて\kanakt{異邦人}{いはうじん}と\kanakt{別}{わかれ}たれば\kana{也}{なり}
\katano{一三} \protect\nolinebreak  その\kana{餘}{ほか}のユダヤ\kana{人}{びと}も\kana{彼}{かれ}と\kana{偕}{とも}に\kanakt{僞}{いつはり}の\kanakt{行}{おこなひ}をなしバルナバも\kana{遂}{つひ}に\kana{其}{その}\kanakt{僞}{いつはり}の\kanakt{行}{おこなひ}に\kanakt{誘}{さそは}れたり
\katano{一四} \protect\nolinebreak  \kana{我}{われ}かれらが\kanakt{福音}{ふくいん}の\kanakt{眞}{まこと}に\kanakt{遵}{したが}ひ\kanakt{正}{ただし}く\kanakt{行}{おこな}はざるを\kanakt{見}{み}すべての\kana{人}{ひと}の\kana{前}{まへ}に\kana{於}{おい}てペテロに\kana{曰}{いひ}けるは\kanakt[3]{爾}{なんぢ}ユダヤ\kana{人}{びと}にして\kanakt{若}{も}し\kanakt{異邦人}{いはうじん}の\kana{如}{ごと}く\kanakt{行}{おこな}ひユダヤ\kana{人}{びと}の\kana{如}{ごと}く\kanakt{行}{おこな}はざるときは\kana{何}{なん}ぞ\kanakt{異邦人}{いはうじん}を\kana{強}{しひ}てユダヤ\kana{人}{びと}の\kanakt{例}{ならはし}に\kanakt{遵}{したが}はせんと\kana{爲}{する}や
\katano{一五} \protect\nolinebreak  \kana{夫}{それ}われらは\kanakt{生來}{うまれながら}のユダヤ\kana{人}{びと}にして\kana{異邦}{いはう}より\kana{出}{いで}たる\kanakt{罪人}{つみびと}に\kana{非}{あら}ず
\katano{一六} \protect\nolinebreak  \kana{然}{され}ど\kana{人}{ひと}の\kanakt{義}{ぎ}とせらるるは\kana{律法}{おきて}の\kanakt{行}{おこなひ}に\kana{由}{よる}に\kana{非}{あら}ず\kana{惟}{ただ}イエス・キリストを\kana{信}{しん}ずるに\kana{由}{よる}なるを\kana{知}{しる}この\kana{故}{ゆゑ}に\kana{我儕}{われら}も\kana{律法}{おきて}の\kanakt{行}{おこなひ}に\kana{由}{よら}ずキリストを\kana{信}{しん}ずるに\kana{由}{より}て\kanakt{義}{ぎ}とせられんが\kana{爲}{ため}にイエス・キリストを\kana{信}{しん}ず\kana{蓋}{そは}\kana{律法}{おきて}の\kanakt{行}{おこなひ}に\kana{由}{より}て\kanakt{義}{ぎ}とせらるる\kana{者}{もの}なければ\kana{也}{なり}
\katano{一七} \protect\nolinebreak  \kana{若}{もし}われらキリストに\kana{在}{あり}て\kanakt{義}{ぎ}とせられん\kana{事}{こと}を\kana{欲}{ねが}ひなほ\kanakt{罪人}{つみびと}ならばキリストは\kana{罪}{つみ}の\kanakt{僕}{しもべ}なるか\kanakt{决}{きはめ}て\kana{然}{しか}らず
\katano{一八} \protect\nolinebreak  \kanakt{我}{わ}が\kana{先}{さき}に\kanakt{毀}{こぼち}し\kana{此}{この}ものを\kana{今}{いま}もし\kanakt{復}{ふたた}び\kana{建}{たて}なば\kanakt{自}{みづか}ら\kana{其}{その}\kanakt{罪人}{つみびと}なるを\kanakt{顯}{あらは}すなり
\katano{一九} \protect\nolinebreak  われ\kana{律法}{おきて}に\kana{由}{より}\kana{律法}{おきて}に\kana{向}{むか}ひて\kana{死}{しね}り\kana{是}{これ}\kana{神}{かみ}に\kanakt{向}{むかひ}て\kana{生}{いき}ん\kana{爲}{ため}なり
\katano{二〇} \protect\nolinebreak  \kana{我}{われ}キリストと\kana{偕}{とも}に\kana{十字架}{じふじか}に\kana{釘}{つけ}られたり\kanakt{既}{もはや}われ\kana{生}{いけ}るに\kana{非}{あら}ずキリスト\kana{我}{われ}に\kana{在}{あり}て\kana{生}{いけ}るなり\kana{今}{いま}われ\kanakt{肉體}{にくたい}に\kana{在}{あり}て\kana{生}{いけ}るは\kana{我}{われ}を\kana{愛}{あい}して\kanakt{我}{わ}が\kana{爲}{ため}に\kanakt{己}{おのれ}を\kana{捨}{すて}し\kana{者}{もの}すなはち\kana{神}{かみ}の\kanakt{子}{こ}を\kana{信}{しん}ずるに\kana{由}{より}て\kana{生}{いけ}るなり
\katano{二一} \protect\nolinebreak  \kana{我}{われ}は\kana{神}{かみ}の\kanakt{恩}{めぐみ}を\kanakt{徒然}{むなしく}せず\kanakt{若}{も}し\kanakt{義}{ぎ}とせらるること\kana{律法}{おきて}に\kana{由}{よら}ばキリストの\kanakt{死}{し}は\kanakt{徒然}{いたづら}なる\kana{業}{わざ}なり


\markboth{章三第書太拉加}{加拉太書第三章}
\goodbreak
\noindent
\begin{wrapfigure}[2]{l}{0pt}
  \vbox to 2zw {\hbox to 3zw {\large \kintou{4zw}{\bf 第三章}}}
\end{wrapfigure}
\noindent\noindent\katano{一} \protect\nolinebreak  \kanakt{愚}{おろか}なる\kana{哉}{かな}すでにイエス・キリストの\kana{十字架}{じふじか}に\kana{釘}{つけ}られし\kana{事}{こと}を\kanakt{明}{あきら}かに\kana{其}{その}\kanakt{目前}{めのまへ}に\kanakt{著}{あらは}されたるガラテヤ\kana{人}{びと}よ\kanakt{誰}{た}が\kanakt{爾曹}{なんぢら}を\kanakt{誑}{たぶら}かしし\kanakt{乎}{や}
%% \refstepcounter{section}\addcontentsline{toc}{section}{第三章 {\tiny  愚なる哉すでにイエス・キリストの十字架に釘ら}}
\katano{二} \protect\nolinebreak  \kana{我}{われ}ただ\kana{此}{この}\kana{事}{こと}を\kanakt{爾曹}{なんぢら}より\kana{聞}{きか}んとす\kanakt{爾曹}{なんぢら}が\kanakt{靈}{みたま}を\kana{受}{うけ}しは\kana{律法}{おきて}を\kanakt{行}{おこな}ふに\kana{由}{よる}か\kana{將}{はた}ききて\kana{信}{しん}ぜしに\kana{由}{よる}か
\katano{三} \protect\nolinebreak  \kanakt{爾曹}{なんぢら}かく\kanakt{愚}{おろか}なる\kanakt{乎}{か}なんぢら\kanakt{靈}{みたま}に\kana{因}{より}て\kanakt{始}{はじま}り\kana{今}{いま}\kana{肉}{にく}に\kana{因}{より}て\kanakt{全}{まつた}うせらるる\kanakt{乎}{や}
\katano{四} \protect\nolinebreak  なんぢら\kanakt{如此}{かく}おほくの\kanakt{苦}{くるしみ}を\kanakt{徒然}{いたづら}に\kana{受}{うけ}しや\kana{實}{じつ}に\kanakt{徒然}{いたづら}には\kana{有}{ある}まじ
\katano{五} \protect\nolinebreak  それ\kanakt{爾曹}{なんぢら}に\kanakt{靈}{みたま}を\kana{予}{あた}へかつ\kanakt{奇跡}{ふしぎなるわざ}を\kanakt{行}{おこな}はしめ\kana{給}{たま}ふ\kana{者}{もの}の\kanakt{如此}{かく}なすは\kanakt{爾曹}{なんぢら}が\kana{律法}{おきて}を\kanakt{行}{おこな}ふに\kana{由}{より}てなる\kanakt{乎}{か}\kana{又}{また}は\kana{聞}{きき}て\kana{信}{しん}ぜしに\kana{由}{より}てなる\kanakt{乎}{か}
\katano{六} \protect\nolinebreak  \kanakt{即}{すなは}ちアブラハム\kana{神}{かみ}を\kana{信}{しん}じ\kana{其}{その}\kanakt{信仰}{しんかう}を\kanakt{義}{ぎ}と\kana{爲}{せら}れたるが\kana{如}{ごと}し
\katano{七} \protect\nolinebreak  \kana{是}{この}\kana{故}{ゆゑ}に\kanakt{信仰}{しんかう}による\kana{者}{もの}は\kana{是}{これ}アブラハムの\kanakt{子}{こ}なりと\kanakt{爾曹}{なんぢら}\kana{知}{しる}べし
\katano{八} \protect\nolinebreak  かつ\kanakt{聖書}{せいしよ}すでに\kanakt{信仰}{しんかう}に\kana{由}{より}て\kana{神}{かみ}の\kanakt{異邦人}{いはうじん}を\kanakt{義}{ぎ}と\kanakt{爲}{し}\kana{給}{たま}ふことを\kanakt{預}{あらか}じめ\kanakt{曉}{さとり}まづ\kanakt{福音}{ふくいん}をアブラハムに\kanakt{傳}{つたへ}て\kanakt{諸國}{すべてのくに}の\kana{民}{たみ}は\kanakt{爾}{なんぢ}に\kana{由}{より}て\kanakt{福}{さいはひ}を\kanakt{獲}{え}んと\kana{云}{いへ}り
\katano{九} \protect\nolinebreak  \kana{是}{この}\kana{故}{ゆゑ}に\kanakt{信仰}{しんかう}に\kana{由}{よる}ものは\kanakt{信仰}{しんかう}ありしアブラハムと\kana{偕}{とも}に\kanakt{福}{さいはひ}を\kana{受}{うく}
\katano{一〇} \protect\nolinebreak  \kanakt{凡}{おほよ}そ\kana{律法}{おきて}の\kanakt{行}{おこなひ}に\kana{由}{よる}ものは\kanakt{詛}{のろは}るべし\kana{蓋}{そは}\kana{律法}{おきて}の\kana{書}{ふみ}に\kana{載}{のせ}たる\kanakt{凡}{すべて}の\kana{事}{こと}を\kana{恆}{つね}に\kanakt{行}{おこな}はざる\kana{者}{もの}は\kanakt{詛}{のろは}ると\kana{録}{しる}されたれば\kana{也}{なり}
\katano{一一} \protect\nolinebreak  かつ\kana{義人}{ぎじん}は\kanakt{信仰}{しんかう}に\kana{由}{より}て\kana{生}{いく}べしと\kana{有}{あれ}ば\kana{律法}{おきて}に\kana{由}{より}て\kana{神}{かみ}の\kana{前}{まへ}に\kanakt{義}{ぎ}とせらるる\kana{者}{もの}なきことは\kanakt{明}{あきら}かなり
\katano{一二} \protect\nolinebreak  それ\kana{律法}{おきて}は\kanakt{信仰}{しんかう}に\kana{由}{よら}ず\kanakt{即}{すなは}ち\kana{曰}{いふ}これを\kanakt{行}{おこな}ふ\kana{者}{もの}は\kana{之}{これ}に\kana{由}{より}て\kana{生}{いく}べしと
\katano{一三} \protect\nolinebreak  キリスト\kana{既}{すで}に\kana{我儕}{われら}の\kana{爲}{ため}に\kana{詛}{のろ}はるる\kana{者}{もの}となりて\kana{我儕}{われら}を\kanakt{贖}{あがな}ひ\kana{律法}{おきて}の\kanakt{詛}{のろひ}より\kanakt{脱}{はなれ}しめ\kana{給}{たま}へり\kana{蓋}{そは}すべて\kanakt{木}{き}に\kana{懸}{かか}る\kana{者}{もの}は\kanakt{詛}{のろは}れし\kana{者}{もの}なりと\kana{録}{しる}されたれば\kana{也}{なり}
\katano{一四} \protect\nolinebreak  \kana{是}{これ}アブラハムに\kanakt{約束}{やくそく}し\kana{給}{たま}ひし\kana{恩惠}{めぐみ}イエス・キリストに\kana{由}{より}て\kanakt{異邦人}{いはうじん}にまで\kana{及}{およ}び\kana{我儕}{われら}にも\kanakt{信仰}{しんかう}に\kana{由}{より}て\kanakt{約束}{やくそく}の\kanakt{靈}{みたま}を\kana{受}{うけ}しめん\kana{爲}{ため}なり
\katano{一五} \protect\nolinebreak  \kanakt{兄弟}{きやうだい}よ\kana{我}{われ}いま\kana{人}{ひと}の\kana{事}{こと}に\kana{由}{より}て\kana{曰}{いは}ん\kana{人}{ひと}の\kanakt{契約}{けいやく}だに\kana{既}{すで}に\kanakt{定}{さだむ}れば\kana{之}{これ}を\kana{廢}{けし}また\kana{加}{くは}ふることなし
\katano{一六} \protect\nolinebreak  それ\kanakt{約束}{やくそく}はアブラハムと\kana{其}{その}\kana{裔}{すゑ}とに\kana{立}{たて}\kana{給}{たま}ひし\kana{者}{もの}にして\kanakt{多}{おほく}の\kana{人}{ひと}を\kana{指}{さし}て\kanakt{裔々}{すゑずゑ}と\kana{言}{いへ}るに\kana{非}{あら}ず\kana{惟}{ただ}\kana{一人}{ひとり}を\kana{指}{さし}て\kanakt{爾}{なんぢ}の\kana{裔}{すゑ}と\kana{言}{いへ}る\kana{也}{なり}これ\kanakt{即}{すなは}ちキリストなり
\katano{一七} \protect\nolinebreak  \kana{我}{われ}これを\kana{言}{いは}ん\kana{神}{かみ}の\kanakt{預}{あらか}じめ\kanakt[3]{定}{さだめ}\kana{給}{たま}ひし\kanakt{契約}{けいやく}は\kanakt{四}{し}\kanakt[3]{百}{ひやく}\kana{三}{さん}\kana{十}{じふ}\kana{年}{ねん}のちの\kana{律法}{おきて}これを\kana{棄}{すて}その\kanakt{約束}{やくそく}の\kanakt{言}{ことば}を\kanakt{徒然}{むなしく}することをせざる\kana{也}{なり}
\katano{一八} \protect\nolinebreak  \kana{嗣業}{よつぎ}と\kana{爲}{なる}こと\kanakt{若}{も}し\kana{律法}{おきて}に\kana{由}{よら}ば\kanakt{約束}{やくそく}には\kana{由}{よら}ざるべし\kana{然}{され}ど\kana{神}{かみ}は\kanakt{約束}{やくそく}に\kana{由}{より}て\kana{之}{これ}をアブラハムに\kana{賜}{たま}へり


\indent
\katano{一九} \protect\nolinebreak  \kana{然}{しか}らば\kana{律法}{おきて}の\kana{用}{よう}は\kana{何}{なに}ぞや\kanakt{此}{こ}は\kanakt{約束}{やくそく}を\kana{受}{うく}べき\kana{裔}{すゑ}の\kana{來}{きた}るまで\kana{罪}{つみ}の\kana{爲}{ため}に\kana{加}{くは}へし\kana{者}{もの}にて\kanakt[3]{天使}{てんのつかひ}\kana{等}{たち}により\kanakt{中保}{なかだち}の\kanakt{手}{て}に\kana{備}{そな}へ\kana{給}{たま}ひし\kana{也}{なり}
\katano{二〇} \protect\nolinebreak  それ\kanakt{中保}{なかだち}は\kana{一人}{ひとり}に\kana{屬}{つけ}る\kana{者}{もの}に\kana{非}{あら}ず\kana{神}{かみ}は\kanakt{即}{すなは}ち\kana{一人}{ひとり}なり
\katano{二一} \protect\nolinebreak  \kana{然}{しか}らば\kana{律法}{おきて}は\kana{神}{かみ}の\kanakt{約束}{やくそく}に\kana{反}{もと}るや\kanakt{决}{きはめ}て\kanakt{非}{しから}ず\kanakt{若}{も}し\kana{人}{ひと}を\kana{生}{いか}しうる\kana{律法}{おきて}を\kanakt{賜}{たまは}りしならば\kanakt{義}{ぎ}とせらるるは\kanakt{必}{かなら}ず\kana{律法}{おきて}に\kana{由}{よる}べし
\katano{二二} \protect\nolinebreak  \kanakt{然}{しかれ}ども\kanakt{聖書}{せいしよ}は\kanakt{反}{かへつ}て\kanakt{萬人}{すべてのひと}を\kana{罪}{つみ}の\kana{下}{した}に\kanakt{拘幽}{とぢこめ}たり\kanakt{此}{こ}はイエス・キリストを\kana{信}{しん}ずるに\kana{由}{よれ}る\kanakt{約束}{やくそく}のものを\kanakt{諸}{もろもろ}の\kanakt{信者}{しんじや}に\kanakt{賜}{たまは}らんが\kana{爲}{ため}なり
\katano{二三} \protect\nolinebreak  \kanakt{信仰}{しんかう}の\kana{來}{きた}らざる\kana{先}{さき}には\kana{我儕}{われら}\kana{律法}{おきて}の\kana{下}{した}に\kanakt{拘幽}{とぢこめ}られ\kana{且}{かつ}\kanakt{守}{まもら}れて\kana{其}{その}\kanakt{顯}{あらは}れんとする\kanakt{信仰}{しんかう}を\kana{俟}{まて}り
\katano{二四} \protect\nolinebreak  かく\kana{律法}{おきて}は\kana{我儕}{われら}をして\kanakt{信仰}{しんかう}に\kana{由}{より}て\kanakt{義}{ぎ}とせらるる\kana{事}{こと}を\kanakt{得}{え}しめんが\kana{爲}{ため}に\kana{我儕}{われら}をキリストに\kanakt{導}{みちび}く\kanakt{師傳}{しふ}となれり
\katano{二五} \protect\nolinebreak  \kanakt{然}{しかれ}ども\kana{今}{いま}\kanakt{信仰}{しんかう}すでに\kanakt{來}{きたり}たれば\kana{我儕}{われら}もはや\kanakt{師傳}{しふ}の\kana{下}{した}にあらず
\katano{二六} \protect\nolinebreak  \kanakt{爾曹}{なんぢら}は\kana{皆}{みな}キリスト・イエスを\kana{信}{しん}ずるに\kana{由}{より}て\kana{神}{かみ}の\kanakt{子}{こ}となれり
\katano{二七} \protect\nolinebreak  そは\kanakt{凡}{おほよ}そバプテスマを\kana{受}{うけ}てキリストに\kana{入}{いれ}る\kanakt{爾曹}{なんぢら}はキリストを\kanakt{衣}{き}たる\kana{者}{もの}なれば\kana{也}{なり}
\katano{二八} \protect\nolinebreak  \kana{斯}{かか}る\kana{者}{もの}の\kana{中}{うち}にはユダヤ\kana{人}{びと}またギリシヤ\kana{人}{びと}あるひは\kana{奴隸}{どれい}あるひは\kana{自主}{じしゆ}あるひは\kanakt{男}{をとこ}あるひは\kanakt{女}{をんな}の\kanakt{分}{わかち}なし\kana{蓋}{そは}なんぢら\kana{皆}{みな}キリスト・イエスに\kana{在}{あり}て\kanakt{一}{ひとつ}なれば\kana{也}{なり}
\katano{二九} \protect\nolinebreak  \kana{若}{もし}なんぢらキリストに\kana{屬}{ぞく}するものならば\kanakt{爾曹}{なんぢら}はアブラハムの\kana{裔}{すゑ}すなはち\kanakt{約束}{やくそく}に\kanakt{循}{したが}ひて\kana{嗣子}{よつぎ}たる\kana{也}{なり}



\markboth{章四第書太拉加}{加拉太書第四章}
\goodbreak
\noindent
\begin{wrapfigure}[2]{l}{0pt}
  \vbox to 2zw {\hbox to 3zw {\large \kintou{4zw}{\bf 第四章}}}
\end{wrapfigure}
\noindent\noindent\katano{一} \protect\nolinebreak  \kana{我}{われ}いはん\kana{嗣子}{よつぎ}たる\kana{者}{もの}は\kanakt{全業}{ぜんげふ}の\kana{主}{しゆ}なれども\kana{其}{その}\kana{童蒙}{わらべ}の\kana{時}{とき}は\kanakt{僕}{しもべ}に\kana{異}{こと}なることなし
%% \refstepcounter{section}\addcontentsline{toc}{section}{第四章 {\tiny  我いはん嗣子たる者は全業の主なれども其童蒙の}}
\katano{二} \protect\nolinebreak  \kana{父}{ちち}の\kanakt{定}{さだめ}し\kana{期}{とき}いたるまで\kana{受託者}{うしろみ}および\kanakt{家宰}{いへづかさ}の\kana{下}{した}に\kana{在}{あり}
\katano{三} \protect\nolinebreak  \kana{此}{かく}の\kana{如}{ごと}く\kana{我儕}{われら}も\kana{童蒙}{わらべ}の\kana{時}{うち}は\kana{此}{この}\kanakt{世}{よ}の\kanakt{小學}{せうがく}の\kana{下}{した}に\kana{在}{あり}て\kanakt{僕}{しもべ}たる\kana{也}{なり}
\katano{四} \protect\nolinebreak  \kanakt{然}{しかれ}ども\kana{期}{とき}すでに\kana{至}{いた}るに\kana{及}{およ}びて\kana{神}{かみ}その\kanakt{子}{こ}を\kanakt{遣}{つかは}し\kana{給}{たま}へり\kana{彼}{かれ}は\kanakt{女}{をんな}よりうまれ\kana{律法}{おきて}の\kana{下}{した}に\kana{生}{うま}れたり
\katano{五} \protect\nolinebreak  これ\kana{律法}{おきて}の\kana{下}{した}にある\kana{者}{もの}を\kanakt{贖}{あがな}ひ\kana{我儕}{われら}をして\kanakt{子}{こ}たることを\kanakt{得}{え}しめんが\kana{爲}{ため}なり
\katano{六} \protect\nolinebreak  \kana{且}{かつ}なんぢら\kana{既}{すで}に\kanakt{子}{こ}たることを\kanakt{得}{え}しが\kana{故}{ゆゑ}に\kana{神}{かみ}その\kanakt{子}{こ}の\kanakt{靈}{みたま}を\kanakt{爾曹}{なんぢら}の\kanakt{心}{こころ}に\kana{遣}{おく}りアバ\kana{父}{ちち}と\kana{呼}{よば}しむ
\katano{七} \protect\nolinebreak  \kana{是}{この}\kana{故}{ゆゑ}に\kanakt{爾}{なんぢ}はもはや\kanakt{僕}{しもべ}に\kana{非}{あら}ず\kanakt{子}{こ}なり\kana{既}{すで}に\kanakt{子}{こ}ならば\kana{亦}{また}\kana{神}{かみ}に\kana{由}{より}て\kana{嗣子}{よつぎ}たる\kana{也}{なり}
\katano{八} \protect\nolinebreak  \kanakt{然}{しかれ}ども\kanakt{爾曹}{なんぢら}\kana{神}{かみ}を\kana{識}{しら}ざりし\kana{時}{とき}は\kana{其}{その}\kana{實}{じつ}\kana{神}{かみ}に\kana{非}{あら}ざる\kana{者}{もの}に\kanakt{事}{つかへ}て\kanakt{僕}{しもべ}たりき
\katano{九} \protect\nolinebreak  \kanakt{然}{しかれ}ども\kanakt{爾曹}{なんぢら}いま\kana{神}{かみ}を\kana{識}{しれ}り\kanakt{反}{かへつ}て\kana{神}{かみ}に\kana{識}{しら}れたりと\kana{謂}{いふ}べし\kana{何}{なん}ぞ\kana{弱}{よわ}く\kanakt{賤}{いやし}き\kanakt{小學}{せうがく}に\kana{返}{かへ}りて\kanakt{復}{ふたた}び\kana{之}{これ}が\kanakt{僕}{しもべ}たらんことを\kana{欲}{ねが}ふや
\katano{一〇} \protect\nolinebreak  なんぢら\kanakt{愼}{つつしみ}て\kana{月}{つき}と\kanakt{日}{ひ}と\kana{節}{せつ}と\kana{歳}{とし}とを\kana{守}{まも}る
\katano{一一} \protect\nolinebreak  われ\kanakt{爾曹}{なんぢら}に\kana{就}{つい}て\kanakt{危}{あやぶ}む\kanakt{恐}{おそら}くは\kanakt{爾曹}{なんぢら}の\kana{爲}{ため}に\kanakt{我}{わ}が\kana{勤}{つと}めし\kana{事}{こと}の\kanakt{徒然}{むなしく}ならんことを


\indent
\katano{一二} \protect\nolinebreak  \kanakt{兄弟}{きやうだい}よ\kanakt{願}{ねがは}くは\kanakt{爾曹}{なんぢら}わが\kana{如}{ごと}くなれ\kana{蓋}{そは}われ\kanakt{爾曹}{なんぢら}の\kanakt{如}{ごとく}なりたれば\kana{也}{なり}なんぢらは\kana{我}{われ}を\kana{害}{がい}せしことなし
\katano{一三} \protect\nolinebreak  \kana{曩}{さき}に\kana{我}{われ}よわき\kanakt{身}{み}にして\kanakt{爾曹}{なんぢら}に\kanakt{福音}{ふくいん}を\kanakt{傳}{つたへ}しことは\kanakt{爾曹}{なんぢら}の\kana{知}{しる}ところ\kana{也}{なり}
\katano{一四} \protect\nolinebreak  \kanakt{爾曹}{なんぢら}を\kanakt{試}{こころむ}る\kana{者}{もの}の\kanakt{我}{わ}が\kanakt{身}{み}に\kana{在}{あり}しを\kanakt{爾曹}{なんぢら}は\kanakt{卑}{いやし}めず\kana{亦}{また}\kanakt{厭}{きらは}ず\kanakt{反}{かへつ}て\kanakt{天使}{てんのつかひ}の\kana{如}{ごと}くキリスト・イエスの\kana{如}{ごと}くに\kana{我}{われ}を\kanakt{待}{あつか}ひたり
\katano{一五} \protect\nolinebreak  \kanakt{爾曹}{なんぢら}その\kana{時}{とき}の\kanakt{福}{さいはひ}は\kana{如何}{いかに}ありし\kanakt{乎}{や}われ\kanakt{爾曹}{なんぢら}に\kanakt{證}{あかし}す\kanakt{若}{も}し\kana{爲}{なし}\kanakt{得}{う}べくば\kanakt{爾曹}{なんぢら}みづからの\kanakt{目}{め}を\kana{抉}{とり}て\kana{我}{われ}に\kanakt{予}{あたへ}んとまで\kanakt{願}{ねがひ}たり
\katano{一六} \protect\nolinebreak  \kana{然}{しか}るに\kana{我}{われ}なんぢらに\kana{眞理}{まこと}を\kana{語}{かた}りしに\kana{縁}{より}て\kana{我}{われ}なんぢらの\kana{仇}{あだ}となりし\kanakt{乎}{や}
\katano{一七} \protect\nolinebreak  \kana{彼等}{かれら}が\kanakt{爾曹}{なんぢら}に\kanakt{熱心}{ねつしん}なるは\kanakt{善意}{よきこころ}に\kana{非}{あら}ず\kanakt{爾曹}{なんぢら}を\kanakt{己}{おのれ}に\kanakt{熱心}{ねつしん}ならしめんとて\kanakt{爾曹}{なんぢら}を\kanakt{離}{はなれ}しめんとする\kana{也}{なり}
\katano{一八} \protect\nolinebreak  \kana{然}{され}ど\kana{唯}{ただ}わが\kanakt{爾曹}{なんぢら}と\kana{偕}{とも}なる\kana{時}{とき}のみならず\kanakt{善事}{よきこと}の\kana{爲}{ため}に\kana{常}{つね}に\kanakt{熱心}{ねつしん}なるは\kanakt{宜}{よろし}きなり
\katano{一九} \protect\nolinebreak  \kanakt{我}{わ}が\kanakt{小子}{をさなご}よ\kana{我}{われ}なんぢらの\kanakt{心}{こころ}にキリストの\kanakt[3]{状}{かたち}\kana{成}{なる}までは\kanakt{復}{ふたた}び\kanakt{爾曹}{なんぢら}の\kana{爲}{ため}に\kana{産}{うみ}の\kanakt{劬勞}{くるしみ}をなす
\katano{二〇} \protect\nolinebreak  \kana{我}{われ}いま\kanakt{爾曹}{なんぢら}と\kana{偕}{とも}に\kana{在}{あり}て\kanakt{口氣}{こゑ}を\kanakt{改}{あらた}めんことを\kana{欲}{ねが}ふ\kana{蓋}{そは}われ\kanakt{爾曹}{なんぢら}に\kana{就}{つい}て\kanakt{惑}{まどへ}ばなり


\indent
\katano{二一} \protect\nolinebreak  なんぢら\kana{律法}{おきて}の\kana{下}{した}に\kana{在}{あら}んことを\kana{欲}{ねが}ふ\kana{者}{もの}よ\kana{我}{われ}に\kana{語}{かた}れ\kanakt{爾曹}{なんぢら}\kana{律法}{おきて}を\kana{聞}{きか}ざる\kanakt{乎}{か}
\katano{二二} \protect\nolinebreak  \kana{録}{しる}してアブラハムに\kana{二人}{ふたり}の\kanakt{子}{こ}あり\kana{一人}{ひとり}は\kanakt{婢}{しもめ}より\kana{一人}{ひとり}は\kana{自主}{じしゆ}の\kanakt{婦}{をんな}より\kanakt{生}{うまれ}たりと\kana{有}{あり}
\katano{二三} \protect\nolinebreak  その\kanakt{婢}{しもめ}より\kana{生}{うま}れし\kana{者}{もの}は\kana{肉}{にく}に\kanakt{循}{したが}ひ\kana{自主}{じしゆ}の\kanakt{婦}{をんな}より\kana{生}{うま}れし\kana{者}{もの}は\kanakt{約束}{やくそく}に\kana{因}{より}て\kana{生}{うま}れたる\kana{也}{なり}
\katano{二四} \protect\nolinebreak  この\kana{言}{こと}は\kana{譬喩}{たとへ}にして\kanakt{即}{すなは}ち\kana{此}{この}\kanakt{婦}{をんな}は\kanakt{二}{ふたつ}の\kanakt{契約}{けいやく}に\kanakt{比}{なぞら}ふべし\kanakt{一}{ひとつ}はシナイ\kana{山}{やま}より\kana{出}{いで}て\kanakt{子}{こ}を\kana{奴隸}{どれい}に\kana{生}{うむ}これ\kanakt{即}{すなは}ちハガルなり
\katano{二五} \protect\nolinebreak  \kana{此}{この}ハガルはアラビヤのシナイ\kana{山}{やま}\kana{今}{いま}のエルサレムに\kanakt{當}{あたれ}るなり\kana{蓋}{そは}かれ\kana{其}{その}\kana{諸子}{こども}と\kana{偕}{とも}に\kana{奴隸}{どれい}たれば\kana{也}{なり}
\katano{二六} \protect\nolinebreak  \kana{然}{され}ど\kana{上}{うへ}に\kana{在}{ある}ところのエルサレムは\kana{自主}{じしゆ}にして\kana{是}{これ}われらの\kana{母}{はは}なり
\katano{二七} \protect\nolinebreak  そは\kana{録}{しる}して\kanakt{姙}{はらま}ず\kana{生}{うま}ざる\kana{者}{もの}よ\kanakt{喜}{よろこ}べ\kana{産}{うみ}の\kanakt{劬勞}{くるしみ}せざる\kana{者}{もの}よ\kana{聲}{こゑ}を\kana{揚}{あげ}て\kanakt{呼}{よばは}れ\kanakt[3]{寡}{ひとり}\kana{居}{すめ}る\kana{者}{もの}の\kanakt{子}{こ}は\kanakt{夫}{をつと}ある\kana{者}{もの}の\kanakt{子}{こ}よりも\kanakt{多}{おほき}が\kana{故}{ゆゑ}なりと\kana{有}{あれ}ばなり
\katano{二八} \protect\nolinebreak  \kanakt{兄弟}{きやうだい}よ\kana{我儕}{われら}はイサクの\kana{如}{ごと}く\kanakt{約束}{やくそく}の\kanakt{子}{こ}なり
\katano{二九} \protect\nolinebreak  \kanakt{然}{しかれ}ども\kana{當時}{むかし}の\kana{肉}{にく}に\kanakt{循}{したが}ひて\kanakt{生}{うまれ}しもの\kanakt{靈}{みたま}に\kanakt{循}{したが}ひて\kana{生}{うま}れし\kana{者}{もの}を\kana{窘}{せめ}し\kana{如}{ごと}く\kana{今}{いま}も\kana{亦}{また}\kana{然}{しか}り
\katano{三〇} \protect\nolinebreak  \kana{然}{され}ど\kanakt{聖書}{せいしよ}は\kana{何}{なに}と\kana{言}{いへ}るや\kanakt{婢}{しもめ}および\kana{其}{その}\kanakt{子}{こ}を\kana{逐}{おへ}そは\kanakt{婢}{しもめ}の\kanakt{子}{こ}は\kana{自主}{じしゆ}の\kanakt{婦}{をんな}の\kanakt{子}{こ}と\kana{共}{とも}に\kana{嗣子}{よつぎ}となる\kana{可}{べか}らざれば\kana{也}{なり}と\kana{言}{いへ}り
\katano{三一} \protect\nolinebreak  \kanakt{兄弟}{きやうだい}よ\kana{此}{かく}の\kanakt{如}{ごとく}なれば\kana{我儕}{われら}は\kanakt{婢}{しもめ}の\kanakt{子}{こ}に\kana{非}{あら}ず\kana{此}{この}\kana{自主}{じしゆ}の\kanakt{婦}{をんな}の\kanakt{子}{こ}なり



\markboth{章五第書太拉加}{加拉太書第五章}
\goodbreak
\noindent
\begin{wrapfigure}[2]{l}{0pt}
  \vbox to 2zw {\hbox to 3zw {\large \kintou{4zw}{\bf 第五章}}}
\end{wrapfigure}
\noindent\noindent\katano{一} \protect\nolinebreak  イエス・キリスト\kana{我儕}{われら}を\kana{釋}{とき}て\kana{自由}{じゆう}を\kanakt{得}{え}させたり\kana{是}{この}\kana{故}{ゆゑ}に\kanakt{爾曹}{なんぢら}\kanakt[3]{堅}{かたく}\kana{立}{たち}て\kanakt{復}{ふたた}び\kana{奴隸}{どれい}の\kanakt{軛}{くびき}に\kanakt{繫}{つなが}るる\kana{勿}{なか}れ
%% \refstepcounter{section}\addcontentsline{toc}{section}{第五章 {\tiny  イエス・キリスト我儕を釋て自由を得させたり是}}
\katano{二} \protect\nolinebreak  \kana{我}{われ}パウロ\kanakt{爾曹}{なんぢら}にいふ\kanakt{爾曹}{なんぢら}もし\kanakt{割禮}{かつれい}を\kana{受}{うけ}なばキリスト\kana{更}{さら}に\kanakt{爾曹}{なんぢら}に\kana{益}{えき}なし
\katano{三} \protect\nolinebreak  \kana{我}{われ}また\kanakt{割禮}{かつれい}を\kana{受}{うけ}たる\kanakt{各々}{おのおの}の\kana{人}{ひと}に\kana{就}{つい}て\kanakt{證}{あかし}す\kana{其}{その}\kana{人}{ひと}は\kanakt{全}{まつた}き\kana{律法}{おきて}を\kanakt{行}{おこな}ふべき\kana{者}{もの}なり
\katano{四} \protect\nolinebreak  なんぢら\kana{律法}{おきて}に\kana{由}{より}て\kanakt{義}{ぎ}とせらるる\kana{者}{もの}はキリストと\kanakt{與}{かかは}りなく\kanakt{恩}{めぐみ}より\kana{墮}{おち}たる\kana{者}{もの}なり
\katano{五} \protect\nolinebreak  われら\kana{望}{のぞ}む\kanakt{所}{ところ}のもの\kanakt{即}{すなは}ち\kanakt{信仰}{しんかう}を\kanakt{以}{も}て\kanakt{義}{ぎ}とせらるることを\kanakt{靈}{みたま}に\kana{由}{より}て\kana{俟}{まつ}なり
\katano{六} \protect\nolinebreak  \kana{夫}{それ}キリスト・イエスに\kana{在}{あり}ては\kanakt{割禮}{かつれい}を\kana{受}{うく}るも\kana{受}{うけ}ざるも\kana{益}{えき}なく\kana{惟}{ただ}\kana{愛}{あい}に\kana{由}{より}て\kanakt{行}{はたら}く\kanakt{所}{ところ}の\kanakt{信仰}{しんかう}のみ\kana{益}{えき}あり
\katano{七} \protect\nolinebreak  なんぢら\kana{前}{さき}には\kana{善}{よく}\kana{走}{はし}りたり\kanakt{誰}{た}が\kanakt{爾曹}{なんぢら}の\kana{眞理}{まこと}に\kanakt{循}{したが}はざるやう\kanakt{阻}{ささふ}ることを\kanakt{爲}{せ}しや
\katano{八} \protect\nolinebreak  その\kanakt{勸}{すすめ}は\kanakt{爾曹}{なんぢら}を\kana{召}{めす}\kana{者}{もの}より\kana{出}{いづ}るに\kana{非}{あら}ず
\katano{九} \protect\nolinebreak  \kana{少許}{すこし}の\kanakt{麪酵}{ぱんだね}は\kanakt{全團}{かたまり}をみな\kanakt{發}{ふくれ}しむ
\katano{一〇} \protect\nolinebreak  \kanakt{爾曹}{なんぢら}に\kana{就}{つい}ては\kana{我}{われ}なんぢらが\kana{少}{すこ}しも\kanakt{異念}{ことなるおもひ}を\kanakt{懷}{いだか}ざることを\kana{主}{しゆ}に\kana{由}{より}て\kana{信}{しん}ず\kana{誰}{たれ}にても\kanakt{爾曹}{なんぢら}を\kanakt{煩}{わづら}はす\kana{者}{もの}は\kana{其}{その}\kana{審判}{さばき}を\kana{受}{うく}べし
\katano{一一} \protect\nolinebreak  \kanakt{兄弟}{きやうだい}よ\kana{我}{われ}もし\kana{今}{いま}も\kana{尚}{なほ}\kanakt{割禮}{かつれい}を\kana{宜}{のべ}ば\kana{何}{なん}ぞ\kana{窘}{せめ}らるる\kana{事}{こと}あらん\kanakt{乎}{や}もし\kana{然}{しか}せば\kana{既}{もは}や\kana{十字架}{じふじか}に\kanakt{礙}{つまづ}くこと\kana{止}{やむ}べし
\katano{一二} \protect\nolinebreak  \kanakt{爾曹}{なんぢら}を\kana{亂}{みだ}す\kana{者}{もの}の\kanakt{自}{みづか}ら\kanakt{爾曹}{なんぢら}より\kanakt{離}{はなれ}んことを\kana{願}{ねが}ふ
\katano{一三} \protect\nolinebreak  そは\kanakt{兄弟}{きやうだい}よ\kanakt{爾曹}{なんぢら}は\kana{召}{めし}を\kanakt{蒙}{かうむ}りて\kana{自由}{じゆう}を\kanakt{得}{え}たる\kana{者}{もの}なれば\kana{也}{なり}されど\kana{其}{その}\kana{自由}{じゆう}を\kana{得}{うる}を\kanakt{機會}{をり}として\kana{肉}{にく}に\kanakt{循}{したが}ふ\kana{勿}{なか}れ\kana{惟}{ただ}\kana{愛}{あい}を\kanakt{以}{も}て\kanakt{互}{たがひ}に\kanakt{事}{つかふ}ることを\kanakt{爲}{せ}よ
\katano{一四} \protect\nolinebreak  それ\kanakt{己}{おのれ}の\kana{如}{ごと}く\kanakt{爾}{なんぢ}の\kanakt{隣}{となり}を\kana{愛}{あい}すべしと\kana{曰}{いへ}る\kana{此}{この}\kanakt{一言}{ひとつのことば}すべての\kana{律法}{おきて}を\kanakt{全}{まつた}うする\kana{也}{なり}
\katano{一五} \protect\nolinebreak  なんぢら\kanakt{愼}{つつしめ}よ\kana{若}{もし}たがひに\kana{呑}{かみ}\kana{噬}{くら}はば\kanakt{恐}{おそら}くは\kanakt{互}{たがひ}に\kanakt{滅}{ほろぼ}されん


\indent
\katano{一六} \protect\nolinebreak  われ\kana{謂}{いふ}なんぢら\kanakt{靈}{みたま}に\kana{由}{より}て\kana{行}{あゆ}むべし\kana{然}{さら}ば\kana{肉}{にく}の\kana{慾}{よく}を\kana{成}{なす}こと\kana{莫}{なか}らん
\katano{一七} \protect\nolinebreak  そは\kana{肉}{にく}の\kanakt{慾}{ねがひ}は\kanakt{靈}{みたま}に\kanakt{逆}{さから}ひ\kanakt{靈}{みたま}の\kanakt{慾}{ねがひ}は\kana{肉}{にく}に\kanakt{逆}{さから}ひ\kana{此}{この}\kanakt{二}{ふたつ}のもの\kanakt{互}{たがひ}に\kana{相}{あひ}\kana{敵}{もと}る\kana{是}{この}\kana{故}{ゆゑ}に\kanakt{爾曹}{なんぢら}\kana{好}{この}む\kanakt{所}{ところ}の\kana{事}{こと}をなすを\kanakt{得}{え}ず
\katano{一八} \protect\nolinebreak  \kana{然}{され}ど\kanakt{爾曹}{なんぢら}もし\kanakt{靈}{みたま}に\kanakt{導}{みちび}かるるときは\kana{律法}{おきて}の\kana{下}{した}に\kana{在}{あら}ざるべし
\katano{一九} \protect\nolinebreak  それ\kana{肉}{にく}の\kanakt{行}{おこなひ}は\kana{顯著}{あらは}なり\kanakt{即}{すなは}ち\kanakt{苟合}{こうがふ}、\kanakt{汚穢}{をくわい}、\kanakt{好色}{かうしよく}
\katano{二〇} \protect\nolinebreak  \kanakt{偶像}{ぐうざう}に\kanakt{事}{つかふ}ること\kanakt{巫術}{ふじゆつ}、\kanakt{仇恨}{きうこん}、\kanakt{爭鬪}{さうとう}、\kanakt{妬忌}{とき}、\kana{忿怒}{ふんど}、\kanakt{分爭}{ぶんさう}、\kanakt{結黨}{けつたう}、\kana{異端}{いたん}
\katano{二一} \protect\nolinebreak  \kanakt{媢嫉}{ばうしつ}、\kanakt{兇殺}{きようさつ}、\kanakt{醉酒}{すゐしゆ}、\kanakt{放蕩}{はうたう}などの\kana{如}{ごと}し\kana{此等}{これら}の\kana{事}{こと}につき\kana{我}{われ}\kana{甞}{かつ}て\kanakt{爾曹}{なんぢら}に\kana{斯}{かか}る\kana{事}{こと}をなす\kana{者}{もの}は\kanakt[3]{神}{かみの}\kana{國}{くに}を\kana{嗣}{つぐ}べからずと\kana{告}{つげ}しその\kana{如}{ごと}く\kana{今}{いま}また\kanakt{預}{あらか}じめ\kana{之}{これ}を\kana{告}{つぐ}
\katano{二二} \protect\nolinebreak  \kanakt{靈}{みたま}の\kana{結}{むす}ぶ\kanakt{所}{ところ}の\kanakt{果}{み}は\kanakt{仁愛}{じんあい}、\kana{喜樂}{きらく}、\kana{平和}{へいわ}、\kanakt{忍耐}{にんたい}、\kanakt{慈悲}{じひ}、\kanakt{良善}{りやうぜん}、\kanakt{忠信}{ちゆうしん}
\katano{二三} \protect\nolinebreak  \kanakt{温柔}{をんじう}、\kanakt{撙節}{そんせつ}かくの\kana{如}{ごと}き\kanakt{類}{たぐひ}を\kana{禁}{きん}ずる\kana{律法}{おきて}はある\kana{事}{こと}なし
\katano{二四} \protect\nolinebreak  \kana{夫}{それ}キリストに\kana{屬}{ぞく}する\kana{者}{もの}は\kana{肉}{にく}と\kana{其}{その}\kanakt{情}{じやう}および\kana{慾}{よく}とを\kana{十字架}{じふじか}に\kana{釘}{つけ}たり
\katano{二五} \protect\nolinebreak  \kana{若}{もし}われら\kanakt{靈}{みたま}に\kana{由}{より}て\kana{生}{いき}なば\kana{亦}{また}\kanakt{靈}{みたま}に\kana{由}{より}て\kana{行}{あゆ}むべし
\katano{二六} \protect\nolinebreak  \kanakt{互}{たがひ}に\kanakt{怒}{いかり}たがひに\kana{妬}{ねた}むことを\kana{爲}{なし}て\kanakt{虚榮}{むなしきほまれ}を\kanakt{求}{もとむ}る\kana{勿}{なか}れ


\markboth{章六第書太拉加}{加拉太書第六章}
\goodbreak
\noindent
\begin{wrapfigure}[2]{l}{0pt}
  \vbox to 2zw {\hbox to 3zw {\large \kintou{4zw}{\bf 第六章}}}
\end{wrapfigure}
\noindent\noindent\katano{一} \protect\nolinebreak  \kanakt{兄弟}{きやうだい}よ\kana{若}{もし}はからずも\kanakt{過}{あやまち}に\kanakt{陷}{おちい}る\kana{者}{もの}あらば\kanakt{爾曹}{なんぢら}のうち\kanakt{靈}{みたま}に\kana{感}{かん}じたる\kana{者}{もの}\kana{柔和}{にうわ}なる\kanakt{心}{こころ}をもて\kana{之}{これ}を\kana{規正}{ただす}べし\kana{亦}{また}\kanakt{自己}{みづから}をも\kanakt{顧}{かへり}みよ\kanakt{恐}{おそら}くは\kanakt[3]{爾}{なんぢ}\kanakt{誘}{さそは}るること\kana{有}{あら}ん
%% \refstepcounter{section}\addcontentsline{toc}{section}{第六章 {\tiny  兄弟よ若はからずも過に陷る者あらば爾曹のうち}}
\katano{二} \protect\nolinebreak  なんぢら\kana{彼此}{たがひ}の\kana{勞}{らう}を\kanakt{任}{お}へ\kana{斯}{かく}してキリストの\kana{律法}{おきて}を\kanakt{全}{まつたう}すべし
\katano{三} \protect\nolinebreak  \kana{人}{ひと}もし\kana{有}{ある}ことなくして\kanakt{自}{みづか}ら\kana{有}{あり}とせば\kana{是}{これ}みづから\kanakt{欺}{あざむ}くなり
\katano{四} \protect\nolinebreak  \kanakt{各人}{おのおの}その\kana{行}{なす}ところを\kanakt{勘}{かんが}へ\kanakt{視}{み}よ\kanakt{如此}{かく}せば\kana{誇}{ほこ}る\kanakt{基}{もとゐ}はただ\kanakt{己}{おのれ}に\kana{在}{あり}て\kana{人}{ひと}に\kana{在}{あら}ず
\katano{五} \protect\nolinebreak  そは\kana{人}{ひと}おのおの\kana{其}{その}\kanakt{荷}{に}を\kana{負}{おふ}べければ\kana{也}{なり}
\katano{六} \protect\nolinebreak  \kana{然}{され}ど\kana{道}{みち}を\kanakt{教}{をしへ}らるる\kana{者}{もの}は\kana{道}{みち}を\kanakt{教}{をしふ}る\kana{者}{もの}に\kana{凡}{すべ}て\kanakt{有益}{いうえき}なる\kana{物}{もの}を\kana{分}{わけ}\kana{予}{あた}ふべし
\katano{七} \protect\nolinebreak  \kanakt{自}{みづか}ら\kanakt{欺}{あざむ}く\kana{勿}{なか}れ\kana{神}{かみ}は\kanakt{慢}{あなど}るべき\kana{者}{もの}に\kana{非}{あら}ず\kana{蓋}{そは}\kana{人}{ひと}の\kana{種}{まく}ところの\kana{者}{もの}は\kana{亦}{また}その\kana{穫}{かる}ところと\kana{爲}{なる}なり
\katano{八} \protect\nolinebreak  \kana{己}{おの}が\kana{肉}{にく}の\kana{爲}{ため}に\kana{種}{まく}ものは\kana{肉}{にく}より\kana{敗壞}{くつる}ものを\kana{穫}{かり}とり\kanakt{靈}{みたま}のために\kana{種}{まく}ものは\kanakt{靈}{みたま}より\kanakt{永生}{かぎりなきいのち}を\kana{穫}{かり}とるべし
\katano{九} \protect\nolinebreak  \kana{善}{ぜん}を\kanakt{行}{おこな}ふに\kana{臆}{おく}する\kana{勿}{なか}れ\kana{蓋}{そは}もし\kana{倦}{うむ}\kana{事}{こと}なくば\kana{我儕}{われら}\kana{時}{とき}に\kana{至}{いた}りて\kana{穫}{かり}\kana{取}{とる}べければ\kana{也}{なり}
\katano{一〇} \protect\nolinebreak  \kana{是}{この}\kana{故}{ゆゑ}に\kanakt{若}{も}し\kanakt{機會}{をり}あらば\kanakt{衆}{すべて}の\kana{人}{ひと}に\kana{善}{ぜん}を\kana{行}{なす}べし\kanakt{信仰}{しんかう}の\kanakt{徒}{ともがら}には\kana{別}{わけ}て\kana{之}{これ}を\kana{行}{なす}べし


\indent
\katano{一一} \protect\nolinebreak  \kanakt{爾曹}{なんぢら}わが\kanakt{親手}{てづから}なんぢらに\kana{書}{かき}\kana{遣}{おく}る\kana{字}{もじ}の\kana{何}{いか}に\kanakt{大}{おほい}なるかを\kanakt{見}{み}よ
\katano{一二} \protect\nolinebreak  \kanakt{凡}{おほよ}そ\kana{肉}{にく}について\kanakt{美}{うるは}しからんことを\kana{欲}{ねが}ふ\kana{者}{もの}は\kanakt{爾曹}{なんぢら}に\kanakt{割禮}{かつれい}を\kanakt{強}{し}ふ\kana{是}{これ}ただ\kanakt[3]{己}{おのれ}キリストの\kana{十字架}{じふじか}の\kana{爲}{ため}に\kana{窘}{せめ}らるることを\kanakt{免}{まぬか}れんが\kana{爲}{ため}なり
\katano{一三} \protect\nolinebreak  そは\kanakt{割禮}{かつれい}をうけたる\kana{彼等}{かれら}なほ\kanakt{自}{みづか}ら\kana{律法}{おきて}を\kana{守}{まも}ることをせず\kana{彼等}{かれら}が\kanakt{爾曹}{なんぢら}に\kanakt{割禮}{かつれい}を\kana{受}{うけ}させんとするは\kanakt{爾曹}{なんぢら}の\kana{肉}{にく}に\kana{於}{おい}て\kana{誇}{ほこ}らんと\kana{欲}{おも}ふなり
\katano{一四} \protect\nolinebreak  \kana{然}{され}ど\kana{我}{われ}には\kana{惟}{ただ}われらの\kana{主}{しゆ}イエス・キリストの\kana{十字架}{じふじか}の\kana{外}{ほか}に\kana{誇}{ほこ}る\kanakt{所}{ところ}なからんことを\kana{願}{ねが}ふ\kana{此}{この}キリストに\kana{由}{より}て\kana{我}{われ}\kanakt{世}{よ}に\kana{向}{むか}へば\kanakt{世}{よ}は\kana{十字架}{じふじか}に\kana{釘}{つけ}られ\kanakt{世}{よ}の\kana{我}{われ}に\kana{向}{むか}ふも\kana{亦}{また}\kana{然}{しか}り
\katano{一五} \protect\nolinebreak  \kana{夫}{それ}イエス・キリストに\kana{於}{おい}ては\kanakt{割禮}{かつれい}を\kana{受}{うく}るも\kana{受}{うけ}ざるも\kana{益}{えき}なく\kana{唯}{ただ}\kanakt{新}{あらた}に\kanakt{作}{つくら}れし\kana{者}{もの}のみ\kana{益}{えき}あり
\katano{一六} \protect\nolinebreak  \kanakt{凡}{おほよ}そ\kana{此}{この}\kanakt{規矩}{のり}に\kanakt{循}{したが}ひて\kana{行}{あゆ}む\kana{者}{もの}に\kanakt{願}{ねがは}くは\kana{平康}{やすき}と\kana{恩惠}{めぐみ}とあれ\kana{神}{かみ}のイスラエルにも\kana{亦}{また}\kana{然}{しか}れ
\katano{一七} \protect\nolinebreak  \kana{今}{いま}よりのち\kana{誰}{たれ}も\kana{我}{われ}を\kanakt{擾}{わづら}はす\kana{勿}{なか}れ\kana{蓋}{そは}われ\kanakt{身}{み}にイエスの\kana{印記}{しるし}を\kana{佩}{おび}たれば\kana{也}{なり}
\katano{一八} \protect\nolinebreak  \kanakt{兄弟}{きやうだい}よ\kanakt{願}{ねがは}くは\kana{我儕}{われら}の\kana{主}{しゆ}イエス・キリストの\kanakt{恩}{めぐみ}なんぢらの\kana{靈}{れい}と\kana{偕}{とも}にならんことをアメン


\vspace*{\fill}
\noindent
\Kanakt{新,約,全,書}{しん,やく,ぜん,しよ}\Kanakt{加,拉,太}{が,ら,てや}\kana[3]{書}{のふみ}\kana[3]{終}{をはり}
